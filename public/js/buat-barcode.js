$(document).ready(function () {
  $('#verifikasi').click(function(e) {
    var ini = $(this);
    var route_verifikasi = $('#data-back').data('route-verifikasi');
    var barcode = $('input[name="barcode_new').val();
    var token = $('#data-back').data('form-token');
    var data_send = {
      barcode: barcode,
      _token: token
    };

    $.ajax({
      url: route_verifikasi,
      type: 'POST',
      data: data_send,
      success: function(data) {
        $('input[name="barcode_new').val(data);
        swal({
          title: 'Perhatian',
          text: 'Berhasil Diupdate',
          type: 'success'
        });
      },
      error: function(request, status, error) {
        swal({
          title: 'Perhatian',
          text: error,
          type: 'error'
        });
      }
    });
  });

  $('.form-send-penjualan').submit(function(e) {
      e.preventDefault();
      var ini = $(this);

      $.ajax({
          url: ini.attr('action'),
          type: ini.attr('method'),
          data: ini.serialize(),
          success: function(data) {
              if(data.redirect) {
                  window.location.href = data.redirect;
              }
          },
          error: function(request, status, error) {
            swal({
              title: 'Perhatian',
              text: 'Data Gagal Disimpan!',
              type: 'error'
            });

            var json = JSON.parse(request.responseText);
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.each(json.errors, function(key, value) {
              $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
              $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
            });
          }
      });

      return false;
  });

  $('#sample_6').on( 'draw.dt', function () {
    $('.btn-pilih-barang').click(function() {
      var ini = $('#modal-barang').data('barang');
      var brg_kode = $(this).data('brg-kode');
      var brg_barkode = $(this).data('brg-barkode');
      var brg_nama = $(this).data('brg-nama');
      var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_barkode).trigger('change');

      $('input[name="nama_stok').val(brg_nama).trigger('change');
      $('input[name="brg_kode_combine').val(brg_kode).trigger('change');

      var ktg_nama = $(this).data('brg-ktg');
      $('input[name="ktg').val(ktg_nama).trigger('change');
      var ktg_kode = $(this).data('brg-id_ktg');
      $('input[name="id_ktg').val(ktg_kode).trigger('change');

      // var grp_nama = $(this).data('brg-grp');
      // var brg_kode_modal = $('input[name="grp').val(grp_nama).trigger('change');
      var grp_kode = $(this).data('brg-id_grp');
      $('input[name="id_grp').val(grp_kode).trigger('change');

      var mrk_nama = $(this).data('brg-mrk');
      $('input[name="mrk').val(mrk_nama).trigger('change');
      var mrk_kode = $(this).data('brg-id_mrk');
      $('input[name="id_mrk').val(mrk_kode).trigger('change');

      var stn_nama = $(this).data('brg-stn');
      $('input[name="stn').val(stn_nama).trigger('change');
      var stn_kode = $(this).data('brg-id_stn');
      $('input[name="id_stn').val(stn_kode).trigger('change');

      $('#modal-barang').modal('hide');
    });

    $('.btn-stok').click(function() {
      var href = $(this).data('href');
      $('#modal-stok').modal('show');
      $('#sample_3').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: "brg_no_seri" },
          { data: "QOH" },
          { data: "Titipan" },
          { data: "gdg_nama" },
          { data: "spl_nama" },
        ]
      });
    });
  });

  $('.btn-stok').click(function() {
    var href = $(this).data('href');
    $('#modal-stok').modal('show');
    $('#sample_3').DataTable({
      destroy : true,
      ajax : {
        url : href,
        dataSrc : ''
      },
      columns: [
        { data: "brg_no_seri" },
        { data: "QOH" },
        { data: "Titipan" },
        { data: "gdg_nama" },
        { data: "spl_nama" },
      ]
    });
  });


  $('.btn-row-plus').click(function() {
    var cus_kode = $('[name="cus_kode"]').val();
    if(cus_kode == '-') {
      swal({
        title: 'Perhatian',
        text: 'Diharuskan Pilih Customer Terlebih Dahulu',
        type: 'warning'
      });
    } else {
      var row = $('.table-row-data tbody').html();
      $('.table-all-data tbody').append(row);
      select_row_barang();
      btn_modal_barang();
      btn_row_delete();
      enter_disc();
      select_harga();
      set_qty();
    }
  });

  function btn_modal_barang() {
    $('.btn-modal-barang').click(function() {
      var tableS2 = $('#sample_6').DataTable();

      var ini = $(this);
      $('#modal-barang').data('barang', ini).modal('show');
    });

    $('.btn-pilih-barang').click(function() {
      var ini = $('#modal-barang').data('barang');
      var brg_kode = $(this).data('brg-kode');
      var brg_barkode = $(this).data('brg-barkode');
      var brg_nama = $(this).data('brg-nama');
      var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_barkode).trigger('change');

      $('input[name="nama_stok').val(brg_nama).trigger('change');

      var ktg_nama = $(this).data('brg-ktg');
      $('input[name="ktg').val(ktg_nama).trigger('change');
      var ktg_kode = $(this).data('brg-id_ktg');
      $('input[name="id_ktg').val(ktg_kode).trigger('change');

      // var grp_nama = $(this).data('brg-grp');
      // var brg_kode_modal = $('input[name="grp').val(grp_nama).trigger('change');
      var grp_kode = $(this).data('brg-id_grp');
      $('input[name="id_grp').val(grp_kode).trigger('change');

      var mrk_nama = $(this).data('brg-mrk');
      $('input[name="mrk').val(mrk_nama).trigger('change');
      var mrk_kode = $(this).data('brg-id_mrk');
      $('input[name="id_mrk').val(mrk_kode).trigger('change');

      var stn_nama = $(this).data('brg-stn');
      $('input[name="stn').val(stn_nama).trigger('change');
      var stn_kode = $(this).data('brg-id_stn');
      $('input[name="id_stn').val(stn_kode).trigger('change');

      $('#modal-barang').modal('hide');
    });
  }

  function btn_row_delete() {
    $('.btn-row-delete').click(function() {
      $(this).parents('tr').remove();
      kalkulasi_subtotal();
      kalkulasi_total_hpp();
      kalkulasi_grandtotal();
    });
  }

  function select_row_barang() {
    var route_penjualan_langsung_barang_row = $('#data-back').data('route-penjualan-langsung-barang-row');
    $('input[name="brg_kode[]"]').change(function() {
      var brg_kode = $(this).val();
      var cus_kode = $('[name="cus_kode"]').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        cus_kode: cus_kode,
        _token: token
      };

      var nama = ini.parents('td').siblings('td.nama').children('input[name="nama[]"]');
      var satuan = ini.parents('td').siblings('td.satuan').children('input[name="satuan[]"]');
      var brg_hpp = ini.parents('td').siblings('td.brg_hpp').children('input[name="brg_hpp[]"]');
      var harga_jual = ini.parents('td').siblings('td.harga_jual').children('select[name="harga_jual[]"]');
      var brg_no_seri = ini.parents('td').siblings('td.brg_no_seri').children('select[name="brg_no_seri[]"]');
      var asal_gudang = ini.parents('td').siblings('td.gdg_kode').children('select[name="gdg_kode[]"]');
      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');

      asal_gudang.html('');
      stok.val('');
      nama.val('');
      satuan.val('');
      brg_hpp.val('');
      harga_jual.html('');
      brg_no_seri.html('');

      $.ajax({
        url: route_penjualan_langsung_barang_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          nama.val(data.nama);
          satuan.val(data.satuan);
          brg_hpp.val(data.brg_hpp);
          harga_jual.html(data.harga_jual);
          brg_no_seri.html(data.stok);
        }
      });
    });

    var route_gudang_row = $('#data-back').data('route-gudang-row');
    $('select[name="brg_no_seri[]"]').change(function() {
      var brg_no_seri = $(this).val();
      var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        _token: token
      };

      var asal_gudang = ini.parents('td').siblings('td.gdg_kode').children('select[name="gdg_kode[]"]');
      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');

      asal_gudang.html('');
      stok.val('');

      $.ajax({
        url: route_gudang_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          asal_gudang.html(data.gudang);
        }
      });
    });

    var route_stok_row = $('#data-back').data('route-stok-row');
    $('select[name="gdg_kode[]"]').change(function() {
      var gdg_kode = $(this).val();
      var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input').val();
      var brg_no_seri = $(this).parents('td').siblings('td.brg_no_seri').children('select').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        gdg_kode: gdg_kode,
        _token: token
      };

      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');
      stok.val('');

      $.ajax({
        url: route_stok_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          if (data.stok != null) {
            stok.off('input');
            stok.on('input', function () {
              var value = $(this).val();
              $(this).val(Math.max(Math.min(value, data.stok), 0));
            });
          }
        }
      });
    });
  }

  function enter_disc() {
    $('[name="disc[]"]').change(function() {
      var form_disc_nom = $(this).parents('td').siblings('td.disc_nom').children('input');
      var form_harga_net = $(this).parents('td').siblings('td.harga_net').children('input');
      var form_total = $(this).parents('td').siblings('td.total').children('input');

      form_disc_nom.val(0);
      form_harga_net.val(0);

      var disc = $(this).val();
      var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('select').val();
      var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
      var harga_net = parseInt(harga_jual) - disc_nom;
      var total = parseInt(harga_net) * parseInt(qty);

      form_disc_nom.val(disc_nom);
      form_harga_net.val(harga_net);
      form_total.val(total);
    });
  }

  function select_harga() {
    $('[name="harga_jual[]"]').change(function() {
      var disc = $(this).parents('td').siblings('td.disc').children('input').val();
      var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      var harga_jual = $(this).val();
      var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
      var harga_net = (parseInt(harga_jual) - disc_nom);
      var total = parseInt(harga_net) * parseInt(qty);
      $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
      $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net);
      $(this).parents('td').siblings('td.total').children('input').val(total);
    });
  }

  function set_qty() {
    $('[name="qty[]"]').change(function() {
      var qty = $(this).val();

      var disc = $(this).parents('td').siblings('td.disc').children('input').val();
      var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('select').val();
      var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
      var harga_net = (parseInt(harga_jual) - disc_nom);
      var total = parseInt(harga_net) * parseInt(qty);
      $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
      $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net);
      $(this).parents('td').siblings('td.total').children('input').val(total);
    });
  }

  select_row_barang();
  btn_modal_barang();
  btn_row_delete();
  enter_disc();
  select_harga();
  set_qty();
});
