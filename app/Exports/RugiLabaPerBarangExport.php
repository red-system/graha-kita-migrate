<?php

namespace App\Exports;

use App\Models\mBarang;
use App\Models\mPerkiraan;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mReturPenjualanDetail;
use App\Models\mDetailSuratJalanPT;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

// class BarangExport implements FromCollection
class RugiLabaPerBarangExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $request = $this->request;

        $data['start_date']    = $request->start_date;
        $data['end_date']      = $request->end_date;

        $today                      = date('Y-m-d');
        

        $data['barang']     = mBarang::all();

        // foreach ($data['barang'] as $brg) {
        //     //jual langsung
        //     $query_langsung         = mDetailPenjualanLangsung::leftJoin('tb_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur', '=', 'tb_detail_penjualan_langsung.pl_no_faktur')->where('brg_kode',$brg->brg_kode)->whereBetween('tb_penjualan_langsung.pl_tgl',[$data['start_date'],$data['end_date']]);
        //     $qty_jual           = $query_langsung->sum('qty');
        //     $hpp_jual           = $brg->brg_hpp;
        //     $total_jual_langsung = $query_langsung->sum('total');

        //     //jual titipan
        //     $query_titipan         = mDetailPenjualanTitipan::leftJoin('tb_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur', '=', 'tb_detail_penjualan_titipan.pt_no_faktur')->where('brg_kode',$brg->brg_kode)->whereBetween('tb_penjualan_titipan.pt_tgl',[$data['start_date'],$data['end_date']]);
        //     $qty_titipan           = $query_titipan->sum('qty');
        //     $hpp_titipan           = $brg->brg_hpp;
        //     $total_jual_titipan    = $query_titipan->sum('total');
        //     $data['item']['total_jual'.$brg->brg_kode] = $total_jual_langsung+$total_jual_titipan;
        //     $data['item']['hpp_jual'.$brg->brg_kode] = $qty_jual*$hpp_jual;

        //     //retur
        //     $query_retur            = mReturPenjualanDetail::leftJoin('tb_retur_penjualan', 'tb_retur_penjualan.no_retur_penjualan', '=', 'tb_detail_retur_penjualan.no_retur_penjualan')->where('brg_kode',$brg->brg_kode)->whereBetween('tb_retur_penjualan.tgl_pengembalian',[$data['start_date'],$data['end_date']]);
        //     $qty_retur           = $query_retur->sum('qty_retur');
        //     $hpp_retur           = $brg->brg_hpp;
        //     $data['item']['total_retur'.$brg->brg_kode] = $query_retur->sum('tb_detail_retur_penjualan.total_retur');
        //     $data['item']['hpp_retur'.$brg->brg_kode] = $qty_retur*$hpp_retur;
        // }
        foreach ($data['barang'] as $brg) {
            //jual langsung
            $query_langsung         = mDetailPenjualanLangsung::leftJoin('tb_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur', '=', 'tb_detail_penjualan_langsung.pl_no_faktur')->where('brg_kode',$brg->brg_kode)->whereBetween('tb_penjualan_langsung.pl_tgl',[$data['start_date'],$data['end_date']])->get();
            // $qty_jual           = $query_langsung->sum('qty');
            // $hpp_jual           = $brg->brg_hpp;
            // $total_jual_langsung = $query_langsung->sum('total');
            $qty_jual = 0;
            $hpp_jual = 0;
            $total_jual_langsung = 0;
            foreach ($query_langsung as $langsung) {                
                $qty_jual  = $qty_jual+$langsung->qty;
                $hpp_jual  = $hpp_jual+($langsung->qty*$langsung->brg_hpp);
                $total_jual_langsung = $total_jual_langsung+($langsung->qty*$langsung->harga_net);
            }

            //jual titipan
            $query_titipan         = mDetailSuratJalanPT::leftJoin('tb_surat_jalan_penjualan_titipan','tb_surat_jalan_penjualan_titipan.sjt_kode','=','tb_detail_surat_jalan_penjualan_titipan.sjt_kode')->leftJoin('tb_detail_penjualan_titipan','tb_detail_surat_jalan_penjualan_titipan.det_kode','=','tb_detail_penjualan_titipan.detail_pt_kode')->where('tb_detail_surat_jalan_penjualan_titipan.brg_kode',$brg->brg_kode)->whereBetween('tb_surat_jalan_penjualan_titipan.sjt_tgl',[$data['start_date'],$data['end_date']])->get();
            $qty_titipan = 0;
            $hpp_titipan = 0;
            $total_jual_titipan = 0;
            foreach ($query_titipan as $titipan) {
                $qty_titipan  = $qty_titipan+$titipan->dikirim;
                $hpp_titipan  = $hpp_titipan+($titipan->dikirim*$titipan->brg_hpp);
                $total_jual_titipan = $total_jual_titipan+($titipan->dikirim*$titipan->harga_net);
                
            }
            
            $data['item']['total_jual'.$brg->brg_kode] = $total_jual_langsung+$total_jual_titipan;
            $data['item']['hpp_jual'.$brg->brg_kode] = $hpp_titipan+$hpp_jual;

            //retur
            $query_retur            = mReturPenjualanDetail::leftJoin('tb_retur_penjualan', 'tb_retur_penjualan.no_retur_penjualan', '=', 'tb_detail_retur_penjualan.no_retur_penjualan')->where('brg_kode',$brg->brg_kode)->whereBetween('tb_retur_penjualan.tgl_pengembalian',[$data['start_date'],$data['end_date']])->get();
            // $qty_retur           = $query_retur->sum('qty_retur');
            // $hpp_retur           = $brg->brg_hpp;
            // $data['item']['total_retur'.$brg->brg_kode] = $query_retur->sum('tb_detail_retur_penjualan.total_retur');
            // $data['item']['hpp_retur'.$brg->brg_kode] = $qty_retur*$hpp_retur;

            $qty_retur = 0;
            $hpp_retur = 0;
            $total_retur = 0;
            foreach ($query_retur as $retur) {                
                $qty_retur  = $qty_retur+$retur->qty_retur;
                $hpp_retur  = $hpp_retur+($retur->qty_retur*$brg->brg_hpp);
                $total_retur = $total_retur+($retur->qty_retur*$retur->harga_jual);
            }
            $data['item']['total_retur'.$brg->brg_kode] = $total_retur;
            $data['item']['hpp_retur'.$brg->brg_kode] = $hpp_retur;
        }

      return view('export.rugi-laba-per-barang', $data);
    }
}
