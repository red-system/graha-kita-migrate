@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.min.css" />
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.view-img').click(function() {
      var ini = $(this);
      // var url_image = ini.data('href');
      var el = document.getElementById('image');
      var viewer = new Viewer(el, {
        inline: false,
        viewed: function() {
          viewer.zoomTo(1);
        }
      });
    });
  });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="portlet light">
        <form action="#" class="form-horizontal" role="form" method="post">
          <div class="row">
            <div class="col-xs-3">
              <table>
                <tbody>
                  <tr>
                    <td><h5>Kode Order : </h5></td>
                    <td><h5>{{$dataList->order_kode}}</h5></td>
                  </tr>
                  <tr>
                    <td><h5>Tgl Order  : </h5></td>
                    <td><h5>{{$dataList->order_tgl}}</h5></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-xs-3">
              <table>
                <tbody>
                  <tr>
                    <td><h5>Kode Pelanggan : </h5></td>
                    <td><h5>{{$kodeCustomer.$dataList->customer['cus_kode']}}</h5></td>
                  </tr>
                  <tr>
                    <td><h5>Pelanggan : </h5></td>
                    <td><h5>{{$dataList->customer['cus_nama']}}</h5></td>
                  </tr>
                  <tr>
                    <td><h5>Telp : </h5></td>
                    <td><h5>{{$dataList->customer['cus_telp']}}</h5></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-xs-3">
              <table>
                <tbody>
                  <tr>
                    <td><h5>Alamat Kirim : </h5></td>
                    <td><h5>{{$dataList->alamat_kirim}}</h5></td>
                  </tr>
                  <tr>
                    <td><h5>Alamat Tagih : </h5></td>
                    <td><h5>{{$dataList->customer['cus_alamat']}}</h5></td>
                  </tr>
                  <tr>
                    <td><h5>Sales Person : </h5></td>
                    <td><h5>{{$dataList->karyawan['kry_nama']}}</h5></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-xs-3">
              <table>
                <tbody>
                  <tr>
                    <td><h5>Syarat Pembayaran : </h5></td>
                    <td><h5>{{$dataList->syarat_pembayaran}}</h5></td>
                  </tr>
                  <tr>
                    <td><h5>Faktur Pajak : </h5></td>
                    @if ($dataList->faktur_pajak == 1)
                      <td><input class="form-control" type="checkbox" checked disabled></td>
                    @else
                      <td><input class="form-control" type="checkbox" disabled></td>
                    @endif
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <br>
          <br>
          <div class="row">
            <div class="col-xs-12">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th> No </th>
                    <th> Kode Barang </th>
                    <th> Nama Barang </th>
                    <th> Harga </th>
                    <th> QTY </th>
                    {{-- <th> Biaya Tambahan </th> --}}
                    {{-- <th> keterangan </th> --}}
                  </tr>
                </thead>
                <tbody>
                  @foreach ($dataList->detail as $rowDet)
                    <tr>
                      <td> {{ $no++ }}. </td>
                      <td> {{ $rowDet->barang['brg_kode'] }} </td>
                      <td> {{ $rowDet->barang['brg_nama'] }} </td>
                      <td align="right"> {{number_format(round($rowDet->harga), 2, "," ,".")}} </td>
                      <td align="right"> {{number_format($rowDet->qty, 2, "," ,".")}} </td>
                      {{-- <td> {{ $rowDet->biaya_tambahan }} </td> --}}
                      {{-- <td> {{ $rowDet->keterangan }} </td> --}}
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <br>
              <div class="row">
                <div class="col-xs-4">
                  <div class="form-group">
                    <label class="col-md-2">Catatan</label>
                    <div class="col-md-8">
                      <textarea class="form-control" name="catatan" rows="4" cols="40" readonly>{{$dataList->catatan}}</textarea>
                    </div>
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="form-group">
                    <label class="col-md-2">Intruksi Khusus</label>
                    <div class="col-md-8">
                      <textarea class="form-control" name="intruksi" rows="4" cols="40" readonly>{{$dataList->instruksi_khusus}}</textarea>
                    </div>
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="form-group">
                    <div class="col-md-8">
                      @if ($dataList->img)
                        <img class="view-img" id="image" src="{{ asset($dataList->img) }}" width="100" alt="Picture">
                      @else
                        <img id="imgScr" src="{{asset('img/icon/no_image.png')}}" alt="image" width="100">
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="portlet light">
                  <a href="{{ route('orderUpdateStatus', ['kode'=>$dataList->order_kode, 'status'=>'1']) }}" class="btn btn-success btn-lg">Confrim</a>
                  <a href="{{ route('orderUpdateStatus', ['kode'=>$dataList->order_kode, 'status'=>'2']) }}" class="btn btn-warning btn-lg">Cancel</a>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@stop
