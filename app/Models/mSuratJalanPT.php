<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mSuratJalanPT extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_surat_jalan_penjualan_titipan';
  protected $primaryKey = 'sjt_kode';
  // public $timestasmps = false;

  public function DetailSuratJalan()
  {
    return $this->hasMany('App\Models\mDetailSuratJalanPT', 'sjt_kode');
  }

  public function noFaktur()
  {
    return $this->belongsTo('App\Models\mPenjualanTitipan', 'pt_no_faktur');
  }

  public function Customer()
  {
    return $this->belongsTo('App\Models\mCustomer', 'cus_kode');
  }
}
