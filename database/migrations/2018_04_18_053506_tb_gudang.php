<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbGudang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_gudang', function (Blueprint $table) {
         $table->increments('gdg_kode');
         $table->string('gdg_nama')->nullable();
         $table->string('gdg_alamat')->nullable();
         $table->text('gdg_keterangan')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_gudang');
     }
}
