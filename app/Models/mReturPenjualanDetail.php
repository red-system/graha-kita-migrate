<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mReturPenjualanDetail extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_detail_retur_penjualan';
  protected $primaryKey = 'no_detail_retur_penjualan';
  public $timestamps = false;

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_barcode', 'brg_barcode')->select('brg_kode', 'brg_barcode', 'brg_nama', 'stn_kode', 'ktg_kode', 'mrk_kode', 'grp_kode');
  }
}
