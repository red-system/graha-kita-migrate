<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mPerkiraan;
use App\Models\mPerkiraanDetail;
use Carbon\Carbon;

use Validator;

class Perkiraan extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Perkiraan';
      $this->kodeLabel = 'perkiraan';
  }

  function index() {
      $breadcrumb = [
          'Perkiraan'=>route('perkiraanList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'perkiraan';
      $data['title'] = $this->title;
      // $data['perkiraan'] = $this->main->perkiraan();
      $data['space1'] = $this->main->perkiraan_space(1);
      $data['space2'] = $this->main->perkiraan_space(2);
      $data['space3'] = $this->main->perkiraan_space(3);
      $data['space4'] = $this->main->perkiraan_space(4);
      $month_now = Carbon::now()->month;
      $year_now = Carbon::now()->year;
      $perkiraan = [];
      $data1 = mPerkiraan::where('mst_master_id',0)
      // ->leftJoin('tb_ac_master_detail', 'tb_ac_master.master_id', '=', 'tb_ac_master_detail.master_id')
      // ->where('msd_year', )
      // ->where('msd_month', )
      ->orderBy('mst_kode_rekening','ASC')->get();
      foreach($data1 as $k=>$r) {
        $r->detail;
          $sub1 = mPerkiraan::where('mst_master_id', $r->master_id)->orderBy('mst_kode_rekening','ASC')->get();
          $perkiraan[$k] = $r;
          $perkiraan[$k]['sub1'] = $sub1;
          foreach($sub1 as $k1=>$r1) {
            $r1->detail;
              $sub2 = mPerkiraan::where('mst_master_id', $r1->master_id)->orderBy('mst_kode_rekening','ASC')->get();
              $perkiraan[$k]['sub1'][$k1]['sub2'] = $sub2;
              foreach($sub2 as $k2=>$r2) {
                $r2->detail;
                  $sub3 = mPerkiraan::where('mst_master_id', $r2->master_id)->orderBy('mst_kode_rekening','ASC')->get();
                  $perkiraan[$k]['sub1'][$k1]['sub2'][$k2]['sub3'] = $sub3;
                  foreach($sub3 as $k3=>$r3) {
                    $r3->detail;
                  }
              }
          }
      }
      $data['perkiraan'] = $perkiraan;

      // return $data;
      return view('perkiraan/perkiraanList', $data);
  }

  function rules($request) {
      $rules = [
          'mst_kode_rekening' => 'required',
          'mst_nama_rekening' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      $data = $request->except('_token', 'nominal');
      $data['mst_tanggal_awal'] = date('d/m/Y');
      $data['mst_date_insert'] = date('Y-m-d H:i:s');
      $data['mst_date_update'] = date('Y-m-d H:i:s');
      $perkiraan = mPerkiraan::insertGetId($data);

      $month =  Carbon::now()->month;
      $year =  Carbon::now()->year;

      $detail = new mPerkiraanDetail();
      $detail->master_id = $perkiraan;
      $detail->msd_year = $year;
      $detail->msd_month = $month;
      if ($request->mst_normal == 'debet') {
        $detail->msd_awal_kredit = 0;
        $detail->msd_awal_debet = $request->nominal;
      }
      else {
        $detail->msd_awal_kredit = $request->nominal;
        $detail->msd_awal_debet = 0;
      }
      $detail->msd_date_insert = date('Y-m-d H:i:s');
      $detail->msd_date_update = date('Y-m-d H:i:s');
      $detail->save();

      return [
          'redirect'=>route('perkiraanList')
      ];
  }

  function edit($master_id='') {
      $response = [
          'action'=>route('perkiraanUpdate', ['kode'=>$master_id]),
          'field'=>mPerkiraan::find($master_id)
      ];
      return $response;
  }

  function update(Request $request, $master_id) {
      $this->rules($request->all());

      $data = $request->except('_token', 'nominal');
      $data['mst_date_update'] = date('Y-m-d H:i:s');

      mPerkiraan::where('master_id', $master_id)->update($request->except('_token', 'nominal'));

      $month =  Carbon::now()->month;
      $year =  Carbon::now()->year;

      $detail = mPerkiraanDetail::where('master_id', $master_id)->where('msd_year', $year)->where('msd_month', $month)->first();
      if ($detail == null) {
        $detailNew = new mPerkiraanDetail();
        $detailNew->master_id = $master_id;
        $detailNew->msd_year = $year;
        $detailNew->msd_month = $month;
        if ($request->mst_normal == 'debet') {
          $detailNew->msd_awal_kredit = 0;
          $detailNew->msd_awal_debet = $request->nominal;
        }
        else {
          $detailNew->msd_awal_kredit = $request->nominal;
          $detailNew->msd_awal_debet = 0;
        }
        $detailNew->msd_date_insert = date('Y-m-d H:i:s');
        $detailNew->msd_date_update = date('Y-m-d H:i:s');
        $detailNew->save();
      }
      else {
        // $detail->master_id = $master_id;
        $detail->msd_year = $year;
        $detail->msd_month = $month;
        if ($request->mst_normal == 'debet') {
          $detail->msd_awal_kredit = 0;
          $detail->msd_awal_debet = $request->nominal;
        }
        else {
          $detail->msd_awal_kredit = $request->nominal;
          $detail->msd_awal_debet = 0;
        }
        $detail->msd_date_insert = date('Y-m-d H:i:s');
        $detail->msd_date_update = date('Y-m-d H:i:s');
        $detail->save();
      }


      return [
          'redirect'=>route('perkiraanList')
      ];
  }

  function delete($master_id) {
      mPerkiraan::where('master_id', $master_id)->delete();
      // mPerkiraan::where('mst_kode_rekening', $master_id)->delete();
      // mPerkiraanDetail::where('master_id', $master_id)->delete();
  }
}
