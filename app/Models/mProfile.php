<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mProfile extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_profile';
  protected $primaryKey = 'prf_kode';
  public $timestamps = false;
}
