<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mKodeBukti extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_ac_kode_bukti';
  protected $primaryKey = 'kode_bukti_id';
  public $timestamps = false;


}
