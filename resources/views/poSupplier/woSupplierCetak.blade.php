@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<!-- <style type="text/css">
    .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
    .tt td{font-family:Tahoma;font-size:12px;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#fff;}
</style> -->
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12>
                <div class="portlet light ">
                    <div class="portlet light">
                        @foreach($gdg as $gudang)
                            <a style="font-size: 11px;" class="btn btn-sm blue hidden-print margin-bottom-5" href="{{route('print_wo',['id'=>$id, 'tipe'=>'print','id_gudang'=>$gudang->gdg_kode])}}" target="_blank"> Print WO untuk {{$gudang->gdg_nama}}
                            </a>
                        @endforeach
                        <div class="invoice">
                            <div class="row">                                
                                <div class="col-xs-4">
                                    <table>
                                        <tr>
                                            <td style="font-size: 12px">W/O Barang</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px">Untuk 
                                                @foreach($gdg as $gudang)
                                                    {{$gudang->gdg_nama}},
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px"> {{$gudang->gdg_alamat}}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px">Si Pengirim : {{$poSupplier->suppliers->spl_nama}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-xs-4 col-xs-offset-4">
                                    <table>
                                        <tr>
                                            <td style="font-size: 12px">No. PO</td>
                                            <td style="font-size: 12px"> : {{$work_order->no_po}}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px">No. SJ</td>
                                            <td style="font-size: 12px"> : {{$work_order->no_sj}}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px">No. W/O</td>
                                            <td style="font-size: 12px"> : {{$work_order->no_wo}}</td>
                                        </tr>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                
                                
                                <div class="col-xs-12">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="font-size: 11px"> No</th>
                                                <th style="font-size: 11px"> Tujuan </th>
                                                <th style="font-size: 11px"> Nama Barang </th>
                                                <th style="font-size: 11px" class="hidden-xs"> Qty </th>
                                                <th style="font-size: 11px" class="hidden-xs"> Satuan </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($poSupplier->detailPoSupplier as $detail)   
                                            <tr>
                                                <td style="font-size: 10px"> {{ $no++ }} </td>
                                                <td style="font-size: 10px"> {{ $detail->gudangs->gdg_nama }} </td>
                                                <td style="font-size: 10px"> {{ $detail->nama_barang }} </td>
                                                <td style="font-size: 10px"> {{ $detail->qty }} </td>
                                                <td style="font-size: 10px"> {{ $detail->satuans->stn_nama }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <table width="100%" class="tt" id="footer">
                                    <tr>
                                        <td colspan="5">
                                            <hr size="2px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" width="5%"></td>
                                        <td style="font-size: 12px" align="center" width="30%">Dibuat Oleh,</td>
                                        <td style="font-size: 12px" align="center" width="30%">Pengirim Barang,</td>
                                        <td style="font-size: 12px" align="center" width="30%" align="center">Penerima Barang,</td>
                                        <td style="font-size: 12px" width="5%"></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" width="5%"></td>
                                        <td style="font-size: 12px" align="center" width="30%"><br><br><br><U>....................</U></td>
                                        <td style="font-size: 12px" align="center" width="30%"><br><br><br><U>....................</U></td>
                                        <td style="font-size: 12px" align="center" width="30%" align="center"><br><br><br><U>....................</U></td>
                                        <td style="font-size: 12px" width="5%"></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" width="5%"></td>
                                        <td style="font-size: 12px" align="center" width="30%"></td>
                                        <td style="font-size: 12px" colspan="2">No. Kendaraan :</td>
                                        <td style="font-size: 12px" width="5%"></td>
                                    </tr>
                                </table>
                            </div>
                            <hr>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop