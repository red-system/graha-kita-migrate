<html moznomarginboxes mozdisallowselectionprint>
    <head>
    <!-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> -->
    </head>
    <body>
    <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Tahoma;font-size:9px;padding:3px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000;color:#333;background-color:#fff;}
                .tg th{font-family:Tahoma;font-size:11px;font-weight:bold;padding:3px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <h3 style="font-family: Tahoma"><center><strong>NERACA</strong></center></h3>
                <h4 style="font-family: Tahoma"><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h4>
                <!-- <table class="tg" width="100%">
                    <thead>
                        <tr class="success">
                            <th width="25%"><center>ASET</center></th>
                            <th width="25%"><center>LIABILITAS</center></th>
                            <th width="25%"><center>EKUITAS</center></th>
                            <th width="25%"><center>STATUS</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{number_format($total_asset,2)}}</td>
                            <td>{{number_format($total_liabilitas,2)}}</td>
                            <td>{{number_format($total_ekuitas,2)}}</td>
                            @if($status=='BALANCE')
                            <td><font color="green"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                            @else
                            <td><font color="red"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                            @endif
                        </tr>
                    </tbody>
                </table> -->
            </div>
        </div>
            <?php
                $total_assets = 0;
                $total_liabilitass = 0;
                $total_ekuitass = 0;
            ?>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <table width="100%">
                    <tr>
                        <td>
                            <table class="tg" width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="2">Activa</th>
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody><?php $activa = 0;?>
                                @foreach($detail_perkiraan as $detail)
                                    @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening != 'AKTIVA TETAP')
                                    <tr>
                                        <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                        <td style="font-weight: bold;" align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                    </tr><?php $activa++;?>
                                    @foreach($detail->childs as $det_child)
                                    <tr>
                                        <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                        <td align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                    </tr><?php $activa++;?>
                                    @endforeach
                                    @endif
                                @endforeach
                                <?php
                                $total_aktiva = 0;
                                ?>
                                @foreach($detail_perkiraan as $detail)
                                    @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening == 'AKTIVA TETAP')
                                    <tr>
                                        <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                        <td style="font-weight: bold;" align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                    </tr><?php $activa++;?>
                                    @foreach($detail->childs as $det_child)
                                    <tr>
                                        <td style="font-weight: bold;">{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                        <td style="font-weight: bold;" align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                    </tr><?php $activa++;?>
                                    @foreach($det_child->childs as $child)
                                    <tr>
                                        <td>{!!$space3!!}{{$child->mst_kode_rekening}} {{$child->mst_nama_rekening}}</td>
                                        <td align="right">{{number_format($asset[$child->mst_kode_rekening],2)}}</td>
                                    </tr><?php $activa++;?>
                                    @endforeach
                                    @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Total Aktiva</th>
                                        <th>{{number_format($total_asset,2)}}</th>
                                    </tr>
                                </tfoot>          
                            </table>
                        </td>
                        <td>
                            <table class="tg" width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="2">Pasiva</th>
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody><?php $passiva = 0;?>
                                @foreach($detail_perkiraan as $detail)
                                    @if($detail->mst_neraca_tipe == 'liabilitas' && $detail->mst_master_id == '0')
                                    <tr>
                                        <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                        <td style="font-weight: bold;" align="right">{{number_format($liabilitas[$detail->mst_kode_rekening],2)}}</td>
                                    </tr><?php $passiva++;?>
                                    @foreach($detail->childs as $det_child)
                                    <tr>
                                        <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                        <td align="right">{{number_format($liabilitas[$det_child->mst_kode_rekening],2)}}</td>
                                    </tr><?php $passiva++;?>
                                    @endforeach
                                    @endif
                                @endforeach
                                @foreach($detail_perkiraan as $detail)
                                    @if($detail->mst_neraca_tipe == 'ekuitas' && $detail->mst_master_id == '0')
                                    <tr>
                                        <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                        <td style="font-weight: bold;" align="right">{{number_format($ekuitas[$detail->mst_kode_rekening],2)}}</td>
                                    </tr><?php $passiva++;?>
                                    @foreach($detail->childs as $det_child)
                                    <tr>
                                        <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                        <td align="right">{{number_format($ekuitas[$det_child->mst_kode_rekening],2)}}</td>
                                    </tr><?php $passiva++;?>
                                    @endforeach
                                    @endif
                                @endforeach
                                <?php $selisih = $activa-$passiva;?>
                                @for($i=1;$i<=$selisih;$i++)
                                    <tr>
                                        <td></td>
                                        <td align="right">0</td>
                                    </tr>
                                @endfor
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Total Pasiva</th>
                                        <th>{{number_format($hitung,2)}}</th>
                                    </tr>
                                </tfoot>       
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
