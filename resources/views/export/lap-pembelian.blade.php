<table>
    <tr>
        <td>Laporan Pembelian Stock</td>
    </tr>
    <tr>
        <td>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</td>
    </tr>
</table>
<table>
                                        <thead>
                                            <tr>
                                                <th> No </th>
                                                <th> Tanggal</th>
                                                <th> No PO </th>
                                                <th> No W/O </th>
                                                <th> No Pembelian </th>
                                                <th> Barcode </th>
                                                <th> Nama Barang </th>
                                                <th> Qty </th>
                                                <th> Satuan </th>
                                                <th> Harga </th>
                                                <th> Total </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $grand_total     = 0;
                                            
                                        ?> 
                                        @foreach($dataList as $spl)
                                            <?php $grand_total_spl = 0;$no=1;?>
                                            @if($jml_detail[$spl->spl_kode]>0)
                                            <tr>
                                                <td>{{$spl->spl_nama}}</td>
                                            </tr>
                                            @endif
                                            @foreach($pembelian[$spl->spl_kode] as $pembelian_spl) 
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{ date('d M Y', strtotime($pembelian_spl->ps_tgl)) }}</td>
                                                <td>{{$pembelian_spl->no_po}}</td>
                                                <td>{{$pembelian_spl->pos_no_po}} - {{ucfirst($pembelian_spl->pb_kondisi)}}</td>
                                                <td>{{$pembelian_spl->no_pembelian}}</td>
                                                <td>{{$pembelian_spl->brg_barcode}}</td>
                                                <td>{{$pembelian_spl->brg_nama}}</td>
                                                <td>{{$pembelian_spl->qty}}</td>
                                                <td>{{$pembelian_spl->stn_nama}}</td>
                                                <td>{{number_format($pembelian_spl->harga_net,2)}}</td>
                                                <td>{{number_format($pembelian_spl->total,2)}}</td>
                                            </tr>
                                            <?php
                                                $grand_total_spl = $grand_total_spl+$pembelian_spl->total;
                                                
                                            ?>
                                            @endforeach
                                            <?php $grand_total     = $grand_total+$grand_total_spl;?>
                                            @if($jml_detail[$spl->spl_kode]>0)
                                            <tr>
                                                <td colspan="10"><strong>Grand Total</strong></td>
                                                <td><strong>{{number_format($grand_total_spl,2)}}</strong></td>
                                            </tr>
                                            @endif 
                                        @endforeach                                                                     
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="10"><strong>Grand Total</strong></td>
                                                <td><strong>{{number_format($grand_total,2)}}</strong></td>
                                            </tr> 
                                        </tfoot>
                                    </table>