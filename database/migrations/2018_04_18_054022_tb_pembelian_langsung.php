<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPembelianLangsung extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_penjualan_langsung', function (Blueprint $table) {
         $table->increments('pl_no_faktur');
         $table->datetime('pl_tgl')->nullable();
         $table->integer('cus_kode')->nullable();
         $table->integer('pl_sales_person')->nullable();
         $table->integer('pl_checker')->nullable();
         $table->integer('pl_sopir')->nullable();
         $table->text('pl_catatan')->nullable();
         $table->datetime('pl_jatuh_tempo')->nullable();
         $table->datetime('pl_tgl_jatuh_tempo')->nullable();
         $table->integer('pl_subtotal')->nullable();
         $table->float('pl_disc')->nullable();
         $table->integer('pl_disc_nom')->nullable();
         $table->float('pl_ppn')->nullable();
         $table->integer('pl_ppn_nom')->nullable();
         $table->integer('pl_ongkos_angkut')->nullable();
         $table->integer('grand_total')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_penjualan_langsung');
     }
}
