<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mBarang;
use App\Models\mStok;

class StokAlert extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Stok Alert';
      $this->kodeLabel = 'barang';
  }

  function index() {
      $breadcrumb = [
          'Stok Alert'=>route('stokAlertList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'stok_alert';
      $data['title'] = $this->title;
      $getBarang = mBarang::Has('stok')->get();
      foreach ($getBarang as &$barang) {
        $total=0;
        $barang['kategory'] = $barang->kategoryProduct;
        $barang['group'] = $barang->groupStok;
        $barang->merek;
        $barang->satuan;
        $stok = $barang->stok;
        foreach ($stok as $key) {
          $total += $key->stok;
          // $key->gudang;
        }
        unset($barang->kategoryProduct);
        unset($barang->groupStok);
        $barang['QOH'] = $total;
      }
      // $data['dataList'] = $getBarang->where('QOH', '<', $getBarang->brg_stok_minimum);

      $filtered = $getBarang->filter(function ($value, $key) {
        return $value->QOH < $value->brg_stok_minimum;
      });
      $filtered->all();

      $data['dataList'] = $filtered;

      // $data['dataList'] = mBarang::leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
      // ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      // ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      // ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      // ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      // ->select('tb_stok.stok', 'tb_barang.brg_kode', 'brg_barcode', 'tb_barang.brg_nama', 'tb_barang.brg_stok_minimum', 'stn_nama', 'ktg_description', 'grp_description', 'mrk_description')
      // ->selectRaw('sum(stok) as QOH')
      // ->where('QOH', '<', 'tb_barang.brg_stok_minimum')
      // ->groupBy('tb_barang.brg_kode')
      // ->get();

      // return $data['dataList'];
      return view('stok-alert/stokAlertList', $data);
  }
}
