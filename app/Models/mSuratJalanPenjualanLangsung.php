<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mSuratJalanPenjualanLangsung extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_surat_jalan_penjualan_langsung';
  protected $primaryKey = 'sjl_kode';
  // public $timestamps = false;

  public function faktur()
  {
    return $this->belongsTo('App\Models\mPenjualanLangsung', 'pl_no_faktur')->select('pl_no_faktur', 'cus_kode');
  }
}
