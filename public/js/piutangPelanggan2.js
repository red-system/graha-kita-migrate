$(document).ready(function () {
    
    $('#table_asset, #table_liabilitas, #table_ekuitas, #table_aktiva').dataTable( {
        "aLengthMenu": [[-1], ["All"]],
        "pageLength": -1
    } );

    $('#tb_daftar_pembelian,#tbl_asset').dataTable( {
        "aLengthMenu": [[10,25,50,-1], [10,25,50,"All"]],
        "pageLength": 10
    } );

    $('#table_penyusutan').dataTable( {
        "aLengthMenu": [[-1], ["All"]],
        "pageLength": -1
    } );

    $('.btn-pilih-periode').click(function(){
        $('#modal-pilih-periode').modal("show");
    });

    $('.payment-btn').click(function(){
        var grand_total = $(this).data('todo').todo;
        var pp_invoice = $(this).data('todo').id;
        var kode_perkiraan = $(this).data('todo').kode_perkiraan;
        var no_faktur = $(this).data('todo').no_faktur;
        var cus_kode = $(this).data('todo').cus_kode;

        $('[name="pp_invoice_2"]').val('PP'+pp_invoice);
        $('[name="pp_invoice"]').val(pp_invoice);
        $('[name="kode_perkiraan"]').val(kode_perkiraan);
        $('[name="cus_kode"]').val(cus_kode);
        $('[name="no_faktur"]').val(no_faktur);
        $('.nominal-grand-total').html(grand_total);
        $('.nominal-sisa').html(grand_total);
        $('#modalPayment').modal("show");

    });

    $('.btn-payment-hutang').click(function(){
        var grand_total = $(this).data('todo').todo;
        var hs_kode = $(this).data('todo').id;
        var kode_perkiraan = $(this).data('todo').kode_perkiraan;
        var no_faktur = $(this).data('todo').no_faktur;
        var spl_kode = $(this).data('todo').spl_kode;


        $('[name="hs_kode_2"]').val('HS'+hs_kode);
        $('[name="hs_kode"]').val(hs_kode);
        $('[name="kode_perkiraan"]').val(kode_perkiraan);
        $('[name="spl_nama"]').val(spl_kode);
        $('[name="no_faktur"]').val(no_faktur);
        $('.nominal-grand-total').html(grand_total);
        $('#modalPaymentHutang').modal("show");

    });

    $('.btn-payment-hutang-lain').click(function(){
        var grand_total = $(this).data('todo').todo;
        var hl_kode = $(this).data('todo').id;
        var kode_perkiraan = $(this).data('todo').kode_perkiraan;

        $('[name="hl_kode"]').val(hl_kode);
        $('[name="hl_kode_2"]').val('HL'+hl_kode);
        $('[name="kode_perkiraan"]').val(kode_perkiraan);
        $('.nominal-sisa').html(grand_total);
        $('.nominal-grand-total').html(grand_total);
        $('#modalPaymentHutangLain').modal("show");

    });

    $('.btn-payment-piutang-lain').click(function(){
        var grand_total = $(this).data('todo').todo;
        var pl_invoice = $(this).data('todo').id;
        var kode_perkiraan_piutang_lain = $(this).data('todo').kode_perkiraan;

        $('[name="pl_invoice"]').val(pl_invoice);
        $('[name="pl_invoice_2"]').val('PL'+pl_invoice);
        $('[name="kode_perkiraan"]').val(kode_perkiraan_piutang_lain);
        $('.nominal-sisa').html(grand_total);
        $('.nominal-grand-total').html(grand_total);
        $('#modalPaymentPiutangLain').modal("show");

    });

    $('.btn-tambah-jurnal-umum').click(function(){
        $('#modal-tambah-jurnal-umum').modal("show");

    });

    ////// JS PAYMENT


    $('.btn-row-payment-plus').click(function() {
        var row_payment = $('.table-row-payment tbody').html(); 
        var el          = $(row_payment);       
        $('.table-data-payment tbody').append(el);

        btn_row_delete_payment();
        payment();
        set_charge();
        setor();
        payment_third_party();
        btn_selectpickerx(el);
    });

    $('.btn-row-transaksi-plus').click(function() {
        var row_transaksi = $('.table-row-transaksi tbody').html();
        var el          = $(row_transaksi);
        $('.table-data-transaksi tbody').append(el);
        btn_row_delete_transaksi();
        btn_selectpickerx(el);
    });

    function btn_row_delete_payment() {
        $('.btn-row-delete-payment').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_sisa();
        });
    }

    function btn_selectpickerx(el) {
        el.children('td:nth-child(1)').children('select').selectpicker();
    }


    function btn_row_delete_transaksi() {
        $('.btn-row-delete-transaksi').click(function() {
            $(this).parents('tr').remove();
            
        });
    }

    function payment() {
        $('[name="payment[]"]').keyup(function() {
            var payment = $(this).val();

            $(this).parents('td').siblings('td.payment_total').children('input').val(payment);

            kalkulasi_sisa();
        });
    }

    function set_charge() {
        $('[name="charge[]"]').keyup(function() {
            var charge = $(this).val();
            var payment = $(this).parents('td').siblings('td.payment').children('input').val();
            var charge_nom = (parseInt(charge)/100) * parseInt(payment);
            var total = parseInt(payment) + parseInt(charge_nom);

            $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
            $(this).parents('td').siblings('td.payment_total').children('input').val(total);

            //kalkulasi_sisa();
        });
    }

    function setor() {
        $('[name="setor[]"]').keyup(function() {
            var setor = $(this).val();
            var total = $(this).parents('td').siblings('td.payment_total').children('input').val();
            var kembalian = parseInt(setor) - parseInt(total);

            $(this).parents('td').siblings('td.kembalian').html(kembalian);
        });
    }

    function kalkulasi_sisa() {
        var payment_total = 0;
        var grand_total = $('.nominal-grand-total').html();
        $('[name="payment_total[]"]').each(function(key, val) {
            var payment_total_ini = $(this).val();
            if(payment_total_ini != '') {
                payment_total = parseInt(payment_total) + parseInt(payment_total_ini);
            }
        });
        sisa = parseInt(grand_total) - parseInt(payment_total);
        $('.nominal-sisa').text(sisa);
    }

    function payment_third_party() {
        $('[name="tgl_pencairan[]"]').datepicker();
        //$('[name="master_id[]"]').select2();
    }

    $('[name="tgl_transaksi"]').datepicker( {
        orientation: 'auto bottom'
    });

    
});
