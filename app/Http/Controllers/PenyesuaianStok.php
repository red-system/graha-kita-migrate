<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Helpers\Main;
use Carbon\Carbon;
use App\Models\mArusStok;
use App\Models\mBarang;
use App\Models\mGudang;
use App\Models\mJurnalUmum;
use App\Models\mPerkiraan;
use App\Models\mTransaksi;
use App\Models\mStok;
use App\Models\mPenyesuaianStok;
use DB;

class PenyesuaianStok extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penyesuaian Stok';
    $this->kodeLabel = 'penyesuaian_stok';
  }

  protected function createID() {
    $kode = $this->main->kodeLabel('penyesuaian_stok');
    $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'.'.$kode.'.';
    // $lastorder = mPenyesuaianStok::where(\DB::raw('LEFT(ps_kode, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('RIGHT(ps_kode, 4) as no_order')->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mPenyesuaianStok::where('ps_kode', 'like', '%'.$preffix.'%')->max('ps_kode');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no.$user;
  }

  public function index() {
    $breadcrumb = [
      'Penyesuaian Stok' => route('penyesuaianStok.index'),
    ];

    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $new_id = $this->createID();
    $data['new_id'] = $new_id;
    $data['title'] = $this->title;
    $data['menu'] = 'penyesuaian_stok';
    $data['kodeBarang'] = $this->main->kodeLabel('barang');
    $data['barang'] = $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')->get();
    return view('penyesuaian-stok/index', $data);
  }

  public function store(Request $request) {
    // return $request;

    DB::beginTransaction();
    try {
      $kode_bukti_id = $this->createID();
      $time = Carbon::now();

      $stok = mStok::where('stk_kode', $request->stk_kode)->first();
      $stok_awal = $stok->stok;
      $stok_akhir = $request->stok;
      $stok->stok = $request->stok;
      $stok->save();

      $total=0;
      $data = mStok::where('brg_kode', $stok->brg_kode)->get();
      foreach ($data as $gdg) {
        $total += $gdg->stok;
      }
      $QOH = $total;

      if ($stok_awal >= $stok_akhir) {
        $arusStok = new mArusStok();
        $arusStok->ars_stok_date = $time;
        $arusStok->brg_kode = $stok->brg_kode;
        $arusStok->stok_in = 0;
        $arusStok->stok_out = ($stok_awal - $stok_akhir);
        $arusStok->stok_prev = $QOH;
        $arusStok->gdg_kode = $stok->gdg_kode;
        $arusStok->keterangan = 'Penyesuaian Stok '.$kode_bukti_id;
        $arusStok->save();
      }
      else {
        $arusStok = new mArusStok();
        $arusStok->ars_stok_date = $time;
        $arusStok->brg_kode = $stok->brg_kode;
        $arusStok->stok_in = ($stok_akhir - $stok_awal);
        $arusStok->stok_out = 0;
        $arusStok->stok_prev = $QOH;
        $arusStok->gdg_kode = $stok->gdg_kode;
        $arusStok->keterangan = 'Penyesuaian Stok '.$kode_bukti_id;
        $arusStok->save();
      }

      $barang = mBarang::select('tb_barang.brg_kode', 'brg_hpp', 'brg_barcode', 'brg_nama', 'ktg_nama', 'mrk_nama', 'stn_nama', 'grp_nama')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->where('tb_barang.brg_kode', $request->brg_kode)
      ->first();

      $total_hpp = abs($stok_awal-$stok_akhir)*$barang->brg_hpp;

      $data = new mPenyesuaianStok();
      $data->ps_kode = $kode_bukti_id;
      $data->ps_tgl = $time;
      $data->brg_barcode = $barang->brg_barcode;
      $data->brg_nama = $barang->brg_nama;
      $data->stn_nama = $barang->stn_nama;
      $data->ktg_nama = $barang->ktg_nama;
      $data->grp_nama = $barang->grp_nama;
      $data->mrk_nama = $barang->mrk_nama;
      $data->stok_awal = $stok_awal;
      $data->stok_akhir = $stok_akhir;
      $data->keterangan = $request->keterangan;
      $data->save();

      // General
      $tipe_arus_kas = 'Operasi';
      $date_db = date('Y-m-d H:i:s');
      $year = date('Y');
      $month = date('m');
      $day = date('d');
      $where = [
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day
      ];
      $jmu_no = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
      if($jmu_no->count() == 0) {
          $jmu_no = 1;
      } else {
          $jmu_no = $jmu_no->first()->jmu_no + 1;
      }

      $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
          ->limit(1)
          ->first();
      $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

      $data_jurnal = [
          'jurnal_umum_id'=>$jurnal_umum_id,
          'no_invoice'=>$kode_bukti_id,
          'jmu_no'=>$jmu_no,
          'jmu_tanggal'=>date('Y-m-d'),
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day,
          'jmu_keterangan'=>'Penyesuaian Stok '.$kode_bukti_id,
          'jmu_date_insert'=>$date_db,
          'jmu_date_update'=>$date_db,
      ];
      mJurnalUmum::insert($data_jurnal);

      if ($stok_awal > $stok_akhir) {
        $master_id_kredit = ["41105", "1601"];
      }
      else {
        $master_id_kredit = ["1601", "41105"];
      }

      foreach($master_id_kredit as $key=>$val) {
          $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
          $master_idK = $masterK->master_id;
          $trs_kode_rekening = $masterK->mst_kode_rekening;
          $trs_nama_rekening = $masterK->mst_nama_rekening;

          if ($stok_awal > $stok_akhir) {
            if ($trs_kode_rekening == "41105") {
              $debet_nom = $total_hpp;
              $kredit_nom = 0;
              $trs_jenis_transaksi = 'debet';
              $trs_catatan = 'debet';
            }
            elseif ($trs_kode_rekening == "1601") {
              $debet_nom = 0;
              $kredit_nom = $total_hpp;
              $trs_jenis_transaksi = 'kredit';
              $trs_catatan = 'kredit';
            }
          }
          else {
            if ($trs_kode_rekening == "41105") {
              $debet_nom = 0;
              $kredit_nom = $total_hpp;
              $trs_jenis_transaksi = 'kredit';
              $trs_catatan = 'kredit';
            }
            elseif ($trs_kode_rekening == "1601") {
              $debet_nom = $total_hpp;
              $kredit_nom = 0;
              $trs_jenis_transaksi = 'debet';
              $trs_catatan = 'debet';
            }
          }

          $data_transaksiK = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'master_id'=>$master_idK,
              'trs_jenis_transaksi'=>$trs_jenis_transaksi,
              'trs_debet'=>$debet_nom,
              'trs_kredit'=>$kredit_nom,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$trs_catatan,
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              'tgl_transaksi'=>date('Y-m-d'),
          ];
          mTransaksi::insert($data_transaksiK);
      }

      DB::commit();
      return 'true';
    } catch (\Exception $e) {
      DB::rollBack();
      throw $e;
    }
  }

  public function detail($id='') {
    $breadcrumb = [
      'Penyesuaian Stok' => route('penyesuaianStok.detail', ['id' => $id]),
    ];

    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $new_id = $this->createID();
    $data['new_id'] = $new_id;
    $data['brg_kode'] = $id;
    $data['title'] = $this->title.' '.$this->main->kodeLabel('barang').$id;
    $data['menu'] = 'penyesuaian_stok';
    // $data['kodeBarang'] = $this->main->kodeLabel('barang');
    // $data['barang'] = $barang = mStok::where('brg_kode', $id)->get();
    return view('penyesuaian-stok/detail', $data);
  }

  public function GetStok($id='')
  {
    $data = mStok::query();
    $data->select('tb_barang.brg_kode', 'brg_barcode', 'brg_no_seri', 'stok', 'tb_stok.gdg_kode', 'gdg_nama', 'stk_kode')
    ->leftJoin('tb_barang', 'tb_stok.brg_kode', '=', 'tb_barang.brg_kode')
    ->leftJoin('tb_gudang', 'tb_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
    ->where('tb_barang.brg_kode', $id);
    // $no=1;
    // foreach ($data as &$barang) {
    //   $barang['no'] = $no++;
    // }

    return Datatables::of($data)->make(true);
  }

  public function historyPenyesuaian()
  {
    $data = mPenyesuaianStok::query()->orderBy('ps_kode', 'desc');
    // $no=1;
    // foreach ($data as &$barang) {
    //   $barang['no'] = $no++;
    // }

    return Datatables::of($data)->make(true);
  }

}
