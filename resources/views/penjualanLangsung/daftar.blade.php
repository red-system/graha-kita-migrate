@php
use App\Models\mDeliveryOrder;
@endphp

@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
  <script type="text/javascript">
    $('.btn-edit-penjualan').click(function() {
      var ini = $(this);
      var token = $('#data-back').data('form-token');
      var href_edit_penjualan = ini.data('href');
      var no_faktur = ini.data('faktur');

      var data_send = {
        no_faktur: no_faktur,
        _token: token
      };

      $.ajax({
        url: "{{route('penjualan.cek_piutang')}}",
        type: 'POST',
        data: data_send,
        success: function(data) {
          if (data.stat != 'ok') {
            window.location.replace(href_edit_penjualan);
          }
          else {
            swal({
              title: 'Perhatian',
              text: 'Data Piutang Sudah Dilunasi',
              type: 'warning'
            });
          }
        },
        error: function(request, status, error) {
          swal({
            title: 'Perhatian',
            text: 'Error : '+error,
            type: 'warning'
          });
        }
      });
    });

    $('#sample_1').on( 'draw.dt', function () {
      $('.btn_print-faktur').click(function() {
        var ini = $(this);
        $('#modal-pass').data('href', ini).modal('show');
        $('input[name="username"]').val('');
        $('input[name="password"]').val('');
      });

      $('.btn-pass').click(function() {
        var ini = $('#modal-pass').data('href');
        var no_faktur = ini.data('faktur');
        var person = $('select[name="person"] :selected').val();
        var keterangan = $('[name="keterangan"]').val();
        var username = $('input[name="username"]').val();
        var password = $('input[name="password"]').val();
        var token = $('#data-back').data('form-token');
        var data_send = {
          no_faktur: no_faktur,
          person: person,
          keterangan: keterangan,
          username: username,
          password: password,
          _token: token
        };

        $.ajax({
          url: "{{route('historyCetak.store')}}",
          type: 'POST',
          data: data_send,
          success: function(data) {
            if (data == 'true') {
              var ini = $('#modal-pass').data('href');
              var url = ini.data('href');
              $('#modal-pass').modal('hide');
              $('input[name="username"]').val('');
              $('input[name="password"]').val('');
              window.open(url, '_blank');
            }
            else {
              swal({
                title: 'Perhatian',
                text: 'Password Salah',
                type: 'warning'
              });
            }
          },
          error: function(request, status, error) {
            swal({
              title: 'Perhatian',
              text: 'Password Salah',
              type: 'warning'
            });
          }
        });
      });
    });
  </script>
@stop

@section('body')
  <span id="data-back" data-form-token="{{ csrf_token() }}"></span>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet light ">
            <div class="portlet light">
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th style="font-size:10px" width="10"> No </th>
                    <th style="font-size:10px"> Tanggal</th>
                    <th style="font-size:10px"> No Faktur </th>
                    <th style="font-size:10px"> Nama Customer </th>
                    <th style="font-size:10px"> Total </th>
                    <th style="font-size:10px"> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td style="font-size:10px"> {{ $no++ }}. </td>
                      <td style="font-size:10px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                      <td style="font-size:10px"> {{ $row->pl_no_faktur }} </td>
                      <td style="font-size:10px"> {{ $row->cus_nama }} </td>
                      <td style="font-size:10px" align="right"> {{number_format($row->grand_total, 2, "," ,".")}} </td>
                      <td>
                        <div class="btn-group-xs">
                          <a type="button" data-href="{{ route('penjualanLangsungGetFakturJual', ['id'=>$row->pl_no_faktur]) }}" data-faktur="{{$row->pl_no_faktur}}" target="_blank" class="btn btn-success btn_print-faktur" >
                            <span class="glyphicon glyphicon-print"></span> Cetak Faktur
                          </a>
                          <a type="button" data-href="{{ route('penjualanLangsungGetFakturJual.mini', ['id'=>$row->pl_no_faktur, 'type'=>'mini']) }}" data-faktur="{{$row->pl_no_faktur}}" target="_blank" class="btn btn-success btn_print-faktur" >
                            <span class="glyphicon glyphicon-print"></span> Cetak Faktur Kecil
                          </a>
                          <button class="btn btn-danger btn-delete-new" data-href="{{ route('penjualanLangsungDelete', ['kode'=>$row->pl_no_faktur]) }}">
                              <span class="icon-trash"></span> Delete
                          </button>
                          @if (mDeliveryOrder::where('no_faktur', $row->pl_no_faktur)->doesntExist())
                            <a type="button" class="btn btn-success btn-edit-penjualan" data-faktur="{{$row->pl_no_faktur}}" data-href="{{route('detailPenjualanLangsung',['kode'=>$row->pl_no_faktur])}}">
                              <span class="icon-pencil"></span> Edit
                            </a>
                          @endif
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-pass" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-red bg-font-red">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            Password
          </h4>
        </div>
        <div class="modal-body form">
          <form class="form-horizontal" role="form">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Person</label>
                <div class="col-md-9">
                  <select id="person" class="form-control selectpicker" data-live-search="true" name="person">
                    @foreach($person as $prsn)
                      <option value="{{ $prsn->kry_nama }}">{{ $prsn->kry_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Keterangan</label>
                <div class="col-md-9">
                  <textarea id="keterangan" class="form-control" name="keterangan" required></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input id="username" type="text" class="form-control" name="username" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input id="password" type="password" class="form-control" name="password" required>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="button" class="btn green btn-pass">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
