<?php

namespace App\Exports;

use App\Models\mDetailPenjualanLangsung;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RekapPenjualanLangsungExport implements FromView
{
    private $start;
    private $end;

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $rowPL = mDetailPenjualanLangsung::select('pl_no_faktur', 'qty', 'harga_net', 'brg_kode', 'gudang', \DB::raw('SUM(qty) as brg_qty'))
      ->whereHas('PL_tgl', function($q) use($start, $end){
        $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
      })
      ->groupBy('brg_kode', 'gudang')->get();
      $total=0;
      $grand_total=0;
      foreach ($rowPL as &$key) {
        $key->barang->satuan;
        $key->gdg;
        $key->PL_tgl;
        $total = $key->harga_net*$key->brg_qty;
        $key['total'] = $total;
        $grand_total += $total;
      }

      //Data Penjualan Langsung
      $data['dataPL'] = $rowPL;
      $data['grand_total'] = $grand_total;

      return view('export.laporan-rekap-penjualan-langsung', $data);
    }
}
