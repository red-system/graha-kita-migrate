<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mHutangLain extends Model
{
  //public $incrementing = false;
  protected $table = 'tb_hutang_lain';
  protected $primaryKey = 'hl_kode';
  public $timestamps = false;

  // protected $fillable=['hl_kode','no_hutang_lain','hl_jatuh_tempo','hl_dari','hl_amount','hs_keterangan','hs_status','kode_perkiraan'];
  protected $guarded =[];

  public function dataList() 
  {
    return static::all();
  }

  public function spl(){
    return $this->belongsTo(mSupplier::class,'hl_dari','spl_kode');
  }

}
