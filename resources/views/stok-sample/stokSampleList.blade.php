@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script type="text/javascript">
  $('.btn-return-stok').click(function() {
    var ini = $(this);
    $('#modal-stok').data('stok', ini).modal('show');
  });

  $('.form-send-stok').submit(function(e) {
    e.preventDefault();
    var ini = $('#modal-stok').data('stok');
    var stok_sample_kode = ini.parents('tr').children('td').children('input[name="stok_sample_kode"]').val();

    var qty = $('input[name="qty"]').val();
    var token = $('input[name="_token"]').val();
    var data_send = {
      stok_sample_kode: stok_sample_kode,
      qty: qty,
      _token: token
    };

    $.ajax({
      url: $(this).attr('action'),
      type: $(this).attr('method'),
      data: data_send,
      success: function(data) {
        // console.log(data);
        $('#modal-stok').modal('hide');

        window.location.href = data.redirect;
      },
      error: function(request, status, error) {
        $('#modal-stok').modal('hide');

        swal({
          title: 'Perhatian',
          text: 'Data Gagal Disimpan!',
          type: 'error'
        });
      },
    });
    return false;
  });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <button class="btn btn-success pull-right btn-print" data-toggle="modal" href="#modal-print">
                <i class="glyphicon glyphicon-print"></i> Print Stok Sample
              </button>
              <br /><br />
              <br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th style="font-size:10px"> No </th>
                    <th style="font-size:10px"> Kode Barang </th>
                    <th style="font-size:10px"> Nama Barang </th>
                    <th style="font-size:10px"> Barang No Seri </th>
                    <th style="font-size:10px"> Merek </th>
                    <th style="font-size:10px"> Kategori </th>
                    <th style="font-size:10px"> Group </th>
                    <th style="font-size:10px"> Supplier</th>
                    <th style="font-size:10px"> Qty </th>
                    <th style="font-size:10px"> Satuan </th>
                    <th style="font-size:10px"> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td style="font-size:10px"> {{ $no++ }}. </td>
                      <td style="font-size:10px"> {{ $row->barang['brg_barcode'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang['brg_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row['brg_no_seri'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang->merek['mrk_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang->Kategori['ktg_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang->group['grp_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->supplier['spl_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row['qty'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang->satuan['stn_nama'] }} </td>
                      <td style="font-size:10px" style="white-space: nowrap">
                        <input type="hidden" class="form-control" name="stok_sample_kode" value="{{ $row->stok_sample_kode }}">

                        <div class="btn-group-xs">
                          <button type="button" class="btn btn-danger btn-return-stok" data-toggle="modal">
                            <span class="icon-pencil"></span> Return Stok
                          </button>
                          <a type="button" class="btn btn-info" href="{{route('stokSample.detail', ['id'=>$row->stok_sample_kode])}}">
                          <span class="icon-eye"></span> Detail
                        </a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-red bg-font-red">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            Kembalikan Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{route('returnStokSample')}}" class="form-horizontal form-send-stok" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">QTY</label>
                <div class="col-md-9">
                  <input type="hidden" class="form-control" name="stok_sample_kode">
                  <input type="number" min="0" class="form-control" name="qty" value="0">
                </div>
              </div>
            </div>
            <br>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green btn-qty-stok">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-print" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Print
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{route('stokSamplePrint')}}" class="form-horizontal" role="form" method="post" target="_blank">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="type">
                    <option value="General">General</option>
                    <option value="Detail">Detail</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kategory</label>
                <div class="col-md-9">
                  <select class="form-control" name="ktg_kode">
                    <option value="0">---Semua---</option>
                    @foreach($k_product as $kategory)
                      <option value="{{ $kategory->ktg_kode }}">{{ $kategory->ktg_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Group</label>
                <div class="col-md-9">
                  <select class="form-control" name="grp_kode">
                    <option value="0">---Semua---</option>
                    @foreach($g_product as $group)
                      <option value="{{ $group->grp_kode }}">{{ $group->grp_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Merek</label>
                <div class="col-md-9">
                  <select class="form-control" name="mrk_kode">
                    <option value="0">---Semua---</option>
                    @foreach($merek as $merk)
                      <option value="{{ $merk->mrk_kode }}">{{ $merk->mrk_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select class="form-control" name="spl_kode">
                  <option value="0">---Semua---</option>
                    @foreach($supplier as $spl)
                      <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
