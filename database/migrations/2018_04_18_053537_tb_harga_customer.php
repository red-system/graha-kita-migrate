<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHargaCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_harga_customer', function (Blueprint $table) {
         $table->increments('hrg_cus_kode');
         $table->integer('cus_kode')->nullable();
         $table->integer('brg_kode')->nullable();
         $table->integer('hrg_cus_harga_jual_eceran')->nullable();
         $table->integer('hrg_cus_harga_jual_partai')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_harga_customer');
     }
}
