<html>
  <head>
    <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
  </head>
  <body>

  <div class="row">
    <div class="col-xs-12">
      <div class="portlet light ">
        <table width="100%">
          <tr>
            <td colspan="3">
              <strong>A.K.I.,</strong> Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088 Fax.(0361)418933
              <br /><br />
            </td>
          </tr>
          <tr>
            <td>Dikirim Kepada</td>
            <td></td>
            <td>No. S.J</td>
            <td>: {{ $noFaktur }}</td>
          </tr>
          <tr>
            <td>Nama</td>
            @if ($row['cus_nama'] == null)
              <td>: Guest </td>
            @else
              <td>: {{ $row['cus_nama'] }}</td>
            @endif
            <td>Tgl S.J</td>
            <td>: {{ $tanggal }}</td>
          </tr>
          <tr>
            <td>Alamat</td>
            @if ($row['cus_alamat'] == null)
              <td>: - </td>
            @else
              <td>: {{ $row['cus_alamat'] }}</td>
            @endif
            <td></td>
            <td></td>
          </tr>
        </table>
        <br /><br />
        <h2 style="letter-spacing: 10px" align="center">Surat Jalan</h2>
        <table class="table table-striped table-bordered table-hover table-header-fixed">
          <thead>
          <tr class="">
            <th width="10"> No </th>
            {{--<th> Kode Gudang </th>--}}
            <th> Kode Stock </th>
            <th> Nama Stock</th>
            <th> Banyak </th>
            <th> Satuan </th>
            {{-- <th> Pack </th> --}}
            {{--<th> Harga Jual </th>
            <th> Disc </th>
            <th> Disc Nom </th>
            <th> Harga Net </th>
            <th> Qty </th>
            <th> Terkirim </th>--}}
          </tr>
          </thead>
          <tbody>
          @foreach($list as $row)
            <tr>
              <td> {{ $no++ }}. </td>
              {{--<td> {{ $kodeGudang.$row->gdg_kode }} </td>--}}
              <td> {{ $kodeBarang.$row->brg_kode }} </td>
              <td> {{ $row->nama_barang }} </td>
              <td> {{ $row->qty }} </td>
              <td> {{ $row->satuan }} </td>
              {{-- <td> </td> --}}
{{--              <td> {{ $row->harga_jual }} </td>
              <td> {{ $row->disc }} </td>
              <td> {{ $row->disc_nom }} </td>
              <td> {{ $row->harga_net }} </td>
              <td> {{ $row->qty }} </td>
              <td> {{ $row->terkirim }} </td>--}}
            </tr>
          @endforeach
          </tbody>
        </table>
        <br /><br />
        Checker : {{ $checker }} | Supir : {{ $sopir }}
        <br /><br /><br />
        <table width="100%">
          <tr>
            <td width="50%" align="center">Pengirim</td>
            <td width="50%" align="center">Penerima</td>
          </tr>
          <tr>
            <td colspan="2"><br /><br /><br /><br /></td>
          </tr>
          <tr>
            <td align="center">
              (......................................)
            </td>
            <td align="center">
              (......................................)
            </td>
          </tr>
        </table>
      </div>
    </div></div>
  </body>
</html>
