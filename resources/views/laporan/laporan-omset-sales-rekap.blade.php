<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Rekap Omset Sales</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <h4>Penjualan Langsung</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">QTY Jual</th>
                  <th style="font-size:12px">Rp. Jual</th>
                  {{-- <th>QTY Retur</th>
                  <th>Rp. Retur</th>
                  <th>Total Qty</th>
                  <th>Total Rp.</th> --}}
                </tr>
              </thead>
                <tbody>
                  @foreach($dataPL as $row)
                    <tr>
                      <td style="font-size:12px"> {{ $no++ }}. </td>
                      <td style="font-size:12px"> {{ $row->kry_nama }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->qty_jual, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->total_jual, 2, "." ,",") }} </td>
                    </tr>
                  @endforeach
                  {{-- <tfoot> --}}
                    <tr>
                      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
                      <td style="font-weight:bold" align="right">{{number_format($rekapTJPL, 2, "." ,",")}}</td>
                    </tr>
                  {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
          <br>
          <h4>Penjualan Titipan</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">QTY Jual</th>
                  <th style="font-size:12px">Rp. Jual</th>
                  {{-- <th>QTY Retur</th>
                  <th>Rp. Retur</th>
                  <th>Total Qty</th>
                  <th>Total Rp.</th> --}}
                </tr>
              </thead>
                <tbody>
                  @foreach($dataPT as $row)
                    <tr>
                      <td style="font-size:12px"> {{ $no_2++ }}. </td>
                      <td style="font-size:12px"> {{ $row->kry_nama }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->qty_jual, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->total_jual, 2, "." ,",") }} </td>
                    </tr>
                  @endforeach
                  {{-- <tfoot> --}}
                    <tr>
                      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
                      <td style="font-weight:bold" align="right">{{number_format($rekapTJPT, 2, "." ,",")}}</td>
                    </tr>
                  {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
