@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <h3><strong><span class="icon-plus"></span> Tambah Jurnal Umum</strong></h3>
                        
                        <!-- <div class="col-md-12"> -->
                            <form action="{{route('insertJurnalUmum')}}" class="form-send form-horizontal" method="post" role="form">
                            {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3">Tanggal Transaksi</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control date-picker" name="tgl_transaksi">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3">Kode Bukti</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="kode_bukti">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3">Keterangan</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="keterangan">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-success btn-row-transaksi-plus" data-toggle="modal"> 
                                                <span class="fa fa-plus"></span> TAMBAH DATA TRANSAKSI
                                            </button>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-transaksi">
                                        <thead>
                                            <tr>
                                                <th>Kode Perkiraan</th>
                                                <th>Jenis Transaksi</th>
                                                <th>Debet</th>
                                                <th>Kredit</th>
                                                <th>Tipe Arus Kas</th>
                                                <th>Catatan</th>
                                                <th>Menu</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                    
                                        </tbody>
                                    </table>

                                    <table class="table-row-transaksi hide">
                                    <tbody>
                                        <tr>
                                            <td>
                                            <select name="master_id[]" class="form-control selectpickerx" id="master_id[]" data-live-search="true" data-placeholder="Kode Akunting">
                                                @foreach($perkiraan as $r)
                                                <option value="{{ $r->master_id }}" 
                                                    data-nama-rek="{{$r->mst_nama_rekening}}"
                                                    data-kode-rek="{{$r->mst_kode_rekening}}"
                                                    data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                                @endforeach
                                            </select>
                                            </td>
                                            <td class="jenis_transaksi">
                                                <select name="jenis_transaksi[]" class="form-control">
                                                    <option value="debet">Debet</option>
                                                    <option value="kredit">Kredit</option>
                                                </select>
                                                <!-- <input type="number" name="jenis_transaksi[]" class="form-control" value="0"> -->
                                            </td>
                                            <td class="debet">
                                                <input type="number" name="debet[]" class="form-control" value="0">
                                            </td>
                                            <td class="kredit">
                                                <input type="number" name="kredit[]" class="form-control" value="0">
                                            </td>
                                            <td class="tipe_arus_kas">
                                                <select name="tipe_arus_kas[]" class="form-control">
                                                    <option>-Pilih Tipe Arus Kas-</option>
                                                    <option value="operasi">Operasi</option>
                                                    <option value="pendanaan">Pendanaan</option>             
                                                    <option value="investasi">Investasi</option>
                                                </select>
                                            </td>
                                            <td class="catatan">
                                                <textarea class="form-control" name="catatan[]"></textarea>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-transaksi">Hapus</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>   
                                
                                    
                                    
                                
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                            <a type="button" class="btn default" href="{{route('jurnalUmum')}}">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop