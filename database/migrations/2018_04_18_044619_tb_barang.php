<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_barang', function (Blueprint $table) {
         $table->increments('brg_kode');
         $table->string('brg_barcode')->nullable();
         $table->string('brg_nama')->nullable();
         $table->integer('gdg_kode')->nullable();
         $table->integer('mrk_kode')->nullable();
         $table->integer('ktg_kode')->nullable();
         $table->integer('stn_kode')->nullable();
         $table->integer('grp_kode')->nullable();
         $table->integer('spl_kode')->nullable();
         $table->integer('brg_stok_maximum')->nullable();
         $table->integer('brg_stok_minimum')->nullable();
         $table->text('brg_product_img')->nullable();
         $table->integer('brg_harga_beli_terakhir')->nullable();
         $table->integer('brg_harga_beli_tertinggi')->nullable();
         $table->integer('brg_hpp')->nullable();
         $table->integer('brg_harga_jual_eceran')->nullable();
         $table->integer('brg_harga_jual_partai')->nullable();
         $table->float('brg_ppn_dari_supplier_persen')->nullable();
         $table->string('brg_status')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_barang');
     }
}
