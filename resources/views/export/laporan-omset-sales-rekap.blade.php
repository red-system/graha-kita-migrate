<table>
  <thead>
    <tr>
      <td colspan="4">Penjualan Langsung</td>
    </tr>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Nama</th>
      <th style="font-size:12px">QTY Jual</th>
      <th style="font-size:12px">Rp. Jual</th>
      {{-- <th>QTY Retur</th>
      <th>Rp. Retur</th>
      <th>Total Qty</th>
      <th>Total Rp.</th> --}}
    </tr>
  </thead>
  <tbody>
    @foreach($dataPL as $row)
      <tr>
        <td style="font-size:12px"> {{ $no++ }}. </td>
        <td style="font-size:12px"> {{ $row->kry_nama }} </td>
        <td style="font-size:12px" align="right"> {{ number_format($row->qty_jual, 2, "." ,",") }} </td>
        <td style="font-size:12px" align="right"> {{ number_format($row->total_jual, 2, "." ,",") }} </td>
      </tr>
    @endforeach
    {{-- <tfoot> --}}
    <tr>
      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
      <td style="font-weight:bold" align="right">{{number_format($rekapTJPL, 2, "." ,",")}}</td>
    </tr>
    {{-- </tfoot> --}}
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
      <td colspan="4">Penjualan Titipan</td>
    </tr>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Nama</th>
      <th style="font-size:12px">QTY Jual</th>
      <th style="font-size:12px">Rp. Jual</th>
      {{-- <th>QTY Retur</th>
      <th>Rp. Retur</th>
      <th>Total Qty</th>
      <th>Total Rp.</th> --}}
    </tr>
  </thead>
  <tbody>
    @foreach($dataPT as $row)
      <tr>
        <td style="font-size:12px"> {{ $no_2++ }}. </td>
        <td style="font-size:12px"> {{ $row->kry_nama }} </td>
        <td style="font-size:12px" align="right"> {{ number_format($row->qty_jual, 2, "." ,",") }} </td>
        <td style="font-size:12px" align="right"> {{ number_format($row->total_jual, 2, "." ,",") }} </td>
      </tr>
    @endforeach
    {{-- <tfoot> --}}
    <tr>
      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
      <td style="font-weight:bold" align="right">{{number_format($rekapTJPT, 2, "." ,",")}}</td>
    </tr>
    {{-- </tfoot> --}}
  </tbody>
</table>
