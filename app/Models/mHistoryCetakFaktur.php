<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mHistoryCetakFaktur extends Model
{
  //public $incrementing = false;
  protected $table = 'tb_history_cetak_faktur';
  protected $primaryKey = 'id';
  public $timestamps = false;
}
