<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Helpers\Main;

class ResetPasswordController extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->main = new Main();
        $this->title = '';
        $this->kodeLabel = '';
    }

    //Show form to user where they can reset password
    public function showResetForm(Request $request, $token = null)
    {
      $breadcrumb = [
          'Reset'=>''
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'Reset Password';
      $data['title'] = 'Reset Password';

      return view('email.reset', $data)->with(
        ['token' => $token, 'email' => $request->email]
      );
    }

    //returns Password broker of customers
    public function broker()
    {
      return Password::broker('customers');
    }

    //returns authentication guard of customers
    protected function guard()
    {
      return Auth::guard('web_customer');
    }
}
