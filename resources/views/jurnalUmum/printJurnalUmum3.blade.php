<html moznomarginboxes mozdisallowselectionprint>
  <head>
    <!-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> -->
     <title>Jurnal Umum - Graha Kita</title>
     <!-- <h2 style=”text-align:justify;”>   
            <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
        </h2> -->
  </head>
  <body>
  <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}


            </style>
    <div class="container-fluid">
        <!-- <h2 style=”text-align:justify;”>   
            <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
        </h2> -->
      <div class="row">
      <table>
        <thead>
            <tr>
                <th>
                    <h2 style=”text-align:justify;”>   
                        <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
                    </h2>
                </th>
            </tr>
        </thead>
        <tbody>
        <h1><center>Jurnal Umum</center></h1>
                @if($bulan=='1')
                    <h2><center>Periode Januari {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='2')
                    <h2><center>Periode Pebruari {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='3')
                    <h2><center>Periode Maret {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='4')
                    <h2><center>Periode April {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='5')
                    <h2><center>Periode Mei {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='6')
                    <h2><center>Periode Juni {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='7')
                    <h2><center>Periode Juli {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='8')
                    <h2><center>Periode Agustus {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='9')
                    <h2><center>Periode September {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='10')
                    <h2><center>Periode Oktober {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='11')
                    <h2><center>Periode November {{$tahun_periode}}</center></h2> 
                @endif
                @if($bulan=='12')
                    <h2><center>Periode Desember {{$tahun_periode}}</center></h2> 
                @endif
                <br>
                <table class="tg">
                <thead>
                    <tr class="">
                        <th class="tg-3wr7"><center> No </center></th>
                        <th class="tg-3wr7"><center> Tanggal </center></th>
                        <th class="tg-3wr7"><center> No Bukti </center></th>
                        <th class="tg-3wr7"><center> Keterangan </center></th>
                        <th class="tg-3wr7"><center> No Akun </center></th>
                        <th class="tg-3wr7"><center> Debet </center></th>
                        <th class="tg-3wr7"><center> Kredit </center></th>
                        <th class="tg-3wr7"><center> Catatan </center></th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach($jurnalUmum as $jmu)
                    <tr>
                        <td align="center" class="tg-rv4w"> {{ $no++ }}. </td>
                        <td class="tg-rv4w"> {{ date('d M Y', strtotime($jmu->jmu_date_insert)) }} </td>
                        <td class="tg-rv4w"><center> {{ $jmu->no_invoice }} </center></td>
                        <td class="tg-rv4w"> {{ $jmu->jmu_keterangan }}</td>
                        <td colspan="4" class="tg-rv4w"></td>
                    </tr>
                    @foreach($jmu->transaksi as $trs)
                    <tr>                                
                        <td class="tg-rv4w"></td>
                        <td class="tg-rv4w"></td>
                        <td class="tg-rv4w"></td>               
                        <td class="tg-rv4w">   {{ $trs->trs_nama_rekening }}</td>
                        <td class="tg-rv4w"><center>   {{ $trs->trs_kode_rekening }}</center></td>
                        <td align="right" class="tg-rv4w">   {{ number_format($trs->trs_debet) }} </td>
                        <td align="right" class="tg-rv4w">   {{ number_format($trs->trs_kredit) }} </td>
                        <td class="tg-rv4w">   {{ $trs->trs_catatan }} </td> 
                    </tr>
                    @endforeach
                @endforeach
                </tbody>
                <tfoot>
                    <tr class="">
                        <th width="10" class="tg-3wr7"></th>
                        <th colspan="4" align="center" class="tg-3wr7"><h4><center><strong> TOTAL </strong></center></h4></th>
                        <th class="tg-3wr7"> <h4><center><strong>{{number_format($jml_debet)}} </strong></center></h4></th>
                        <th class="tg-3wr7"> <h4><center><strong>{{number_format($jml_kredit)}} </th>
                        @if($jml_debet==$jml_kredit && $jml_debet>0 && $jml_kredit>0)
                        <th class="tg-3wr7">
                            <h4><strong>Status : <font color="green">Balance</font></strong></h4>
                        </th>
                        @endif
                        @if($jml_debet!=$jml_kredit)
                        <th class="tg-3wr7">
                            <h4><strong>Status : <font color="red">Not Balance</font></strong></h4>
                        </th>
                        @endif
                        @if($jml_debet==0 && $jml_kredit==0)
                        <th class="tg-3wr7">
                            <h4><strong>Status : <font color="red"></font></strong></h4>
                        </th>
                        @endif
                    </tr>
                </tfoot>
            </table>
        </tbody>
      </table>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
