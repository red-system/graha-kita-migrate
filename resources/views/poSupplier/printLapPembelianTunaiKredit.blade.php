<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>Laporan Pembelian Supplier</title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h3><center>Laporan Pembelian Tunai Kredit</center></h3>
                                        <h4><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h4> 
                    </div>
                    <br>
                    <div class="portlet light ">
                        <table class="tg">
                            <thead>
                                <tr class="success">
                                    <th width="10" style="font-size: 12px"> No </th>
                                                <th style="font-size:12px;" align="center"><center> Tanggal </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Jurnal </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Invoice </center></th>
                                                <th style="font-size:12px;" align="center"><center> Nama </center></th>
                                                <th style="font-size:12px;" align="center"><center> Harga Beli (DPP) </center></th>
                                                <th style="font-size:12px;" align="center"><center> Diskon </center></th>
                                                <th style="font-size:12px;" align="center"><center> PPN </center></th>
                                                <th style="font-size:12px;" align="center"><center> Cash </center></th>
                                                <th style="font-size:12px;" align="center"><center> Credit </center></th>
                                                <th style="font-size:12px;" align="center"><center> Tgl Jatuh Tempo </center></th>
                                </tr>
                            </thead>
                            <?php
                                            $total_cash = 0;
                                            $total_credit = 0;

                                            $total_cash_ppn = 0;
                                            $total_credit_ppn = 0;
                                            $total_cash_non_ppn = 0;
                                            $total_credit_non_ppn = 0;
                                            
                                        ?>
                            <tbody>
                                            @foreach($dataListPpn as $data)
                                            <tr>
                                                <td style="font-size:11px;" align="center">{{$no++}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($data->ps_tgl))}}</td>
                                                <td style="font-size:11px;">{{$data->no_pembelian}}</td>
                                                <td style="font-size:11px;">{{$data->no_invoice}}</td>
                                                <td style="font-size:11px;">{{$data->supplier->spl_nama}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->ps_subtotal)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->ps_disc_nom)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->ps_ppn_nom)}}</td>
                                            @if($data->tipe=='cash')
                                                <!-- <td style="font-size:11px;" align="right">{{number_format($data->ps_subtotal-$data->ps_disc_nom+$data->ps_ppn_nom)}}</td> -->
                                                <td style="font-size:11px;" align="right">{{number_format($data->grand_total)}}</td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;">-</td>
                                                <?php 
                                                    $total_cash += $data->grand_total;
                                                    $total_cash_ppn += $data->grand_total;                                                    
                                                ?>
                                            @endif
                                            @if($data->tipe=='credit')
                                                <td style="font-size:11px;" align="right">-</td>
                                                <!-- <td style="font-size:11px;">{{number_format($data->ps_subtotal-$data->ps_disc_nom+$data->ps_ppn_nom)}}</td> -->
                                                <td style="font-size:11px;" align="right">{{number_format($data->grand_total)}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($data->hutangSupplier->js_jatuh_tempo))}}</td>
                                                <?php 
                                                    $total_credit += $data->grand_total;
                                                    $total_credit_ppn += $data->grand_total;
                                                ?>
                                            @endif
                                                
                                            </tr> 
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListPpn->sum('ps_subtotal'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListPpn->sum('ps_disc_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListPpn->sum('ps_ppn_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_cash_ppn)}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_credit_ppn)}}</strong></td>
                                                <td></td>
                                            </tr> 
                                            @foreach($dataListNonPpn as $dataCredit)
                                            <tr>
                                                <td style="font-size:11px;" align="center">{{$no++}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($dataCredit->ps_tgl))}}</td>
                                                <td style="font-size:11px;">{{$dataCredit->no_pembelian}}</td>
                                                <td style="font-size:11px;">{{$dataCredit->no_invoice}}</td>
                                                <td style="font-size:11px;">{{$dataCredit->supplier->spl_nama}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->ps_subtotal)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->ps_disc_nom)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->ps_ppn_nom)}}</td>
                                            @if($dataCredit->tipe=='cash')
                                               
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->grand_total)}}</td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;">-</td>
                                                <?php 
                                                    $total_cash += $dataCredit->grand_total;
                                                    $total_cash_non_ppn += $dataCredit->grand_total;
                                                ?>
                                            @endif
                                            @if($dataCredit->tipe=='credit')
                                                <td style="font-size:11px;" align="right">-</td>
                                                
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->grand_total)}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($dataCredit->hutangSupplier->js_jatuh_tempo))}}</td>
                                                <?php 
                                                    $total_credit += $dataCredit->grand_total;
                                                    $total_credit_non_ppn += $dataCredit->grand_total;
                                                ?>
                                            @endif
                                                
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_subtotal'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_disc_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_ppn_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_cash_non_ppn)}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_credit_non_ppn)}}</strong></td>
                                                <td></td>
                                            </tr>                                                                                 
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Grand Total</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_subtotal')+$dataListPpn->sum('ps_subtotal'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_disc_nom')+$dataListPpn->sum('ps_disc_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_ppn_nom')+$dataListPpn->sum('ps_ppn_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_cash)}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_credit)}}</strong></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
