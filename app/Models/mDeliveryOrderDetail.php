<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDeliveryOrderDetail extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_delivery_order_detail';
  protected $primaryKey = 'do_det_kode';
  // public $timestamps = false;

  public function do()
  {
    return $this->belongsTo('App\Models\mDeliveryOrder', 'do_kode');
  }
}
