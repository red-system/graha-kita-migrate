<?php

namespace App\Exports;

use App\Models\mHutangSuplier;
use App\Models\mHutangLain;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class AllPenjualanKasirExport implements FromCollection
class LapHutangExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    // private $request;
    private $start;
    private $end;

    // public function __construct($request)
    // {
    //     $this->request = $request;
    // }

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      // $request = $this->request;
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeCustomer = $this->main->kodeLabel('supplier');
    // $kodeHutangSupplier = $this->main->kodeLabel('hutangSuplier');
    // $kodeHutangLain = $this->main->kodeLabel('hutangLain');
    // $date_start = $request->start_date;
    // $date_end = $request->end_date;
    // $start = date('Y-m-d', strtotime($start));
    // $end = date('Y-m-d', strtotime($end));

    $disc_HS=0;
    $angkut_HS=0;
    $total_HS=0;
    $rowHS = mHutangSuplier::leftJoin('tb_pembelian_supplier', 'tb_hutang_supplier.ps_no_faktur', '=', 'tb_pembelian_supplier.ps_no_faktur')
    ->leftJoin('tb_supplier', 'tb_pembelian_supplier.spl_id', '=', 'tb_supplier.spl_kode')
    ->where('ps_tgl', '>=', $start)->where('ps_tgl', '<=', $end)->get();

    foreach ($rowHS as $hutang) {
      $disc_HS += $hutang->ps_disc_nom;
      $angkut_HS += $hutang->biaya_lain;
      $total_HS += $hutang->ps_subtotal;
    }

    // $rowHS = mHutangSuplier::all();
    // foreach ($rowHS as &$hutang) {
    //   $hutang->suppliers;
    //   $hutang['pembelian'] = $hutang->pembelianSupplier;
    //   $pembelian = mPembelianSupplier::where('ps_no_faktur', $hutang->ps_no_faktur)->get();
    //   foreach ($pembelian as $key) {
    //     $disc_HS += $key->ps_disc_nom;
    //     $angkut_HS += $key->biaya_lain;
    //     $total_HS += $key->grand_total;
    //   }
    // }

    $total_HL=0;
    // $rowHL = mHutangLain::all();
    $rowHL = mHutangLain::where('hl_tgl', '>=', $start)->where('hl_tgl', '<=', $end)->get();
    foreach ($rowHL as &$hutang) {
      $total_HL += $hutang->hl_amount;
    }

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Hutang Supplier
    // $data['kode_HS'] = $kodeHutangSupplier;
    $data['dataHS'] = $rowHS;
    $data['DiscHS'] = $disc_HS;
    $data['AngkutHS'] = $angkut_HS;
    $data['TotalHS'] = $total_HS;

    //Data Hutang Lain
    // $data['kode_HL'] = $kodeHutangLain;
    $data['dataHL'] = $rowHL;
    $data['TotalHL'] = $total_HL;

      return view('export.lapHutang', $data);
    }
}
