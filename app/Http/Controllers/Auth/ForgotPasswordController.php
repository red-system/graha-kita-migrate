<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Helpers\Main;

class ForgotPasswordController extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->main = new Main();
        $this->title = '';
        $this->kodeLabel = '';
    }

    //Shows form to request password reset
    public function showLinkRequestForm()
    {
      $breadcrumb = [
          'Reset'=>''
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'Reset Password';
      $data['title'] = 'Reset Password';
      return view('email.pass_reset', $data);
    }

    //Password Broker for customers Model
    public function broker()
    {
      return Password::broker('customers');
    }
}
