<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4 >Laporan Detail Omset Sales</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <div class="portlet light ">
            @foreach($data as $row)
            <table class="table table-bordered table-hover table-header-fixed">
              <tr>
                <td colspan="8"><h5><b>Sales : {{$row->kry_nama}}</b></h5></td>
              </tr>
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Pelanggan</th>
                  <th style="font-size:12px">QTY Jual</th>
                  <th style="font-size:12px">Rp. Jual</th>
                  {{-- <th>QTY Retur</th> --}}
                  {{-- <th>Rp. Retur</th> --}}
                  {{-- <th>Total QTY</th> --}}
                  {{-- <th>Total Rp.</th> --}}
                  {{-- <th>Hari</th> --}}
                  <th style="font-size:12px">Terbayar</th>
                  <th style="font-size:12px">Sisa Bayar</th>
                  {{-- <th>jenis</th> --}}
                </tr>
              </thead>
                <tbody>
                  @foreach ($row->penjualan as $key)
                    <tr>
                      <td style="font-size:12px"> {{ $no++ }}. </td>
                      <td style="font-size:12px"> {{ date('Y-m-d', strtotime($key->pl_tgl)) }} </td>
                      <td style="font-size:12px"> {{ $kode.$key->pl_no_faktur }} </td>
                      <td style="font-size:12px"> {{ $key->customer['cus_nama'] }} </td>
                      <td style="font-size:12px"> {{ $key->qty_jual }} </td>
                      <td style="font-size:12px"> {{ number_format($key->total_jual, 0, "." ,".")  }} </td>
                      <td style="font-size:12px"> {{ number_format($key->terbayar, 0, "." ,".")  }} </td>
                      <td style="font-size:12px"> {{ number_format($key->pp_amount, 0, "." ,".")  }} </td>
                    </tr>
                  @endforeach
                  <tr>
                    <td colspan="5" align="right" style="font-weight:bold">Total Total Omset Sales : </td>
                    <td style="font-weight:bold">{{number_format($row->rekapTJ, 0, "." ,".") }}</td>
                    <td style="font-weight:bold">{{number_format($row->rekapTB, 0, "." ,".") }}</td>
                    {{-- <td style="font-weight:bold">{{number_format(0, 0, "." ,".") }}</td> --}}
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
