<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem as File;
use App\Helpers\Main;
use App\Models\mBarang;
use App\Models\mKategoryProduct;
use App\Models\mGroupStok;
use App\Models\mMerek;
use App\Models\mSatuan;
use App\Models\mSupplier;
use App\Models\mGudang;
use App\Models\mStok;
use App\Models\mArusStok;
use App\Models\mStokSample;
use App\Rules\BarangKodeUniq;

use Validator;

class BarangMix extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Buat Barcode Cat Oplosan';
      $this->kodeLabel = 'barang';
  }

  protected function createBarcode() {
    $preffix = 'C'.date('ymd');
    // $lastorder = mBarang::withTrashed()->where(\DB::raw('LEFT(brg_barcode, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('RIGHT(brg_barcode, 4) as no_order')
    // ->whereNotNull('combine_id')
    // ->whereNotIn('combine_id', [0])
    // ->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mBarang::withTrashed()->where('brg_barcode', 'like', '%'.$preffix.'%')
    ->whereNotNull('combine_id')
    ->whereNotIn('combine_id', [0])
    ->max('brg_barcode');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 7, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no;
  }

  public function verifikasiBarcode(Request $request) {
    $preffix = 'C'.date('ymd');
    // $lastorder = mBarang::withTrashed()->where(\DB::raw('LEFT(brg_barcode, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('RIGHT(brg_barcode, 4) as no_order')
    // ->whereNotNull('combine_id')
    // ->whereNotIn('combine_id', [0])
    // ->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mBarang::withTrashed()->where('brg_barcode', 'like', '%'.$preffix.'%')
    ->whereNotNull('combine_id')
    ->whereNotIn('combine_id', [0])
    ->max('brg_barcode');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 7, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    if ($request->barcode==$preffix.$no) {
      return $preffix.$no;
    }
    else {
      return $preffix.$no;
    }
  }

  public function index()
  {
    $breadcrumb = [
        'Data Barang & Harga'=>route('BarangMix.index'),
    ];

    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'buat_barcode';
    $data['title'] = $this->title;
    $data['barang'] = mBarang::with(['kategoryProduct:ktg_kode,ktg_nama', 'groupStok:grp_kode,grp_nama', 'merek:mrk_kode,mrk_nama', 'satuan:stn_kode,stn_nama'])->get();
    // $datalast = mBarang::select('brg_barcode')->whereNotNull('combine_id')->whereNotIn('combine_id', [0])->orderBy('brg_kode', 'desc')->first();
    // $str = $datalast->brg_barcode;
    // preg_match('!\d+!', $str, $matches);
    // $last = (int) $matches[0];
    // $last++;
    // $data['barcode'] = 'C'.$last;
    $data['barcode'] = $this->createBarcode();

    // return $data;
    return view('buat-barcode/buat-barcode', $data);
  }

  function rules($request) {
      $rules = [
          'barcode_new' => 'unique:tb_barang,brg_barcode',
          'nama_stok' => 'required',
          'id_mrk' => 'required',
          'id_ktg' => 'required',
          'id_stn' => 'required',
          'id_grp' => 'required',
          // 'brg_kode_combine' => 'required',
          'harga_jual_new'  => 'required',
          // 'brg_stok_minimum' => 'required',
          // 'brg_harga_beli_terakhir' => 'required',
          // 'brg_harga_beli_tertinggi' => 'required',
          // 'brg_hpp' => 'required',
          // 'brg_harga_jual_eceran' => 'required',
          // 'brg_harga_jual_partai' => 'required',
          // 'brg_ppn_dari_supplier_persen' => 'required',
          // 'brg_status' => ''
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  public function store(Request $request)
  {
    // return $request;

    \DB::beginTransaction();
    try {
      $this->rules($request->all());

      $barangStok = mStok::where('brg_kode', $request->brg_kode_combine)
      ->where('gdg_kode', $request->gdg_kode)->where('brg_no_seri', $request->brg_no_seri)->first();
      $barangStok->stok = ($barangStok->stok - $request->qty[0]);
      $barangStok->save();

      $barang = new mBarang();
      $barang->brg_kode                     = $request->barcode_new;
      $barang->brg_barcode                  = $request->barcode_new;
      $barang->brg_nama                     = $request->nama_stok;
      $barang->mrk_kode                     = $request->id_mrk;
      $barang->ktg_kode                     = $request->id_ktg;
      $barang->stn_kode                     = $request->id_stn;
      $barang->grp_kode                     = $request->id_grp;
      $barang->spl_kode                     = $barangStok->spl_kode;
      // $barang->brg_stok_maximum             = $request->brg_stok_maximum;
      // $barang->brg_stok_minimum             = $request->brg_stok_minimum;
      $barang->brg_stok_maximum             = 0;
      $barang->brg_stok_minimum             = 0;
      $barang->brg_harga_beli_terakhir      = $request->harga_jual_new;
      $barang->brg_harga_beli_tertinggi     = $request->harga_jual_new;
      $barang->brg_hpp                      = $request->brg_hpp[0];
      $barang->brg_harga_jual_eceran        = $request->harga_jual_new;
      $barang->brg_harga_jual_partai        = $request->harga_jual_new;
      $barang->brg_ppn_dari_supplier_persen = 0;
      // $barang->gdg_kode                     = $request->gdg_kode;
      $barang->combine_id                     = $request->brg_kode_combine;
      // if($request->hasfile('brg_product_img'))
      // {
      //   $file = $request->file('brg_product_img');
      //   $extension = $file->getClientOriginalExtension();
      //   $filename =time().'.'.$extension;
      //   $file->move('img/barang/', $filename);
      //   $path = 'img/barang/' . $filename;
      //   $barang->brg_product_img              = $path;
      // }
      // if ($request->brg_status == null) {
      //   $barang->brg_status                   = "Tidak Aktif";
      // }
      // else {
      //   $barang->brg_status                   = $request->brg_status;
      // }
      $barang->save();

      $total=0;
      $data = mStok::where('brg_kode', $request->brg_kode_combine)->get();
      foreach ($data as $gdg) {
        $total += $gdg->stok;
      }
      $QOH = $total;

      $Stok = new mStok();
      $Stok->brg_kode = $barang->brg_kode;
      $Stok->gdg_kode = $barangStok->gdg_kode;
      $Stok->spl_kode = $barangStok->spl_kode;
      $Stok->stok = $request->qty[0];
      $Stok->brg_no_seri = $request->brg_no_seri[0];
      $Stok->stk_hpp = 0;
      $Stok->save();

      $time = Carbon::now();
      $arusStok = new mArusStok();
      $arusStok->ars_stok_date = $time;
      $arusStok->brg_kode = $request->brg_kode_combine;
      $arusStok->stok_in = 0;
      $arusStok->stok_out = $request->qty[0];
      $arusStok->stok_prev = $QOH;
      $arusStok->gdg_kode = $barangStok->gdg_kode;
      $arusStok->keterangan = 'Barang Mix';
      $arusStok->save();

      $arusStok = new mArusStok();
      $arusStok->ars_stok_date = $time;
      $arusStok->brg_kode = $barang->brg_kode;
      $arusStok->stok_in = $request->qty[0];
      $arusStok->stok_out = 0;
      $arusStok->stok_prev = $request->qty[0];
      $arusStok->gdg_kode = $barangStok->gdg_kode;
      $arusStok->keterangan = 'Barang Mix';
      $arusStok->save();

      \DB::commit();
      return [
        'redirect'=>route('BarangMix.index')
      ];
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }
}
