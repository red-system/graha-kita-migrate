<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHutangSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_hutang_supplier', function (Blueprint $table) {
         $table->increments('hs_kode');
         $table->datetime('js_jatuh_tempo')->nullable();
         $table->integer('ps_no_faktur')->nullable();
         $table->integer('spl_kode')->nullable();
         $table->integer('hs_amount')->nullable();
         $table->text('hs_keterangan')->nullable();
         $table->string('hs_status')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_hutang_supplier');
     }
}
