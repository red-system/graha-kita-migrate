<?php

namespace App\Exports;

use App\Models\mDetailPenjualanTitipan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RekapPenjualanTitipanExport implements FromView
{
    private $start;
    private $end;

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $rowPT = mDetailPenjualanTitipan::select('pt_no_faktur', 'qty', 'harga_net', 'brg_kode', 'gudang', \DB::raw('SUM(qty) as brg_qty'))
      ->whereHas('PT_tgl', function($q) use($start, $end){
        $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
      })
      ->groupBy('brg_kode', 'gudang')->get();
      $total=0;
      $grand_total=0;
      foreach ($rowPT as &$key) {
        $key->barang->satuan;
        $key->gdg;
        $key->PT_tgl;
        $total = $key->harga_net*$key->brg_qty;
        $key['total'] = $total;
        $grand_total += $total;
      }

      $data['dataPT'] = $rowPT;
      $data['grand_total'] = $grand_total;

      return view('export.laporan-rekap-penjualan-titipan', $data);
    }
}
