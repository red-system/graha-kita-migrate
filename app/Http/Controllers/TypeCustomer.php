<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mTypeCustomer;

use Validator;

class TypeCustomer extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Type Customer';
      $this->kodeLabel = 'type_customer';
  }

  function index() {
      $breadcrumb = [
          'Type Customer'=>route('TypeCustomerList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'type_customer';
      $data['title'] = $this->title;
      $data['dataList'] = mTypeCustomer::all();
      return view('type-customer/TypeCustomerList', $data);
  }

  function rules($request) {
      $rules = [
          'type_cus_nama' => 'required'
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mTypeCustomer::insert($request->except('_token'));
      return [
          'redirect'=>route('TypeCustomerList')
      ];
  }

  function edit($type_cus_kode='') {
      $response = [
          'action'=>route('TypeCustomerUpdate', ['kode'=>$type_cus_kode]),
          'field'=>mTypeCustomer::find($type_cus_kode)
      ];
      return $response;
  }

  function update(Request $request, $type_cus_kode) {
      $this->rules($request->all());
      mTypeCustomer::where('type_cus_kode', $type_cus_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('TypeCustomerList')
      ];
  }

  function delete($type_cus_kode) {
      mTypeCustomer::where('type_cus_kode', $type_cus_kode)->delete();
  }
}
