<table>
  <thead>
    <tr>
      <th><center> No </center></th>
                                                <th><center> Supplier </center></th>
                                                <th><center> Kode Stock </center></th>
                                                <th><center> Nama Stock </center></th>
                                                <th><center> Merek </center></th>
                                                <th><center> Group </center></th>
                                                <th><center> Qty </center></th>
                                                <th><center> Stn </center></th>
                                                <th><center> Harga </center></th>
                                                <th><center> Total </center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($jml_pembelian > 0)
                                            @foreach($dataList as $data)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$data->supplier->spl_nama}}</td>
                                                <td>{{$data->brg_barcode}}</td>
                                                <td>{{$data->brg_nama}}</td>
                                                <td>{{$data->mrk_nama}}</td>
                                                <td>{{$data->grp_nama}}</td>
                                                <td>{{$data->qty}}</td>
                                                <td>{{$data->stn_nama}}</td>
                                                <td>{{number_format($data->harga_net)}}</td>
                                                <td>{{number_format($data->total)}}</td>
                                            </tr> 
                                            @endforeach  
                                            @endif                           
                                        </tbody>
                                        <tfoot>
                                            @if($jml_pembelian > 0)
                                            <tr>
                                                <td colspan="9">Grand Total</td>
                                                <td>{{number_format($dataList->sum('total'))}}</td>
                                            </tr>
                                            @endif                                            
                                        </tfoot>
                                    </table>
