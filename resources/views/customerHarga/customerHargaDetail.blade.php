@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
                <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                  <i class="fa fa-plus"></i> Tambah Harga Barang
                </button>
                <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th width="10"> No </th>
                    <th> Kode Barang </th>
                    <th> Nama </th>
                    <th> Satuan </th>
                    <th> Kategori </th>
                    <th> Group Stok </th>
                    <th> Merek </th>
                    <th> Harga Jual Retail </th>
                    <th> Harga Jual Partai </th>
                    <th> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td> {{ $no++ }}. </td>
                      <td> {{ $row->brg_kode }} </td>
                      <td> {{ $row->brg_nama }} </td>
                      <td> {{ $kodeSatuan.$row->stn_kode }} </td>
                      <td> {{ $kodeKategoryStok.$row->ktg_kode }} </td>
                      <td> {{ $kodeGroupStok.$row->grp_kode }} </td>
                      <td> {{ $kodeMerek.$row->mrk_kode }} </td>
                      <td> {{ $mainHelper->nominal($row->hrg_cus_harga_jual_eceran) }} </td>
                      <td> {{ $mainHelper->nominal($row->hrg_cus_harga_jual_partai) }} </td>
                      <td>
                        <div class="btn-group-vertical btn-group-xs">
                            <button class="btn btn-success btn-edit" data-href="{{ route('hargaCustomerEdit', ['kode'=>$row->hrg_cus_kode]) }}">
                                <span class="icon-pencil"></span> Edit
                              </button>
                              <button class="btn btn-danger btn-delete-new" data-href="{{ route('hargaCustomerDelete', ['kode'=>$row->hrg_cus_kode]) }}">
                                <span class="icon-trash"></span> Delete
                              </button>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-tambah" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Harga Customer
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{ route('hargaCustomerInsert') }}" class="form-horizontal form-send" role="form" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="cus_kode" value="{{ $cus_kode }} ">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Barang</label>
                <div class="col-md-9">
                  <select class="form-control select2" name="brg_kode">
                    @foreach($barangList as $r)
                    <option value="{{ $r->brg_kode }} ">{{ $kodeBarang.$r->brg_kode.' - '.$r->brg_nama }} </option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Harga Jual Retail</label>
                <div class="col-md-9">
                  <input type="number" class="form-control" name="hrg_cus_harga_jual_eceran" min="0" value="0">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Harga Jual Partai</label>
                <div class="col-md-9">
                  <input type="number" class="form-control" name="hrg_cus_harga_jual_partai" min="0" value="0">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal draggable-modal" id="modal-edit" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Customer
          </h4>
        </div>
        <div class="modal-body form">
            <form action=" " class="form-horizontal form-send" role="form" method="put">
                <input type="hidden" name="cus_kode" value="{{ $cus_kode }} ">
                {{ csrf_field() }}
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Kode Barang</label>
                    <div class="col-md-9">
                      <select class="form-control brg-kode select2" name="brg_kode">
                        @foreach($barangList as $r)
                        <option value="{{ $r->brg_kode }} ">{{ $kodeBarang.$r->brg_kode.' - '.$r->brg_nama }} </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Jual Retail</label>
                    <div class="col-md-9">
                      <input type="number" class="form-control" name="hrg_cus_harga_jual_eceran" min="0" value="0">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Jual Partai</label>
                    <div class="col-md-9">
                      <input type="number" class="form-control" name="hrg_cus_harga_jual_partai" min="0" value="0">
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn green">Perbarui</button>
                      <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </form>
        </div>
      </div>
    </div>
  </div>
@stop
