<html>
  <head>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Pembelian</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Supplier</th>
                  {{-- <th>Nama</th> --}}
                  <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Biaya Lain</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no++ }}. </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->ps_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $row->ps_no_faktur }} </td>
                    <td style="font-size:12px"> {{ $row->supplier['spl_nama'] }} </td>
                    {{-- <td> {{ $row->ps_sales_person }} </td> --}}
                    <td style="font-size:12px" align="right"> {{ number_format($row->ps_disc_nom, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->biaya_lain, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->grand_total, 2, "." ,",")  }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{number_format($Disc, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($Angkut, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($Total, 2, "." ,",") }}</td>
                  </tr>
              {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
