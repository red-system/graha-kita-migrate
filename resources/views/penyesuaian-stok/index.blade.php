@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <style media="screen">
  td.wrapok {
    white-space:nowrap
  }

  td.fontsize{
    font-size:10px
  }
  </style>
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('js/penyesuaianSC.js') }}" type="text/javascript"></script> --}}
  <script type="text/javascript">
  var l = window.location;
  var base_url = l.protocol + "//" + l.host + "/";
  $('#table_1').DataTable({
    destroy : true,
    processing: true,
    serverSide: true,
    ajax: base_url+'datatable/barang',
    columns: [
      {data: 'no', class:"fontsize"},
      {data: 'brg_kode', class:"wrapok fontsize"},
      {data: 'brg_barcode', class:"wrapok fontsize"},
      {data: 'brg_nama', class:"wrapok fontsize"},
      {data: 'stn_nama', class:"wrapok fontsize"},
      {data: 'ktg_nama', class:"wrapok fontsize"},
      {data: 'grp_nama', class:"wrapok fontsize"},
      {data: 'mrk_nama', class:"wrapok fontsize"},
      {data: 'QOH', class:"fontsize"},
      { data: null, orderable: false, searchable: false, render: function ( data, type, row ) {
        return '<div style="font-size:10px; white-space: nowrap" class="btn-group-xs"> <a class="btn btn-info" href="'+base_url+'penyesuaian-stok/'+data.brg_kode+'/detail"><span class="icon-pencil"></span> Stok</a></div>';
        }
      },
    ]
  });

  $('#table_2').DataTable({
    destroy : true,
    processing: true,
    serverSide: true,
    ajax: base_url+'penyesuaian-stok/history',
    columns: [
      {data: 'ps_kode', class:"wrapok fontsize"},
      {data: 'ps_tgl', class:"wrapok fontsize"},
      {data: 'brg_barcode', class:"wrapok fontsize"},
      {data: 'brg_nama', class:"wrapok fontsize"},
      {data: 'stn_nama', class:"wrapok fontsize"},
      {data: 'ktg_nama', class:"wrapok fontsize"},
      {data: 'grp_nama', class:"wrapok fontsize"},
      {data: 'mrk_nama', class:"wrapok fontsize"},
      {data: 'stok_awal', class:"wrapok fontsize"},
      {data: 'stok_akhir', class:"wrapok fontsize"},
      {data: 'keterangan', class:"wrapok fontsize"},
    ]
  });
  </script>
@stop

@section('body')
  <span id="data-back"
  data-form-token="{{ csrf_token() }}"></span>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Penyesuaian </a>
              </li>
              <li>
                <a href="#tab_1_2" data-toggle="tab"> History Penyesuaian </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active in" id="tab_1_1">
                <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_1">
                  <thead>
                    <tr class="">
                      <th>No. </th>
                      <th>Kode Barang</th>
                      <th>Barcode</th>
                      <th>Nama</th>
                      <th>Satuan</th>
                      <th>Kategori</th>
                      <th>Group</th>
                      <th>Merek</th>
                      <th>QOH</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>

              <div class="tab-pane" id="tab_1_2">
                <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_2">
                  <thead>
                    <tr class="">
                      <th>Kode</th>
                      <th>Tanggal</th>
                      <th>Barcode</th>
                      <th>Nama</th>
                      <th>Satuan</th>
                      <th>Kategori</th>
                      <th>Group</th>
                      <th>Merek</th>
                      <th>Stok Awal</th>
                      <th>Stok Akhir</th>
                      <th>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
