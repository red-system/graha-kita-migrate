<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use Carbon\Carbon;
use Meneses\LaravelMpdf\Facades\LaravelMpdf As MPDF;
use App\Models\mReturPenjualan;
use App\Models\mReturPenjualanDetail;
use App\Models\mPenjualanLangsung;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mKaryawan;
use App\Models\mStok;
use App\Models\mBarang;
use App\Models\mArusStok;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mPerkiraan;
use App\Models\mAcMaster;
use App\Models\mPiutangPelanggan;
use App\Models\mDeliveryOrder;
use App\Models\mDeliveryOrderDetail;
use App\Models\mCheque;

use Validator;

class ReturPenjualan extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main = new Main();
        $this->title = 'Retur Penjualan';
        $this->kodeLabel = 'retur-penjualan';
    }

    protected function createID() {
      $kode = $this->main->kodeLabel('retur-penjualan');
      $user = '.'.\Auth::user()->kode;
      $preffix = date('ymd').'.'.$kode.'.';
      // $lastorder = mReturPenjualan::where(\DB::raw('LEFT(no_retur_penjualan, '.strlen($preffix).')'), $preffix)
      // ->selectRaw('RIGHT(no_retur_penjualan, 4) as no_order')->orderBy('no_order', 'desc')->first();
      //
      // if (!empty($lastorder)) {
      //   $now = intval($lastorder->no_order)+1;
      //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
      // } else {
      //   $no = '0001';
      // }

      $lastorder = mReturPenjualan::where('no_retur_penjualan', 'like', '%'.$preffix.'%')->max('no_retur_penjualan');

      if ($lastorder != null) {
        $lastno = substr($lastorder, 11, 4);
        $now = $lastno+1;
        $no = str_pad($now, 4, '0', STR_PAD_LEFT);
      } else {
        $no = '0001';
      }

      return $preffix.$no.$user;
    }

    function index() {
        $breadcrumb = [
            'Retur Penjualan'=>route('ReturPenjualanList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'retur_penjualan';
        $data['title'] = $this->title;
        $data['dataList'] = mPenjualanLangsung::has('detail_PL')->orderBy('created_at', 'desc')->get();
        // $data['PL_label'] = $this->main->kodeLabel('penjualanLangsung');
        $data['dataList_PT'] = mPenjualanTitipan::has('detail_PT')->orderBy('created_at', 'desc')->get();
        // $data['PT_label'] = $this->main->kodeLabel('penjualanTitipan');

        return view('retur-penjualan/returList', $data);
    }

    function rules($request) {
        $rules = [
            // 'gdg_nama' => 'required',
            // 'gdg_alamat' => 'required',
            // 'gdg_keterangan' => '',
        ];
        $customeMessage = [
            'required'=>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
      // return $request;
        // $this->rules($request->all());
        \DB::beginTransaction();
        try {
          $time = Carbon::now();
          $ID = $this->createID();

          $brg_kode = $request->input('brg_kode');
          $brg_no_seri = $request->input('brg_no_seri');
          $brg_nama = $request->input('brg_nama');
          $harga_jual = $request->input('harga_net');
          $brg_hpp = $request->input('brg_hpp');
          $qty_retur = $request->input('qty_retur');
          $total_retur_detail = $request->input('total_retur_detail');
          $total_retur_hpp = $request->input('total_retur_hpp');
          $gudang = $request->input('gudang');
          $spl_kode = $request->input('spl_kode');
          $det_kode = $request->input('det_kode');
          $kembalian_uang = $request->input('kembalian_uang');

          if ($request->type_penjualan == 'langsung') {
            $DPL = mPenjualanLangsung::where('pl_no_faktur', $request->no_faktur)->first();
            $cus_kode = $DPL->cus_kode;
          }
          else {
            $DPT = mPenjualanTitipan::where('pt_no_faktur', $request->no_faktur)->first();
            $cus_kode = $DPT->cus_kode;
          }

          // Transaksi
          $master_id = $request->input('master_id');
          $charge = $request->input('charge');
          $no_check_bg = $request->input('no_check_bg');
          $tgl_pencairan = $request->input('tgl_pencairan');
          $setor = $request->input('setor');
          $jenis_transaksi = 'debet';
          $tipe_arus_kas = 'Operasi';
          $payment = $request->input('payment');
          $kredit_nominal = $request->input('payment_total');
          $catatan = $request->input('keterangan_payment');
          $subtotal = $request->input('subtotal');
          $total_retur = $request->input('total_retur');

          $total_bayar = $subtotal;
          // $total_bayar = 0;
          // foreach ($kredit_nominal as $key => $value) {
          //   $total_bayar += $value;
          // }
          $ppn_keluaran = 0;
          $ppn_keluaran = round($total_bayar*0.1);
          $retur_penjualan = round($total_bayar)-$ppn_keluaran;

          $returnPenjualan = new mReturPenjualan();
          $returnPenjualan->no_retur_penjualan = $ID;
          $returnPenjualan->tgl_pengembalian = $request->tgl_pengembalian;
          $returnPenjualan->pelaksana = $request->pelaksana;
          $returnPenjualan->alasan = $request->alasan;
          $returnPenjualan->keterangan = $request->keterangan;
          $returnPenjualan->ongkos_angkut = $request->ongkos_angkut;
          $returnPenjualan->potongan_penjualan = $request->potongan_penjualan;
          $returnPenjualan->potongan_piutang = $request->potongan_piutang;
          $returnPenjualan->total_retur = $request->total_retur;
          $returnPenjualan->total_hpp = $request->total_hpp;
          $returnPenjualan->no_faktur = $request->no_faktur;
          $returnPenjualan->total_payment = round($total_bayar);
          $returnPenjualan->ppn = $ppn_keluaran;
          $returnPenjualan->retur_penjualan = $retur_penjualan;

          $penjualan_cash=0;
          $penjualan_transfer=0;
          $penjualan_cek=0;
          $penjualan_edc=0;
          $penjualan_piutang=0;
          $penjualan_pembulatan=0;
          $bayar=0;
          foreach($master_id as $key=>$val) {
            $master = mPerkiraan::find($master_id[$key]);
            $trs_kode_rekening = $master->mst_kode_rekening;
            $trs_nama_rekening = $master->mst_nama_rekening;

            if ($trs_kode_rekening == '1101') {
              $bayar += $kredit_nominal[$key];
              $penjualan_cash += $payment[$key];
            }
            elseif ($trs_kode_rekening == '1301') {
              $penjualan_piutang += $payment[$key];
            }
            elseif ($trs_kode_rekening == '1302') {
              $penjualan_edc += $payment[$key];
            }
            elseif ($trs_kode_rekening == '1303') {
              $penjualan_cek += $payment[$key];
            }
            elseif ($trs_kode_rekening == '1305') {
              $penjualan_transfer += $payment[$key];
            }
            elseif ($trs_kode_rekening == '519006') {
              $penjualan_pembulatan += $payment[$key];
            }

            $returnPenjualan->bayar = $bayar;
            $returnPenjualan->cash = $penjualan_cash;
            $returnPenjualan->piutang = $penjualan_piutang;
            $returnPenjualan->edc = $penjualan_edc;
            $returnPenjualan->cek_bg = $penjualan_cek;
            $returnPenjualan->transfer = $penjualan_transfer;
            $returnPenjualan->pembulatan = $penjualan_pembulatan;
          }
          $returnPenjualan->kembalian_uang = $kembalian_uang;
          $returnPenjualan->save();

          foreach ($brg_kode as $key => $value) {
            $sisa_terkirim = 0;
            // $brg_kodex = mBarang::select('brg_barcode')->where('brg_kode', $brg_kode[$key])->first();
            // $brg_barcode = $brg_kodex['brg_barcode'];

            //Stok
            if ($qty_retur[$key] != 0) {
              //Add QTY retur di Detail Penjulan
              if ($request->type_penjualan == 'langsung') {
                $PL = mDetailPenjualanLangsung::where('detail_pl_kode', $det_kode[$key])->first();
                $PL->retur = $PL->retur + $qty_retur[$key];
                $PL->save();
              }
              else {
                $PT = mDetailPenjualanTitipan::where('detail_pt_kode', $det_kode[$key])->first();
                $PT->retur = $PT->retur + $qty_retur[$key];
                $PT->save();
                // $do = mDeliveryOrder::where('no_faktur', $PT->pt_no_faktur)->doesntExist();
                // if ($do) {
                // }
              }

              //Add Stok di tb_stok
              if ($request->type_penjualan == 'langsung') {
                if ($request->alasan == 'Kembali') {
                  $stok = mStok::where('gdg_kode', $gudang[$key])
                  ->where('brg_kode', $brg_kode[$key])
                  ->where('brg_no_seri', $brg_no_seri[$key])
                  ->first();

                  if ($stok) {
                    $stok->stok = ($stok->stok + $qty_retur[$key]);
                    $stok->save();
                  }
                  else {
                    $stok = new mStok();
                    $stok->brg_kode = $brg_kode[$key];
                    $stok->gdg_kode = $gudang[$key];
                    $stok->spl_kode = $spl_kode[$key];
                    $stok->stok = $qty_retur[$key];
                    $stok->brg_no_seri = $brg_no_seri[$key];
                    $stok->stk_hpp = 0;
                    $stok->save();
                  }
                }
                else {
                  $stok = mStok::where('gdg_kode', $gudang[$key])
                  ->where('brg_kode', $brg_kode[$key])
                  ->where('brg_no_seri', 'Reject')
                  ->first();

                  if ($stok) {
                    $stok->stok = ($stok->stok + $qty_retur[$key]);
                    $stok->save();
                  }
                  else {
                    $stok = new mStok();
                    $stok->brg_kode = $brg_kode[$key];
                    $stok->gdg_kode = $gudang[$key];
                    $stok->spl_kode = $spl_kode[$key];
                    $stok->stok = $qty_retur[$key];
                    $stok->brg_no_seri = 'Reject';
                    $stok->stk_hpp = 0;
                    $stok->save();
                  }
                }
                //Arus Stok
                $total=0;
                $data = mStok::where('brg_kode', $brg_kode[$key])->get();
                foreach ($data as $gdg) {
                  $total += $gdg->stok;
                }
                $QOH = $total;

                $arusStok = new mArusStok();
                $arusStok->ars_stok_date = $time;
                $arusStok->brg_kode = $brg_kode[$key];
                $arusStok->stok_in = $qty_retur[$key];
                $arusStok->stok_out = 0;
                $arusStok->stok_prev = $QOH;
                $arusStok->gdg_kode = $gudang[$key];
                $arusStok->keterangan = 'Retur Stok '.$ID;
                $arusStok->save();
              }
              else {
                $sisa_terkirim = ($request->terkirim[$key] - $request->retur[$key]);
                // return $sisa_terkirim;
                if ($request->alasan == 'Kembali') {
                  $stok = mStok::where('gdg_kode', $gudang[$key])
                  ->where('brg_kode', $brg_kode[$key])
                  ->where('brg_no_seri', $brg_no_seri[$key])
                  ->first();

                  if ($stok) {
                    if ($sisa_terkirim <= $qty_retur[$key] && $sisa_terkirim > 0) {
                      $stok->stok = ($stok->stok + $sisa_terkirim);
                      $stok->stok_titipan = $stok->stok_titipan - ($qty_retur[$key] - $sisa_terkirim);
                    }
                    elseif ($sisa_terkirim > 0) {
                      $stok->stok = ($stok->stok + $qty_retur[$key]);
                    }
                    else {
                      $stok->stok_titipan = $stok->stok_titipan - ($qty_retur[$key]);
                    }
                    $stok->save();
                  }
                  else {
                    $stok = new mStok();
                    $stok->brg_kode = $brg_kode[$key];
                    $stok->gdg_kode = $gudang[$key];
                    $stok->spl_kode = $spl_kode[$key];
                    if ($sisa_terkirim <= $qty_retur[$key] && $sisa_terkirim > 0) {
                      $stok->stok = $sisa_terkirim;
                    }
                    elseif ($sisa_terkirim > 0) {
                      $stok->stok = $qty_retur[$key];
                    }
                    else {
                      $stok->stok = 0;
                    }
                    // $stok->stok = $det_do->dikirim;
                    $stok->brg_no_seri = $brg_no_seri[$key];
                    $stok->stk_hpp = 0;
                    $stok->save();
                  }

                  //Arus Stok
                  $total=0;
                  $data = mStok::where('brg_kode', $brg_kode[$key])->get();
                  foreach ($data as $gdg) {
                    $total += $gdg->stok;
                  }
                  $QOH = $total;

                  if ($sisa_terkirim <= $qty_retur[$key] && $sisa_terkirim > 0) {
                    $arusStok = new mArusStok();
                    $arusStok->ars_stok_date = $time;
                    $arusStok->brg_kode = $brg_kode[$key];
                    $arusStok->stok_in = $sisa_terkirim;
                    $arusStok->stok_out = 0;
                    $arusStok->stok_prev = $QOH;
                    $arusStok->gdg_kode = $gudang[$key];
                    $arusStok->keterangan = 'Retur Stok '.$ID;
                    $arusStok->save();
                  }
                  elseif ($sisa_terkirim > 0) {
                    $arusStok = new mArusStok();
                    $arusStok->ars_stok_date = $time;
                    $arusStok->brg_kode = $brg_kode[$key];
                    $arusStok->stok_in = $qty_retur[$key];
                    $arusStok->stok_out = 0;
                    $arusStok->stok_prev = $QOH;
                    $arusStok->gdg_kode = $gudang[$key];
                    $arusStok->keterangan = 'Retur Stok '.$ID;
                    $arusStok->save();
                  }
                  else {
                    // $arusStok->stok_in = 0;
                  }
                }
                else {
                  $stok = mStok::where('gdg_kode', $gudang[$key])
                  ->where('brg_kode', $brg_kode[$key])
                  ->where('brg_no_seri', 'Reject')
                  ->first();

                  if ($stok) {
                    if ($sisa_terkirim <= $qty_retur[$key] && $sisa_terkirim > 0) {
                      $stok->stok = ($stok->stok + $sisa_terkirim);
                      $stok->stok_titipan = $stok->stok_titipan - ($qty_retur[$key] - $sisa_terkirim);
                    }
                    elseif ($sisa_terkirim > 0) {
                      $stok->stok = ($stok->stok + $qty_retur[$key]);
                    }
                    else {
                      $stok->stok_titipan = $stok->stok_titipan - ($qty_retur[$key]);
                    }
                    $stok->save();
                  }
                  else {
                    $stok = new mStok();
                    $stok->brg_kode = $brg_kode[$key];
                    $stok->gdg_kode = $gudang[$key];
                    $stok->spl_kode = $spl_kode[$key];
                    if ($sisa_terkirim <= $qty_retur[$key] && $sisa_terkirim > 0) {
                      $stok->stok = $sisa_terkirim;
                    }
                    elseif ($sisa_terkirim > 0) {
                      $stok->stok = $qty_retur[$key];
                    }
                    else {
                      $stok->stok = 0;
                    }
                    // $stok->stok = $det_do->dikirim;
                    $stok->brg_no_seri = 'Reject';
                    $stok->stk_hpp = 0;
                    $stok->save();
                  }

                  //Arus Stok
                  $total=0;
                  $data = mStok::where('brg_kode', $brg_kode[$key])->get();
                  foreach ($data as $gdg) {
                    $total += $gdg->stok;
                  }
                  $QOH = $total;

                  if ($sisa_terkirim <= $qty_retur[$key] && $sisa_terkirim > 0) {
                    $arusStok = new mArusStok();
                    $arusStok->ars_stok_date = $time;
                    $arusStok->brg_kode = $brg_kode[$key];
                    $arusStok->stok_in = $sisa_terkirim;
                    $arusStok->stok_out = 0;
                    $arusStok->stok_prev = $QOH;
                    $arusStok->gdg_kode = $gudang[$key];
                    $arusStok->keterangan = 'Retur Stok '.$ID;
                    $arusStok->save();
                  }
                  elseif ($sisa_terkirim > 0) {
                    $arusStok = new mArusStok();
                    $arusStok->ars_stok_date = $time;
                    $arusStok->brg_kode = $brg_kode[$key];
                    $arusStok->stok_in = $qty_retur[$key];
                    $arusStok->stok_out = 0;
                    $arusStok->stok_prev = $QOH;
                    $arusStok->gdg_kode = $gudang[$key];
                    $arusStok->keterangan = 'Retur Stok '.$ID;
                    $arusStok->save();
                  }
                  else {
                    // $arusStok->stok_in = $qty_retur[$key];
                  }
                }
              }

              //Detail
              $returnPenjualanDetail = new mReturPenjualanDetail();
              $returnPenjualanDetail->brg_kode = $brg_kode[$key];
              $returnPenjualanDetail->brg_nama = $brg_nama[$key];
              $returnPenjualanDetail->harga_jual = $harga_jual[$key];
              $returnPenjualanDetail->brg_hpp = $brg_hpp[$key];
              $returnPenjualanDetail->qty_retur = $qty_retur[$key];
              $returnPenjualanDetail->total_retur = $total_retur_detail[$key];
              $returnPenjualanDetail->total_hpp = $total_retur_hpp[$key];
              $returnPenjualanDetail->no_retur_penjualan = $returnPenjualan->no_retur_penjualan;
              $returnPenjualanDetail->stk_kode = $stok->stk_kode;
              $returnPenjualanDetail->save();
            }
          }

          // General
          $tipe_arus_kas = 'Operasi';
          $kode_bukti_id = $ID;
          $date_db = date('Y-m-d H:i:s');
          $year = date('Y');
          $month = date('m');
          $day = date('d');
          $where = [
              'jmu_year'=>$year,
              'jmu_month'=>$month,
              'jmu_day'=>$day
          ];
          $jmu_no = mJurnalUmum::where($where)
                                      ->orderBy('jmu_no','DESC')
                                      ->select('jmu_no')
                                      ->limit(1);
          if($jmu_no->count() == 0) {
              $jmu_no = 1;
          } else {
              $jmu_no = $jmu_no->first()->jmu_no + 1;
          }

          $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
              ->limit(1)
              ->first();
          $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

          $data_jurnal = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'no_invoice'=>$kode_bukti_id,
              'id_pel'=>'CUS'.$cus_kode,
              'jmu_no'=>$jmu_no,
              'jmu_tanggal'=>$request->tgl_pengembalian,
              'jmu_year'=>$year,
              'jmu_month'=>$month,
              'jmu_day'=>$day,
              'jmu_keterangan'=>'Retur Penjualan '.$kode_bukti_id,
              'jmu_date_insert'=>$date_db,
              'jmu_date_update'=>$date_db,
          ];
          mJurnalUmum::insert($data_jurnal);

          $total_hutang_penjualan = 0;
          foreach($master_id as $key=>$val) {
              $master = mPerkiraan::find($master_id[$key]);
              $trs_kode_rekening = $master->mst_kode_rekening;
              $trs_nama_rekening = $master->mst_nama_rekening;

              if ($trs_kode_rekening == '1101' || $trs_kode_rekening == '1102') {

              }
              elseif ($trs_kode_rekening == '1303') {
                $piutang = mCheque::where('no_invoice', $request->no_faktur)->first();
                if ($piutang != null) {
                  $sisa = ($piutang->sisa - $kredit_nominal[$key]);
                  if ($sisa <= 0) {
                    $piutang->sisa = 0;
                  }
                  else {
                    $piutang->sisa = $sisa;
                  }
                  $piutang->save();
                }
              }
              else {
                $piutang = mPiutangPelanggan::where('pp_no_faktur', $request->no_faktur)->first();
                if ($piutang != null) {
                  $sisa = ($piutang->pp_sisa_amount - $kredit_nominal[$key]);
                  if ($sisa <= 0) {
                    $piutang->pp_sisa_amount = 0;
                    $piutang->pp_status = 'Lunas';
                  }
                  else {
                    $piutang->pp_sisa_amount = $sisa;
                  }
                  $piutang->save();
                }
              }

              $data_transaksi = [
                'jurnal_umum_id'=>$jurnal_umum_id,
                'master_id'=>$master_id[$key],
                'trs_jenis_transaksi'=>'kredit',
                'trs_debet'=>0,
                'trs_kredit'=>$kredit_nominal[$key],
                'user_id'=>0,
                'trs_year'=>$year,
                'trs_month'=>$month,
                'trs_kode_rekening'=>$trs_kode_rekening,
                'trs_nama_rekening'=>$trs_nama_rekening,
                'trs_tipe_arus_kas'=>$tipe_arus_kas,
                'trs_catatan'=>$catatan[$key],
                'trs_date_insert'=>$date_db,
                'trs_date_update'=>$date_db,
                'tgl_transaksi'=>$request->tgl_pengembalian,
              ];
              mTransaksi::insert($data_transaksi);

              $total_hutang_penjualan += $kredit_nominal[$key];
          }

          if ($request->type_penjualan == 'langsung') {
            $master_id_kredit = ["1601", "2205", "41101", "311012", "311013"];
            foreach($master_id_kredit as $key=>$val) {
              $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
              $master_idK = $masterK->master_id;
              $trs_kode_rekening = $masterK->mst_kode_rekening;
              $trs_nama_rekening = $masterK->mst_nama_rekening;
              if ($trs_kode_rekening == "1601") {
                $debet_nom = $request->total_hpp;
                $kredit_nom = 0;
                $trs_jenis_transaksi = 'debet';
                $trs_catatan = 'debet';
              }
              elseif ($trs_kode_rekening == "2205") {
                $debet_nom = $ppn_keluaran;
                $kredit_nom = 0;
                $trs_jenis_transaksi = 'debet';
                $trs_catatan = 'debet';
              }
              elseif ($trs_kode_rekening == "41101") {
                $debet_nom = 0;
                $kredit_nom = $request->total_hpp;
                $trs_jenis_transaksi = 'kredit';
                $trs_catatan = 'kredit';
              }
              elseif ($trs_kode_rekening == "311012") {
                $debet_nom = $retur_penjualan;
                $kredit_nom = 0;
                $trs_jenis_transaksi = 'debet';
                $trs_catatan = 'debet';
              }
              elseif ($trs_kode_rekening == "311013") {
                $debet_nom = 0;
                $kredit_nom = round($request->potongan_penjualan);
                $trs_jenis_transaksi = 'kredit';
                $trs_catatan = 'kredit';
              }
              else {
                $debet_nom = 0;
                $kredit_nom = 0;
                $trs_jenis_transaksi = 'kredit';
                $trs_catatan = 'kredit';
              }
              $data_transaksiK = [
                'jurnal_umum_id'=>$jurnal_umum_id,
                'master_id'=>$master_idK,
                'trs_jenis_transaksi'=>$trs_jenis_transaksi,
                'trs_debet'=>$debet_nom,
                'trs_kredit'=>$kredit_nom,
                'user_id'=>0,
                'trs_year'=>$year,
                'trs_month'=>$month,
                'trs_kode_rekening'=>$trs_kode_rekening,
                'trs_nama_rekening'=>$trs_nama_rekening,
                'trs_tipe_arus_kas'=>$tipe_arus_kas,
                'trs_catatan'=>$trs_catatan,
                'trs_date_insert'=>$date_db,
                'trs_date_update'=>$date_db,
                'tgl_transaksi'=>$request->tgl_pengembalian,
                ];
                mTransaksi::insert($data_transaksiK);
              }
          }
          else {
            //Belum Ada DO/SJ
            $do_transaksi = mDeliveryOrder::where('no_faktur', $request->no_faktur)->doesntExist();
            if ($do_transaksi) {
              $master_id_kredit = ["2102"];
              foreach($master_id_kredit as $key=>$val) {
                  $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
                  $master_idK = $masterK->master_id;
                  $trs_kode_rekening = $masterK->mst_kode_rekening;
                  $trs_nama_rekening = $masterK->mst_nama_rekening;
                  $debet_nom = $total_hutang_penjualan;
                  $kredit_nom = 0;
                  $trs_jenis_transaksi = 'debet';
                  $trs_catatan = 'debet';

                  $data_transaksiK = [
                      'jurnal_umum_id'=>$jurnal_umum_id,
                      'master_id'=>$master_idK,
                      'trs_jenis_transaksi'=>$trs_jenis_transaksi,
                      'trs_debet'=>$debet_nom,
                      'trs_kredit'=>$kredit_nom,
                      'user_id'=>0,
                      'trs_year'=>$year,
                      'trs_month'=>$month,
                      'trs_kode_rekening'=>$trs_kode_rekening,
                      'trs_nama_rekening'=>$trs_nama_rekening,
                      'trs_tipe_arus_kas'=>$tipe_arus_kas,
                      'trs_catatan'=>$trs_catatan,
                      'trs_date_insert'=>$date_db,
                      'trs_date_update'=>$date_db,
                      // 'trs_charge'=>0,
                      // 'trs_no_check_bg'=>0,
                      // 'trs_tgl_pencairan'=>'',
                      // 'trs_setor'=>0,
                      'tgl_transaksi'=>$request->tgl_pengembalian,
                  ];
                  mTransaksi::insert($data_transaksiK);
              }
            }
            else {
              $master_id_kredit = ["1601", "2205", "41101", "311012", "311013"];
              foreach($master_id_kredit as $key=>$val) {
                $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
                $master_idK = $masterK->master_id;
                $trs_kode_rekening = $masterK->mst_kode_rekening;
                $trs_nama_rekening = $masterK->mst_nama_rekening;
                if ($trs_kode_rekening == "1601") {
                  $debet_nom = $request->total_hpp;
                  $kredit_nom = 0;
                  $trs_jenis_transaksi = 'debet';
                  $trs_catatan = 'debet';
                }
                elseif ($trs_kode_rekening == "2205") {
                  $debet_nom = $ppn_keluaran;
                  $kredit_nom = 0;
                  $trs_jenis_transaksi = 'debet';
                  $trs_catatan = 'debet';
                }
                elseif ($trs_kode_rekening == "41101") {
                  $debet_nom = 0;
                  $kredit_nom = $request->total_hpp;
                  $trs_jenis_transaksi = 'kredit';
                  $trs_catatan = 'kredit';
                }
                elseif ($trs_kode_rekening == "311012") {
                  $debet_nom = $retur_penjualan;
                  $kredit_nom = 0;
                  $trs_jenis_transaksi = 'debet';
                  $trs_catatan = 'debet';
                }
                elseif ($trs_kode_rekening == "311013") {
                  $debet_nom = 0;
                  $kredit_nom = round($request->potongan_penjualan);
                  $trs_jenis_transaksi = 'kredit';
                  $trs_catatan = 'kredit';
                }
                else {
                  $debet_nom = 0;
                  $kredit_nom = 0;
                  $trs_jenis_transaksi = 'kredit';
                  $trs_catatan = 'kredit';
                }
                $data_transaksiK = [
                  'jurnal_umum_id'=>$jurnal_umum_id,
                  'master_id'=>$master_idK,
                  'trs_jenis_transaksi'=>$trs_jenis_transaksi,
                  'trs_debet'=>$debet_nom,
                  'trs_kredit'=>$kredit_nom,
                  'user_id'=>0,
                  'trs_year'=>$year,
                  'trs_month'=>$month,
                  'trs_kode_rekening'=>$trs_kode_rekening,
                  'trs_nama_rekening'=>$trs_nama_rekening,
                  'trs_tipe_arus_kas'=>$tipe_arus_kas,
                  'trs_catatan'=>$trs_catatan,
                  'trs_date_insert'=>$date_db,
                  'trs_date_update'=>$date_db,
                  'tgl_transaksi'=>$request->tgl_pengembalian,
                  ];
                  mTransaksi::insert($data_transaksiK);
                }
            }
          }

          if ($request->type_penjualan == 'langsung') {
            $type_retur = 'langsung';
          }
          else {
            $type_retur = 'titipan';
          }

          // if ($request->type_penjualan == 'langsung') {
          //   $type_retur = 'langsung';
          //   $penjualanL = mPenjualanLangsung::where('pl_no_faktur', $request->no_faktur)->first();
          //   $penjualanL->grand_total = ($penjualanL->grand_total - $request->total_retur);
          //   $penjualanL->save();
          //
          //   $detailPenjualan = mDetailPenjualanLangsung::where('pl_no_faktur',$request->no_faktur)->get();
          //   foreach ($detailPenjualan as $key => $value) {
          //     $detail = mDetailPenjualanLangsung::where('detail_pl_kode', $value->detail_pl_kode)->first();
          //     $stok = mStok::where('gdg_kode', $gudang[$key])->where('brg_kode', $brg_kode[$key])->where('brg_no_seri', $detail->brg_no_seri)->first();
          //     $stok->stok = ($stok->stok + $qty_retur[$key]);
          //     $stok->save();
          //     $hasil = ($detail->qty - $qty_retur[$key]);
          //     if ($hasil <= 0) {
          //       mDetailPenjualanLangsung::where('detail_pl_kode', $value->detail_pl_kode)->delete();
          //     }
          //     else {
          //       $detail->qty = $hasil;
          //       $detail->save();
          //     }
          //   }
          // }
          // else {
          //   $type_retur = 'titipan';
          //   $penjualanT = mPenjualanTitipan::where('pt_no_faktur', $request->no_faktur)->first();
          //   $penjualanT->grand_total = ($penjualanT->grand_total - $request->total_retur);
          //   $penjualanT->save();
          //
          //   $detailPenjualan = mDetailPenjualanTitipan::where('pt_no_faktur',$request->no_faktur)->get();
          //   foreach ($detailPenjualan as $key => $value) {
          //     $detail = mDetailPenjualanTitipan::where('detail_pt_kode', $value->detail_pt_kode)->first();
          //     $stok = mStok::where('gdg_kode', $gudang[$key])->where('brg_kode', $brg_kode[$key])->where('brg_no_seri', $detail->brg_no_seri)->first();
          //     $stok->stok = ($stok->stok + $qty_retur[$key]);
          //     $stok->save();
          //     $hasil = ($detail->qty - $qty_retur[$key]);
          //     if ($hasil <= 0) {
          //       mDetailPenjualanTitipan::where('detail_pt_kode', $value->detail_pt_kode)->delete();
          //     }
          //     else {
          //       $detail->qty = $hasil;
          //       $detail->save();
          //     }
          //   }
          // }

          \DB::commit();

          return [
              'redirect'=>route('ReturPenjualanList'),
              'faktur' => $returnPenjualan->no_retur_penjualan,
              'type' => $type_retur
          ];
        } catch (\Exception $e) {
          \DB::rollBack();
          throw $e;
        }
    }

    function edit($no_retur_penjualan='' , $type='') {
      // return $no_retur_penjualan;

      $breadcrumb = [
          'Retur Penjualan'=>route('ReturPenjualanList'),
      ];
      if ($type == 'langsung') {
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'retur_penjualan';
        $data['title'] = $this->title;
        $data['dataList'] = mDetailPenjualanLangsung::where('pl_no_faktur',$no_retur_penjualan)->get();
        $data['PL_label'] = $this->main->kodeLabel('penjualanLangsung');
        $data['barang_label'] = $this->main->kodeLabel('barang');
        $PLData = mPenjualanLangsung::where('pl_no_faktur',$no_retur_penjualan)->first();
        $data['data_penjualan'] = $PLData;
        $data['no_faktur'] = $no_retur_penjualan;
        $data['tgl_faktur'] = date('Y-m-d', strtotime($PLData->pl_tgl));
        $data['jenis_transaksi'] = $PLData->pl_transaksi;
        $data['type_penjualan'] = $type;
        $data['karyawan'] = mKaryawan::orderBy('kry_nama', 'asc')->get();
      }
      else {
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'retur_penjualan';
        $data['title'] = $this->title;
        $data['dataList'] = mDetailPenjualanTitipan::where('pt_no_faktur',$no_retur_penjualan)->get();
        $data['PT_label'] = $this->main->kodeLabel('penjualanTitipan');
        $data['barang_label'] = $this->main->kodeLabel('barang');
        $PTData = mPenjualanTitipan::where('pt_no_faktur',$no_retur_penjualan)->first();
        $data['data_penjualan'] = $PTData;
        $data['no_faktur'] = $no_retur_penjualan;
        $data['tgl_faktur'] = date('Y-m-d', strtotime($PTData->pt_tgl));
        $data['jenis_transaksi'] = $PTData->pt_transaksi;
        $data['type_penjualan'] = $type;
        $data['karyawan'] = mKaryawan::orderBy('kry_nama', 'asc')->get();
      }

      $data['perkiraan'] = mAcMaster::whereIn('mst_kode_rekening', [1101, 1301, 1302, 1303, 1305, 519006])->get();

      return view('retur-penjualan/returDetailList', $data);

        // $response = [
        //     'action'=>route('ReturPenjualanUpdate', ['kode'=>$no_retur_penjualan]),
        //     'field'=>mReturPenjualan::find($no_retur_penjualan)
        // ];
        // return $response;
    }

    function update(Request $request, $no_retur_penjualan) {
        $this->rules($request->all());
        mReturPenjualan::where('no_retur_penjualan', $no_retur_penjualan)->update($request->except('_token'));
        return [
            'redirect'=>route('ReturPenjualanList')
        ];
    }

    function delete($no_retur_penjualan) {
        mReturPenjualan::where('no_retur_penjualan', $no_retur_penjualan)->delete();
    }

    function ReturnPenjualanPrint($id='', $type='') {
      $faktur = mReturPenjualan::where('no_retur_penjualan', $id)->first();
      // $faktur['penjualan']= mPenjualanLangsung::where('pl_no_faktur', $faktur->pl_no_faktur)->first();
      if ($type=='langsung') {
        $faktur->penjualanL;
        $faktur['kode_bukti_id'] = $faktur->penjualanL->pl_no_faktur;
        $faktur['tglFaktur'] = date('d/m/Y', strtotime($faktur->penjualanL->pl_tgl));
      }
      else {
        $faktur->penjualanT;
        $faktur['kode_bukti_id'] = $faktur->penjualanT->pt_no_faktur;
        $faktur['tglFaktur'] = date('d/m/Y', strtotime($faktur->penjualanT->pt_tgl));
      }
      $faktur->detail;
      $faktur['retur_id'] = $faktur->no_retur_penjualan;
      $faktur['tglRetur'] = date('d/m/Y', strtotime($faktur->tgl_pengembalian));
      $faktur['no'] = 1;
      $faktur['type'] = $type;

      // return $faktur;
      return view('retur-penjualan.ReturnPenjualanPrint', compact('faktur'));

      // return MPDF::loadView('retur-penjualan.ReturnPenjualanPrint', ['faktur' => $faktur], [], [
      //   'title' => 'Return Penjualan',
      //   'format' => 'A5-L'
      // ])
      // ->stream('retur-penjualan.ReturnPenjualanPrint.pdf')
      // ->getMpdf()->SetDisplayMode('fullpage','continuous');
    }
}
