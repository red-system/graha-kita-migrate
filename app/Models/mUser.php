<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class mUser extends Authenticatable
{
    use HasApiTokens, Notifiable;

    public $incrementing = false;
    protected $table = 'tb_user';
    protected $primaryKey = 'user_kode';
    public $timestamps = false;
    // protected $fillable = [];
    protected $fillable = [
      'username', 'password', 'kry_kode', 'role',
    ];

    protected $hidden = [
      'password',
    ];

    public function Karyawan()
    {
      return $this->belongsTo('App\Models\mKaryawan', 'kry_kode')->select('kry_kode', 'kry_nama');
    }
}
