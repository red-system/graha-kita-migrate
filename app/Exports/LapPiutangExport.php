<?php

namespace App\Exports;

use App\Models\mPiutangPelanggan;
use App\Models\mPiutangLain;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class AllPenjualanKasirExport implements FromCollection
class LapPiutangExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    // private $request;
    private $start;
    private $end;

    // public function __construct($request)
    // {
    //     $this->request = $request;
    // }

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      // $request = $this->request;
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;
      $data['no_3'] = 1;
      
      // $start = date('Y-m-d', strtotime($start));
      // $end = date('Y-m-d', strtotime($end));

      $disc_PP=0;
      $angkut_PP=0;
      $total_PP=0;
      $rowPP = mPiutangPelanggan::leftJoin('tb_penjualan_langsung', 'tb_piutang_pelanggan.pp_no_faktur', '=', 'tb_penjualan_langsung.pl_no_faktur')
      ->leftJoin('tb_customer', 'tb_penjualan_langsung.cus_kode', '=', 'tb_customer.cus_kode')
      ->leftJoin('tb_karyawan', 'tb_penjualan_langsung.pl_sales_person', '=', 'tb_karyawan.kry_kode')
      ->select('pp_invoice', 'pp_no_faktur', 'pl_no_faktur', 'pl_tgl', 'pl_sales_person', 'pl_disc_nom', 'pl_ongkos_angkut', 'pl_subtotal', 'grand_total', 'tb_customer.cus_nama', 'kry_nama')
      ->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->get();

      foreach ($rowPP as $piutang) {
        $disc_PP += $piutang->pl_disc_nom;
        $angkut_PP += $piutang->pl_ongkos_angkut;
        $total_PP += $piutang->pl_subtotal;
      }

      $disc_PPPT=0;
      $angkut_PPPT=0;
      $total_PPPT=0;
      $rowPPPT = mPiutangPelanggan::leftJoin('tb_penjualan_titipan', 'tb_piutang_pelanggan.pp_no_faktur', '=', 'tb_penjualan_titipan.pt_no_faktur')
      ->leftJoin('tb_customer', 'tb_penjualan_titipan.cus_kode', '=', 'tb_customer.cus_kode')
      ->leftJoin('tb_karyawan', 'tb_penjualan_titipan.pt_sales_person', '=', 'tb_karyawan.kry_kode')
      ->select('pp_invoice', 'pp_no_faktur', 'pt_no_faktur', 'pt_tgl', 'pt_sales_person', 'pt_disc_nom', 'pt_ongkos_angkut', 'pt_subtotal', 'grand_total', 'tb_customer.cus_nama', 'kry_nama')
      ->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end)->get();

      foreach ($rowPPPT as $piutang) {
        $disc_PPPT += $piutang->pt_disc_nom;
        $angkut_PPPT += $piutang->pt_ongkos_angkut;
        $total_PPPT += $piutang->pt_subtotal;
      }

      $total_PL=0;
      // $rowPL = mPiutangLain::all();
      $rowPL = mPiutangLain::where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->get();
      foreach ($rowPL as &$piutang) {
        $piutang->customer;
        $total_PL += $piutang->pl_amount;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Piutang Pelanggan
      // $data['kode_PP'] = $kodePenjualanTitipan;
      $data['dataPP'] = $rowPP;
      $data['DiscPP'] = $disc_PP;
      $data['AngkutPP'] = $angkut_PP;
      $data['TotalPP'] = $total_PP;

      $data['dataPPPT'] = $rowPPPT;
      $data['DiscPPPT'] = $disc_PPPT;
      $data['AngkutPPPT'] = $angkut_PPPT;
      $data['TotalPPPT'] = $total_PPPT;
      //Data Piutang Lain
      // $data['kode_PL'] = $kodePenjualanTitipan;
      $data['dataPL'] = $rowPL;
      $data['TotalPL'] = $total_PL;

      return view('export.lapPiutang', $data);
    }
}
