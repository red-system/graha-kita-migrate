<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailOnlineOrder extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_detail_online_order';
  protected $primaryKey = 'detail_order_kode';
  public $timestamps = false;
  protected $fillable = [];

  public function Order()
  {
    return $this->belongsTo('App\Models\mOnlineOrder', 'order_kode');
  }

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode')->select('brg_kode', 'brg_barcode', 'brg_nama');
  }
}
