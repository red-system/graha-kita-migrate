<?php

namespace App\Exports;

use App\Models\mReturPenjualan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReturPenjualanExport implements FromView
{
    private $start;
    private $end;

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $row = mReturPenjualan::where('tgl_pengembalian', '>=', $start)->where('tgl_pengembalian', '<=', $end)->get();
      $grand_total=0;
      foreach ($row as $key => $value) {
        $span=1;
        $det = $value->detail;
        foreach ($det as $keyDet => $valueDet) {
          $value['span'] = $span++;
        }
        $grand_total+= $value->total_retur;
      }

      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      return view('export.laporan-retur-penjualan', $data);
    }
}
