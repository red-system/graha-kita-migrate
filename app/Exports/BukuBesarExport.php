<?php

namespace App\Exports;

use App\Models\mBarang;
use App\Models\mPerkiraan;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class BarangExport implements FromCollection
class BukuBesarExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
      $request = $this->request;

      $data['perkiraan']          = mPerkiraan::orderBy('master_id','ASC')->get();
      $data['start_date']         = $request->start_date;
      $data['end_date']           = $request->end_date;
      $coa                        = $request->kode_perkiraan;

      if($coa!='all'){
        $data['perkiraan']  = mPerkiraan::where('master_id',$coa)->get();
        $data['coa']        = $coa;
      }
      foreach ($data['perkiraan'] as $perkiraan) {
        if($perkiraan->mst_normal=='debet'){
          $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
          $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
          $saldo_awal = $debet - $kredit;
          $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
        }else{
          $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
          $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
          $saldo_awal = $kredit - $debet;
          $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
        }
      }
      $data['tgl_saldo_awal']     = date('Y-m-d', strtotime('-1 days', strtotime($data['start_date'])));

      return view('export.buku-besar', $data);
    }
}
