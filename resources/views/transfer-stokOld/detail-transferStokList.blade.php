@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    // $('#stok').on('input', function () {
    //   var value = $(this).val();
    //   if ((value !== '') && (value.indexOf('.') === -1)) {
    //     $(this).val(Math.max(Math.min(value, 90), 0));
    //   }
    // });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th> No </th>
                    <th> Gudang </th>
                    <th> Kode Barang </th>
                    <th> Nama Barang </th>
                    <th> Satuan </th>
                    <th> Kategory </th>
                    <th> Group Stok </th>
                    <th> Merek </th>
                    <th> QOH </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    @foreach ($row->stok_gudang as $stok)
                    <tr>
                      <td> {{ $no++ }}. </td>
                      <td> {{ $stok->gudang->gdg_nama }} </td>
                      <td> {{ $row->brg_barcode }} </td>
                      <td> {{ $row->brg_nama }} </td>
                      <td> {{ $row->satuan->stn_description }} </td>
                      <td> {{ $row->kategory->ktg_description }} </td>
                      <td> {{ $row->group->grp_description }} </td>
                      <td> {{ $row->merek->mrk_description }} </td>
                      <td> {{ $stok->stok }} </td>
                      <td style="font-size:12px">
                        <div class="btn-group btn-group-xs">
                          <button class="btn btn-success btn-edit-transfer" data-href="{{ route('transferStokEdit', ['kode'=>$row->brg_kode, 'kode_gudang'=>$stok->gudang->gdg_kode, 'stok'=>$stok->stok]) }}">
                            <span class="icon-pencil"></span> Edit
                          </button>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Transfer Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send" role="form" method="get">
            {{ csrf_field() }}
            <div class="form-body">
              {{-- <div class="form-group">
                <label class="col-md-3 control-label">Kode</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="brg_kode" readonly>
                </div>
              </div> --}}
              <div class="form-group">
                <input type="hidden" class="form-control" name="brg_kode" readonly>
                <label class="col-md-3 control-label">Nama Barang</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="brg_nama" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">QTY</label>
                <div class="col-md-9">
                  {{-- @if ($row->stok_gudang != null)
                    <input type="number" min="0" max="{{$stok->stok}}" class="form-control" name="stok">
                  @else --}}
                    <input id="stok" type="number" min="0" class="form-control" name="stok">
                  {{-- @endif --}}
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Gudang Tujuan</label>
                <div class="col-md-9">
                  <select class="form-control" name="gdg_kode">
                    @foreach($gudang as $gdg)
                      <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
