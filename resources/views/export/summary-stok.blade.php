<table>
  <thead>
    <tr>
      <th> Kode Barang </th>
      <th> Nama Barang </th>
      <th> Satuan </th>
      <th> Kategory </th>
      <th> Group Stok </th>
      <th> Merek </th>
      <th> Lokasi </th>
      <th> Total In </th>
      <th> Total Out </th>
      <th> Last Stok </th>
    </tr>
  </thead>
  <tbody>
    @foreach($dataList as $row)
      <tr>
        <td> {{ $row->brg_barcode }} </td>
        <td> {{ $row->brg_nama }} </td>
        <td> {{ $row->stn_nama }} </td>
        <td> {{ $row->ktg_nama }} </td>
        <td> {{ $row->grp_nama }} </td>
        <td> {{ $row->mrk_nama }} </td>
        <td> {{ $row->gdg_nama }} </td>
        @if ($row->stokIn == null)
          <td> - </td>
        @else
          <td> {{ $row->stokIn }} </td>
        @endif
        @if ($row->stokOut == null)
          <td> - </td>
        @else
          <td> {{ $row->stokOut }} </td>
        @endif
        <td> {{ $row->QOH }} </td>
      </tr>
    @endforeach
  </tbody>
</table>
