@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="{{ asset('assets/global/plugins/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    var description = document.getElementById("TC");
    CKEDITOR.replace(description,{
      language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <h4>Store Profile</h4>
              <form action="{{ route('profileInsert') }}" class="form-horizontal" role="form" method="post">
                {{ csrf_field() }}
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Alamat</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="prf_alamat">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Telp</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="prf_telp">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Fax</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="prf_fax">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Term & Condition</label>
                    <div class="col-md-6">
                      <textarea class="form-control" id="TC" name="prf_TC"></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn green">Simpan</button>
                      <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
