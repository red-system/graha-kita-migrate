<?php

namespace App\Http\Controllers;

use View,
    Session;
use Auth;
use Illuminate\Http\Request;

class Login extends Controller
{
    function index() {
        return view('main/login');
    }

    function form(Request $request) {
      // return $request;
      // $session = ['login'=>TRUE];
      // Session::put($session);
      // return redirect()->route('dashboardPage');

      $this->validate($request, [
        'username' => 'required',
        'password' => 'required|min:4'
      ]);
      
      if (Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
        $session = ['login'=>TRUE];
        Session::put($session);
        if(auth()->user()->role=='sales_person'){
          return redirect()->route('DataReport.sales');
        }else{
          return redirect()->route('dashboardPage');
        }
      }
      else {
        return redirect()->back();
      }
    }
}
