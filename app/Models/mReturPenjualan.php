<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mReturPenjualan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_retur_penjualan';
  protected $primaryKey = 'no_retur_penjualan';
  public $timestamps = false;

  public function detail()
  {
    return $this->hasMany('App\Models\mReturPenjualanDetail', 'no_retur_penjualan', 'no_retur_penjualan');
  }

  public function penjualanL()
  {
    return $this->belongsTo('App\Models\mPenjualanLangsung', 'no_faktur', 'pl_no_faktur');
  }

  public function penjualanT()
  {
    return $this->belongsTo('App\Models\mPenjualanTitipan', 'no_faktur', 'pt_no_faktur');
  }
}
