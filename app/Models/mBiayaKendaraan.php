<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mBiayaKendaraan extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];
  
  public $incrementing = false;
  protected $table = 'tb_biaya_kendaraan';
  protected $primaryKey = 'biaya_kendaraan_kode';
  // public $timestamps = false;

  public function kendaraan()
  {
    return $this->belongsTo('App\Models\mKendaraanOperasional', 'kendaraan_operasional_kode');
  }

  public function biaya()
  {
    return $this->belongsTo('App\Models\mAcMaster', 'biaya_kode', 'master_id')->select('master_id', 'mst_kode_rekening', 'mst_nama_rekening');
  }

  public function pembayaran()
  {
    return $this->belongsTo('App\Models\mAcMaster', 'pembayaran_kode', 'master_id')->select('master_id', 'mst_kode_rekening', 'mst_nama_rekening');
  }

  public function jpk()
  {
    return $this->belongsTo('App\Models\mJPK', 'jpk_kode');
  }

  public function spl()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode')->select('spl_kode', 'spl_nama');
  }
}
