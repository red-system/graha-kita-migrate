<?php

namespace App\Exports;

use App\Models\mSuratJalanPenjualanTitipan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SuratJalanSisaExport implements FromView
{
    private $start;
    private $end;

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $rowPT = mSuratJalanPenjualanTitipan::where('sjt_tgl', '>=', $start)->where('sjt_tgl', '<=', $end)
      ->groupBy('sjt_kode')
      ->get();
      $total=0;
      $grand_totalPT=0;
      foreach ($rowPT as &$key) {
        $faktur = $key->faktur;
        $key['customer'] = $faktur->customer;
        $key['detail'] = $faktur->detail_PT;
        foreach ($key['detail'] as $keyDet) {
          $keyDet->barang->satuan;
          $keyDet['qty_akhir'] = $keyDet->qty - $keyDet->terkirim;
        }
        unset($key->faktur);
        unset($faktur->customer);
        unset($faktur->detail_PT);
      }

      $data['dataPT'] = $rowPT;
      $data['grand_total_PT'] = $grand_totalPT;

      return view('export.laporan-surat-jalan-sisa', $data);
    }
}
