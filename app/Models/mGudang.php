<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mGudang extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];
  
  protected $table = 'tb_gudang';
  protected $primaryKey = 'gdg_kode';
  public $timestamps = false;

  public function stok()
  {
    return $this->hasMany('App\Models\mStok', 'gdg_kode');
  }
}
