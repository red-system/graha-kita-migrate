<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mSupplier;

use Validator;

class Supplier extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Supplier';
      $this->kodeLabel = 'supplier';
  }

  function index() {
      $breadcrumb = [
          'Supplier'=>route('supplierList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'supplier';
      $data['title'] = $this->title;
      $data['dataList'] = mSupplier::all();
      return view('supplier/supplierList', $data);
  }

  function rules($request) {
      $rules = [
          'spl_nama' => 'required',
          'spl_alamat' => 'required',
          'spl_telp' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mSupplier::insert($request->except('_token'));
      return [
          'redirect'=>route('supplierList')
      ];
  }

  function edit($spl_kode='') {
      $response = [
          'action'=>route('supplierUpdate', ['kode'=>$spl_kode]),
          'field'=>mSupplier::find($spl_kode)
      ];
      return $response;
  }

  function update(Request $request, $spl_kode) {
      $this->rules($request->all());
      mSupplier::where('spl_kode', $spl_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('supplierList')
      ];
  }

  function delete($spl_kode) {
      mSupplier::where('spl_kode', $spl_kode)->delete();
  }
}
