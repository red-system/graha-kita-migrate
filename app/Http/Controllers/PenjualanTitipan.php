<?php

namespace App\Http\Controllers;
use Meneses\LaravelMpdf\Facades\LaravelMpdf As MPDF;
use Carbon\Carbon;
use App\Models\mArusStok;
use App\Models\mBarang;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mPerkiraan;
use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mPenjualanTitipan;
use App\Models\mDetailHargaCustomer;
use App\Models\mHargaCustomer;
use App\Models\mKodeBukti;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mStok;
use App\Models\mCheque;
use App\Models\mPiutangPelanggan;
use App\Models\mUser;
use Illuminate\Support\Facades\Hash;
use App\Models\mSuratJalanPenjualanTitipan;
use App\Models\mDetailSuratJalanPT;
use App\Models\mAcMaster;
use App\Models\mDeliveryOrder;
use App\Models\mDeliveryOrderDetail;

use RioAstamal\AngkaTerbilang\Terbilang;
use Validator,
    DB,
    View;

class PenjualanTitipan extends Controller {

  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penjualan Titipan';
    $this->kodeLabel = 'penjualanTitipan';
  }

  protected function createNoFaktur() {
    $kode = $this->main->kodeLabel('penjualanTitipan');
    $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'.'.$kode.'.';
    // $lastorder = mPenjualanTitipan::where(\DB::raw('LEFT(pt_no_faktur, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('RIGHT(pt_no_faktur, 4) as no_order')->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mPenjualanTitipan::where('pt_no_faktur', 'like', '%'.$preffix.'%')->max('pt_no_faktur');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no.$user;
  }

  protected function createNoPiutang() {
    $kode = $this->main->kodeLabel('piutangPelanggan');

    $preffix = '/'.$kode.'/'.date('my');
    // $lastorder = mPiutangPelanggan::where(\DB::raw('RIGHT(no_piutang_pelanggan, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('LEFT(no_piutang_pelanggan, 4) as no_order')->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mPiutangPelanggan::where('no_piutang_pelanggan', 'like', '%'.$preffix.'%')->max('no_piutang_pelanggan');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 0, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $no.$preffix;
  }

  function index() {
      $col_label = 3;
      $col_form = 9;
    $breadcrumb = [
      'Penjualan Titipan' => route('penjualanTitipanList'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    // $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
    // $last_pt_no_faktur = mPenjualanTitipan::orderBy('pt_no_faktur','DESC')
    //                                        ->limit('1')
    //                                        ->first()['pt_no_faktur'];
    // $no_faktur =  $last_pt_no_faktur+ 1;
    $no_faktur = $this->createNoFaktur();
    $kodeBarang = $this->main->kodeLabel('barang');
    $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
                      ->get();

    $data['menu'] = 'penjualan_langsung';
    $time = date('Y-m-d', strtotime(Carbon::now()));
    $data['default_customer'] = mCustomer::where('cus_nama', 'GUEST')->first();
    $customer = mCustomer::
    select('cus_kode', 'cus_nama', 'cus_alamat', 'cus_telp', 'kry_kode', 'cus_tipe', 'banned')
    ->orderBy('cus_kode', 'ASC')->get();
    foreach ($customer as $key) {
      $lewat = 0;
      $bg = 0;
      $piutang = $key->piutang;
      foreach ($piutang as $keyP) {
        if ($time > $keyP->pp_jatuh_tempo && $keyP->pp_status == 'belum dibayar') {
          $lewat++;
        }
      }

      $cheque = $key->cheque;
      foreach ($cheque as $keyC) {
        if ($keyC->bg_jatuh_tempo != null) {
          if ($keyC->tgl_pencairan > $keyC->bg_jatuh_tempo && $keyC->sisa > 0) {
            $bg++;
          }
        }
        else {
          if ($keyC->sisa > 0) {
            $bg++;
          }
        }
      }

      $key['lewat'] = $lewat;
      $key['bg'] = $bg;
    }

    $data['customer'] = $customer;
    // return $customer;
    $data['title'] = $this->title;
    $data['karyawan'] = mKaryawan::select('kry_kode', 'kry_nama')->where('kry_posisi', 'Salesgirl')->orWhere('kry_posisi', 'Salesman')->orderBy('kry_kode', 'ASC')->get();
    $data['sopir'] = mKaryawan::select('kry_kode', 'kry_nama')->where('kry_posisi', 'sopir')->orderBy('kry_kode', 'ASC')->get();
    $data['kodeKaryawan'] = $this->main->kodeLabel('karyawan');
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');
    $data['no_faktur'] = $no_faktur;
    // $data['kodePenjualanTitipan'] = $kodePenjualanTitipan;
    $data['barang'] = $barang;
    $data['kodeBarang'] = $kodeBarang;
    $data['col_label'] = $col_label;
    $data['col_form'] = $col_form;
    $data['perkiraan'] = mAcMaster::whereIn('mst_kode_rekening', [1101, 1301, 1302, 1303, 1305, 519006])->get();

    // return $data['barang'];
    return view('penjualanTitipan/penjualanTitipanList', $data);
  }

  function test() {
      return '1';
  }

  function barangRow(Request $request) {
      $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $request->input('brg_kode'))->first();
      $brg_kode = $brg_kodex->brg_kode;
      // $brg_kode = $request->input('brg_kode');
      $cus_kode = $request->input('cus_kode');
      $brg = mBarang::leftJoin('tb_satuan AS stn', 'stn.stn_kode', '=', 'tb_barang.stn_kode')
                    ->where('brg_kode', $brg_kode)
                    ->first();

      $detailHargaCustomer = mHargaCustomer::where([
                                                    'brg_kode'=>$brg_kode,
                                                    'cus_kode'=>$cus_kode
                                                  ]);
      $harga_jual = '
        <option value="">Pilih Harga</option>
        <option value="'.round($brg->brg_harga_jual_eceran).'">'.number_format(round($brg->brg_harga_jual_eceran), 2, "," ,".").' [Eceran]</option>
        <option value="'.round($brg->brg_harga_jual_partai).'">'.number_format(round($brg->brg_harga_jual_partai), 2, "," ,".").' [Partai]</option>
      ';

      if($detailHargaCustomer->count() != 0) {
          $harga = $detailHargaCustomer->first();
          $harga_jual .= '
          <option value="'.round($harga->hrg_cus_harga_jual_eceran).'">'.number_format(round($harga->hrg_cus_harga_jual_eceran), 2, "," ,".").' [Member Eceran]</option>
          <option value="'.round($harga->hrg_cus_harga_jual_partai).'">'.number_format(round($harga->hrg_cus_harga_jual_partai), 2, "," ,".").' [Member Partai]</option>
          ';
      }

      $stk_gdg = mStok::where('brg_kode', $brg_kode)
      ->groupBy('brg_no_seri')
      // ->groupBy('brg_kode', 'brg_no_seri', 'gdg_kode', 'spl_kode')
      ->get();
      $seri = '<option value="0">Pilih No Seri</option>';

      foreach ($stk_gdg as $stk) {
        $seri .= '<option value="'.$stk->brg_no_seri.'">'.$stk->brg_no_seri.'</option>';
      }

      $ppn = $brg->brg_ppn_dari_supplier_persen;
      $response = [
          'kode'=>$brg->brg_kode,
          'nama'=>$brg->brg_nama,
          'satuan'=>$brg->stn_kode,
          'brg_hpp'=>$brg->brg_hpp,
          'harga_jual'=>$harga_jual,
          'stok'=>$seri,
          'a'=>$detailHargaCustomer->count(),
          'ppn'=>$ppn,
      ];

      return $response;
  }

  function rules($request) {
    $rules = [
      'cus_kode' => 'required',
      'pt_transaksi' => 'required',
      'pt_sales_person' => 'required',
      'pt_checker' => 'required',
      'pt_sopir' => 'required',
      'pt_subtotal' => 'required',
      'pt_disc' => 'required',
      'pt_disc_nom' => 'required',
      'pt_ppn' => 'required',
      'pt_ppn_nom' => 'required',
      'pt_ongkos_angkut' => 'required',
      'grand_total'
    ];

    if($request['pt_tgl_jatuh_tempo'] == '') {
        $rules['pt_tgl_jatuh_tempo'] = 'required';
    }

    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    // return $request;
    \DB::beginTransaction();
    try {
      $time = Carbon::now();

      // Penjualan Titipan
      $cus_kode = $request->input('cus_kode');
      $cus_nama = $request->input('cus_nama');
      $cus_alamat = $request->input('cus_alamat');
      $cus_telp = $request->input('cus_telp');
      $cus_kecamatan = $request->input('cus_kecamatan');
      $cus_kabupaten = $request->input('cus_kabupaten');
      $pt_tgl = date('Y-m-d');
      $pt_transaksi = $request->input('pt_transaksi');
      $pt_sales_person = $request->input('pt_sales_person');
      $pt_checker = $request->input('pt_checker');
      $pt_sopir = $request->input('pt_sopir');
      $pt_catatan = $request->input('pt_catatan');
      $pt_kirim_semua = $request->input('pt_kirim_semua');
      $pt_lama_kredit = $request->input('pt_lama_kredit');
      $pt_tgl_jatuh_tempo = $request->input('pt_tgl_jatuh_tempo');
      $pt_subtotal = $request->input('pt_subtotal');
      $pt_disc = $request->input('pt_disc');
      $pt_ppn = $request->input('pt_ppn');
      $pt_disc_nom = $request->input('pt_disc_nom');
      $pt_subtotal_barang = $request->input('pt_subtotal_barang');
      $pt_subtotal_nom_disc = $request->input('pt_subtotal_nom_disc');
      $pt_total_disc = (float)$pt_subtotal_nom_disc + (float)$pt_disc_nom;
      // $pt_total_disc = ((float)$pt_subtotal_nom_disc - (float)$pt_subtotal) + (float)$pt_disc_nom;
      $pt_ppn_nom =  $request->input('pt_ppn_nom');
      $pt_ongkos_angkut = $request->input('pt_ongkos_angkut');
      $grand_total = $request->input('grand_total');
      $pt_total_hpp = $request->input('pt_total_hpp');
      $sisa_uang = $request->input('sisa_uang');
      $kembalian_uang = $request->input('kembalian_uang');
      $charge_persen = $request->input('charge_persen');
      $charge_nom = $request->input('charge_nom');

      // Detail Penjualan Langsung
      $gdg_kode = $request->input('gdg_kode');
      $brg_barcode = $request->input('brg_barcode');
      $brg_kode = $request->input('brg_kode');
      $nama_barang = $request->input('nama');
      $brg_no_seri = $request->input('brg_no_seri');
      $satuan = $request->input('satuan');
      $harga_jual = $request->input('harga_jual');
      $disc = $request->input('disc');
      $disc_nom = $request->input('disc_nom');
      $harga_net = $request->input('harga_net');
      $qty = $request->input('qty');
      $total = $request->input('total');
      $brg_hpp= $request->input('brg_hpp');
      $payment_total = $request->input('payment_total');
      $ppn_brg = $request->input('ppn');
      $ppn_nom_brg = $request->input('ppn_nom');
      $total_nom_ppn_brg = $request->input('total_nom_ppn');

      // Transaksi
      $master_id = $request->input('master_id');
      $charge = $request->input('charge');
      $no_check_bg = $request->input('no_check_bg');
      $tgl_pencairan = $request->input('tgl_pencairan');
      $setor = $request->input('setor');
      $jenis_transaksi = 'debet';
      $tipe_arus_kas = 'Operasi';
      $payment = $request->input('payment');
      $debet_nominal = $request->input('payment_total');
      $catatan = $request->input('keterangan');

      $total_bayar = 0;
      foreach ($payment_total as $key => $value) {
        $total_bayar += $value;
      }

      $total_hpp_C = 0;
      foreach ($brg_hpp as $key => $value) {
        $total_hpp_C += $value * $qty[$key];
      }

      // General
      $date_db = date('Y-m-d H:i:s');

      // $pt_no_faktur = mPenjualanTitipan::orderBy('pt_no_faktur','DESC')
      //                                    ->limit(1)
      //                                    ->first();
      // $pt_no_faktur = $pt_no_faktur['pt_no_faktur'] + 1;
      $pt_no_faktur = $this->createNoFaktur();

      $data_penjualan = new mPenjualanTitipan();
      $data_penjualan->pt_no_faktur = $pt_no_faktur;
      $data_penjualan->pt_tgl = $pt_tgl;
      $data_penjualan->cus_kode = $cus_kode;
      $data_penjualan->cus_nama = $cus_nama;
      $data_penjualan->cus_alamat = $cus_alamat;
      $data_penjualan->cus_telp = $cus_telp;
      $data_penjualan->cus_kecamatan = $cus_kecamatan;
      $data_penjualan->cus_kabupaten = $cus_kabupaten;
      $data_penjualan->pt_transaksi = $pt_transaksi;
      $data_penjualan->pt_sales_person = $pt_sales_person;
      $data_penjualan->pt_checker = $pt_checker;
      $data_penjualan->pt_sopir = $pt_sopir;
      $data_penjualan->pt_catatan = $pt_catatan;
      $data_penjualan->pt_lama_kredit = $pt_lama_kredit;
      if ($pt_transaksi == 'kredit') {
        $data_penjualan->pt_tgl_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo);
      }
      else {
        $data_penjualan->pt_tgl_jatuh_tempo = null;
      }
      $data_penjualan->pt_kirim_semua = $pt_kirim_semua;
      $data_penjualan->pt_subtotal = $pt_subtotal;
      $data_penjualan->pt_disc = $pt_disc;
      $data_penjualan->pt_disc_nom = $pt_disc_nom;
      $data_penjualan->pt_ppn = $pt_ppn;
      $data_penjualan->pt_ppn_nom = $pt_ppn_nom;
      $data_penjualan->pt_ongkos_angkut = $pt_ongkos_angkut;
      $data_penjualan->grand_total = $grand_total;
      // $data_penjualan->hpp_total = $pt_total_hpp;
      $data_penjualan->hpp_total = $total_hpp_C;
      $data_penjualan->total_bayar = $total_bayar;
      $data_penjualan->sisa_uang = $sisa_uang;
      $data_penjualan->kembalian_uang = $kembalian_uang;

      $penjualan_cash=0;
      $penjualan_transfer=0;
      $penjualan_cek=0;
      $penjualan_edc=0;
      $penjualan_piutang=0;
      $penjualan_pembulatan=0;
      $bayar=0;
      foreach($master_id as $key=>$val) {
        $master = mPerkiraan::find($master_id[$key]);
        $trs_kode_rekening = $master->mst_kode_rekening;
        $trs_nama_rekening = $master->mst_nama_rekening;

        if ($trs_kode_rekening == '1101') {
          $bayar += $debet_nominal[$key];
          $penjualan_cash += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1301') {
          $penjualan_piutang += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1302') {
          $penjualan_edc += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1303') {
          $penjualan_cek += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1305') {
          $penjualan_transfer += $payment[$key];
        }
        elseif ($trs_kode_rekening == '519006') {
          $penjualan_pembulatan += $payment[$key];
        }

        $data_penjualan->bayar = $bayar;
        $data_penjualan->cash = $penjualan_cash;
        $data_penjualan->piutang = $penjualan_piutang;
        $data_penjualan->edc = $penjualan_edc;
        $data_penjualan->cek_bg = $penjualan_cek;
        $data_penjualan->transfer = $penjualan_transfer;
        $data_penjualan->pembulatan = $penjualan_pembulatan;
      }
      $data_penjualan->charge_persen = $charge_persen;
      $data_penjualan->charge_nom = $charge_nom;
      $data_penjualan->save();

      foreach ($gdg_kode as $key => $value) {
        // $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $brg_kode[$key])->first();
        // $brg_barcode = $brg_kodex['brg_kode'];
        // $terkirim = $pt_kirim_semua == 'ya' ? $qty[$key] : 0;
        $data_barang = mBarang::select('brg_kode', 'mrk_kode', 'stn_kode', 'grp_kode')->where('brg_kode', $brg_kode[$key])->first();

        $kurang = mStok::where('brg_kode', $brg_kode[$key])->where('gdg_kode', $gdg_kode[$key])->where('brg_no_seri', $brg_no_seri[$key])->first();
        $kurang->stok_titipan = ($kurang->stok_titipan + $qty[$key]);
        $kurang->save();

        $terkirim = 0;
        $detailPT = new mDetailPenjualanTitipan();
        $detailPT->pt_no_faktur = $pt_no_faktur;
        $detailPT->gudang = $gdg_kode[$key];
        // $detailPT->gdg_kode = $gdg_kode[$key];
        // $detailPT->brg_kode = $brg_kode[$key];
        $detailPT->brg_kode = $brg_kode[$key];
        $detailPT->brg_no_seri = $brg_no_seri[$key];
        $detailPT->nama_barang = $nama_barang[$key];
        $detailPT->satuan = $satuan[$key];
        $detailPT->mrk_kode = $data_barang['mrk_kode'];
        $detailPT->grp_kode = $data_barang['grp_kode'];
        $detailPT->spl_kode = $kurang['spl_kode'];
        $detailPT->harga_jual = $harga_jual[$key];
        $detailPT->disc = $disc[$key];
        $detailPT->disc_nom = $disc_nom[$key];
        $detailPT->harga_net = $harga_net[$key];
        $detailPT->qty = $qty[$key];
        $detailPT->terkirim = $terkirim;
        $detailPT->total = $total[$key];
        $detailPT->brg_hpp = $brg_hpp[$key];
        $detailPT->ppn = $ppn_brg[$key];
        $detailPT->ppn_nom = $ppn_nom_brg[$key];
        $detailPT->total_nom_ppn = $total_nom_ppn_brg[$key];
        $detailPT->stk_kode = $kurang->stk_kode;
        $detailPT->save();
      }

      // Jurnal Umum
      // $kode_bukti_id = $request->input('kode_bukti_id');
      $kode_bukti_id = $pt_no_faktur;

      // General
      $year = date('Y');
      $month = date('m');
      $day = date('d');
      $where = [
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day
      ];
      $jmu_no = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
      if($jmu_no->count() == 0) {
          $jmu_no = 1;
      } else {
          $jmu_no = $jmu_no->first()->jmu_no + 1;
      }

      $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
          ->limit(1)
          ->first();
      $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

      $data_jurnal = [
          'jurnal_umum_id'=>$jurnal_umum_id,
          'no_invoice'=>$kode_bukti_id,
          'id_pel'=>'CUS'.$cus_kode,
          // 'kode_bukti_id'=>$kode_bukti_id,
          'jmu_no'=>$jmu_no,
          'jmu_tanggal'=>date('Y-m-d'),
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day,
          'jmu_keterangan'=>'Penjualan '.$kode_bukti_id,
          'jmu_date_insert'=>$date_db,
          'jmu_date_update'=>$date_db,
      ];
      mJurnalUmum::insert($data_jurnal);

      foreach($master_id as $key=>$val) {
          $master = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening = $master->mst_kode_rekening;
          $trs_nama_rekening = $master->mst_nama_rekening;

          if ($trs_kode_rekening == '1303') {
            $cekBG = new mCheque();
            $cekBG->no_bg_cek = $no_check_bg[$key];
            $cekBG->no_invoice = $pt_no_faktur;
            $cekBG->tgl_pencairan = $tgl_pencairan[$key];
            $cekBG->cek_amount =  $debet_nominal[$key];
            $cekBG->sisa =  $debet_nominal[$key];
            $cekBG->cek_dari = $cus_kode;
            $cekBG->cek_keterangan = $catatan[$key];
            $cekBG->tgl_cek = $time;
            $cekBG->bg_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo);
            $cekBG->save();
          }
          elseif ($trs_kode_rekening == '1301' || $trs_kode_rekening == '1302' || $trs_kode_rekening == '1305') {
            $piutang = new mPiutangPelanggan();
            $piutang->pp_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo); //PENJUALAN TITIPAN
            $piutang->no_piutang_pelanggan = $this->createNoPiutang();
            $piutang->pp_no_faktur = $pt_no_faktur; //PENJUALAN TITIPAN
            $piutang->cus_kode = $cus_kode;
            $piutang->pp_amount = $debet_nominal[$key];
            $piutang->pp_sisa_amount = $debet_nominal[$key];
            $piutang->pp_keterangan = $catatan[$key];
            $piutang->pp_status = 'belum dibayar';
            $piutang->kode_perkiraan = $trs_kode_rekening;
            $piutang->tipe_penjualan = 'ptp';
            $piutang->tgl_piutang = $time;
            $piutang->save();
          }

          $data_transaksi = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'master_id'=>$master_id[$key],
              'trs_jenis_transaksi'=>$jenis_transaksi,
              'trs_debet'=>$debet_nominal[$key],
              'trs_kredit'=>0,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$catatan[$key],
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              'trs_charge'=>0,
              // 'trs_charge'=>$charge[$key],
              'trs_no_check_bg'=>$no_check_bg[$key],
              'trs_tgl_pencairan'=>$tgl_pencairan[$key],
              'trs_setor'=>$setor[$key],
              'tgl_transaksi'=>date('Y-m-d'),
          ];
          mTransaksi::insert($data_transaksi);
      }

      // penjualan:311011,
      // PPN Keluaran:2205,
      // persediaan:1601,
      // Potongan Penjualan:311013,
      // Biaya Oks Kirim Penjualan:512023,
      // Harga Pokok Penjualan:41101
      // Penjualan Titipan:311014
      // Hutang Penjualan Titipan:2102

      $master_id_kredit = ["2102"];
      // $master_id_kredit = ["311014", "2205", "512023", "311014", "2205", "512023", "311013", "2102"];
      $count_311014 = 0;
      $count_2205 = 0;
      $count_512023 = 0;

      foreach($master_id_kredit as $key=>$val) {
          $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
          $master_idK = $masterK->master_id;
          $trs_kode_rekening = $masterK->mst_kode_rekening;
          $trs_nama_rekening = $masterK->mst_nama_rekening;
          $debet_nom = 0;
          $kredit_nom = $grand_total;
          $trs_jenis_transaksi = 'kredit';
          $trs_catatan = 'kredit';

          // if ($trs_kode_rekening == "311014") {
          //   if ($count_311014 == 0) {
          //     $count_311014++;
          //     $debet_nom = 0;
          //     $kredit_nom = $pt_subtotal;
          //     $trs_jenis_transaksi = 'kredit';
          //     $trs_catatan = 'kredit';
          //   }
          //   else {
          //     $count_311014 = 0;
          //     $debet_nom = $pt_subtotal;
          //     $kredit_nom = 0;
          //     $trs_jenis_transaksi = 'debet';
          //     $trs_catatan = 'debet';
          //   }
          // }
          // elseif ($trs_kode_rekening == "2205") {
          //   if ($count_2205 == 0) {
          //     $count_2205++;
          //     $debet_nom = 0;
          //     $kredit_nom = $pt_ppn_nom;
          //     $trs_jenis_transaksi = 'kredit';
          //     $trs_catatan = 'kredit';
          //   }
          //   else {
          //     $count_2205=0;
          //     $debet_nom = $pt_ppn_nom;
          //     $kredit_nom = 0;
          //     $trs_jenis_transaksi = 'debet';
          //     $trs_catatan = 'debet';
          //   }
          // }
          // elseif ($trs_kode_rekening == "512023") {
          //   if ($count_512023 == 0) {
          //     $count_512023++;
          //     $debet_nom = 0;
          //     $kredit_nom = $pt_ongkos_angkut;
          //     $trs_jenis_transaksi = 'kredit';
          //     $trs_catatan = 'kredit';
          //   }
          //   else {
          //     $count_512023 = 0;
          //     $debet_nom = $pt_ongkos_angkut;
          //     $kredit_nom = 0;
          //     $trs_jenis_transaksi = 'debet';
          //     $trs_catatan = 'debet';
          //   }
          // }
          // elseif ($trs_kode_rekening == "311013") {
          //   $debet_nom = $pt_disc_nom;
          //   $kredit_nom = 0;
          //   $trs_jenis_transaksi = 'debet';
          //   $trs_catatan = 'debet';
          // }
          // elseif ($trs_kode_rekening == "2102") {
          //   $debet_nom = 0;
          //   $kredit_nom = $grand_total;
          //   $trs_jenis_transaksi = 'kredit';
          //   $trs_catatan = 'kredit';
          // }
          // else {
          //   $debet_nom = 0;
          //   $kredit_nom = 0;
          //   $trs_jenis_transaksi = 'kredit';
          //   $trs_catatan = 'kredit';
          // }
          $data_transaksiK = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'master_id'=>$master_idK,
              'trs_jenis_transaksi'=>$trs_jenis_transaksi,
              'trs_debet'=>$debet_nom,
              'trs_kredit'=>$kredit_nom,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$trs_catatan,
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              // 'trs_charge'=>0,
              // 'trs_no_check_bg'=>0,
              // 'trs_tgl_pencairan'=>'',
              // 'trs_setor'=>0,
              'tgl_transaksi'=>date('Y-m-d'),
          ];
          mTransaksi::insert($data_transaksiK);
          // return $masterK;
      }

      \DB::commit();

      return [
          'redirect'=>route('penjualanTitipanList'),
          'faktur' => $pt_no_faktur
      ];

    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }

  function update(Request $request, $pt_no_faktur) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pt_tgl'] = date('Y-m-d H:i:s');
    mPenjualanTitipan::where(['pt_no_faktur'=>$pt_no_faktur])->update($data);
    return [
      'redirect' => route('penjualanTitipanList')
    ];
  }


  function delete($pt_no_faktur) {
    \DB::beginTransaction();
    try {
      mPenjualanTitipan::where('pt_no_faktur', $pt_no_faktur)->delete();
      $detail = mDetailPenjualanTitipan::where('pt_no_faktur', $pt_no_faktur)->get();
      foreach ($detail as $key => $value) {
        $kurang = mStok::where('stk_kode', $value->stk_kode)->first();
        $kurang->stok_titipan = ($kurang->stok_titipan - ($value->qty - $value->terkirim));
        $kurang->save();

        // $total_x = 0;
        // $data = mStok::where('brg_kode', $value->brg_kode)->get();
        // foreach ($data as $gdg) {
        //   $total_x += $gdg->stok;
        // }
        // $QOH = $total_x;
        //
        // $time = Carbon::now();
        // $arusStok = new mArusStok();
        // $arusStok->ars_stok_date = $time;
        // $arusStok->brg_kode = $value->brg_kode;
        // $arusStok->stok_in = $value->qty;
        // $arusStok->stok_out = 0;
        // $arusStok->stok_prev = $QOH;
        // $arusStok->gdg_kode = $value->gudang;
        // $arusStok->keterangan = 'Delete Penjualan Titipan '.$pt_no_faktur;
        // $arusStok->save();
      }
      mDetailPenjualanTitipan::where('pt_no_faktur', $pt_no_faktur)->delete();
      $cekBG = mCheque::where('no_invoice', $pt_no_faktur)->delete();
      $piutang = mPiutangPelanggan::where('pp_no_faktur', $pt_no_faktur)->delete();

      $jurnalIDPenjualan = mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $pt_no_faktur)->first();
      if ($jurnalIDPenjualan) {
        mTransaksi::where('jurnal_umum_id', $jurnalIDPenjualan->jurnal_umum_id)->delete();
      }
      mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $pt_no_faktur)->delete();

      $DO = mDeliveryOrder::select('do_kode')->where('no_faktur', $pt_no_faktur)->first();
      if ($DO) {
        mDeliveryOrderDetail::where('do_kode', $DO->do_kode)->delete();
        mDeliveryOrder::where('no_faktur', $pt_no_faktur)->delete();
      }

      $SJPT = mSuratJalanPenjualanTitipan::where('pt_no_faktur', $pt_no_faktur)->first();
      if ($SJPT) {
        $detail = mDetailSuratJalanPT::where('sjt_kode', $SJPT->sjt_kode)->get();
        foreach ($detail as $key => $value) {
          $tambah = mStok::where('brg_kode', $value->brg_kode)
          ->where('brg_no_seri', $value->brg_no_seri)
          ->where('gdg_kode', $value->gdg_kode)
          ->first();
          $tambah->stok = ($tambah->stok + $value->dikirim);
          $tambah->save();

          $total_x = 0;
          $data = mStok::where('brg_kode', $value->brg_kode)->get();
          foreach ($data as $gdg) {
            $total_x += $gdg->stok;
          }
          $QOH_masuk = $total_x;

          $time = Carbon::now();
          $arusStok = new mArusStok();
          $arusStok->ars_stok_date = $time;
          $arusStok->brg_kode = $value->brg_kode;
          $arusStok->stok_in = $value->dikirim;
          $arusStok->stok_out = 0;
          $arusStok->stok_prev = $QOH_masuk;
          $arusStok->gdg_kode = $value->gdg_kode;
          $arusStok->keterangan = 'Delete Surat Jalan '.$SJPT->sjt_kode;
          $arusStok->save();
        }
        mDetailSuratJalanPT::where('sjt_kode', $SJPT->sjt_kode)->delete();

        $jurnalIDSJ = mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $SJPT->sjt_kode)->first();
        if ($jurnalIDSJ) {
          mTransaksi::where('jurnal_umum_id', $jurnalIDSJ->jurnal_umum_id)->delete();
        }
        mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $SJPT->sjt_kode)->delete();
      }
      mSuratJalanPenjualanTitipan::where('pt_no_faktur', $pt_no_faktur)->delete();

      \DB::commit();
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }

  function gudangRow(Request $request) {
    $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $request->input('brg_kode'))->first();
    $brg_kode = $brg_kodex->brg_kode;
    // $brg_kode = $request->input('brg_kode');
    $brg_no_seri = $request->input('brg_no_seri');

    $stk_gdg = mStok::leftJoin('tb_gudang', 'tb_gudang.gdg_kode', '=', 'tb_stok.gdg_kode')
    ->where('brg_kode', $brg_kode)
    ->where('brg_no_seri', $brg_no_seri)
    // ->groupBy('tb_stok.gdg_kode')
    ->get();
    $gudang = '<option value="0">Pilih Gudang</option>';
    foreach ($stk_gdg as $stk) {
      $gudang .= '<option value="'.$stk->gdg_kode.'">'.$stk->gdg_nama.'</option>';
    }

    $response = [
      'gudang'=>$gudang,
    ];

    return $response;
  }

  function stokRow(Request $request) {
    $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $request->input('brg_kode'))->first();
    $brg_kode = $brg_kodex->brg_kode;
    // $brg_kode = $request->input('brg_kode');
    $brg_no_seri = $request->input('brg_no_seri');
    $gdg_kode = $request->input('gdg_kode');

    $stk_gdg = mStok::select('stok')
    ->where('brg_kode', $brg_kode)
    ->where('brg_no_seri', $brg_no_seri)
    ->where('gdg_kode', $gdg_kode)
    ->first();

    // $response = [
    //     'stok'=>$stk_gdg,
    // ];

    return $stk_gdg;
  }

  function getStok($brg_kode='') {
      $response = mBarang::leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
      ->leftJoin('tb_gudang', 'tb_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      ->leftJoin('tb_supplier', 'tb_stok.spl_kode', '=', 'tb_supplier.spl_kode')
      ->select('stk_kode', 'tb_stok.brg_no_seri', 'tb_stok.brg_kode', 'tb_stok.gdg_kode', 'gdg_nama', 'spl_nama', 'stok', 'stok_titipan')
      ->selectRaw('(stok) as QOH, (stok_titipan) as Titipan')
      // ->selectRaw('sum(stok) as QOH, sum(stok_titipan) as Titipan')
      ->where('tb_stok.brg_kode', $brg_kode)
      // ->whereNotNull('tb_stok.gdg_kode')
      // ->groupBy('tb_stok.brg_kode', 'tb_stok.brg_no_seri', 'tb_stok.gdg_kode', 'tb_stok.spl_kode')
      ->get();

      return $response;
  }

  function getPiutang($cus_kode='') {
    $time = date('Y-m-d', strtotime(Carbon::now()));
    $response = mPiutangPelanggan::
    select('pp_jatuh_tempo', 'pp_no_faktur', 'pp_amount', 'pp_sisa_amount')
    // ->leftJoin('tb_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur', '=', 'tb_piutang_pelanggan.pp_no_faktur')
    ->where('tb_piutang_pelanggan.cus_kode', $cus_kode)
    ->where('pp_jatuh_tempo', '<', $time)
    ->where('pp_status', 'belum dibayar')
    ->get();

    $no = 1;
    foreach ($response as $key => $value) {
      $value['no'] = $no++;
    }
    return $response;
  }

  function PiutangPass(Request $request) {
    $target = mUser::where('role', 'master')->where('username', $request->username)->first();
    if (Hash::check($request->password, $target->password)) {
      return 'true';
    }
    else {
      return 'false';
    }
  }

  function FakturJual($id='', $type='') {
    $terbilang = new Terbilang();
    $faktur = mPenjualanTitipan::where('pt_no_faktur', $id)->first();
    $faktur['terbilang'] = $terbilang->terbilang(number_format($faktur['grand_total'], 2, "." ,",")).' rupiah';
    // $faktur['detail'] = $faktur->detail_PT;
    // unset($faktur->detail_PT);

    $noFaktur=$id;
    $detail=mDetailPenjualanTitipan::select('nama_barang', 'harga_jual', 'ppn', 'disc', 'disc_nom', 'harga_net',\DB::raw('sum(qty) as qty'),\DB::raw('sum(total) as total'))
    ->join('tb_penjualan_titipan','tb_detail_penjualan_titipan.pt_no_faktur','=','tb_penjualan_titipan.pt_no_faktur')
    ->where('tb_detail_penjualan_titipan.pt_no_faktur','=',$noFaktur)
    ->groupBy('tb_detail_penjualan_titipan.brg_kode')
    ->get();
    $detailCollection=$detail;
    $faktur['detail']=$detail;

    $faktur['no'] = 1;
    $faktur['tglPrint'] = date('d/m/Y', strtotime($faktur->pt_tgl));
    $faktur['tglJT'] = date('d/m/Y', strtotime($faktur->pt_tgl_jatuh_tempo));
    $kodeSuratJalan = $this->main->kodeLabel('suratJalan');
    $faktur['kode_bukti_id'] = $id;

    $faktur['salesPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pt_sales_person)->first();

    if ($type=='mini') {
      return view('penjualanTitipan.ReportFakturJualMini', compact('faktur'));
    }
    else {
      return view('penjualanTitipan.ReportFakturJual', compact('faktur'));
    }

    // // return $faktur;
    // return MPDF::loadView('penjualanTitipan.ReportFakturJual', ['faktur' => $faktur], [], [
    //   'title' => 'Faktur Jual',
    //   'format' => 'A5-L'
    // ])
    // ->stream('penjualanTitipan.ReportFakturJual.pdf')
    // ->getMpdf()->SetDisplayMode('fullpage','continuous');
  }

  function getCheque($cus_nama='') {
    $response = mCheque::where('cek_dari', $cus_nama)
    ->where('sisa', '>', 0)
    ->get();

    return $response;
  }

  function daftarPenjualanTitipan() {
    $breadcrumb = [
        'Daftar Penjualan Titipan'=>route('daftarPenjualanTitipan'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'penjualan_titipan';
    $data['title'] = 'Daftar Penjualan Titipan';
    $data['person'] = mKaryawan::orderBy('kry_nama', 'asc')->get();
    $data['dataList'] = mPenjualanTitipan::orderBy('created_at', 'desc')->get();
    return view('penjualanTitipan/daftar', $data);
  }

  function detailPenjualanTitipan($kode='') {
    $col_label = 3;
    $col_form = 9;
    $breadcrumb = [
      'Daftar Penjualan Titipan' => route('daftarPenjualanTitipan'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
    $no_faktur = $kode;
    $gudang = mGudang::all();
    $kodeBukti = mKodeBukti::orderBy('kbt_kode_nama','ASC')->get();
    $kodeGudang = $this->main->kodeLabel('gudang');
    $kodeBarang = $this->main->kodeLabel('barang');
    $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
    ->get();

    $data['menu'] = 'penjualan_titipan';
    $time = date('Y-m-d', strtotime(Carbon::now()));
    $customer = mCustomer::
    select('cus_kode', 'cus_nama', 'cus_alamat', 'cus_telp', 'kry_kode', 'cus_tipe', 'banned')
    ->orderBy('cus_kode', 'ASC')->get();
    foreach ($customer as $key) {
      $lewat = 0;
      $bg = 0;
      $piutang = $key->piutang;
      foreach ($piutang as $keyP) {
        if ($time > $keyP->pp_jatuh_tempo && $keyP->pp_status == 'belum dibayar') {
          $lewat++;
        }
      }

      $cheque = $key->cheque;
      foreach ($cheque as $keyC) {
        if ($keyC->bg_jatuh_tempo != null) {
          if ($keyC->tgl_pencairan > $keyC->bg_jatuh_tempo && $keyC->sisa > 0) {
            $bg++;
          }
        }
        else {
          if ($keyC->sisa > 0) {
            $bg++;
          }
        }
      }

      $key['lewat'] = $lewat;
      $key['bg'] = $bg;
    }

    $data['time'] = $time;
    $data['customer'] = $customer;
    $data['title'] = 'Edit Penjualan Titipan';
    $data['dataList'] = mGudang::all();
    $data['karyawan'] = mKaryawan::select('kry_kode', 'kry_nama')->where('kry_posisi', 'Salesgirl')->orWhere('kry_posisi', 'Salesman')->orderBy('kry_kode', 'ASC')->get();
    $data['sopir'] = mKaryawan::select('kry_kode', 'kry_nama')->where('kry_posisi', 'sopir')->orderBy('kry_kode', 'ASC')->get();
    $data['penjualanTitipan'] = mPenjualanTitipan::where('pt_no_faktur', $no_faktur)->first();
    $data['detail'] = mDetailPenjualanTitipan::where('pt_no_faktur', $no_faktur)->get();

    // $cus = $data['penjualanTitipan']->customer;
    // if ($cus->cus_tipe && $cus->cus_tipe != 0) {
    //   $cus_tipe = $cus->typecus->type_cus_nama;
    // }
    // else {
    //   $cus_tipe = 'Non Member';
    // }
    $cus_tipe = $data['penjualanTitipan']->customer->cus_tipe;
    $data['cus_tipe'] = $cus_tipe;
    $data['kodeKaryawan'] = $this->main->kodeLabel('karyawan');
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');

    $data['no_faktur'] = $no_faktur;
    $data['kodePenjualanTitipan'] = $kodePenjualanTitipan;
    $data['gudang'] = $gudang;
    $data['kodeGudang'] = $kodeGudang;
    $data['barang'] = $barang;
    $data['kodeBarang'] = $kodeBarang;
    $data['col_label'] = $col_label;
    $data['col_form'] = $col_form;
    $data['kodeBukti'] = $kodeBukti;
    $data['perkiraan'] = mAcMaster::whereIn('mst_kode_rekening', [1101, 1301, 1302, 1303, 1305, 519006])->get();

    // return $data;
    return view('penjualanTitipan/detail', $data);
  }

  function updatePenjualanTitipan(Request $request) {
    \DB::beginTransaction();
    try {
      $time = Carbon::now();

      // Penjualan Titipan
      $no_faktur = $request->input('no_faktur');

      $cus_kode = $request->input('cus_kode');
      $cus_nama = $request->input('cus_nama');
      $cus_alamat = $request->input('cus_alamat');
      $cus_telp = $request->input('cus_telp');
      $cus_kecamatan = $request->input('cus_kecamatan');
      $cus_kabupaten = $request->input('cus_kabupaten');
      $pt_tgl = date('Y-m-d');
      $pt_transaksi = $request->input('pt_transaksi');
      $pt_sales_person = $request->input('pt_sales_person');
      $pt_checker = $request->input('pt_checker');
      $pt_sopir = $request->input('pt_sopir');
      $pt_catatan = $request->input('pt_catatan');
      $pt_kirim_semua = $request->input('pt_kirim_semua');
      $pt_lama_kredit = $request->input('pt_lama_kredit');
      $pt_tgl_jatuh_tempo = $request->input('pt_tgl_jatuh_tempo');
      $pt_subtotal = $request->input('pt_subtotal');
      $pt_disc = $request->input('pt_disc');
      $pt_ppn = $request->input('pt_ppn');
      $pt_disc_nom = $request->input('pt_disc_nom');
      $pt_subtotal_barang = $request->input('pt_subtotal_barang');
      $pt_subtotal_nom_disc = $request->input('pt_subtotal_nom_disc');
      $pt_total_disc = (float)$pt_subtotal_nom_disc + (float)$pt_disc_nom;
      // $pt_total_disc = ((float)$pt_subtotal_nom_disc - (float)$pt_subtotal) + (float)$pt_disc_nom;
      $pt_ppn_nom =  $request->input('pt_ppn_nom');
      $pt_ongkos_angkut = $request->input('pt_ongkos_angkut');
      $grand_total = $request->input('grand_total');
      $pt_total_hpp = $request->input('pt_total_hpp');
      $sisa_uang = $request->input('sisa_uang');
      $kembalian_uang = $request->input('kembalian_uang');
      $charge_persen = $request->input('charge_persen');
      $charge_nom = $request->input('charge_nom');

      // Detail Penjualan Langsung
      $gdg_kode = $request->input('gdg_kode');
      $brg_barcode = $request->input('brg_barcode');
      $brg_kode = $request->input('brg_kode');
      $nama_barang = $request->input('nama');
      $brg_no_seri = $request->input('brg_no_seri');
      $satuan = $request->input('satuan');
      $harga_jual = $request->input('harga_jual');
      $disc = $request->input('disc');
      $disc_nom = $request->input('disc_nom');
      $harga_net = $request->input('harga_net');
      $qty = $request->input('qty');
      $total = $request->input('total');
      $brg_hpp= $request->input('brg_hpp');
      $payment_total = $request->input('payment_total');
      $ppn_brg = $request->input('ppn');
      $ppn_nom_brg = $request->input('ppn_nom');
      $total_nom_ppn_brg = $request->input('total_nom_ppn');

      // Transaksi
      $master_id = $request->input('master_id');
      $charge = $request->input('charge');
      $no_check_bg = $request->input('no_check_bg');
      $tgl_pencairan = $request->input('tgl_pencairan');
      $setor = $request->input('setor');
      $jenis_transaksi = 'debet';
      $tipe_arus_kas = 'Operasi';
      $payment = $request->input('payment');
      $debet_nominal = $request->input('payment_total');
      $catatan = $request->input('keterangan');

      $total_bayar = 0;
      foreach ($payment_total as $key => $value) {
        $total_bayar += $value;
      }

      $total_hpp_C = 0;
      foreach ($brg_hpp as $key => $value) {
        $total_hpp_C += $value * $qty[$key];
      }

      // General
      $date_db = date('Y-m-d H:i:s');

      $data_penjualan = mPenjualanTitipan::where('pt_no_faktur', $no_faktur)->first();
      // $data_penjualan->pt_tgl = $pt_tgl;
      $data_penjualan->cus_kode = $cus_kode;
      $data_penjualan->cus_nama = $cus_nama;
      $data_penjualan->cus_alamat = $cus_alamat;
      $data_penjualan->cus_telp = $cus_telp;
      $data_penjualan->cus_kecamatan = $cus_kecamatan;
      $data_penjualan->cus_kabupaten = $cus_kabupaten;
      $data_penjualan->pt_transaksi = $pt_transaksi;
      $data_penjualan->pt_sales_person = $pt_sales_person;
      $data_penjualan->pt_checker = $pt_checker;
      $data_penjualan->pt_sopir = $pt_sopir;
      $data_penjualan->pt_catatan = $pt_catatan;
      $data_penjualan->pt_lama_kredit = $pt_lama_kredit;
      if ($pt_transaksi == 'kredit') {
        $data_penjualan->pt_tgl_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo);
      }
      else {
        $data_penjualan->pt_tgl_jatuh_tempo = null;
      }
      $data_penjualan->pt_kirim_semua = $pt_kirim_semua;
      $data_penjualan->pt_subtotal = $pt_subtotal;
      $data_penjualan->pt_disc = $pt_disc;
      $data_penjualan->pt_disc_nom = $pt_disc_nom;
      $data_penjualan->pt_ppn = $pt_ppn;
      $data_penjualan->pt_ppn_nom = $pt_ppn_nom;
      $data_penjualan->pt_ongkos_angkut = $pt_ongkos_angkut;
      $data_penjualan->grand_total = $grand_total;
      // $data_penjualan->hpp_total = $pt_total_hpp;
      $data_penjualan->hpp_total = $total_hpp_C;
      $data_penjualan->total_bayar = $total_bayar;
      $data_penjualan->sisa_uang = $sisa_uang;
      $data_penjualan->kembalian_uang = $kembalian_uang;

      $penjualan_cash=0;
      $penjualan_transfer=0;
      $penjualan_cek=0;
      $penjualan_edc=0;
      $penjualan_piutang=0;
      $penjualan_pembulatan=0;
      $bayar=0;
      foreach($master_id as $key=>$val) {
        $master = mPerkiraan::find($master_id[$key]);
        $trs_kode_rekening = $master->mst_kode_rekening;
        $trs_nama_rekening = $master->mst_nama_rekening;

        if ($trs_kode_rekening == '1101') {
          $bayar += $debet_nominal[$key];
          $penjualan_cash += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1301') {
          $penjualan_piutang += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1302') {
          $penjualan_edc += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1303') {
          $penjualan_cek += $payment[$key];
        }
        elseif ($trs_kode_rekening == '1305') {
          $penjualan_transfer += $payment[$key];
        }
        elseif ($trs_kode_rekening == '519006') {
          $penjualan_pembulatan += $payment[$key];
        }

        $data_penjualan->bayar = $bayar;
        $data_penjualan->cash = $penjualan_cash;
        $data_penjualan->piutang = $penjualan_piutang;
        $data_penjualan->edc = $penjualan_edc;
        $data_penjualan->cek_bg = $penjualan_cek;
        $data_penjualan->transfer = $penjualan_transfer;
        $data_penjualan->pembulatan = $penjualan_pembulatan;
      }
      $data_penjualan->charge_persen = $charge_persen;
      $data_penjualan->charge_nom = $charge_nom;
      $data_penjualan->save();

      $updateDetail = mDetailPenjualanTitipan::where('pt_no_faktur', $no_faktur)->get();
      foreach ($updateDetail as $key => $value) {
        $kurang = mStok::where('brg_kode', $value->brg_kode)->where('gdg_kode', $value->gudang)->where('brg_no_seri', $value->brg_no_seri)->first();
        $kurang->stok_titipan = ($kurang->stok_titipan - $value->qty);
        $kurang->save();
      }

      mDetailPenjualanTitipan::where('pt_no_faktur', $no_faktur)->delete();

      foreach ($gdg_kode as $key => $value) {
        $kurang = mStok::where('brg_kode', $brg_kode[$key])->where('gdg_kode', $gdg_kode[$key])->where('brg_no_seri', $brg_no_seri[$key])->first();
        $kurang->stok_titipan = ($kurang->stok_titipan + $qty[$key]);
        $kurang->save();

        // $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $brg_kode[$key])->first();
        // $brg_barcode = $brg_kodex['brg_kode'];
        $data_barang = mBarang::select('brg_kode', 'mrk_kode', 'stn_kode', 'grp_kode')->where('brg_kode', $brg_kode[$key])->first();

        $terkirim = 0;
        $detailPT = new mDetailPenjualanTitipan();
        $detailPT->pt_no_faktur = $no_faktur;
        $detailPT->gudang = $gdg_kode[$key];
        // $detailPT->brg_kode = $brg_kode[$key];
        $detailPT->brg_kode = $brg_kode[$key];
        $detailPT->brg_no_seri = $brg_no_seri[$key];
        $detailPT->nama_barang = $nama_barang[$key];
        $detailPT->satuan = $satuan[$key];
        $detailPT->mrk_kode = $data_barang['mrk_kode'];
        $detailPT->grp_kode = $data_barang['grp_kode'];
        $detailPT->spl_kode = $kurang['spl_kode'];
        $detailPT->harga_jual = $harga_jual[$key];
        $detailPT->disc = $disc[$key];
        $detailPT->disc_nom = $disc_nom[$key];
        $detailPT->harga_net = $harga_net[$key];
        $detailPT->qty = $qty[$key];
        $detailPT->terkirim = $terkirim;
        $detailPT->total = $total[$key];
        $detailPT->brg_hpp = $brg_hpp[$key];
        $detailPT->ppn = $ppn_brg[$key];
        $detailPT->ppn_nom = $ppn_nom_brg[$key];
        $detailPT->total_nom_ppn = $total_nom_ppn_brg[$key];
        $detailPT->stk_kode = $kurang->stk_kode;
        $detailPT->save();
      }

      // Jurnal Umum
      $kode_bukti_id = $request->input('kode_bukti_id');
      $jurnalID = mJurnalUmum::select('jurnal_umum_id', 'jmu_tanggal')->where('no_invoice', $kode_bukti_id)->first();
      mTransaksi::where('jurnal_umum_id', $jurnalID->jurnal_umum_id)->delete();

      // General
      $year = date('Y');
      $month = date('m');
      $day = date('d');

      $cekBG = mCheque::where('no_invoice', $no_faktur)->delete();
      $piutang = mPiutangPelanggan::where('pp_no_faktur', $no_faktur)->delete();

      foreach($master_id as $key=>$val) {
          $master = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening = $master->mst_kode_rekening;
          $trs_nama_rekening = $master->mst_nama_rekening;

          if ($trs_kode_rekening == '1303') {
            // $cekBG = mCheque::where('no_bg_cek', $no_check_bg[$key])->where('no_invoice', $no_faktur)->first();
            // if (empty($cekBG)) {
            //   // $cekBG->no_bg_cek = $no_check_bg[$key];
            //   $cekBG->tgl_pencairan = $tgl_pencairan[$key];
            //   $cekBG->cek_amount =  $debet_nominal[$key];
            //   $cekBG->sisa =  $debet_nominal[$key];
            //   $cekBG->cek_dari = $cus_kode;
            //   $cekBG->cek_keterangan = $catatan[$key];
            //   $cekBG->tgl_cek = $time;
            //   $cekBG->save();
            // }
            // else {
            //   $cekBG = new mCheque();
            //   $cekBG->no_bg_cek = $no_check_bg[$key];
            //   $cekBG->no_invoice = $no_faktur;
            //   $cekBG->tgl_pencairan = $tgl_pencairan[$key];
            //   $cekBG->cek_amount =  $debet_nominal[$key];
            //   $cekBG->sisa =  $debet_nominal[$key];
            //   $cekBG->cek_dari = $cus_kode;
            //   $cekBG->cek_keterangan = $catatan[$key];
            //   $cekBG->tgl_cek = $time;
            //   $cekBG->save();
            // }

            $cekBGNew = new mCheque();
            $cekBGNew->no_bg_cek = $no_check_bg[$key];
            $cekBGNew->no_invoice = $no_faktur;
            $cekBGNew->tgl_pencairan = $tgl_pencairan[$key];
            $cekBGNew->cek_amount =  $debet_nominal[$key];
            $cekBGNew->sisa =  $debet_nominal[$key];
            $cekBGNew->cek_dari = $cus_kode;
            $cekBGNew->cek_keterangan = $catatan[$key];
            $cekBGNew->tgl_cek = $time;
            $cekBGNew->bg_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo);
            $cekBGNew->save();
          }
          elseif ($trs_kode_rekening == '1301' || $trs_kode_rekening == '1302' || $trs_kode_rekening == '1305') {
            // $piutang = mPiutangPelanggan::where('pp_no_faktur', $no_faktur)->get();
            // if (empty($piutang)) {
            //   foreach ($piutang as $keyEdit => $value) {
            //     $value->pp_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo); //PENJUALAN TITIPAN
            //     // $value->no_piutang_pelanggan = $this->createNoPiutang();
            //     // $value->pp_no_faktur = $no_faktur; //PENJUALAN TITIPAN
            //     $value->cus_kode = $cus_kode;
            //     $value->pp_amount = $debet_nominal[$keyEdit];
            //     $value->pp_sisa_amount = $debet_nominal[$keyEdit];
            //     $value->pp_keterangan = $catatan[$keyEdit];
            //     $value->pp_status = 'belum dibayar';
            //     $value->kode_perkiraan = $trs_kode_rekening;
            //     $piutang->tipe_penjualan = 'ptp';
            //     $value->save();
            //   }
            // }
            // else {
            //   $piutang = new mPiutangPelanggan();
            //   $piutang->pp_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo); //PENJUALAN TITIPAN
            //   $piutang->no_piutang_pelanggan = $this->createNoPiutang();
            //   $piutang->pp_no_faktur = $no_faktur; //PENJUALAN TITIPAN
            //   $piutang->cus_kode = $cus_kode;
            //   $piutang->pp_amount = $debet_nominal[$key];
            //   $piutang->pp_sisa_amount = $debet_nominal[$key];
            //   $piutang->pp_keterangan = $catatan[$key];
            //   $piutang->pp_status = 'belum dibayar';
            //   $piutang->kode_perkiraan = $trs_kode_rekening;
            //   $piutang->tipe_penjualan = 'ptp';
            //   $piutang->save();
            // }

            $piutangNew = new mPiutangPelanggan();
            $piutangNew->pp_jatuh_tempo = Carbon::parse($pt_tgl_jatuh_tempo); //PENJUALAN TITIPAN
            $piutangNew->no_piutang_pelanggan = $this->createNoPiutang();
            $piutangNew->pp_no_faktur = $no_faktur; //PENJUALAN TITIPAN
            $piutangNew->cus_kode = $cus_kode;
            $piutangNew->pp_amount = $debet_nominal[$key];
            $piutangNew->pp_sisa_amount = $debet_nominal[$key];
            $piutangNew->pp_keterangan = $catatan[$key];
            $piutangNew->pp_status = 'belum dibayar';
            $piutangNew->kode_perkiraan = $trs_kode_rekening;
            $piutangNew->tipe_penjualan = 'ptp';
            $piutangNew->save();
          }

          $data_transaksi = [
              'jurnal_umum_id'=>$jurnalID['jurnal_umum_id'],
              'master_id'=>$master_id[$key],
              'trs_jenis_transaksi'=>$jenis_transaksi,
              'trs_debet'=>$debet_nominal[$key],
              'trs_kredit'=>0,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$catatan[$key],
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              'trs_charge'=>0,
              // 'trs_charge'=>$charge[$key],
              'trs_no_check_bg'=>$no_check_bg[$key],
              'trs_tgl_pencairan'=>$tgl_pencairan[$key],
              'trs_setor'=>$setor[$key],
              // 'tgl_transaksi'=>date('Y-m-d'),
              'tgl_transaksi'=>$jurnalID->jmu_tanggal,
          ];
          mTransaksi::insert($data_transaksi);
      }

      $master_id_kredit = ["2102"];
      // $master_id_kredit = ["311014", "2205", "512023", "311014", "2205", "512023", "311013", "2102"];
      $count_311014 = 0;
      $count_2205 = 0;
      $count_512023 = 0;

      foreach($master_id_kredit as $key=>$val) {
          $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
          $master_idK = $masterK->master_id;
          $trs_kode_rekening = $masterK->mst_kode_rekening;
          $trs_nama_rekening = $masterK->mst_nama_rekening;
          $debet_nom = 0;
          $kredit_nom = $grand_total;
          $trs_jenis_transaksi = 'kredit';
          $trs_catatan = 'kredit';

          // if ($trs_kode_rekening == "311014") {
          //   if ($count_311014 == 0) {
          //     $count_311014++;
          //     $debet_nom = 0;
          //     $kredit_nom = $pt_subtotal;
          //     $trs_jenis_transaksi = 'kredit';
          //     $trs_catatan = 'kredit';
          //   }
          //   else {
          //     $count_311014 = 0;
          //     $debet_nom = $pt_subtotal;
          //     $kredit_nom = 0;
          //     $trs_jenis_transaksi = 'debet';
          //     $trs_catatan = 'debet';
          //   }
          // }
          // elseif ($trs_kode_rekening == "2205") {
          //   if ($count_2205 == 0) {
          //     $count_2205++;
          //     $debet_nom = 0;
          //     $kredit_nom = $pt_ppn_nom;
          //     $trs_jenis_transaksi = 'kredit';
          //     $trs_catatan = 'kredit';
          //   }
          //   else {
          //     $count_2205=0;
          //     $debet_nom = $pt_ppn_nom;
          //     $kredit_nom = 0;
          //     $trs_jenis_transaksi = 'debet';
          //     $trs_catatan = 'debet';
          //   }
          // }
          // elseif ($trs_kode_rekening == "512023") {
          //   if ($count_512023 == 0) {
          //     $count_512023++;
          //     $debet_nom = 0;
          //     $kredit_nom = $pt_ongkos_angkut;
          //     $trs_jenis_transaksi = 'kredit';
          //     $trs_catatan = 'kredit';
          //   }
          //   else {
          //     $count_512023 = 0;
          //     $debet_nom = $pt_ongkos_angkut;
          //     $kredit_nom = 0;
          //     $trs_jenis_transaksi = 'debet';
          //     $trs_catatan = 'debet';
          //   }
          // }
          // elseif ($trs_kode_rekening == "311013") {
          //   $debet_nom = $pt_disc_nom;
          //   $kredit_nom = 0;
          //   $trs_jenis_transaksi = 'debet';
          //   $trs_catatan = 'debet';
          // }
          // elseif ($trs_kode_rekening == "2102") {
          //   $debet_nom = 0;
          //   $kredit_nom = $grand_total;
          //   $trs_jenis_transaksi = 'kredit';
          //   $trs_catatan = 'kredit';
          // }
          // else {
          //   $debet_nom = 0;
          //   $kredit_nom = 0;
          //   $trs_jenis_transaksi = 'kredit';
          //   $trs_catatan = 'kredit';
          // }
          $data_transaksiK = [
              'jurnal_umum_id'=>$jurnalID['jurnal_umum_id'],
              'master_id'=>$master_idK,
              'trs_jenis_transaksi'=>$trs_jenis_transaksi,
              'trs_debet'=>$debet_nom,
              'trs_kredit'=>$kredit_nom,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$trs_catatan,
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              // 'tgl_transaksi'=>date('Y-m-d'),
              'tgl_transaksi'=>$jurnalID->jmu_tanggal,
          ];
          mTransaksi::insert($data_transaksiK);
          // return $masterK;
      }

      \DB::commit();

      return [
          'redirect'=>route('daftarPenjualanTitipan'),
          'faktur' => $no_faktur
      ];

    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }
}
