<?php

namespace App\Exports;

use App\Models\mBarang;
use App\Models\mPerkiraan;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class BarangExport implements FromCollection
class RugiLabaExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $request = $this->request;

        $data['start_date']    = $request->start_date;
        $data['end_date']      = $request->end_date;

        //transaksi pendapatan
        $data['kode_pendapatan']  = mPerkiraan::where('mst_kode_rekening','3100')->get();
        $biaya = 0;
        foreach($data['kode_pendapatan'] as $pendapatan){
            $total_pendapatanUsaha = 0;
            foreach($pendapatan->childs as $pendapatanUsaha){
                $total_penjualan = 0;
                foreach($pendapatanUsaha->childs as $penjualan){
                    $total_penjualanChilds = 0;
                    foreach($penjualan->childs as $penjualanChilds){
                        if($penjualanChilds->mst_normal == 'kredit'){                                                        
                            $data['biaya'][$penjualanChilds->mst_kode_rekening]=$penjualanChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                            $total_penjualanChilds=$total_penjualanChilds+$data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }if($penjualanChilds->mst_normal == 'debet'){
                            $data['biaya'][$penjualanChilds->mst_kode_rekening]=$penjualanChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                            $total_penjualanChilds=$total_penjualanChilds-$data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                    }
                    if($penjualan->mst_normal == 'kredit'){
                        $total_childs = $total_penjualanChilds;                                                       
                        $data['biaya'][$penjualan->mst_kode_rekening]=$total_childs+$penjualan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                        $total_penjualan = $total_penjualan+$data['biaya'][$penjualan->mst_kode_rekening];
                    }if($penjualan->mst_normal == 'debet'){
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening]=$total_childs-$penjualan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                        $total_penjualan = $total_penjualan-$data['biaya'][$penjualan->mst_kode_rekening];
                    }
                }
                if($pendapatanUsaha->mst_normal == 'kredit'){
                    $penjualan_total =$total_penjualan;                                                        
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening]=$penjualan_total+$pendapatanUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                    $total_pendapatanUsaha = $total_pendapatanUsaha+$data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }if($pendapatanUsaha->mst_normal == 'debet'){
                    $penjualan_total =$total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening]=$penjualan_total-$pendapatanUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                    $total_pendapatanUsaha = $total_pendapatanUsaha-$data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
            }
            if($pendapatan->mst_normal == 'kredit'){   
                $total_pendapatan = $total_pendapatanUsaha;                                                      
                $data['biaya'][$pendapatan->mst_kode_rekening]=$total_pendapatanUsaha+$pendapatan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
            }if($pendapatan->mst_normal == 'debet'){
                $total_pendapatan = $total_pendapatanUsaha;                
                $data['biaya'][$pendapatan->mst_kode_rekening]=$total_pendapatanUsaha-$pendapatan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
            }
        }
        //transaksi pendapatan

        //transaksi pendapatan diluar usaha
        $data['kode_pendapatan_diluar_usaha']  = mPerkiraan::where('mst_kode_rekening','3120')->get();
        foreach($data['kode_pendapatan_diluar_usaha'] as $pendapatanDiluarUsaha){
            $total_pendapatanDiluarUsahaChilds = 0;
            foreach($pendapatanDiluarUsaha->childs as $pendapatanDiluarUsahaChilds){
                if($pendapatanDiluarUsahaChilds->mst_normal == 'kredit'){
                                                                          
                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening]=$pendapatanDiluarUsahaChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                    $total_pendapatanDiluarUsahaChilds = $total_pendapatanDiluarUsahaChilds+$data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }if($pendapatanDiluarUsahaChilds->mst_normal == 'debet'){
                    
                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening]=$pendapatanDiluarUsahaChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                    $total_pendapatanDiluarUsahaChilds = $total_pendapatanDiluarUsahaChilds-$data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
            }
            if($pendapatanDiluarUsaha->mst_normal == 'kredit'){   
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;                                                      
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening]=$total_pendapatanDiluarUsaha+$pendapatanDiluarUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
            }if($pendapatanDiluarUsaha->mst_normal == 'debet'){
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;                
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening]=$total_pendapatanDiluarUsaha-$pendapatanDiluarUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
            }
        }
        //transaksi pendapatan diluar usaha

        //transaksi hpp
        $data['kode_hpp']  = mPerkiraan::where('mst_kode_rekening','4100')->get();
        foreach($data['kode_hpp'] as $hpp){
            $total_penjualan = 0;
            foreach($hpp->childs as $hppChilds){
                $total_hppChild = 0;
                foreach($hppChilds->childs as $hppChild){
                    if($hppChild->mst_normal == 'kredit'){                                                        
                        $data['biaya'][$hppChild->mst_kode_rekening]=$hppChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                        $total_hppChild=$total_hppChild-$data['biaya'][$hppChild->mst_kode_rekening];
                    }if($hppChild->mst_normal == 'debet'){
                        $data['biaya'][$hppChild->mst_kode_rekening]=$hppChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                        $total_hppChild=$total_hppChild+$data['biaya'][$hppChild->mst_kode_rekening];
                    }
                }
                if($hppChilds->mst_normal == 'kredit'){
                    $total_hppChilds = $total_hppChild;                                                       
                    $data['biaya'][$hppChilds->mst_kode_rekening]=$total_hppChilds-$hppChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                    $total_hppChilds = $total_hppChilds-$data['biaya'][$hppChilds->mst_kode_rekening];
                }if($hppChilds->mst_normal == 'debet'){
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening]=$total_hppChilds+$hppChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                    $total_hppChilds = $data['biaya'][$hppChilds->mst_kode_rekening];
                }
            }
            if($hpp->mst_normal == 'kredit'){
                $total_hpp = $total_hppChilds;                                                       
                $data['biaya'][$hpp->mst_kode_rekening]=$total_hpp-$hpp->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                // $total_pendapatanUsaha = $total_pendapatanUsaha+$data['biaya'][$hpp->mst_kode_rekening];
            }if($hpp->mst_normal == 'debet'){
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening]=$total_hpp+$hpp->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                // $total_hpp = $total_hpp-$data['biaya'][$hpp->mst_kode_rekening];
            }
        }
        //transaksi hpp

        //transaksi biaya-biaya
        $data['kode_biaya']  = mPerkiraan::where('mst_kode_rekening','5100')->get();
        foreach($data['kode_biaya'] as $biaya){
            $total_biayaChild = 0;
            foreach($biaya->childs as $biayaChild){
                $total_biayaChild2 = 0;
                foreach($biayaChild->childs as $biayaChild2){
                    if($biayaChild2->mst_normal == 'kredit'){                                                        
                        $data['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                        $total_biayaChild2=$total_biayaChild2-$data['biaya'][$biayaChild2->mst_kode_rekening];
                    }if($biayaChild2->mst_normal == 'debet'){
                        $data['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                        $total_biayaChild2=$total_biayaChild2+$data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                }

                if($biayaChild->mst_normal == 'kredit'){                                                       
                    $data['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild-$hppChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                    $total_biayaChild = $total_biayaChild-$data['biaya'][$biayaChild->mst_kode_rekening];
                }if($biayaChild->mst_normal == 'debet'){
                    $data['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild2+$biayaChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                    $total_biayaChild = $total_biayaChild+$data['biaya'][$biayaChild->mst_kode_rekening];
                }
            }
            if($biaya->mst_normal == 'kredit'){
                $total_hpp = $total_biayaChild;                                                       
                $data['biaya'][$biaya->mst_kode_rekening]=$total_hpp-$biaya->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                
            }if($hpp->mst_normal == 'debet'){
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening]=$total_hpp+$biaya->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                
            }
        }

      return view('export.rugi-laba', $data);
    }
}
