$(document).ready(function () {

    $('#brg_kode').autocomplete({
        source : "/addBarcode",
        minLength :3,
        select: function( event, ui ) {
            event.preventDefault();
            $("#brg_kode").val(ui.item.id);
        }
    });

    // $table.on('post-body.bs.table', function () {
    //     $('#master_id[]').select2();
    // });
    
    $('.btn-isi-data-supplier').click(function() {
        $('#modal-isi-data-supplier').modal('show');
      });


    $('[name="spl_kode"]').change(function () {
        var spl_kode = $(this).val();
        var spl_alamat = $(this).find(':selected').attr('data-alamat');

        set_supplier(spl_kode, spl_alamat);
    });

    function set_supplier(spl_kode, spl_alamat) {
        var kode_supplier = $('#data-back').data('kode-customer');
        $('.spl_alamat').text(spl_alamat);
        $('[name="spl_kode_label"]').val(kode_supplier+spl_kode);
    }

    $('.btn-row-plus').click(function() {
        var spl_kode = $('[name="spl_nama"]').val();
        if(spl_kode == '') {
            swal({
                title: 'Perhatian',
                text: 'Diharuskan Pilih Supplier Terlebih Dahulu',
                type: 'warning'
            });
        } else {
            var row = $('.table-row-data tbody').html();
            $('.table-all-data tbody').append(row);
            select_row_barang();
            btn_row_delete();
            enter_disc();
            set_qty();
            detail_third_party();
            //$('#brg_kode').select2();
        }
    });
        

    $('[name="pl_transaksi"]').change(function () {
        var pl_transaksi = $(this).val();
        if (pl_transaksi === 'kredit') {
            $('.form-kredit').removeClass('hide');
        } else {
            $('.form-kredit').addClass('hide');
        }
    });

    $('.btn-pilih-item').click(function() {
        var brg_kode = $(this).data('brg-kode');
        var brg_barcode = $(this).data('brg-barcode');
        var brg_nama = $(this).data('brg-nama');
        var brg_harga_beli = $(this).data('brg-hrgbeli');        
        var brg_satuan = $(this).data('brg-satuan');
        var satuan_kode = $(this).data('brg-satuan-kode');
        //var brg_seri = $(this).data('brg-seri');
        $('[name="brg_kode"]').val(brg_kode);
        $('[name="brg_barcode"]').val(brg_barcode);
        $('[name="nama_barang"]').val(brg_nama);        
        $('[name="harga_beli"]').val(brg_harga_beli);
        $('[name="harga_net"]').val(brg_harga_beli);
        $('[name="satuan"]').val(brg_satuan);
        $('[name="satuan_kode"]').val(satuan_kode);
        
        // $('[name="no_seri"]').val(brg_seri);
        $('#modal-item').modal('hide');
    });

    $('.btn-pilih-no-seri').click(function() {
        var stok_kode = $(this).data('stok-kode');
        var no_seri = $(this).data('no-seri');
        $('[name="no_seri"]').val(no_seri);
        $('[name="stock_id"]').val(stok_kode);
        $('#modal-no-seri').modal('hide');
    });

    $('.btn-cari-item').click(function() {      
        $('#modal-item').modal('show');
    });

    $('.btn-cari-no-seri').click(function() {      
        $('#modal-no-seri').modal('show');
    });

    $('[name="pl_disc"], [name="pl_ppn"], [name="pl_ongkos_angkut"]').keyup(function() {
        //kalkulasi_subtotal();
        kalkulasi_grandtotal();
    });

    function btn_row_delete() {
        $('.btn-row-delete').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_subtotal();
            kalkulasi_grandtotal();
        });

    }

    $('.btn-pilih-supplier').click(function() {
        var spl_kode = $(this).data('spl-kode');
        var spl_nama = $(this).data('spl-nama');
        var spl_alamat = $(this).data('spl-alamat');
        var spl_telp = $(this).data('spl-telp');
        $('select[name="spl_kode"]').val(spl_kode).trigger('change');
        set_supplier(spl_kode, spl_alamat);
        $('#modal-supplier').modal('hide');
    });

    $('select[name="kode_barang"]').change(function() {
            var route_po_supplier_barang_row = $('#data-back').data('route-po-supplier-barang-row');
            var brg_kode = $(this).val();
            var token = $('#data-back').data('form-token');
            var ini = $(this);
            var data_send = {
                brg_kode: brg_kode,
                _token: token
            };

            var nama = $('[name="nama_barang"]');
            var satuan = $('[name="satuan"]');
            var satuan_kode = $('[name="satuan_kode"]');
            var harga_beli = $('[name="harga_beli"]');
            var harga_net = $('[name="harga_net"]');
            //var gdg_nama = $('[name="gudang"]');
            //var gdg_kode = $('[name="gudang_kode"]');
            var no_seri= $('[name="no_seri"]');


            nama.val('');
            satuan.val('');
            satuan_kode.val('');
            harga_beli.val('');
            harga_net.val('');
            //gdg_nama.val('');            
            //gdg_kode.val('');
            no_seri.empty();

            $.ajax({
                url: route_po_supplier_barang_row,
                type: 'POST',
                data: data_send,
                success: function(data) {
                    nama.val(data.nama);
                    satuan.val(data.satuan);
                    satuan_kode.val(data.stn_kode);
                    harga_beli.val(data.harga_beli);
                    harga_net.val(data.harga_beli);
                    //gdg_nama.val(data.gdg_nama);
                    //gdg_kode.val(data.gdg_kode);
                    // no_seri.append('<option></option>');
                    $.each(data.no_seri,function(index,value){
                        no_seri.append('<option class="select" value="'+value['stk_kode']+'">'+value['brg_seri']+'</option>')
                    });
                }
            });
        });

    $('select[name="pl_waktu"]').change(function() {
        hitung_jatuh_tempo();
    });

    $('input[name="pl_lama_kredit"]').keyup(function() {
        hitung_jatuh_tempo();
    });

    function hitung_jatuh_tempo(){
        var route_po_supplier_hitung_kredit = $('#data-back').data('route-po-supplier-hitung-kredit');
        var waktu=$('select[name="pl_waktu"]').val();
        var lama_kredit=$('input[name="pl_lama_kredit"]').val();
        var token = $('#data-back').data('form-token');
        var data_send = {
            waktu: waktu,
            lama_kredit: lama_kredit,
            _token :token
        };

        var jatuh_tempo=$('input[name="pl_tgl_jatuh_tempo"]').val('');
        $.ajax({
            url: route_po_supplier_hitung_kredit,
            type: 'POST',
            data: data_send,
            success: function(data) {
                jatuh_tempo.val(data.jatuh_tempo);
            }
        });
    }

    //function set_qty() {
        $('[name="qty"]').keyup(function() {
            var qty = $(this).val();
            

            if(qty !=='' || qty > 0){
                var harga_net = $('[name="harga_net"]').val();
                var total = parseInt(qty) * parseInt(harga_net);
                $('[name="total"]').val(total);
                //$('[name="sub_total_modal"]').val(total);

                //kalkulasi_subtotal();
            }


            //kalkulasi_grandtotal();
        });
    //}

    //function enter_disc() {
        $('[name="diskon"]').keyup(function() {
            // var form_disc_nom = $('[name="disc_nom"]');
            // var form_harga_net = $('[name="harga_net"]');
            // var form_total = $('[name="total"]');

            // form_disc_nom.val(0);
            // //form_harga_net.val(0);

            // var disc = $('[name="diskon"]').val();

            // if(disc !== '' || disc > 0){
            //     var qty = $('[name="qty"]').val();
            //     var harga_net_awal = $('[name="harga_net"]').val();
            //     var harga_beli = $('[name="harga_beli"]').val();
            //     var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_beli);
            //     var harga_net = parseInt(harga_beli) - disc_nom;
            //     var total = parseInt(harga_net) * parseInt(qty);

            //     form_disc_nom.val(disc_nom);
            //     form_harga_net.val(harga_net);
            //     form_total.val(total);
            // }
            

            //kalkulasi_subtotal();
            //kalkulasi_grandtotal();

            hitung_hargaNet();


        });
    //}

    $('[name="ppn"]').keyup(function() {

            hitung_hargaNet();

        });

    function hitung_hargaNet(){
        var ppn              = $('[name="ppn"]').val();
        var disc             = $('[name="diskon"]').val();
        //var disc_nom         = $('[name="disc_nom"]').val();
        var harga_beli       = $('[name="harga_beli"]').val();
        var harga_net_awal   = $('[name="harga_net"]').val();
        var qty              = $('[name="qty"]').val();

        if(ppn !== '' || disc !==''){
            var ppn_nom          = (parseInt(ppn) / parseInt(100)) * parseInt(harga_beli);
            var disc_nom         = (parseInt(disc) / parseInt(100)) * parseInt(harga_beli);
            var harga_net        = parseInt(harga_beli)+parseInt(ppn_nom)-parseInt(disc_nom);
            var total            = parseInt(harga_net) * parseInt(qty);

            $('[name="harga_net"]').val(harga_net);
            $('[name="disc_nom"]').val(disc_nom);
            $('[name="total"]').val(total);
            //$('[name="sub_total_modal"]').val(total);
        }

        
    }

    // function kalkulasi_subtotal() {
    //     var subtotal = $('[name="sub_total_modal"]').val();
    //     var total = $('[name="total"]').val();
    //     var sub_total = parseInt(subtotal) + parseInt(total);
    //     $('[name="sub_total_modal"]').val(sub_total);
    // }

    function kalkulasi_grandtotal() {
        var subtotal = $('[name="pl_subtotal"]').val();
        var disc = $('[name="pl_disc"]').val();
        var ppn = $('[name="pl_ppn"]').val();
        var ongkos_angkut = $('[name="pl_ongkos_angkut"]').val();

        var disc_kal = (parseInt(disc) / parseInt(100)) * parseInt(subtotal);
        var ppn_kal = (parseInt(ppn) / parseInt(100)) * parseInt(subtotal);

        var grand_total = parseInt(subtotal) - parseInt(disc_kal) + parseInt(ppn_kal) + parseInt(ongkos_angkut);

        $('[name="grand_total"]').val(grand_total);
        $('.nominal-grand-total').html(grand_total);
        //$('#sisa_pembayaran').text(grand_total);
    }

    function detail_third_party() {
        //$('[name="gdg_kode[]"], [name="brg_kode[]"], [name="harga_jual[]"]').select2();
    }

    
    //payment

    $('.btn-payment').click(function(){
        var grandtotal=$('[name="grand_total"]').val();
        $('.nominal-grand-total').html(grandtotal);
        $('.nominal-sisa').html(grandtotal);
        $('[name="sisa_payment"]').val(grandtotal);
        //$('#modalPayment').modal("show");

    });

    $('.btn-row-payment-plus').click(function() {
        var row_payment = $('.table-row-payment tbody').html();
        var el          = $(row_payment);
        $('.table-data-payment tbody').append(el);

        btn_row_delete_payment();
        payment();
        set_charge();
        setor();
        payment_third_party();
        // selectKodeMaster();
        btn_selectpickerx(el);
    });

    function selectKodeMaster(){
        $('[name="master_id[]"]').click(function() {
            $('#modalKodeMaster').modal("show");
        });
    
    }

    $('.btn-pilih-kodemaster').click(function() {
        var mst_rek = $(this).data('mast-rek');
        $('input[name="master_id[]"]').val(mst_rek);
        $('#modalKodeMaster').modal('hide');
    });

    function btn_selectpickerx(el) {
        el.children('td:nth-child(1)').children('select').selectpicker();
    }
      

    
    

    function btn_row_delete_payment() {
        $('.btn-row-delete-payment').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_sisa();
        });
    }

    function payment() {
        $('[name="payment[]"]').keyup(function() {
            var payment = $(this).val();

            $(this).parents('td').siblings('td.payment_total').children('input').val(payment);

            kalkulasi_sisa();
        });
    }

    function set_charge() {
        $('[name="charge[]"]').keyup(function() {
            var charge = $(this).val();
            var payment = $(this).parents('td').siblings('td.payment').children('input').val();
            var charge_nom = (parseInt(charge)/100) * parseInt(payment);
            var total = parseInt(payment) + parseInt(charge_nom);

            $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
            $(this).parents('td').siblings('td.payment_total').children('input').val(total);

            kalkulasi_sisa();
        });
    }

    function setor() {
        $('[name="setor[]"]').keyup(function() {
            var setor = $(this).val();
            var total = $(this).parents('td').siblings('td.payment_total').children('input').val();
            var kembalian = parseInt(setor) - parseInt(total);

            $(this).parents('td').siblings('td.kembalian').html(kembalian);
        });
    }

    function kalkulasi_sisa() {
        var payment_total = 0;
        var grand_total = $('.nominal-grand-total').html();
        $('[name="payment_total[]"]').each(function(key, val) {
            var payment_total_ini = $(this).val();
            if(payment_total_ini != '') {
                payment_total = parseInt(payment_total) + parseInt(payment_total_ini);
            }
        });
        sisa = parseInt(grand_total) - parseInt(payment_total);
        $('.nominal-sisa').text(sisa);
        //$('.sisa-payment').val(sisa);
        $('[name="sisa_payment"]').val(sisa);
    }

    function payment_third_party() {
        $('[name="tgl_pencairan[]"]').datepicker();
        //$('[name="master_id[]"]').select2();
    }



    //end
});