<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbWilayah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_wilayah', function (Blueprint $table) {
         $table->increments('wlh_kode');
         $table->integer('prov_kode')->nullable();
         $table->string('wlh_nama')->nullable();
         $table->string('wlh_description')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_wilayah');
     }
}
