@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
@stop

@section('body')
<style type="text/css">
    .tg th{font-size: 12px;}
    .tg td{font-size: 11px;}
</style>
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <a style="font-size: 12px;" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                            Pilih Periode
                        </a>
                        <a style="font-size: 12px;" type="button" class="btn btn-danger" href="{{route('printArusKas', ['start_date'=>$start_date, 'end_date'=>$end_date, 'tipe'=>'print'])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                        </a>

                        <h4><center><strong>LAPORAN ARUS KAS</strong></center></h4>
                        <h5><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h5>

                        <table class="table table-striped table-bordered table-hover tg" width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>I. ARUS KAS DARI KEGIATAN OPERASI</strong></td>                                   
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                </tr>
                                <?php
                                    $total_operasi=0;
                                    $total_pendanaan=0;
                                    $total_investasi=0;
                                ?>
                                @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach

                                
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                </tr>
                                
                                @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach 
                                <tr>
                                    <td><h5><strong>TOTAL KEGIATAN OPERASI</strong></h5></td> 
                                    <td><h5><strong>{{number_format($total_operasi)}}</strong></h5></td>                                  
                                </tr>
                                <tr>
                                    <td></td> 
                                    <td></td>                                  
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>II. ARUS KAS DARI KEGIATAN PENDANAAN</strong></td>                                   
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                </tr>
                                
                                @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                </tr>
                                @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td><h5><strong>TOTAL KEGIATAN PENDANAAN</strong></h5></td> 
                                    <td><h5><strong>{{number_format($total_pendanaan)}}</strong></h5></td>                                  
                                </tr>
                                <tr>
                                    <td></td> 
                                    <td></td>                                  
                                </tr>
                                
                                <tr>
                                    <td colspan="2"><strong>III. ARUS KAS DARI KEGIATAN INVESTASI</strong></td>                                   
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                </tr>
                                
                                @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                </tr>
                                @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td><h5><strong>TOTAL KEGIATAN INVESTASI</strong></h5></td> 
                                    <td><h5><strong>{{number_format($total_investasi)}}</strong></h5></td>                                  
                                </tr>
                                <tr>
                                    <td></td> 
                                    <td></td>                                  
                                </tr>
                                <tr>
                                    <td><h4><strong>TOTAL<strong></h4></td> 
                                    <td><h4><strong>{{number_format($total_investasi+$total_operasi+$total_pendanaan)}}<strong></h4></td>                                  
                                </tr>
                                <tr>
                                    <td><h4><strong>Saldo Awal<strong></h4></td> 
                                    <td><h4><strong>{{number_format($kas_awal)}}<strong></h4></td>                                  
                                </tr>
                                <tr>
                                    <td><h4><strong>Saldo Akhir<strong></h4></td> 
                                    <td><h4><strong>{{number_format($kas_awal+($total_investasi+$total_operasi+$total_pendanaan))}}<strong></h4></td>                                  
                                </tr>
                            </tbody>
                        </table>                  
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeArusKas') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date" value="{{$end_date}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop