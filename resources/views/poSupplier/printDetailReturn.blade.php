@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="invoice">
                            <p>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</p>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <ul class="list-unstyled">
                                        <li> No. Faktur : {{$returPembelian->ps_no_faktur}} </li>
                                        <li> Tgl. Beli  : {{$returPembelian->pembelianSupplier->ps_tgl}} </li>
                                        <li> No. Retur  : {{$returPembelian->no_return_pembelian}}</li>
                                        <li> Tgl. Beli  : {{$returPembelian->tgl_pengembalian}}</li>
                                    </ul>
                                </div>
                                <div class="col-xs-4">
                                </div>
                                <div class="col-xs-4">
                                    <h5></h5>
                                    <ul class="list-unstyled">
                                        <li> Supplier : </li>
                                        <li> {{$returPembelian->supplier->spl_nama}} </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="font-size: 12px"><center> No </center></th>
                                                <th style="font-size: 12px"><center> Nama Barang </center></th>
                                                <th style="font-size: 12px"><center> No Seri </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Satuan </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> QTY </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Harga </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Total </center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                                                   
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="font-size: 12px" colspan="6" align="right">Grand Total</td>
                                                <td style="font-size: 12px" align="right">{{number_format($returPembelian->total_retur)}}</td>
                                            </tr>
                                        </foot>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <table class="col-md-12" width="100%">
                                        <tr>
                                            <td colspan="5" width="45%">n.b : </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" width="45%"> -  </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table class="col-md-12" width="100%">
                                        <tr><td>Denpasar</td></tr>
                                        <tr><td>GrahaKita 88</td></tr>
                                        <tr></tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your PO
                                        <i class="fa fa-check"></i>
                                    </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop