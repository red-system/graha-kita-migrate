<h4>Piutang Pelanggan (Penjualan Langsung)</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Jth. Tempo</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Sales Person</th>
                  <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Ongkos Angkut</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataPP as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no++ }}. </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $row->pl_no_faktur }} </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pp_jatuh_tempo)) }} </td>
                    <td style="font-size:12px"> {{ $row->cus_nama }} </td>
                    <td style="font-size:12px"> {{ $row->kry_nama }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->pl_disc_nom, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->pl_ongkos_angkut, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->grand_total, 2, "." ,",")  }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="6" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{number_format($DiscPP, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($AngkutPP, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($TotalPP, 2, "." ,",") }}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <h4>Piutang Pelanggan (Penjualan Titipan)</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Jth. Tempo</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Sales Person</th>
                  <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Ongkos Angkut</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataPPPT as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no_2++ }}. </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $row->pt_no_faktur }} </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pp_jatuh_tempo)) }} </td>
                    <td style="font-size:12px"> {{ $row->cus_nama }} </td>
                    <td style="font-size:12px"> {{ $row->kry_nama }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->pt_disc_nom, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->pt_ongkos_angkut, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->grand_total, 2, "." ,",")  }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="6" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{number_format($DiscPPPT, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($AngkutPPPT, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($TotalPPPT, 2, "." ,",") }}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <h4>Piutang Lain</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Total</th>
                  <th style="font-size:12px">Jth. Tempo</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataPL as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no_3++ }}. </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_jatuh_tempo)) }} </td>
                    <td style="font-size:12px"> {{ $row->pl_dari }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->pl_amount, 2, "." ,",")  }} </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_jatuh_tempo)) }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{number_format($TotalPL, 2, "." ,",") }}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
