<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use DataTables;
use Illuminate\Support\Facades\DB;
//use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem as File;
use App\Helpers\Main;
use App\Models\mBarang;
use App\Models\mKategoryProduct;
use App\Models\mGroupStok;
use App\Models\mMerek;
use App\Models\mSatuan;
use App\Models\mSupplier;
use App\Models\mGudang;
use App\Models\mStok;
use App\Models\mArusStok;
use App\Models\mStokSample;
use App\Rules\BarangKodeUniq;
use App\Models\mStokTitipan;

use Validator;

class Barang extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct()
    {
        $this->main = new Main();
        $this->title = 'Data Barang & Harga';
        $this->kodeLabel = 'barang';
    }

    function createID(Request $request)
    {
        if (strlen($request->ktg_kode) <= 3) {
            $v_ktg = str_pad($request->ktg_kode, 3, "0", STR_PAD_LEFT);
        }
        if (strlen($request->grp_kode) <= 3) {
            $v_grp = str_pad($request->grp_kode, 3, "0", STR_PAD_LEFT);
        }
        if (strlen($request->mrk_kode) <= 3) {
            $v_mrk = str_pad($request->mrk_kode, 3, "0", STR_PAD_LEFT);
        }

        $preffix = $v_ktg . '.' . $v_grp . '.' . $v_mrk . '.';
        $preffix_barcode = $request->ktg_kode . $request->grp_kode . $request->mrk_kode;
        // $lastorder = mBarang::withTrashed()->where(\DB::raw('LEFT(brg_kode, '.strlen($preffix).')'), $preffix)
        // ->selectRaw('RIGHT(brg_kode, 4) as no_order')
        // ->orderBy('no_order', 'desc')
        // ->first();
        //
        // if (!empty($lastorder)) {
        //   $now = intval($lastorder->no_order)+1;
        //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        // } else {
        //   $no = '0001';
        // }

        $lastorder = mBarang::withTrashed()->where('brg_kode', 'like', '%' . $preffix . '%')->max('brg_kode');

        if ($lastorder != null) {
            $lastno = substr($lastorder, 12, 4);
            $now = $lastno + 1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return [
            'kode' => $preffix . $no,
            'barcode' => $preffix_barcode . $no,
        ];
    }

    function test() {
        return mBarang::all();
    }

    function index()
    {
        $breadcrumb = [
            'Data Barang & Harga' => route('barangList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'barang_dan_harga';
        $data['title'] = $this->title;

        $data['dataList'] = mBarang::select('tb_barang.brg_kode', 'brg_barcode', 'brg_nama', 'ktg_nama', 'mrk_nama', 'stn_nama', 'grp_nama',
            'brg_harga_beli_terakhir', 'brg_ppn_dari_supplier_persen', 'brg_harga_beli_tertinggi',
            'brg_hpp', 'brg_harga_jual_eceran', 'brg_harga_jual_partai', 'brg_status', \DB::raw('sum(stok) as QOH'), \DB::raw('(brg_harga_beli_terakhir+(brg_harga_beli_terakhir*(brg_ppn_dari_supplier_persen/100))) AS hargaPPN'))
            ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
            ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
            ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
            ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
            ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
            ->groupBy('tb_barang.brg_kode')
            ->get();

        // $data['dataList'] = mBarang::all();
        // foreach ($data['dataList'] as &$barang) {
        //   $total=0;
        //   $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
        //   foreach ($barang['stok_gudang'] as $gdg) {
        //     $total += $gdg->stok;
        //     $gdg->gudang;
        //   }
        //   $barang['QOH'] = $total;
        //   $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen/100));
        //   $barang['hargaPPN'] = $hargaPPN;
        //   if ($hargaPPN != 0) {
        //     $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN)*100;
        //     $barang['margin'] = number_format($margin, 2) ;
        //   }
        //   else {
        //     $barang['margin'] = number_format(0, 2) ;
        //   }
        //   $barang['kategory'] = $barang->kategoryProduct;
        //   $barang['group'] = $barang->groupStok;
        //   $merek = $barang->merek;
        //   $satuan = $barang->satuan;
        //   $supplier = $barang->supplier;
        // }
        $data['k_product'] = mKategoryProduct::select('ktg_kode', 'ktg_nama')->orderBy('ktg_nama', 'asc')->get();
        $data['g_product'] = mGroupStok::select('grp_kode', 'grp_nama')->orderBy('grp_nama', 'asc')->get();
        $data['merek'] = mMerek::select('mrk_kode', 'mrk_nama')->orderBy('mrk_nama', 'asc')->get();
        $data['satuan'] = mSatuan::select('stn_kode', 'stn_nama')->orderBy('stn_nama', 'asc')->get();
        $data['supplier'] = mSupplier::select('spl_kode', 'spl_nama')->orderBy('spl_nama', 'asc')->get();
        $data['gudang'] = mGudang::select('gdg_kode', 'gdg_nama')->orderBy('gdg_nama', 'asc')->get();
        // $data['stok'] = mStok::all();

        // return $data['dataList'];

        return view('barang/barangList', $data);
    }

    function rules($request)
    {
        $rules = [
            'brg_kode' => new BarangKodeUniq,
            'brg_nama' => 'required',
            'mrk_kode' => 'required',
            'ktg_kode' => 'required',
            'stn_kode' => 'required',
            'grp_kode' => 'required',
            // 'spl_kode' => 'required',
            'brg_stok_maximum' => 'required',
            'brg_stok_minimum' => 'required',
            'brg_product_img' => '',
            'brg_harga_beli_terakhir' => 'required',
            'brg_harga_beli_tertinggi' => 'required',
            'brg_hpp' => 'required',
            'brg_harga_jual_eceran' => 'required',
            'brg_harga_jual_partai' => 'required',
            'brg_ppn_dari_supplier_persen' => 'required',
            'brg_status' => ''
        ];
        $customeMessage = [
            'required' => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function rulesStok($request)
    {
        $rules = [
            'brg_kode' => 'required',
            'gdg_kode' => 'required',
            'stok' => 'required'
        ];
        $customeMessage = [
            'required' => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request)
    {
        \DB::beginTransaction();
        try {
            // $this->rules($request->all());
            $barang = new mBarang();
            $barang->brg_kode = $request->brg_kode;
            $barang->brg_barcode = $request->brg_barcode;
            $barang->brg_nama = $request->brg_nama;
            $barang->mrk_kode = $request->mrk_kode;
            $barang->ktg_kode = $request->ktg_kode;
            $barang->stn_kode = $request->stn_kode;
            $barang->grp_kode = $request->grp_kode;
            $barang->spl_kode = $request->spl_kode;
            $barang->brg_stok_maximum = $request->brg_stok_maximum;
            $barang->brg_stok_minimum = $request->brg_stok_minimum;
            $barang->brg_harga_beli_terakhir = $request->brg_harga_beli_terakhir;
            $barang->brg_harga_beli_tertinggi = $request->brg_harga_beli_tertinggi;
            $barang->brg_hpp = $request->brg_hpp;
            $barang->brg_harga_jual_eceran = $request->brg_harga_jual_eceran;
            $barang->brg_harga_jual_partai = $request->brg_harga_jual_partai;
            $barang->brg_ppn_dari_supplier_persen = $request->brg_ppn_dari_supplier_persen;
            $barang->gdg_kode = $request->gdg_kode;
            $barang->brg_harga_beli_ppn = ($request->brg_harga_beli_terakhir * ($request->brg_ppn_dari_supplier_persen / 100)) + $request->brg_harga_beli_terakhir;
            if ($request->hasfile('brg_product_img')) {
                $file = $request->file('brg_product_img');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('img/barang/', $filename);
                $path = 'img/barang/' . $filename;
                $barang->brg_product_img = $path;
            }
            if ($request->brg_status == null) {
                $barang->brg_status = "Tidak Aktif";
            } else {
                $barang->brg_status = $request->brg_status;
            }
            $barang->save();

            // mBarang::insert($request->except('_token', 'stok', 'gdg_kode'));

            \DB::commit();
            // return redirect()->back();
            return [
                'redirect' => route('barangList')
            ];
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    function edit($brg_kode = '')
    {
        $response = [
            'action' => route('barangUpdate', ['kode' => $brg_kode]),
            'field' => mBarang::find($brg_kode)
        ];
        return $response;
    }

    function update(Request $request, $brg_kode)
    {
        // $this->rules($request->all());
        \DB::beginTransaction();
        try {
            $barang = mBarang::where('brg_kode', $brg_kode)->first();
            $barang->brg_kode = $request->brg_kode;
            $barang->brg_barcode = $request->brg_barcode;
            $barang->brg_nama = $request->brg_nama;
            $barang->mrk_kode = $request->mrk_kode;
            $barang->ktg_kode = $request->ktg_kode;
            $barang->stn_kode = $request->stn_kode;
            $barang->grp_kode = $request->grp_kode;
            $barang->spl_kode = $request->spl_kode;
            $barang->brg_stok_maximum = $request->brg_stok_maximum;
            $barang->brg_stok_minimum = $request->brg_stok_minimum;
            $barang->brg_harga_beli_terakhir = $request->brg_harga_beli_terakhir;
            $barang->brg_harga_beli_tertinggi = $request->brg_harga_beli_tertinggi;
            $barang->brg_hpp = $request->brg_hpp;
            $barang->brg_harga_jual_eceran = $request->brg_harga_jual_eceran;
            $barang->brg_harga_jual_partai = $request->brg_harga_jual_partai;
            $barang->brg_ppn_dari_supplier_persen = $request->brg_ppn_dari_supplier_persen;
            $barang->gdg_kode = $request->gdg_kode;
            $barang->brg_harga_beli_ppn = ($request->brg_harga_beli_terakhir * ($request->brg_ppn_dari_supplier_persen / 100)) + $request->brg_harga_beli_terakhir;
            if ($request->hasfile('brg_product_img')) {
                if ($barang->brg_product_img != null) {
                    $fileSystem = new File;
                    if ($fileSystem->exists(public_path($barang->brg_product_img))) {
                        $fileSystem->delete(public_path($barang->brg_product_img));
                    }
                }
                $file = $request->file('brg_product_img');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('img/barang/', $filename);
                $path = 'img/barang/' . $filename;
                $barang->brg_product_img = $path;
            }
            if ($request->brg_status == null) {
                $barang->brg_status = "Tidak Aktif";
            } else {
                $barang->brg_status = $request->brg_status;
            }
            $barang->save();

            // mBarang::where('brg_kode', $brg_kode)->update($request->except('_token', 'stok', 'gdg_kode'));
            // return redirect()->back();
            \DB::commit();

            return [
                'redirect' => route('barangList')
            ];
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    function delete($brg_kode)
    {
        // mBarang::where('brg_kode', $brg_kode)->delete();
        \DB::beginTransaction();
        try {
            $findData = mBarang::findOrFail($brg_kode);
            $stok = mStok::where('brg_kode', $brg_kode)->delete();
            if ($findData != null) {
                $fileSystem = new File;
                $fileSystem->delete(public_path($findData->brg_product_img));
            }
            $hapus = $findData->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    function insertStok(Request $request)
    {
        // $this->rulesStok($request->all());
        // $simpan = mStok::create(['brg_kode' => $request->brg_kode, 'gdg_kode' => $request->gdg_kode , 'stok' => $request->stok]);

        \DB::beginTransaction();
        try {
            $barang = mBarang::findOrFail($request->brg_kode);
            $total = 0;
            $data = mStok::where('brg_kode', $request->brg_kode)->get();
            foreach ($data as $gdg) {
                $total += $gdg->stok;
            }
            $QOH = $total;
            if (($QOH + $request->stok) != 0) {
                $barang->brg_hpp = (($barang->brg_hpp * $QOH) + ($request->stk_hpp * $request->stok)) / ($QOH + $request->stok);
            }
            // $barang->brg_hpp = (($barang->brg_hpp*$QOH)+($request->stk_hpp*$request->stok))/($QOH+$request->stok);
            $barang->save();

            $Stok = mStok::where('brg_kode', $request->brg_kode)
                ->where('gdg_kode', $request->gdg_kode)
                ->where('spl_kode', $request->spl_kode)
                ->where('brg_no_seri', $request->brg_no_seri)
                ->first();
            if ($Stok != null) {
                $Stok = mStok::where('brg_kode', $request->brg_kode)
                    ->where('gdg_kode', $request->gdg_kode)
                    ->where('spl_kode', $request->spl_kode)
                    ->where('brg_no_seri', $request->brg_no_seri)
                    ->first();
                $Stok->stok = ($Stok->stok + $request->stok);
                $Stok->stk_hpp = ($Stok->stk_hpp + $request->stk_hpp);
                $Stok->save();
            } else {
                $Stok = new mStok();
                $Stok->brg_kode = $request->brg_kode;
                $Stok->gdg_kode = $request->gdg_kode;
                $Stok->spl_kode = $request->spl_kode;
                $Stok->stok = $request->stok;
                $Stok->brg_no_seri = $request->brg_no_seri;
                $Stok->stk_hpp = $request->stk_hpp;
                $Stok->save();
            }

            $time = Carbon::now();
            $arusStok = new mArusStok();
            $arusStok->ars_stok_date = $time;
            $arusStok->brg_kode = $request->brg_kode;
            $arusStok->stok_in = $request->stok;
            $arusStok->stok_out = 0;
            $arusStok->stok_prev = $QOH + $request->stok;
            $arusStok->gdg_kode = $request->gdg_kode;
            $arusStok->keterangan = 'Insert Stok Barang';
            $arusStok->save();

            \DB::commit();
            return $Stok;
            // return redirect()->back();
            // return [
            //     'redirect'=>route('barangList')
            // ];
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    function getStok($brg_kode = '')
    {
        $response = mBarang::leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
            ->leftJoin('tb_gudang', 'tb_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
            ->leftJoin('tb_supplier', 'tb_stok.spl_kode', '=', 'tb_supplier.spl_kode')
            ->select('stk_kode', 'tb_stok.brg_no_seri', 'tb_stok.brg_kode', 'tb_stok.gdg_kode', 'gdg_nama', 'spl_nama', 'stok', 'stok_titipan', 'tb_stok.spl_kode')
            ->selectRaw('(stok) as QOH, (stok_titipan) as Titipan')
            // ->selectRaw('sum(stok) as QOH, sum(stok_titipan) as Titipan')
            ->where('tb_barang.brg_kode', $brg_kode)
            ->whereNull('tb_stok.deleted_at')
            // ->whereNotNull('tb_stok.gdg_kode')
            // ->groupBy('tb_stok.brg_no_seri', 'tb_stok.spl_kode', 'tb_stok.gdg_kode')
            ->get();

        // $response = $response->filter(function ($value, $key) {
        //   return $value->QOH > 0;
        // });

        $response->all();

        $data = [];
        foreach ($response as $key => $value) {
            $data[] = $value;
        }
        return $data;
    }

    function editStok($brg_kode = '')
    {
        $response = [
            'action' => route('stokbarangInsert'),
            'field' => mBarang::where('brg_kode', $brg_kode)->select('brg_kode', 'gdg_kode', 'spl_kode')->first(),
            // 'data_barang' =>mBarang::leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
            // ->leftJoin('tb_gudang', 'tb_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
            // ->select('gdg_nama', 'stok')
            // ->where('tb_barang.brg_kode', $brg_kode)
            // ->get()
        ];
        return $response;
    }

    function updateStok(Request $request, $brg_kode)
    {
        // $this->rules($request->all());

        // $stokBarang = mStok::where('brg_kode', $brg_kode)->where('gdg_kode', $gdg_kode)->first();
        // $stokBarang->brg_kode = $request->brg_kode;
        // $stokBarang->gdg_kode = $request->gdg_kode;
        // $stokBarang->stok = $request->stok;
        // $stokBarang->save();

        return [
            'redirect' => route('barangList')
        ];
    }

    function deleteStok($stk_kode = '')
    {
        $stok = mStok::where('stk_kode', $stk_kode)->first();
        $stok->stok = 0;
        $stok->stok_titipan = 0;
        $stok->save();
        $stok = mStok::where('stk_kode', $stk_kode)->delete();

        return redirect()->back();
    }

    function StokSample(Request $request)
    {
        // return $request;
        \DB::beginTransaction();
        try {
            $stok = mStok::where('stk_kode', $request->stk_kode_sample)->first();
            $stok->stok = ($stok->stok - $request->qty_sample);
            if ($stok->stok <= 0) {
                $stok->stok = 0;
            }
            $stok->save();

            $brg = mBarang::where('brg_kode', $request->brg_kode_sample)->first();

            $stokSample = new mStokSample();
            $stokSample->brg_no_seri = $request->brg_no_seri_sample;
            $stokSample->brg_kode = $request->brg_kode_sample;
            $stokSample->brg_barcode = $brg->brg_barcode;
            $stokSample->gdg_kode = $request->gdg_kode_sample;
            $stokSample->spl_kode = $request->spl_kode_sample;
            $stokSample->qty = $request->qty_sample;
            $stokSample->save();

            $total = 0;
            $data = mStok::where('brg_kode', $request->brg_kode_sample)->get();
            foreach ($data as $gdg) {
                $total += $gdg->stok;
            }
            $QOH = $total;

            $time = Carbon::now();
            $arusStok = new mArusStok();
            $arusStok->ars_stok_date = $time;
            $arusStok->brg_kode = $request->brg_kode_sample;
            $arusStok->stok_in = 0;
            $arusStok->stok_out = $request->qty_sample;
            $arusStok->stok_prev = $QOH;
            $arusStok->gdg_kode = $request->gdg_kode_sample;
            $arusStok->keterangan = 'Stok Sample ' . $stokSample->stok_sample_kode;
            $arusStok->save();

            \DB::commit();

            return redirect()->back();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    // function barangPrint(Request $request) {
    //   return $request;
    //     $breadcrumb = [
    //         'Transfer Stok'=>route('barangList'),
    //     ];
    //     $data = $this->main->data($breadcrumb, $this->kodeLabel);
    //     $data['menu'] = 'barang';
    //     $data['title'] = $this->title;
    //     $data['dataList'] = mBarang::all();
    //     $time = date('Y-m-d');
    //     $data['time'] = $time;
    //     foreach ($data['dataList'] as &$barang) {
    //       $total=0;
    //       $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
    //       foreach ($barang['stok_gudang'] as $gdg) {
    //         $total += $gdg->stok;
    //         $gdg->gudang;
    //       }
    //       $barang['QOH'] = $total;
    //       $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen/100));
    //       $barang['hargaPPN'] = $hargaPPN;
    // if ($hargaPPN != 0) {
    //   $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN)*100;
    //   $barang['margin'] = number_format($margin, 2) ;
    // }
    // else {
    //   $barang['margin'] = number_format(0, 2) ;
    // }
    //       $barang['kategory'] = $barang->kategoryProduct;
    //       $barang['group'] = $barang->groupStok;
    //       $merek = $barang->merek;
    //       $satuan = $barang->satuan;
    //       $supplier = $barang->supplier;
    //     }
    //     if ($request->group_type == 'Merek') {
    //       $data['dataList'] = $data['dataList']->where('mrk_kode', $request->type);
    //     }
    //     elseif ($request->group_type == 'Kategory') {
    //       $data['dataList'] = $data['dataList']->where('ktg_kode', $request->type);
    //     }
    //     elseif ($request->group_type == 'Group') {
    //       $data['dataList'] = $data['dataList']->where('grp_kode', $request->type);
    //     }
    //     elseif ($request->group_type == 'Supplier') {
    //       $data['dataList'] = $data['dataList']->where('spl_kode', $request->type);
    //     }
    //     else {
    //       $data['dataList'] = $data['dataList'];
    //     }
    //     // return $data;
    //     return view('barang/barangPrint', $data);
    // }

    function getGroup($group = '')
    {
        if ($group == 'Merek') {
            $response = mMerek::pluck('mrk_nama', 'mrk_kode');
        } elseif ($group == 'Kategory') {
            $response = mKategoryProduct::pluck('ktg_nama', 'ktg_kode');
        } elseif ($group == 'Group') {
            $response = mGroupStok::pluck('grp_nama', 'grp_kode');
        } elseif ($group == 'Supplier') {
            $response = mSupplier::pluck('spl_nama', 'spl_kode');
        } else {
            $response = false;
        }

        return json_encode($response);

        // return response()->json([
        //     'group' => $response
        // ]);

        // return $response;
    }

    function barangPrint(Request $request)
    {
        // return $request;
        $breadcrumb = [
            'Transfer Stok' => route('barangList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'barang_dan_harga';
        $data['title'] = $this->title;
        $time = date('Y-m-d');
        $data['time'] = $time;
        $data['dataList'] = mBarang::all();
        if ($request->type == 'General') {
            foreach ($data['dataList'] as &$barang) {
                $qty_total = 0;
                $total = 0;
                if ($request->spl_kode != 0) {
                    $barang['stok'] = mStok::select('brg_no_seri', 'tb_stok.spl_kode', 'tb_supplier.spl_nama', \DB::raw('SUM(stok) as total'))
                        ->leftJoin('tb_supplier', 'tb_stok.spl_kode', '=', 'tb_supplier.spl_kode')
                        ->where('brg_kode', $barang->brg_kode)
                        ->where('tb_stok.spl_kode', $request->spl_kode)
                        ->groupBy('brg_kode', 'brg_no_seri')
                        ->get();

                    foreach ($barang['stok'] as $key => $value) {
                        $qty_total += $value->total;
                    }
                    $barang['QOH'] = $qty_total;
                } else {
                    $barang['stok'] = mStok::select('brg_no_seri', 'tb_stok.spl_kode', 'tb_supplier.spl_nama', \DB::raw('SUM(stok) as total'))
                        ->leftJoin('tb_supplier', 'tb_stok.spl_kode', '=', 'tb_supplier.spl_kode')
                        ->where('brg_kode', $barang->brg_kode)
                        ->groupBy('brg_kode', 'brg_no_seri')
                        ->get();

                    foreach ($barang['stok'] as $key => $value) {
                        $qty_total += $value->total;
                    }
                    $barang['QOH'] = $qty_total;
                }
                $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen / 100));
                $barang['hargaPPN'] = $hargaPPN;
                if ($hargaPPN != 0) {
                    $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN) * 100;
                    $barang['margin'] = number_format($margin, 2);
                } else {
                    $barang['margin'] = number_format(0, 2);
                }
                $barang['kategory'] = $barang->kategoryProduct;
                $barang['group'] = $barang->groupStok;
                $merek = $barang->merek;
                $satuan = $barang->satuan;
                $supplier = $barang->supplier;
            }
            if ($request->ktg_kode != 0) {
                $data['dataList'] = $data['dataList']->where('ktg_kode', $request->ktg_kode);
            }
            if ($request->grp_kode != 0) {
                $data['dataList'] = $data['dataList']->where('grp_kode', $request->grp_kode);
            }
            if ($request->mrk_kode != 0) {
                $data['dataList'] = $data['dataList']->where('mrk_kode', $request->mrk_kode);
            }
            if ($request->spl_kode != 0) {
                $data['dataList'] = $data['dataList']->where('spl_kode', $request->spl_kode);
            }

            // return $data['dataList'];

            return view('barang/barangPrint', $data);

            $pdf = PDF::loadView('barang.barangPrint', $data)
                ->setPaper('a4', 'landscape');
            return $pdf->stream();
        } else {
            foreach ($data['dataList'] as &$barang) {
                $total = 0;
                if ($request->spl_kode != 0) {
                    $barang['stok'] = mStok::where('brg_kode', $barang->brg_kode)
                        ->where('spl_kode', $request->spl_kode)
                        ->get();
                } else {
                    $barang['stok'] = mStok::where('brg_kode', $barang->brg_kode)
                        ->get();
                }
                // $barang['stok'] = mStok::where('gdg_kode', $barang->brg_kode)
                // ->where('spl_kode', $request->spl_kode)
                // ->get();
                foreach ($barang['stok'] as $key) {
                    $key->gudang;
                    $key->supplier;
                }

                $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen / 100));
                $barang['hargaPPN'] = $hargaPPN;
                if ($hargaPPN != 0) {
                    $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN) * 100;
                    $barang['margin'] = number_format($margin, 2);
                } else {
                    $barang['margin'] = number_format(0, 2);
                }
                $barang['kategory'] = $barang->kategoryProduct;
                $barang['group'] = $barang->groupStok;
                $merek = $barang->merek;
                $satuan = $barang->satuan;
                $supplier = $barang->supplier;
            }
            if ($request->ktg_kode != 0) {
                $data['dataList'] = $data['dataList']->where('ktg_kode', $request->ktg_kode);
            }
            if ($request->grp_kode != 0) {
                $data['dataList'] = $data['dataList']->where('grp_kode', $request->grp_kode);
            }
            if ($request->mrk_kode != 0) {
                $data['dataList'] = $data['dataList']->where('mrk_kode', $request->mrk_kode);
            }
            if ($request->spl_kode != 0) {
                $data['dataList'] = $data['dataList']->where('spl_kode', $request->spl_kode);
            }
        }

        // return $data['dataList'];
        return view('barang/barangPrintDetail', $data);

        $pdf = PDF::loadView('barang.barangPrintDetail', $data)
            ->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function DataTableBarang()
    {
        // $data = mBarang::select('tb_barang.brg_kode', 'brg_barcode', 'brg_nama', 'ktg_nama', 'mrk_nama', 'stn_nama', 'grp_nama',
        // 'brg_harga_beli_terakhir', 'brg_ppn_dari_supplier_persen', 'brg_harga_beli_tertinggi',
        // 'brg_hpp', 'brg_harga_jual_eceran', 'brg_harga_jual_partai', 'brg_status', \DB::raw('sum(stok) as QOH'), \DB::raw('(brg_harga_beli_terakhir+(brg_harga_beli_terakhir*(brg_ppn_dari_supplier_persen/100))) AS hargaPPN'))
        // ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
        // ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
        // ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
        // ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
        // ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
        // ->groupBy('tb_barang.brg_kode')
        // ->get();

        $data = mBarang::select('tb_barang.brg_kode', 'brg_barcode', 'brg_nama', 'ktg_nama', 'mrk_nama', 'stn_nama', 'grp_nama', 'spl_nama',
                'brg_harga_beli_terakhir', 'brg_ppn_dari_supplier_persen', 'brg_harga_beli_tertinggi',
                'brg_hpp', 'brg_harga_jual_eceran', 'brg_harga_jual_partai', 'brg_status', DB::raw('sum(stok) as QOH'), DB::raw('sum(stok_titipan) as QOH_titipan'), DB::raw('brg_harga_beli_ppn AS hargaPPN'))
            ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
            ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
            ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
            ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
            ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
            ->leftJoin('tb_supplier', 'tb_barang.spl_kode', '=', 'tb_supplier.spl_kode')
            ->groupBy('tb_barang.brg_kode')
            ->get();

//        return $data;

        $no = 1;
//        $data_new = [];
//        foreach ($data as $key => $row) {
//            $new = array_merge((array) $row, [
//                'no' => $no++,
//                'retailPPN' => (($row->brg_harga_jual_partai * ($row->brg_ppn_dari_supplier_persen / 100)) + $row->brg_harga_jual_partai),
//                'eceranPPN' => (($row->brg_harga_jual_eceran * ($row->brg_ppn_dari_supplier_persen / 100)) + $row->brg_harga_jual_eceran)
//            ]);
//            $data_new[$key] = (object) $new;
////            $data_new[$key] = [];
////            $data_new[$key]['no'] = $no++;
////            $data_new[$key]['retailPPN'] = (($row->brg_harga_jual_partai * ($row->brg_ppn_dari_supplier_persen / 100)) + $row->brg_harga_jual_partai);
////            $data_new[$key]['eceranPPN'] = (($row->brg_harga_jual_eceran * ($row->brg_ppn_dari_supplier_persen / 100)) + $row->brg_harga_jual_eceran);
//        }
//
//        return $data_new;

        foreach($data as &$barang) {
            $barang['no'] = $no++;
            $barang['retailPPN'] = (($barang->brg_harga_jual_partai * ($barang->brg_ppn_dari_supplier_persen / 100)) + $barang->brg_harga_jual_partai);
            $barang['eceranPPN'] = (($barang->brg_harga_jual_eceran * ($barang->brg_ppn_dari_supplier_persen / 100)) + $barang->brg_harga_jual_eceran);
        }

        return Datatables::of($data)->make(true);
    }

    function Reject(Request $request)
    {
        // return $request;
        \DB::beginTransaction();
        try {
            $stok = mStok::where('stk_kode', $request->stk_kode_reject)->first();
            $stok->stok = ($stok->stok - $request->qty_reject);
            if ($stok->stok <= 0) {
                $stok->stok = 0;
            }
            $stok->save();

            $reject = mStok::where('brg_kode', $stok->brg_kode)->where('gdg_kode', $stok->gdg_kode)->where('spl_kode', $stok->spl_kode)->where('brg_no_seri', 'reject')->first();
            if ($reject == null) {
                $Stok = new mStok();
                $Stok->brg_kode = $stok->brg_kode;
                $Stok->gdg_kode = $stok->gdg_kode;
                $Stok->spl_kode = $stok->spl_kode;
                $Stok->stok = $request->qty_reject;
                $Stok->brg_no_seri = 'reject';
                $Stok->stk_hpp = 0;
                $Stok->save();
            } else {
                $reject->stok = $reject->stok + $request->qty_reject;
                $reject->save();
            }

            \DB::commit();
            return redirect()->back();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    function getStokTitipan($brg_kode = '')
    {
        // $response = mBarang::leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
        // ->leftJoin('tb_gudang', 'tb_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
        // ->leftJoin('tb_supplier', 'tb_stok.spl_kode', '=', 'tb_supplier.spl_kode')
        // ->select('stk_kode', 'tb_stok.brg_no_seri', 'tb_stok.brg_kode', 'tb_stok.gdg_kode', 'gdg_nama', 'spl_nama', 'stok', 'stok_titipan')
        // ->selectRaw('(stok) as QOH, (stok_titipan) as Titipan')
        // // ->selectRaw('sum(stok) as QOH, sum(stok_titipan) as Titipan')
        // ->where('tb_barang.brg_kode', $brg_kode)
        // ->whereNull('tb_stok.deleted_at')
        // // ->whereNotNull('tb_stok.gdg_kode')
        // // ->groupBy('tb_stok.brg_no_seri', 'tb_stok.spl_kode', 'tb_stok.gdg_kode')
        // ->get();

        $response = mStokTitipan::join('tb_barang', 'tb_barang.brg_kode', '=', 'tb_detail_penjualan_titipan.brg_kode')->join('tb_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur', '=', 'tb_detail_penjualan_titipan.pt_no_faktur')->join('tb_customer', 'tb_customer.cus_kode', '=', 'tb_penjualan_titipan.cus_kode')->where('tb_detail_penjualan_titipan.brg_kode', $brg_kode)->whereRaw('tb_detail_penjualan_titipan.qty > tb_detail_penjualan_titipan.terkirim')->get();
        // $response = mStokTitipan::select('tb_penjualan_titipan.pt_no_faktur','tb_customer.cus_nama','tb_detail_penjualan_titipan.brg_kode','tb_barang.nama_barang','tb_detail_penjualan_titipan.brg_no_seri','tb_detail_penjualan_titipan.qty','tb_detail_penjualan_titipan.terkirim')->join('tb_barang','tb_barang.brg_kode','=','tb_detail_penjualan_titipan.brg_kode')->join('tb_penjualan_titipan','tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')->join('tb_customer','tb_customer.cus_kode','=','tb_penjualan_titipan.cus_kode')->where('tb_detail_penjualan_titipan.brg_kode',$brg_kode)->whereRaw('tb_detail_penjualan_titipan.qty > tb_detail_penjualan_titipan.terkirim')->get();

        //dd($response);

        // $response = $response->filter(function ($value, $key) {
        //   return $value->QOH > 0;
        // });

        $response->all();

        $data = [];
        foreach ($response as $key => $value) {
            $data[] = $value;
        }
        return $data;
    }
}
