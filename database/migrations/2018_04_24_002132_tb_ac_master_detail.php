<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAcMasterDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_ac_master_detail', function (Blueprint $table) {
           $table->increments('master_detail_id');
           $table->integer('master_id')->nullable();
           $table->bigInteger('msd_year')->nullable();
           $table->tinyInteger('msd_month')->nullable();
           $table->bigInteger('msd_awal_kredit')->nullable();
           $table->bigInteger('msd_awal_debet')->nullable();
           $table->datetime('msd_date_insert')->nullable();
           $table->datetime('msd_date_update')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_ac_master_detail');
     }
}
