@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                            Pilih Periode
                        </button>
                        <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah-jurnal-umum">
                            Tambah Jurnal Umum
                        </button>
                        <br /><br />
                        <table class="table table-striped table-bordered table-header-fixed " id="sample_1">
                            <thead>
                                <tr class="">
                                    <th width="10"><center> No </center></th>
                                    <th><center> Tanggal </center></th>
                                    <th><center> No Bukti </center></th>
                                    <th><center> Keterangan </center></th>
                                    <th><center> No Akun </center></th>
                                    <th><center> Debet </center></th>
                                    <th><center> Kredit </center></th>
                                    <th><center> Catatan </center></th>
                                    <th><center> Menu </center></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr class="">
                                    <th width="10"></th>
                                    <th colspan="4" align="center"><h4><center><strong> TOTAL </strong></center></h4></th>
                                    <th> <h4><center><strong>{{number_format($jml_debet)}} </strong></center></h4></th>
                                    <th> <h4><center><strong>{{number_format($jml_kredit)}} </th>
                                    @if($jml_debet==$jml_kredit)
                                    <th>
                                       <h4><strong>Status : <font color="green">Balance</font></strong></h4>
                                    </th>
                                    @endif
                                    @if($jml_debet!=$jml_kredit)
                                    <th>
                                       <h4><strong>Status : <font color="red">Not Balance</font></strong></h4>
                                    </th>
                                    @endif
                                    <th> </th>
                                </tr>
                            </tfoot>
                            <tbody>
                            @foreach($jurnalUmum as $jmu)
                                <tr>
                                    <td align="center"> {{ $no++ }}. </td>
                                    <td> {{ date('d M Y', strtotime($jmu->jmu_date_insert)) }} </td>
                                    <td><center> {{ $jmu->kode_bukti_id }} </center></td>
                                    <td> {{ $jmu->jmu_keterangan }}</td>
                                    <td colspan="5"></td>
                                </tr>
                                @foreach($jmu->transaksi as $trs)
                                <tr>                                
                                    <td></td>
                                    <td></td>
                                    <td></td>                                
                                    <td>   {{ $trs->trs_nama_rekening }}</td>
                                    <td><center>   {{ $trs->trs_kode_rekening }}</center></td>
                                    <td align="right">   {{ number_format($trs->trs_debet) }} </td>
                                    <td align="right">   {{ number_format($trs->trs_kredit) }} </td>
                                    <td>   {{ $trs->trs_catatan }} </td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            <button class="btn btn-success btn-edit" data-href="{{ route('chequeBgEdit', ['kode'=>$jmu->jurnal_umum_id]) }}">
                                                <span class="icon-pencil"></span>
                                            </button>
                                            <button class="btn btn-danger btn-delete" data-href="{{ route('chequeBgDelete', ['kode'=>$jmu->jurnal_umum_id]) }}">
                                                <span class="icon-trash"></span>
                                            </button>
                                        </div>
                                    </td>                                    
                                </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode Akuntansi
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('chequeBgInsert') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Tanggal</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Tahun</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" />
                            </div>
                        </div>    
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal draggable-modal" id="modalPaymentHutangLain" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Payment </h4>
          </div>
          <div class="modal-body form-horizontal">
            <form class="form-send" action="{{ route('hutangLainInsertPayment') }}" method="post" role="form">
                {{ csrf_field() }}
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-4">Tanggal Transaksi</label>
                    <div class="col-md-4">
                      <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_transaksi" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">No Invoice</label>
                    <div class="col-md-4">
                      <input type="text" name="hl_kode" class="form-control" value="0">
                      <input type="text" name="kode_perkiraan" class="form-control" value="0">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-4" style="margin-top: -20px">
                      <br>
                        <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal"> 
                        <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                        </button>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                    <thead>
                        <tr>
                            <th>Kode Perkiraan</th>
                            <th>Payment</th>
                            <th>Charge(%)</th>
                            <th>Charge Nom.</th>
                            <th>Total</th>
                            <th>No. Cek/BG</th>
                            <th>Tanggal Pencairan</th>
                            <th>Keterangan</th>
                            <th>Setor</th>
                            <th>Kembalian</th>
                            <th>Menu</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <br />
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h1>Amount</h1>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h1 class="nominal-grand-total" name="nominal-grand-total" id="nominal-grand-total"></h1>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h1>Sisa</h1>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <h1 class="nominal-sisa">0</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-md-offset-8">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-success btn-lg">SAVE</button>
                                <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
      </div>
    </div>
    <table class="table-row-payment hide">
    <tbody>
      <tr>
        <td>
          <select name="master_id[]" class="form-control">
            <option></option>
          </select>
        </td>
        <td class="payment">
          <input type="number" name="payment[]" class="form-control" value="0">
        </td>
        <td class="charge">
          <input type="number" name="charge[]" class="form-control" value="0">
        </td>
        <td class="charge_nom"></td>
        <td class="payment_total">
          <input type="number" name="payment_total[]" class="form-control" value="0" readonly>
        </td>
        <td>
          <input type="text" name="no_check_bg[]" class="form-control">
        </td>
        <td>
          <input type="text" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="{{ date('Y-m-d') }}">
        </td>
        <td>
          <input type="text" name="keterangan[]" class="form-control">
        </td>
        <td class="setor">
          <input type="number" name="setor[]" class="form-control" value="0">
        </td>
        <td class="kembalian">
          -
        </td>
        <td>
          <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<div class="modal draggable-modal" id="modal-tambah-jurnal-umum" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> New BG/Cheque
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('chequeBgInsert') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">No BG/Cek</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal Pencairan</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Amount</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" name="cek_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dari</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="cek_dari">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="cek_keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop