<html moznomarginboxes mozdisallowselectionprint>
    <head>
    <!-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> -->
    </head>
    <body>
    <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000000;color:#000000;background-color:#fff;}
                .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000000;color:#000000;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <div class="container-fluid">
    <h2 style="text-align:justify;">   
            <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
        </h2>
        <hr>
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                <h2><center><strong>LAPORAN ARUS KAS</strong></center></h2>
                        <h3><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h3>
          </div>
          <br>
          <div class="portlet light ">
            <table class="tg" width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>I. ARUS KAS DARI KEGIATAN OPERASI</strong></td>                                   
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                </tr>
                                <?php
                                    $total_operasi=0;
                                    $total_pendanaan=0;
                                    $total_investasi=0;
                                ?>
                                @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach                             
                                
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                </tr>
                                @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Operasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_operasi=$total_operasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach 
                                <tr>
                                    <td><h5><strong>TOTAL KEGIATAN OPERASI</strong></h5></td> 
                                    <td><h5><strong>{{number_format($total_operasi)}}</strong></h5></td>                                  
                                </tr>
                                <tr>
                                    <td></td> 
                                    <td></td>                                  
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>II. ARUS KAS DARI KEGIATAN PENDANAAN</strong></td>                                   
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                </tr>
                                
                                @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                </tr>
                                @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_pendanaan=$total_pendanaan-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td><h5><strong>TOTAL KEGIATAN PENDANAAN</strong></h5></td> 
                                    <td><h5><strong>{{number_format($total_pendanaan)}}</strong></h5></td>                                  
                                </tr>
                                <tr>
                                    <td></td> 
                                    <td></td>                                  
                                </tr>
                                
                                <tr>
                                    <td colspan="2"><strong>III. ARUS KAS DARI KEGIATAN INVESTASI</strong></td>                                   
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                </tr>
                                
                                @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='debet')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi+$trs->trs_debet;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                </tr>
                                @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Investasi') as $trs)
                                    @if($trs->trs_jenis_transaksi=='kredit')
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                    </tr>
                                    <?php
                                        $total_investasi=$total_investasi-$trs->trs_kredit;
                                    ?>
                                    @endif
                                @endforeach
                                <tr>
                                    <td><h5><strong>TOTAL KEGIATAN INVESTASI</strong></h5></td> 
                                    <td><h5><strong>{{number_format($total_investasi)}}</strong></h5></td>                                  
                                </tr>
                                <tr>
                                    <td></td> 
                                    <td></td>                                  
                                </tr>
                                <tr>
                                    <td><h4><strong>TOTAL<strong></h4></td> 
                                    <td><h4><strong>{{number_format($total_investasi+$total_operasi+$total_pendanaan)}}<strong></h4></td>                                  
                                </tr>
                                <tr>
                                    <td><h4><strong>Saldo Awal<strong></h4></td> 
                                    <td><h4><strong>{{number_format($kas_awal)}}<strong></h4></td>                                  
                                </tr>
                                <tr>
                                    <td><h4><strong>Saldo Akhir<strong></h4></td> 
                                    <td><h4><strong>{{number_format($kas_awal+($total_investasi+$total_operasi+$total_pendanaan))}}<strong></h4></td>                                  
                                </tr>
                            </tbody>
                        </table> 
          </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
