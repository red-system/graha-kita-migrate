@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css"> --}}
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/daftarPenjualanLangsung.js') }}" type="text/javascript"></script>
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script> --}}
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script> --}}
@stop

@section('body')
  <form class="form-send-penjualan" action="{{route('updatePenjualanLangsung')}}" method="put">
  {{-- <form class="form-send-penjualan" action="{{ route('ReturPenjualanInsert') }}" method="post"> --}}
    {{ csrf_field() }}
    <div class="page-content-inner">
      <div class="mt-content-body">
        <div class="row">
          <div class="col-xs-12">
            <div class="portlet light ">
              <div class="row form-horizontal">
                <div class="col-xs-12 col-sm-6 ">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">No Faktur</label>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control" name="no_faktur" value="{{$no_faktur}}">
                        <input type="text" class="form-control" placeholder="No Faktur" value="{{ $kodeLabel.$no_faktur }}" readonly>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data ">
                <thead>
                  <tr class="">
                    <th> Kode Barang </th>
                    <th> Nama Barang </th>
                    <th> Barang Seri </th>
                    <th> Qty </th>
                    <th width="10%"> Qty Retur </th>
                    <th> Harga </th>
                    <th width="10%"> Total </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td>
                        <input class="form-control" type="hidden" name="brg_kode[]" value="{{ $row->brg_kode }}">
                        {{ $barang_label.$row->brg_kode }}
                      </td>
                      <td>
                        <input class="form-control" type="hidden" name="nama_barang[]" value="{{ $row->nama_barang }}">
                        {{ $row->nama_barang }}
                      </td>
                      <td>
                        <input class="form-control" type="hidden" name="gudang[]" value="{{$row->gudang}}" readonly>

                        <input class="form-control" type="hidden" name="brg_no_seri[]" value="{{ $row->brg_no_seri }}">
                        {{ $row->brg_no_seri }}
                      </td>
                      <td>
                        <input class="form-control" type="hidden" name="qty[]" value="{{ $row->qty }}">
                        {{ $row->qty }}
                      </td>
                      <td>
                        <input class="form-control" type="number" min="0" name="qty_retur[]" value="0">
                      </td>
                      <td>
                        <input class="form-control" type="hidden" min="0" name="harga_net[]" value="{{ $row->harga_net }}">
                        {{number_format($row->harga_net, 0, "." ,".")}}
                      </td>
                      <td>
                        <input class="form-control" type="number" min="0" name="total_retur_detail[]" value="0">
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <hr/>
              <div class="row">
                <div class="col-xs-12 col-sm-8 form-horizontal">
                </div>
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">Sub Total</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="subtotal" value="0" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Disc (%)</label>
                      <div  class="col-md-9">
                        <input type="number" class="form-control" name="disc" min="0" max="100" value="0">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">PPN</label>
                      <div  class="col-md-9">
                        <input type="number" class="form-control" name="ppn" min="0" max="100" value="0">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Ongkos Angkut</label>
                      <div  class="col-md-9">
                        <input type="number" class="form-control" name="ongkos_angkut" value="0">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Grand Total</label>
                      <div  class="col-md-9">
                        <input type="number" class="form-control" name="grand_total" value="0" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-lg btn-block">SAVE</button>
                      <a href="{{ route('daftarPenjualanLangsung') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
@stop
