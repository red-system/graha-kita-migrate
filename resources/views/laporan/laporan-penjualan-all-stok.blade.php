<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan All Penjualan Stock</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <h4 >Penjualan Langsung</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th style="font-size:12px" style="white-space: nowrap;">Tanggal</th>
                  <th style="font-size:12px" style="white-space: nowrap;">No. Invoice</th>
                  <th style="font-size:12px" style="white-space: nowrap;">Customer</th>
                  <th style="font-size:12px" style="white-space: nowrap;">Nama Stock</th>
                  <th style="font-size:12px">QTY</th>
                  <th style="font-size:12px">STN</th>
                  <th style="font-size:12px">IN</th>
                  <th style="font-size:12px">EX</th>
                  <th style="font-size:12px">DPP</th>
                  <th style="font-size:12px">PPN</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
                <tbody>
                  @php
                    $sub_totalx=0;
                    $total_dicx=0;
                    $grand_totalx=0;
                    $HPPx=0;
                    $no_print = 0;
                  @endphp
                  @foreach($dataPL as $row)
                    <tr>
                      @if ($no_print == 0 || $no_print != $row->pl_no_faktur)
                        <td style="font-size:12px"> {{ $no++ }}. </td>
                        @php
                        $no_print = $row->pl_no_faktur;
                        @endphp
                      @else
                        <td style="font-size:12px"> </td>
                      @endif
                      <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                      <td style="font-size:12px"> {{ $row->pl_no_faktur }} </td>
                      <td style="font-size:12px"> {{ $row['cus_nama'] }} </td>
                      <td style="font-size:12px"> {{ $row['nama_barang'] }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->qty, 2, "." ,",") }} </td>
                      <td style="font-size:12px"> {{ $row->satuanJ['stn_nama'] }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->harga_net, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format(($row->harga_jual - $row->disc_nom), 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format(($row->harga_jual - $row->disc_nom)*$row->qty, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->ppn_nom*$row->qty, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format(($row->harga_net*$row->qty), 2, "." ,",") }} </td>
                    </tr>
                    @php
                      $sub_totalx += $row->total;
                      $total_dicx += $row->disc_nom;
                      $HPPx += $row->brg_hpp;
                    @endphp
                  @endforeach
                  @php
                  $grand_totalx = $sub_totalx - $total_dicx;
                  @endphp
                  {{-- <tfoot> --}}
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Sub Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($sub_total, 0, "." ,".")}}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($sub_totalx, 2, "." ,",")}}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Grand Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($grand_total, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($grand_totalx, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    {{-- <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Total Komisi</td>
                      <td style="font-weight:bold" align="right">{{number_format($total_komisi, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr> --}}
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">HPP</td>
                      {{-- <td style="font-weight:bold">{{number_format($HPP, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($HPPx, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
          <br>
          <h4 >Penjualan Titipan</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th style="font-size:12px" style="white-space: nowrap;">Tanggal</th>
                  <th style="font-size:12px" style="white-space: nowrap;">No. Invoice</th>
                  <th style="font-size:12px" style="white-space: nowrap;">Customer</th>
                  <th style="font-size:12px" style="white-space: nowrap;">Nama Stock</th>
                  <th style="font-size:12px">QTY</th>
                  <th style="font-size:12px">STN</th>
                  <th style="font-size:12px">IN</th>
                  <th style="font-size:12px">EX</th>
                  <th style="font-size:12px">DPP</th>
                  <th style="font-size:12px">PPN</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
                <tbody>
                  @php
                    $sub_totalx=0;
                    $total_dicx=0;
                    $grand_totalx=0;
                    $HPPx=0;
                    $no_print = 0;
                  @endphp
                  @foreach($dataPT as $row)
                    <tr>
                      @if ($no_print == 0 || $no_print != $row->pt_no_faktur)
                        <td style="font-size:12px"> {{ $no_2++ }}. </td>
                        @php
                        $no_print = $row->pt_no_faktur;
                        @endphp
                      @else
                        <td style="font-size:12px"> </td>
                      @endif
                      <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                      <td style="font-size:12px"> {{ $row->pt_no_faktur }} </td>
                      <td style="font-size:12px"> {{ $row['cus_nama'] }} </td>
                      <td style="font-size:12px"> {{ $row['nama_barang'] }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->qty, 2, "." ,",") }} </td>
                      <td style="font-size:12px"> {{ $row->satuanJ['stn_nama'] }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->harga_net, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format(($row->harga_jual - $row->disc_nom), 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format(($row->harga_jual - $row->disc_nom)*$row->qty, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format($row->ppn_nom*$row->qty, 2, "." ,",") }} </td>
                      <td style="font-size:12px" align="right"> {{ number_format(($row->harga_net*$row->qty), 2, "." ,",") }} </td>
                    </tr>
                    @php
                      $sub_totalx += $row->total;
                      $total_dicx += $row->disc_nom;
                      $HPPx += $row->brg_hpp;
                      $no_print++;
                    @endphp
                  @endforeach
                  @php
                  $grand_totalx = $sub_totalx - $total_dicx;
                  @endphp
                  {{-- <tfoot> --}}
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Sub Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($sub_total, 0, "." ,".")}}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($sub_totalx, 2, "." ,",")}}</td>
                      {{-- <td style="font-weight:bold" align="right">{{number_format($sub_totalPT, 2, "." ,",")}}</td> --}}
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Grand Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($grand_total, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($grand_totalx, 2, "." ,",") }}</td>
                      {{-- <td style="font-weight:bold" align="right">{{number_format($grand_totalPT, 2, "." ,",")}}</td> --}}
                      <td></td>
                      <td></td>
                    </tr>
                    {{-- <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Total Komisi</td>
                      <td style="font-weight:bold" align="right">{{number_format($total_komisiPT, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr> --}}
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">HPP</td>
                      {{-- <td style="font-weight:bold">{{number_format($HPP, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($HPPx, 2, "." ,",") }}</td>
                      {{-- <td style="font-weight:bold" align="right">{{number_format($HPPPT, 2, "." ,",") }}</td> --}}
                      <td></td>
                      <td></td>
                    </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
