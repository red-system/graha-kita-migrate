@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                <i class="fa fa-plus"></i> Tambah Type Customer
              </button>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th style="font-size:10px" width="10"> No </th>
                    <th style="font-size:10px"> Kode Type Customer </th>
                    <th style="font-size:10px"> Type Customer </th>
                    {{-- <th> Deskripsi </th> --}}
                    <th style="font-size:10px"> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td style="font-size:10px"> {{ $no++ }}. </td>
                      <td style="font-size:10px"> {{ $kodeLabel.$row->type_cus_kode }} </td>
                      <td style="font-size:10px"> {{ $row->type_cus_nama }} </td>
                      {{-- <td> {{ $row->type_cus_description }} </td> --}}
                      <td style="font-size:10px">
                        <div class="btn-group-xs">
                          <button class="btn btn-success btn-edit" data-href="{{ route('TypeCustomerEdit', ['kode'=>$row->type_cus_kode]) }}">
                            <span class="icon-pencil"></span> Edit
                          </button>
                          <button class="btn btn-danger btn-delete-new" data-href="{{ route('TypeCustomerDelete', ['kode'=>$row->type_cus_kode]) }}">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Type Customer
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{ route('TypeCustomerInsert') }}" class="form-horizontal form-send" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="type_cus_nama">
                </div>
              </div>
              {{-- <div class="form-group">
                <label class="col-md-3 control-label">Deskripsi</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="type_cus_description"></textarea>
                </div>
              </div> --}}
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Type Customer
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send" role="form" method="put">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="type_cus_nama">
                </div>
              </div>
              {{-- <div class="form-group">
                <label class="col-md-3 control-label">Deskripsi</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="type_cus_description"></textarea>
                </div>
              </div> --}}
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
