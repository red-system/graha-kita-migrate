@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <a style="font-size: 12px;" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                            Pilih Periode
                        </a>
                        <a style="font-size: 12px;" type="button" class="btn btn-danger" href="{{route('printNeraca', ['start_date'=>$start_date, 'end_date'=>$end_date,$tipe='print'])}}" target="_blank">
                            <span><i class="fa fa-print"></i></span> Print
                        </a>

                        <h3><center><strong>LAPORAN NERACA</strong></center></h3>
                        <h4><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h4>

                        <table class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr class="success">
                                    <th width="25%"><center>ASET</center></th>
                                    <th width="25%"><center>LIABILITAS</center></th>
                                    <th width="25%"><center>EKUITAS</center></th>
                                    <th width="25%"><center>STATUS</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{number_format($total_asset,2)}}</td>
                                    <td>{{number_format($total_liabilitas,2)}}</td>
                                    <td>{{number_format($total_ekuitas,2)}}</td>
                                    
                                    @if($status=='BALANCE')
                                    <td><font color="green"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                                    @else
                                    <td><font color="red"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#data_asset" data-toggle="tab"> DATA ASSET </a>
                                </li>
                                <li>
                                    <a href="#data_liabilitas" data-toggle="tab"> DATA LIABILITAS </a>
                                </li>
                                <li>
                                    <a href="#data_ekuitas" data-toggle="tab"> DATA EKUITAS </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="data_asset">
                                    <h4><center><strong>DATA ASSET</strong></center></h4>
                                    <h5><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h5>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_asset" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        <?php
                                            

                                            $total_assets = 0;
                                            $total_liabilitass = 0;
                                            $total_ekuitass = 0;

                                        ?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening != 'AKTIVA TETAP')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}}</td>
                                                <td style="font-weight: bold;">{{$detail->mst_nama_rekening}}</td>
                                                <?php
                                                    
                                                    $total_assets = $total_assets+$asset[$detail->mst_kode_rekening];
                                                ?>
                                                <td style="font-weight: bold;" align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td>{{$det_child->mst_kode_rekening}}</td>
                                                <td>{{$det_child->mst_nama_rekening}}</td>
                                                <?php
                                                    
                                                    $total_assets = $total_assets+$asset[$det_child->mst_kode_rekening];
                                                ?>
                                                <td align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        @endforeach
                                                                                    
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_assets,2)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_aktiva" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        <?php
                                            

                                            $total_aktiva = 0;

                                        ?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening == 'AKTIVA TETAP')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}}</td>
                                                <td style="font-weight: bold;">{{$detail->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->mst_normal == 'kredit'){
                                                        $total_aktiva = $total_aktiva-$asset[$detail->mst_kode_rekening];
                                                    }
                                                    if($detail->mst_normal == 'debet'){
                                                        $total_aktiva = $total_aktiva+$asset[$detail->mst_kode_rekening];
                                                    }
                                                    // foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                    //     if($detail->perkiraan->mst_normal == 'kredit'){
                                                    //         $activa  = $activa+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                    //     }
                                                    //     if($detail->perkiraan->mst_normal == 'debet'){
                                                    //         $activa  = $activa-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                    //     }

                                                    // }
                                                    
                                                ?>
                                                <td align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td style="font-weight: bold;">{{$det_child->mst_kode_rekening}}</td>
                                                <td style="font-weight: bold;">{{$det_child->mst_nama_rekening}}</td>
                                                <?php
                                                    if($det_child->mst_normal == 'kredit'){
                                                        $total_aktiva = $total_aktiva-$asset[$det_child->mst_kode_rekening];
                                                    }
                                                    if($det_child->mst_normal == 'debet'){
                                                        $total_aktiva = $total_aktiva+$asset[$det_child->mst_kode_rekening];
                                                    }
                                                    // foreach($det_child->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                    //     if($det_child->mst_normal == 'kredit'){
                                                    //         $activa  = $activa+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                    //     }
                                                    //     if($det_child->mst_normal == 'debet'){
                                                    //         $activa  = $activa-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                    //     }

                                                    // }
                                                    
                                                ?>
                                                <td align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @foreach($det_child->childs as $child)
                                            <tr>
                                                <td>{{$child->mst_kode_rekening}}</td>
                                                <td>{{$child->mst_nama_rekening}}</td>
                                                <?php
                                                    if($child->mst_normal == 'kredit'){
                                                        $total_aktiva = $total_aktiva-$asset[$child->mst_kode_rekening];
                                                    }
                                                    if($child->mst_normal == 'debet'){
                                                        $total_aktiva = $total_aktiva+$asset[$child->mst_kode_rekening];
                                                    }
                                                    // foreach($child->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                    //     if($child->mst_normal == 'kredit'){
                                                    //         $activa  = $activa+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                    //     }
                                                    //     if($child->mst_normal == 'debet'){
                                                    //         $activa  = $activa-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                    //     }

                                                    // }
                                                    
                                                ?>
                                                <td align="right">{{number_format($asset[$child->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                            @endif
                                        @endforeach 
                                                                                    
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_aktiva,2)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                
                                <div class="tab-pane fade" id="data_liabilitas">
                                    <h4><center><strong>DATA LIABILITAS</strong></center></h4>
                                    <h5><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h5>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_liabilitas" width="100%"> 
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                            @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'liabilitas'&& $detail->mst_master_id == '0')
                                            <tr>
                                                <td style="font-weight: bold;" width="25%">{{$detail->mst_kode_rekening}}</td>
                                                <td style="font-weight: bold;" width="45%">{{$detail->mst_nama_rekening}}</td>
                                                <?php
                                                    // if($detail->perkiraan->mst_normal == 'kredit'){
                                                    //     $liabilitas  = $detail->msd_awal_kredit;
                                                    // }
                                                    // if($detail->perkiraan->mst_normal == 'debet'){
                                                    //     $liabilitas  = $detail->msd_awal_debet;
                                                    // }
                                                    // foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                    //     if($detail->perkiraan->mst_normal == 'kredit'){
                                                    //         $liabilitas  = $liabilitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                    //     }
                                                    //     if($detail->perkiraan->mst_normal == 'debet'){
                                                    //         $liabilitas  = $liabilitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                    //     }
                                                                        
                                                    // }
                                                    $total_liabilitass     = $total_liabilitass+$liabilitas[$detail->mst_kode_rekening];
                                                ?>
                                                <td width="20%">{{number_format($liabilitas[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @foreach($detail->childs as $det_childs)
                                            <tr>
                                                <td width="25%">{{$det_childs->mst_kode_rekening}}</td>
                                                <td width="45%">{{$det_childs->mst_nama_rekening}}</td>
                                                <?php
                                                    // if($detail->perkiraan->mst_normal == 'kredit'){
                                                    //     $liabilitas  = $detail->msd_awal_kredit;
                                                    // }
                                                    // if($detail->perkiraan->mst_normal == 'debet'){
                                                    //     $liabilitas  = $detail->msd_awal_debet;
                                                    // }
                                                    // foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                    //     if($detail->perkiraan->mst_normal == 'kredit'){
                                                    //         $liabilitas  = $liabilitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                    //     }
                                                    //     if($detail->perkiraan->mst_normal == 'debet'){
                                                    //         $liabilitas  = $liabilitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                    //     }
                                                                        
                                                    // }
                                                    $total_liabilitass     = $total_liabilitass+$liabilitas[$det_childs->mst_kode_rekening];
                                                ?>
                                                <td width="20%">{{number_format($liabilitas[$det_childs->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        @endforeach                                   
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_liabilitass)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="tab-pane fade" id="data_ekuitas">
                                    <h4><center><strong>DATA EKUITAS</strong></center></h4>
                                    <h5><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h5>                                   
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_ekuitas" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                            @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'ekuitas')
                                            <tr>
                                                <td width="25%" <?php if($detail->mst_master_id == '0') echo 'style="font-weight:bold;"';?>>{{$detail->mst_kode_rekening}}</td>
                                                <td width="45%" <?php if($detail->mst_master_id == '0') echo 'style="font-weight:bold;"';?>>{{$detail->mst_nama_rekening}}</td>
                                                <?php
                                                    // if($detail->perkiraan->mst_normal == 'kredit'){
                                                    //     $ekuitas  = $detail->msd_awal_kredit;
                                                    // }
                                                    // if($detail->perkiraan->mst_normal == 'debet'){
                                                    //     $ekuitas  = $detail->msd_awal_debet;
                                                    // }
                                                    // foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                    //     if($detail->perkiraan->mst_normal == 'kredit'){
                                                    //         $ekuitas  = $ekuitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                    //     }
                                                    //     if($detail->perkiraan->mst_normal == 'debet'){
                                                    //         $ekuitas  = $ekuitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                            
                                                    //     }
                                                                        
                                                    // }
                                                    $total_ekuitass     = $total_ekuitass+$ekuitas[$detail->mst_kode_rekening];
                                                ?>
                                                <td width="20%">{{number_format($ekuitas[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            @endif
                                        @endforeach                                     
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_ekuitass,2)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeNeraca') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date" value="{{$end_date}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop