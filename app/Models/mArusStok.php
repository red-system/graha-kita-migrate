<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mArusStok extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_arus_stok';
  protected $primaryKey = 'ars_stok_kode';
  public $timestamps = false;

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode');
  }
}
