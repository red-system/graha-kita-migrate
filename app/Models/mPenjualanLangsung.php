<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPenjualanLangsung extends Model
{
  public $incrementing = false;
  protected $table = 'tb_penjualan_langsung';
  protected $primaryKey = 'pl_no_faktur';
  // public $timestamps = false;
  public $fillable = [
      'pl_no_faktur',
      'pl_tgl',
      'cus_kode',
      'pl_transaksi',
      'pl_sales_person',
      'pl_checker',
      'pl_sopir',
      'pl_catatan',
      'pl_lama_kredit',
      'pl_tgl_jatuh_tempo',
      'pl_kirim_semua',
      'pl_subtotal',
      'pl_disc',
      'pl_disc_nom',
      'pl_ppn',
      'pl_ppn_nom',
      'pl_ongkos_angkut',
      'grand_total
'];

    public function piutang_pelanggan(){
      return $this->hasOne(mPiutangPelanggan::class);
    }

    public function detail_penjualanLangsung(){
      return $this->hasMany(mDetailPenjualanLangsung::class,'pl_no_faktur','pl_no_faktur');
    }


  public function detail_PL()
  {
    // return $this->hasMany('App\Models\mDetailPenjualanLangsung', 'pl_no_faktur')->select('detail_pl_kode', 'brg_kode', 'gudang', 'nama_barang', 'satuan', 'harga_jual', 'harga_net', 'qty', 'terkirim', 'total', 'brg_no_seri');
    return $this->hasMany('App\Models\mDetailPenjualanLangsung', 'pl_no_faktur');
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\mCustomer', 'cus_kode')->select('cus_kode', 'cus_tipe', 'cus_nama');
  }

  public function karyawan()
  {
    return $this->belongsTo('App\Models\mKaryawan','pl_sales_person', 'kry_kode')->select('kry_nama');
  }

  public function piutangPelanggan()
  {
    return $this->hasMany('App\Models\mPiutangPelanggan', 'pp_no_faktur', 'pl_no_faktur');
  }

  public function satuanJ()
  {
    return $this->belongsTo('App\Models\mSatuan', 'satuan', 'stn_kode')->select('stn_kode', 'stn_nama');
  }
}
