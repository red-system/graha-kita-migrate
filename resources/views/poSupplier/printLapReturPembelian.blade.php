<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>Laporan Pembelian Supplier</title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <p style="font-weight: bold;"><center>Laporan Retur Pembelian Stock</center></p>
                        <p style="font-weight: bold;"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></p> 
                    </div>
                    <br>
                    <div class="portlet light ">
                        <table class="tg">
                            <thead>
                                <tr class="success">

                                        <th width="10" style="font-size: 12px"> No </th>
                                        <th style="font-size: 12px"> Tanggal</th>
                                        <th style="font-size: 12px"> No Invoice </th>
                                        <th style="font-size: 12px"> Barcode </th>
                                        <th style="font-size: 12px"> Nama Barang </th>
                                        <th style="font-size: 12px"> Qty </th>
                                        <th style="font-size: 12px"> Satuan </th>
                                        <th style="font-size: 12px"> Harga </th>
                                        <th style="font-size: 12px"> Total </th>
                                </tr>
                            </thead>
                            <tbody>
                                            @if($jml_pembelian > 0)
                                <?php
                                    $grand_total     = 0;
                                    $grand_total_spl = 0;
                                ?>
                                @foreach($dataList as $poS)                                
                                <tr>                                    
                                    <td colspan="9"  style="font-size: 12px"><strong> {{ $poS->spl_nama }} </strong></td>
                                </tr>  
                                @foreach($poS->returnPembelian->where('tgl_pengembalian','>=',$start_date)->where('tgl_pengembalian','<=',$end_date) as $returnPembelian)                                               
                                    @foreach($returnPembelian->detailReturPembelian as $detail)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ date('d M Y', strtotime($returnPembelian->tgl_pengembalian)) }}</td>
                                        <td>{{$returnPembelian->no_return_pembelian}}</td>
                                        <td>{{$detail->barang->brg_barcode}}</td>
                                        <td>{{$detail->barang->brg_nama}}</td>
                                        <td align="center">{{$detail->qty_retur}}</td>
                                        <td align="center">{{$detail->barang->satuan->stn_nama}}</td>
                                        <td align="right">{{number_format($detail->harga_jual)}}</td>
                                        <td align="right">{{number_format($detail->total_retur)}}</td>
                                    </tr>
                                    <?php
                                        $grand_total_spl = $grand_total_spl+$detail->total_retur;
                                        // $grand_total     = $grand_total+$grand_total_spl;
                                    ?>                                 
                                    
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td colspan="8" align="right" style="font-size: 12px"><strong>Grand Total</strong></td>
                                    <td align="right" style="font-size: 12px"><strong>{{number_format($grand_total_spl)}}</strong></td>
                                </tr>
                                @endforeach
                                
                                @endif                           
                                        </tbody>
                                        
                        </table> 
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
