<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mStokSample;
use App\Models\mDetailStokSample;
use App\Models\mStok;
use App\Models\mArusStok;
use PDF;
use Carbon\Carbon;

use App\Models\mKategoryProduct;
use App\Models\mGroupStok;
use App\Models\mMerek;
use App\Models\mSupplier;
use App\Models\mCustomer;

use Validator;

class StokSample extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Stok Sample';
      $this->kodeLabel = 'stok_sample';
  }

  function index() {
      $breadcrumb = [
          'Stok Sample'=>route('stokSampleList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['barang'] = $this->main->kodeLabel('barang');
      // $data['gudang'] = $this->main->kodeLabel('gudang');
      // $data['supplier'] = $this->main->kodeLabel('supplier');

      $data['menu'] = 'stok_sample';
      $data['title'] = $this->title;
      $getStokSample = mStokSample::get();
      foreach ($getStokSample as $key => $value) {
        $brg = $value->barang;
        $brg->merek;
        $brg['Kategori'] = $brg->kategoryProduct;
        $brg['group'] = $brg->groupStok;

        unset($brg->kategoryProduct);
        unset($brg->groupStok);
        // $value->gudang;
        $value->supplier;
      }
      $data['dataList'] = $getStokSample;
      $data['k_product'] = mKategoryProduct::orderBy('ktg_nama', 'asc')->get();
      $data['g_product'] = mGroupStok::orderBy('grp_nama', 'asc')->get();
      $data['merek'] = mMerek::orderBy('mrk_nama', 'asc')->get();
      $data['supplier'] = mSupplier::orderBy('spl_nama', 'asc')->get();
      // return $data;
      return view('stok-sample/stokSampleList', $data);
  }

  function returnStokSample(Request $request) {
    // return $request;
    $stokSample = mStokSample::where('stok_sample_kode', $request->stok_sample_kode)->first();
    $stok = mStok::where('gdg_kode', $stokSample->gdg_kode)->where('brg_kode', $stokSample->brg_kode)->where('spl_kode', $stokSample->spl_kode)->where('brg_no_seri', $stokSample->brg_no_seri)->first();
    if ($stok) {
      $stok->stok = ($stok->stok + $request->qty);
      $stok->save();
    }
    else {
      $Stok_new = new mStok();
      $Stok_new->brg_kode = $stokSample->brg_kode;
      $Stok_new->gdg_kode = $stokSample->gdg_kode;
      $Stok_new->spl_kode = $stokSample->spl_kode;
      $Stok_new->stok = $request->qty;
      $Stok_new->brg_no_seri = $stokSample->brg_no_seri;
      $Stok_new->stk_hpp = 0;
      $Stok_new->save();
    }

    $hasil = ($stokSample->qty - $request->qty);
    if ($hasil <= 0) {
      mStokSample::where('stok_sample_kode', $request->stok_sample_kode)->delete();
      mDetailStokSample::where('stok_sample_kode', $request->stok_sample_kode)->delete();
    }
    else {
      $stokSample->qty = $hasil;
      $stokSample->save();
    }

    $total=0;
    $data = mStok::where('brg_kode', $stokSample->brg_kode)->get();
    foreach ($data as $gdg) {
      $total += $gdg->stok;
    }
    $QOH = $total;

    $time = Carbon::now();
    $arusStok = new mArusStok();
    $arusStok->ars_stok_date = $time;
    $arusStok->brg_kode = $stokSample->brg_kode;
    $arusStok->stok_in = $request->qty;
    $arusStok->stok_out = 0;
    $arusStok->stok_prev = $QOH;
    $arusStok->gdg_kode = $stokSample->gdg_kode;
    $arusStok->keterangan = 'Stok Sample '.$stokSample->stok_sample_kode;
    $arusStok->save();

    return [
      'redirect' => route('stokSampleList')
    ];
  }

  function stokSamplePrint(Request $request) {
    // return $request;
    $data['no'] = $no = 1;

      $time = date('Y-m-d');
      $data['time'] = $time;
      // $data['dataList'] = mStokSample::all();
      $rowQB = mStokSample::query();
      if ($request->ktg_kode != 0) {
        $rowQB->whereHas('barang', function ($query) use($request) {
          $query->where('ktg_kode', $request->ktg_kode);
        });
      }
      if ($request->grp_kode != 0) {
        $rowQB->whereHas('barang', function ($query) use($request) {
          $query->where('grp_kode', $request->grp_kode);
        });
      }
      if ($request->mrk_kode != 0) {
        $rowQB->whereHas('barang', function ($query) use($request) {
          $query->where('mrk_kode', $request->mrk_kode);
        });
      }
      if ($request->spl_kode != 0) {
        $rowQB->whereHas('barang.stok', function ($query) use($request) {
          $query->where('spl_kode', $request->spl_kode);
        });
      }

      $data['dataList'] =  $rowQB->get();

      if ($request->type == 'General') {
        foreach ($data['dataList'] as &$barang) {
          $barang->barang;
          $barang['stok'] = mStok::select('brg_no_seri', 'tb_stok.spl_kode', 'tb_supplier.spl_nama', \DB::raw('SUM(stok) as total'))
          ->leftJoin('tb_supplier', 'tb_stok.spl_kode', '=', 'tb_supplier.spl_kode')
          ->where('brg_kode', $barang->brg_kode)
          ->groupBy('brg_kode', 'brg_no_seri')
          ->get();

          $barang['kategory'] = $barang->barang->kategoryProduct;
          $barang['group'] = $barang->barang->groupStok;
          $barang['merek'] = $barang->barang->merek;
          $barang['satuan'] = $barang->barang->satuan;
          $barang['supplier'] = $barang->barang->supplier;
          unset($barang->barang->kategoryProduct);
          unset($barang->barang->groupStok);
          unset($barang->barang->merek);
          unset($barang->barang->satuan);
          unset($barang->barang->supplier);
        }

        // return $data;
        return view('stok-sample.barangPrint', $data);

        $pdf = PDF::loadView('stok-sample.barangPrint', $data)
                    ->setPaper('a4', 'landscape');
        return $pdf->stream();
      }else {
        foreach ($data['dataList'] as &$barang) {
          $barang->barang;
          $barang['stok'] = mStok::where('brg_kode', $barang->brg_kode)
          ->get();

          foreach ($barang['stok'] as $key) {
            $key->gudang;
            $key->supplier;
          }
          $barang['kategory'] = $barang->barang->kategoryProduct;
          $barang['group'] = $barang->barang->groupStok;
          $barang['merek'] = $barang->barang->merek;
          $barang['satuan'] = $barang->barang->satuan;
          $barang['supplier'] = $barang->barang->supplier;
          unset($barang->barang->kategoryProduct);
          unset($barang->barang->groupStok);
          unset($barang->barang->merek);
          unset($barang->barang->satuan);
          unset($barang->barang->supplier);
        }
      }

      // return $data;
      return view('stok-sample.barangPrintDetail', $data);

      $pdf = PDF::loadView('stok-sample.barangPrintDetail', $data)
                  ->setPaper('a4', 'landscape');
      return $pdf->stream();
  }

  function rules($request) {
      $rules = [
          'qty' => 'required',
          'alamat' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function store(Request $request) {
      $this->rules($request->all());
      \DB::beginTransaction();
      try {
        mDetailStokSample::insert($request->except('_token'));
        // $sample = mStokSample::where('stok_sample_kode', $request->stok_sample_kode)->first();
        // $sample->qty = $sample->qty - $request->qty;
        // $sample->save();

        \DB::commit();

        return [
          // 'redirect'=>route('stokSampleList')
          'redirect'=>route('stokSample.detail', ['id'=>$request->stok_sample_kode])
        ];

      } catch (\Exception $e) {
        \DB::rollBack();
        throw $e;
      }
  }

  function delete($id) {
    \DB::beginTransaction();
    try {
      $detail = mDetailStokSample::where('detail_stok_sample_kode', $id)->first();
      // $sample = mStokSample::where('stok_sample_kode', $detail->stok_sample_kode)->first();
      // $sample->qty = $sample->qty + $detail->qty;
      // $sample->save();
      $detail->delete();

      \DB::commit();
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }

  function detail($id='') {
    $breadcrumb = [
      'Stok Sample'=>route('stokSample.detail', ['id'=>$id]),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'stok_sample';
    $data['title'] = $this->title;
    $data['dataList'] = mDetailStokSample::
    leftJoin('tb_customer', 'tb_customer.cus_kode', '=', 'tb_detail_stok_sample.cus_kode')
    ->select('detail_stok_sample_kode', 'stok_sample_kode', 'qty', 'cus_nama', 'alamat')
    ->where('stok_sample_kode', $id)->get();
    $data['customer'] = mCustomer::orderBy('cus_kode', 'ASC')->get();
    $data['stok_sample_kode'] = $id;
    // return $data;
    return view('stok-sample/detail', $data);
  }

  function edit($id='') {
      $response = [
          'action'=>route('stokSample.update', ['id'=>$id]),
          'field'=>mDetailStokSample::find($id)
      ];
      return $response;
  }

  function update(Request $request, $id) {
      $this->rules($request->all());
      mDetailStokSample::where('detail_stok_sample_kode', $id)->update($request->except('_token'));
      return [
          'redirect'=>route('stokSampleList')
      ];
  }
}
