<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mKategoryAsset extends Model
{
  	use SoftDeletes;
  	protected $dates =['deleted_at'];
  
  	public $incrementing = false;
  	protected $table = 'tb_kategori_asset';
  	protected $primaryKey = 'ka_kode';
  	public $timestamps = false;

  	public function Asset(){
		return $this->hasMany(mAsset::class,'kategori','ka_kode');
	}
}
