<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPembelianSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_pembelian_supplier', function (Blueprint $table) {
         $table->increments('ps_no_faktur');
         $table->integer('pos_no_po')->nullable();
         $table->datetime('ps_tgl')->nullable();
         $table->integer('spl_kode')->nullable();
         $table->text('ps_catatan')->nullable();
         $table->integer('ps_subtotal')->nullable();
         $table->float('ps_disc')->nullable();
         $table->integer('ps_disc_nom')->nullable();
         $table->float('ps_ppn')->nullable();
         $table->integer('ps_ppn_nom')->nullable();
         $table->integer('biaya_lain')->nullable();
         $table->integer('grand_total')->nullable();

         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_pembelian_supplier');
     }
}
