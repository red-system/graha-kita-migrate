<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mSlider extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_slider';
  protected $primaryKey = 'sldr_kode';
  public $timestamps = false;
  protected $fillable = [];
}
