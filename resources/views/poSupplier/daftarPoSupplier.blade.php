<?php
 
use App\Models\mWoSupplier;
?>
@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-print-faktur').click(function() {
                var id = $(this).data('todo');
                $('#modal-print [name="id"]').val(id);
                $('#modal-print').modal('show');
            });
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet-body">
                        <a href="{{ route('poSupplierList') }}" class="tooltips btn btn-primary btn-tambah-asset">
                            Create Purchase Order
                        </a>
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="tb_daftar_pembelian" width="100%">
                            <thead>
                                <tr class="">
                                    <th width="5%" style="font-size: 12px"> No </th>
                                    <th width="15%" style="font-size: 12px"> No. PO </th>
                                    <th width="5%" style="font-size: 12px"> Tanggal</th>
                                    <th width="15%" style="font-size: 12px"> Supplier </th>
                                    <th width="15%" style="font-size: 12px"> Alamat </th>
                                    <th width="10%" style="font-size: 12px"> Telepon </th>
                                    <th width="10%" style="font-size: 12px"> Total PO </th>
                                    <th width="10%" style="font-size: 12px"> Keterangan </th>
                                    <th width="5%" style="font-size: 12px"> Tipe Transaksi </th>
                                    <th width="10%" style="font-size: 12px"> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dataList as $poS)
                                <tr>
                                    <td style="font-size: 11px" align="center"> {{ $no++ }}. </td>
                                    <td style="font-size: 11px"> {{ $poS->no }} </td>
                                    <td style="font-size: 11px"> {{ date('d M Y', strtotime($poS->pos_tgl)) }} </td>
                                    <td style="font-size: 11px"> SPL{{ $poS->spl_kode }} - {{ $poS->suppliers->spl_nama }} </td>
                                    <td style="font-size: 11px"> {{ $poS->suppliers->spl_alamat}}</td>
                                    <td style="font-size: 11px"> {{ $poS->suppliers->spl_telp}} </td>
                                    <td style="font-size: 11px; text-align: right;"> {{ number_format($poS->grand_total,2) }} </td>
                                    <td style="font-size: 11px"> Untuk {{ ucfirst($poS->kondisi) }} </td>
                                    <td style="font-size: 11px"> {{ ucwords($poS->tipe_transaksi)}} </td>
                                    <td style="font-size: 11px">
                                        <!-- <div class="btn-group btn-group-xs"> -->
                                        <a type="button" name="btn-print" class="btn btn-danger btn-xs" href="{{ route('pembelianSupplierCetak', ['kode'=>$poS->pos_no_po]) }}" target="_blank">
                                            <span class="fa fa-print"></span>
                                        </a>
                                        <!-- <button class="btn btn-danger btn-print-faktur btn-xs" data-todo="{{$poS->pos_no_po}}">
                                            <span class="fa fa-print"></span>
                                        </button> -->
                                        <a type="button" name="btn-edit" class="btn btn-success btn-xs" href="{{route('editPurchase',['id_po'=>$poS->pos_no_po])}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a type="button" name="btn-edit" class="btn btn-primary btn-xs" href="{{route('viewPurchase',['id'=>$poS->pos_no_po])}}" data-target="#viewDetailProduksi" data-toggle="modal">
                                            <span class="fa fa-eye"></span>
                                        </a>
                                        @if(mWoSupplier::where('no_po','=', $poS->no)->doesntExist())
                                        <a type="button" name="btn-cart" class="btn btn-info btn-xs" href="{{ route('createWo', ['kode'=>$poS->pos_no_po]) }}">
                                            <span class="fa fa-sticky-note-o"></span>
                                        </a>
                                        @endif
                                        
                                        
                                        <!-- </div> -->
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="viewDetailProduksi" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                    
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>


<div class="modal draggable-modal" id="modal-print" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Print Dialog </h4>
            </div>
            <form method="post" class="form-horizontal form-send" action="{{route('printDialog')}}">
          {{ csrf_field() }}
                <div class="modal-body form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="kode_barang" class="col-md-2 col-xs-2 control-label">View Harga</label>
                                    <div class="col-md-8 col-xs-8">
                                        <select name="view_harga" class="form-control select2" required>
                                            <option value="no">Tanpa Harga</option>
                                            <option value="yes">Dengan Harga</option>
                                        </select>
                                        <input type="text" name="id" value="">
                                        <input type="text" name="tipe" value="print">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="kode_barang" class="col-md-2 col-xs-2 control-label">Kertas</label>
                                    <div class="col-md-8 col-xs-8">
                                        <select name="kertas" class="form-control select2" required>
                                            <option value="kecil">Kecil (91/2 x 11 : 2)</option>
                                            <option value="besar">Besar (91/2 x 11)</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="kode_barang" class="col-md-2 col-xs-2 control-label">Kertas</label>
                                    <div class="col-md-8 col-xs-8">
                                        <select name="kop" class="form-control select2" required>
                                            <option value="aki">PT. ANGSA KUSUMA INDAH</option>
                                            <option value="gk18">GRAHAKITA 18</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-6 col-md-6">
                                <button type="submit" class="btn green" formtarget="_blank">OK</button>
                                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
