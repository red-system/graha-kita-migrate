<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDetailPsSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_detail_ps_supplier', function (Blueprint $table) {
         $table->increments('detail_ps_kode');
         $table->integer('ps_no_faktur')->nullable();
         $table->integer('gudang')->nullable();
         $table->integer('brg_kode')->nullable();
         $table->string('nama_barang')->nullable();
         $table->integer('satuan')->nullable();
         $table->integer('harga_beli')->nullable();
         $table->integer('ppn')->nullable();
         $table->integer('ppn_nom')->nullable();
         $table->float('disc')->nullable();
         $table->integer('disc_nom')->nullable();
         $table->integer('harga_net')->nullable();
         $table->integer('qty')->nullable();
         $table->integer('total')->nullable();
         $table->string('keterangan')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_detail_ps_supplier');
     }
}
