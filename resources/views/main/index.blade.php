<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8"/>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ $title }} - Graha Kita</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta content="Preview page of Metronic Admin Theme #3 for dashboard & statistics" name="description"/>
  <meta content="" name="author"/>
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
  type="text/css"/>
  <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
  type="text/css"/>
  <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet"
  type="text/css"/>
  <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet"
  type="text/css"/>
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet"
  type="text/css"/>
  <link href="{{ asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('assets/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet"
  type="text/css"/>
  <link href="{{ asset('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css"/>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components"
  type="text/css"/>
  <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css"/>
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="{{ asset('assets/layouts/layout3/css/layout.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('assets/layouts/layout3/css/themes/default.min.css') }}" rel="stylesheet" type="text/css"
  id="style_color"/>
  <link href="{{ asset('assets/layouts/layout3/css/custom.css') }}" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.29/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  @yield('css')
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}

  @yield('css')
  <link rel="shortcut icon" href="favicon.ico"/>
  <style media="screen">
  .swal-overlay {
    z-index: 100000 !important;
  }

  .swal-modal {
    z-index: 100001 !important;
  }

  .sweet-alert {
    z-index: 100001 !important;
  }

  .swal2-container {
    zoom: 1.5 !important;
    z-index: 100001 !important;
  }
  </style>
</head>
<!-- END HEAD -->
<audio id="notification_sound" preload="none" src="{{asset('sound/notification.mp3')}}" type="audio/mpeg">
</audio>
<body class="page-container-bg-solid page-header-menu-fixed">
  <div class="page-wrapper">
    <div class="page-wrapper-row">
      <div class="page-wrapper-top">
        <!-- BEGIN HEADER -->
        <div class="page-header">
          <!-- BEGIN HEADER TOP -->
          <div class="page-header-top">
            <div class="container-fluid">
              <!-- BEGIN LOGO -->
              <div class="page-logo">
                <a>
                  <img src="{{ asset('assets/pages/img/garaha-kita-lg.png') }}" style="height: 40px; margin-top: 20px;" alt="">
                </a>
              </div>
              <!-- END LOGO -->
              <!-- BEGIN RESPONSIVE MENU TOGGLER -->
              <a href="javascript:;" class="menu-toggler"></a>
              <!-- END RESPONSIVE MENU TOGGLER -->
              <!-- BEGIN TOP NAVIGATION MENU -->
              <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                  <!-- BEGIN NOTIFICATION DROPDOWN -->
                  <!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                  <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                  <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                      <i class="icon-bell"></i>
                      <span class="badge badge-default">@{{total}}</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="external">
                        <h3>You have <strong>@{{total}} pending</strong> Order</h3>
                        <a href="{{route('orderList')}}">view all</a>
                      </li>
                      <li>
                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                          <li v-for="order in orders">
                            <a v-bind:href="'/order/'+order.order_kode+'/detail'">
                              <span class="time">@{{order.order_tgl}}</span>
                              <span class="details">
                                <span class="label label-sm label-warning text-center">
                                  <i class="fa fa-bell-o"></i>
                                </span> &nbsp @{{order.karyawan.kry_nama}}
                              </span>
                            </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <!-- END NOTIFICATION DROPDOWN -->
                  <!-- BEGIN TODO DROPDOWN -->
                  <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                      <i class="icon-settings"></i>
                    </a>
                    <ul class="dropdown-menu extended tasks">
                      <li class="external">
                        <h3>You have <strong>12 pending</strong> tasks</h3>
                        <a href="app_todo_2.html">view all</a>
                      </li>
                      <li>
                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                          <li>
                            <a href="javascript:;">
                              <span class="task">
                                <span class="desc">New release v1.2 </span>
                                <span class="percent">30%</span>
                              </span>
                              <span class="progress">
                                <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                  <span class="sr-only">40% Complete
                                  </span>
                                </span>
                              </span>
                            </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <!-- END TODO DROPDOWN -->
                  <li class="droddown dropdown-separator">
                    <span class="separator"></span>
                  </li>
                  <!-- BEGIN INBOX DROPDOWN -->
                  <!-- END INBOX DROPDOWN -->
                  <!-- BEGIN USER LOGIN DROPDOWN -->
                  <li class="dropdown dropdown-user dropdown-dark">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                      <img alt="" class="img-circle" src="{{ asset('img/icon/GK_icon.jpg') }}">
{{--                      <span class="username username-hide-mobile">{{auth()->user()->karyawan->kry_nama}}</span>--}}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                      <li>
                        <a href="page_user_profile_1.html">
                          <i class="icon-user"></i> My Profile
                        </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="{{route('logoutProcess')}}">
                          <i class="icon-key"></i> Log Out
                        </a>
                      </li>
                    </ul>
                  </li>
                          <!-- END USER LOGIN DROPDOWN -->
                </ul>
              </div>
                      <!-- END TOP NAVIGATION MENU -->
            </div>
          </div>
                  <!-- END HEADER TOP -->
                  <!-- BEGIN HEADER MENU -->
          <div class="page-header-menu">
            <div class="container">
              <div class="hor-menu  ">
                <ul class="nav navbar-nav">
                  @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan') || \Gate::allows('as_purchasing') || \Gate::allows('as_logistik') || \Gate::allows('as_admin'))
                  <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown {{ $menu=='dashboard'?'active':'' }}">
                    <a href="{{route('dashboardPage')}}">
                      <span class="icon-home"></span> Dashboard
                    </a>
                  </li>
                  @endif
                  @if(\Gate::allows('as_master')  || \Gate::allows('as_purchasing') || \Gate::allows('as_admin'))
                  <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown {{ in_array($menu, $menuAll['master_data'])?'active':'' }}">
                    <a href="javascript:;">
                      <span class="icon-folder"></span> Master Data
                      <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 400px">
                      <li>
                          <div class="mega-menu-content">
                            <div class="row">
                              <div class="col-md-6">
                                <ul class="mega-menu-submenu">
                                  <li>
                                    <a href="{{ route('gudangList') }}"> Data Gudang </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('merekList') }}"> Data Merek </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('kategoryStokList') }}"> Data Kategori </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('groupStokList') }}"> Data Group Stok </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('satuanList') }}"> Data Satuan </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('provinsiList') }}"> Data Provinsi</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('wilayahList') }}"> Data Wilayah</a>
                                  </li>
                                </ul>
                              </div>
                              <div class="col-md-6">
                                <ul class="mega-menu-submenu">
                                  <li>
                                    <a href="{{ route('supplierList') }}"> Data Supplier</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('TypeCustomerList') }}"> Data Type Customer</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('customerList') }}"> Data Customer</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('karyawanList') }}"> Data Karyawan</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('kendaraanList') }}"> Data Kendaraan</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('jpkList') }}"> Data Biaya Operasional</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('kategoryAssetList') }}"> Data Kategori Asset</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('perkiraanList') }}"> Data Kode Perkiraan</a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                      </li>
                    </ul>
                  </li>
                  @endif
                  @if(\Gate::allows('as_master')  || \Gate::allows('as_logistik') || \Gate::allows('as_purchasing') || \Gate::allows('as_sales'))
                  <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown {{ in_array($menu, $menuAll['inventory'])?'active':'' }}">
                    <a href="javascript:;">
                      <span class="icon-drawer"></span> Inventory
                      <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 200px">
                      <li>
                        <div class="mega-menu-content">
                          <div class="row">
                            @if(\Gate::allows('as_master')  || \Gate::allows('as_logistik') || \Gate::allows('as_purchasing'))
                            <div class="col-md-6">
                              <ul class="mega-menu-submenu">
                                <li>
                                  <a href="{{ route('barangList') }}"> Data Barang & Harga </a>
                                </li>
                                <li>
                                  <a href="{{ route('BarangMix.index') }}"> Buat Barcode Cat Oplosan </a>
                                </li>
                                <li>
                                  <a href="{{ route('hargaCustomerList') }}"> Harga Customer </a>
                                </li>
                                <li>
                                  <a href="{{route('transferStokList')}}"> Transfer Stok </a>
                                </li>
                                <li>
                                  <a href="{{route('summaryStokList')}}"> Summary Stok </a>
                                </li>
                                <li>
                                  <a href="{{route('kartuStokList')}}"> Kartu Stok </a>
                                </li>
                                <li>
                                  <a href="{{route('stokAlertList')}}"> Stok Alert</a>
                                </li>
                                <li>
                                  <a href="{{route('stokSampleList')}}"> Stok Sample</a>
                                </li>
                              </ul>
                            </div>
                            @endif
                            <div class="col-md-6">
                              <ul class="mega-menu-submenu">
                                @if(\Gate::allows('as_master')  || \Gate::allows('as_logistik') || \Gate::allows('as_purchasing'))
                                <li>
                                  <a href="{{route('penyesuaianStok.index')}}"> Penyesuaian Stok</a>
                                </li>
                                <li>
                                  <a href="{{route('penyesuaianSC.index')}}"> Penyesuaian Stok Cat Oplosan</a>
                                </li>
                                @endif
                                @if(\Gate::allows('as_master')  || \Gate::allows('as_logistik') || \Gate::allows('as_purchasing'))
                                <li>
                                  <a href="{{route('DataReport.barang')}}"> Stok Barang</a>
                                </li>
                                @endif
                                @if(\Gate::allows('as_sales'))
                                <li>
                                  <a href="{{route('DataReport.sales')}}"> Stok Sales Person</a>
                                </li>
                                @endif
                                {{-- <li>
                                  <a href="{{route('DataReport.supplier')}}"> Stok Supplier</a>
                                </li> --}}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>
                  @endif
                  @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan') || \Gate::allows('as_purchasing') || \Gate::allows('as_logistik') || \Gate::allows('as_sales'))
                  <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown {{ in_array($menu, $menuAll['transaksi'])?'active':'' }}">
                    <a href="javascript:;">
                      <span class="icon-handbag"></span> Transaksi
                      <i class="fa fa-angle-down"></i>
                    </a>
                      <ul class="dropdown-menu pull-left">
                        @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan') || \Gate::allows('as_logistik'))
                        <li aria-haspopup="true" class="dropdown-submenu ">
                          <a href="javascript:;" class="nav-link nav-toggle ">
                            Penjualan
                            <span class="arrow"></span>
                          </a>
                          <ul class="dropdown-menu">
                            @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan'))
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('penjualanLangsungList') }} " class="nav-link "> Penjualan Langsung</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('daftarPenjualanLangsung') }} " class="nav-link "> Daftar Penjualan Langsung</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('ReturPenjualanList') }}" class="nav-link "> Retur Penjualan</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('penjualanTitipanList') }}" class="nav-link "> Penjualan Titipan</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('daftarPenjualanTitipan') }} " class="nav-link "> Daftar Penjualan Titipan</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('HistoryCetakFakturList') }}" class="nav-link "> History Cetak Ulang Faktur</a>
                            </li>
                            @endif
                            @if(\Gate::allows('as_master') || \Gate::allows('as_logistik'))
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('suratJalanList') }}" class="nav-link "> DO/SJ</a>
                            </li>
                            @endif
                          </ul>
                        </li>
                        @endif
                        @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan'))
                        <li aria-haspopup="true" class="dropdown-submenu ">
                          <a href="javascript:;" class="nav-link nav-toggle ">
                            Online Order
                            <span class="arrow"></span>
                          </a>
                          <ul class="dropdown-menu">
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('orderList') }} " class="nav-link "> Order</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('history-orderList') }}" class="nav-link "> History Order</a>
                            </li>
                          </ul>
                        </li>
                        @endif
                        @if(\Gate::allows('as_master') || \Gate::allows('as_purchasing'))
                        <li aria-haspopup="true" class="dropdown-submenu ">
                          <a href="javascript:;" class="nav-link nav-toggle ">
                            Pembelian
                            <span class="arrow"></span>
                          </a>
                          <ul class="dropdown-menu">
                            <li aria-haspopup="true" class=" ">
                                  <!-- <a href="{{ route('poSupplierList') }}" class="nav-link "> Purchase Order</a> -->
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('poSupplierDaftar') }}" class="nav-link "> Purchase Orders</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('daftarWo') }}" class="nav-link "> Work Orders List</a>
                            </li>
                                <!-- <li aria-haspopup="true" class=" ">
                                  <a href="{{ route('createPembelian') }}" class="nav-link"> Pembelian</a>
                                </li> -->

                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('daftarPembelian') }}" class="nav-link "> Daftar Pembelian Supplier</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('daftarReturPembelian') }}" class="nav-link "> Return Pembelian</a>
                            </li>
                          </ul>
                        </li>
                        @endif
                        @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan') || \Gate::allows('as_sales'))
                        <li aria-haspopup="true" class="dropdown-submenu ">
                          <a href="javascript:;" class="nav-link nav-toggle ">
                            Laporan Penjualan
                            <span class="arrow"></span>
                          </a>
                          <ul class="dropdown-menu">
                            @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan'))
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('penjualanView')}}" class="nav-link btn-laporan"> All Penjualan Kasir</a>
                            </li>
                            {{-- <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('penjualanStokView')}}" class="nav-link btn-laporan"> All Penjualan Stock</a>
                            </li> --}}
                            <li aria-haspopup="true" class=" ">
                              <a class="nav-link btn-laporan-stok"> All Penjualan Stock</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('penjualanLangsungView')}}" class="nav-link btn-laporan-customer"> Penjualan Stock Langsung</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('rekapPenjualanLangsungView')}}" class="nav-link btn-laporan"> Rekap Penjualan Stock Langsung</a>
                            </li>
                            @endif
                            @if(\Gate::allows('as_logistik') || \Gate::allows('as_master'))
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('suratJalanSisaView')}}" class="nav-link btn-laporan"> Sisa Surat Jalan</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('suratJalanDetailView')}}" class="nav-link btn-laporan"> Detail Surat Jalan</a>
                            </li>
                            @endif
                            @if(\Gate::allows('as_master') || \Gate::allows('as_penjualan'))
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('penjualanTitipanView')}}" class="nav-link btn-laporan-customer"> Penjualan Stock order</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('rekapPenjualanTitipanView')}}" class="nav-link btn-laporan"> Rekap Penjualan Order</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('returPenjualanView')}}" class="nav-link btn-laporan"> Retur Penjualan Stock</a>
                            </li>
                            @endif
                            @if(\Gate::allows('as_sales') || \Gate::allows('as_master'))
                            <li aria-haspopup="true" class=" ">
                              <a href="{{route('salesView')}}" class="nav-link"> Omset Sales</a>
                            </li>
                            {{-- <li aria-haspopup="true" class=" ">
                              <a data-href="{{route('omsetSalesDetailView')}}" class="nav-link btn-laporan"> Detail Omset Sales</a>
                            </li> --}}
                            @endif
                          </ul>
                        </li>
                        @endif
                        @if(\Gate::allows('as_master') || \Gate::allows('as_purchasing'))
                        <li aria-haspopup="true" class="dropdown-submenu ">
                          <a href="javascript:;" class="nav-link nav-toggle ">
                            Laporan Pembelian
                            <span class="arrow"></span>
                          </a>
                          <ul class="dropdown-menu">
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('laporanPembelian') }}" class="nav-link"> Laporan Pembelian Stock</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('laporanRekapPembelian') }}" class="nav-link"> Laporan Rekap Pembelian Stock</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('laporanPembelianTunaiKredit') }}" class="nav-link"> Laporan Pembelian Tunai Kredit</a>
                            </li>
                          </ul>
                        </li>
                        <li aria-haspopup="true" class="dropdown-submenu ">
                          <a href="javascript:;" class="nav-link nav-toggle ">
                            Lap. Return Pembelian
                            <span class="arrow"></span>
                          </a>
                          <ul class="dropdown-menu">
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('lapReturnPembelian') }}" class="nav-link"> Lap Return Pembelian</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                              <a href="{{ route('lapReturnPembelianTunaiKredit') }}" class="nav-link"> Lap Return Tunai Kredit</a>
                            </li>
                          </ul>
                        </li>
                        @endif
                      </ul>
                  </li>
                  @endif

                  @if(\Gate::allows('as_master') || \Gate::allows('as_accounting'))
                  <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown {{ in_array($menu, $menuAll['hutang_piutang'])?'active':'' }}">
                    <a href="javascript:;">
                      <span class="icon-wallet"></span> Hutang/Piutang
                      <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-left">
                      <li aria-haspopup="true" class="dropdown-submenu ">
                        <a href="javascript:;" class="nav-link nav-toggle ">
                          Hutang
                          <span class="arrow"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{route('hutangSuplier')}}">
                              Hutang Suplier
                            </a>
                          </li>
                          <li>
                            <a href="{{route('hutangLain')}}">
                              Hutang Lain-Lain
                            </a>
                          </li>
                          <li>
                            <a href="{{route('hutangCek')}}" class="nav-link nav-toggle ">
                              Hutang Cek/BG
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li aria-haspopup="true" class="dropdown-submenu ">
                        <a href="javascript:;" class="nav-link nav-toggle ">
                          Piutang
                          <span class="arrow"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{route('piutangPelanggan')}}">
                              Piutang Pelanggan
                            </a>
                          </li>

                          <li>
                            <a href="{{route('piutangLain')}}">
                              Piutang Lain-Lain
                            </a>
                          </li>
                          <li>
                            <a href="{{route('chequeBg')}}" class="nav-link nav-toggle ">
                              Piutang Cek/BG
                            </a>
                          </li>
                        </ul>
                      </li>

                      <li aria-haspopup="true" class="dropdown-submenu ">
                        <a href="javascript:;" class="nav-link nav-toggle ">
                          Laporan
                          <span class="arrow"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{route('kartuHutangSupplier')}}" class="nav-link nav-toggle ">
                              Kartu Hutang
                            </a>
                          </li>
                          <li>
                            <a href="{{route('kartuPiutang')}}" class="nav-link nav-toggle ">
                              Kartu Piutang
                            </a>
                          </li>
                          <li>
                            <a href="{{route('umurHutang')}}" class="nav-link nav-toggle ">
                              Umur Hutang
                            </a>
                          </li>
                          <li>
                            <a href="{{route('umurPiutang')}}" class="nav-link nav-toggle ">
                              Umur Piutang
                            </a>
                          </li>
                          <li aria-haspopup="true" class=" ">
                            <a data-href="{{route('hutangView')}}" class="nav-link btn-laporan"> Laporan Hutang</a>
                          </li>
                          <li aria-haspopup="true" class=" ">
                            <a data-href="{{route('piutangView')}}" class="nav-link btn-laporan"> Laporan Piutang</a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  @endif

                  @if(\Gate::allows('as_master') || \Gate::allows('as_accounting'))
                  <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown {{ in_array($menu, $menuAll['accounting'])?'active':'' }}">
                    <a href="javascript:;">
                      <span class="icon-bar-chart"></span> Accounting
                      <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 200px">
                      <li>
                        <div class="mega-menu-content">
                          <div class="row">
                            <div class="col-md-12">
                              <ul class="mega-menu-submenu">
                                <li>
                                  <a href="{{route('jurnal-umum',['jurnal'=>'jurnal'])}}"> Jurnal Umum </a>
                                </li>
                                <li>
                                  <a href="{{route('daftarAsset')}}"> Asset </a>
                                </li>
                                <li>
                                  <a href="{{route('BOKList')}}"> Biaya Operasional Kendaraan</a>
                                </li>
                                <li aria-haspopup="true" class="dropdown-submenu ">
                                  <a href="javascript:;" class="nav-link nav-toggle ">
                                    Laporan
                                    <span class="arrow"></span>
                                  </a>
                                  <ul class="dropdown-menu">
                                    <li>
                                      <a href="{{route('jurnalUmum')}}"> Laporan Jurnal Umum </a>
                                    </li>
                                    <li>
                                      <a href="{{route('bukuBesar')}}"> Buku Besar </a>
                                    </li>
                                    <li>
                                      <a href="{{route('rugiLaba')}}"> Rugi/Laba </a>
                                    </li>
                                    <li>
                                      <a href="{{route('rugiLabaPerBarang')}}"> Rugi/Laba Per Barang </a>
                                    </li>
                                    <li>
                                      <a href="{{route('arusKas')}}"> Arus Kas </a>
                                    </li>
                                    <li>
                                      <a href="{{route('neraca')}}"> Neraca </a>
                                    </li>
                                    <li>
                                      <a href="{{route('neraca-tidak-balance')}}"> Neraca Tidak Balance </a>
                                    </li>
                                    <li>
                                      <a href="{{route('penyusutanAsset')}}"> Penyusutan Asset </a>
                                    </li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>
                  @endif
                  @if(\Gate::allows('as_master') || \Gate::allows('as_admin'))
                  <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown {{ in_array($menu, $menuAll['utilitas'])?'active':'' }}">
                    <a href="javascript:;">
                      <span class="icon-settings"></span> Utilitas
                      <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 200px">
                      <li>
                        <div class="mega-menu-content">
                          <div class="row">
                            <div class="col-md-12">
                              <ul class="mega-menu-submenu">
                                <li>
                                  <a href="{{ route('userList') }}"> User </a>
                                </li>
                                <li>
                                  <a href="{{ route('profile') }}"> Profile </a>
                                </li>
                                <li>
                                  <a href="{{ route('sliderList') }}"> Home Slider </a>
                                </li>
                                <li>
                                  <a href="{{ route('promoList') }}"> Promo </a>
                                </li>
                                <!-- <li>
                                  <a href="#"> Exit </a>
                                </li> -->
                              </ul>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>
                  @endif
                </ul>
              </div>
                    <!-- END MEGA MENU -->
            </div>
          </div>
                <!-- END HEADER MENU -->
        </div>
              <!-- END HEADER -->
      </div>
    </div>
    <div class="page-wrapper-row full-height">
      <div class="page-wrapper-middle">
        <div class="page-container">
          <div class="page-content-wrapper">
            <div class="page-head">
              <div class="container-fluid">
                <div class="page-title">
                  <h1>{{ $title }}</h1>
                </div>
                <div class="pull-right">
                  {!! $breadcrumb !!}
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="page-content">
              <div class="container-fluid">
                @yield('body')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-wrapper-row">
    <div class="page-wrapper-bottom">
              <!-- BEGIN FOOTER -->
              <!-- BEGIN INNER FOOTER -->
      <div class="page-footer">
        <div class="container"> 2018 &copy; Graha Kita By
          <a target="_blank" href="http://ganeshcomstudio.com">Ganeshcom Studio</a>
        </div>
      </div>
      <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
      </div>
              <!-- END INNER FOOTER -->
              <!-- END FOOTER -->
    </div>
  </div>
</div>

    <div class="modal" id="modal-laporan" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Laporan
            </h4>
          </div>
          <div class="modal-body form">
            <form action="" class="form-horizontal form-laporan" role="form" method="post">
              {{ csrf_field() }}
              <div class="form-body">
                <div class="row" id="input-date">
                  {{-- <div class="input-daterange"> --}}
                    <div class="col-md-5">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" name="start_date" id="start_date" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{date('Y-m-d')}}">
                      </div>
                    </div>
                    <div class="col-md-2 text-center">
                      <h4>S/d</h4>
                    </div>
                    <div class="col-md-5">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" name="end_date" id="end_date" class="form-control" data-date-format="yyyy-mm-dd" value="{{date('Y-m-d')}}">
                      </div>
                    </div>
                  {{-- </div> --}}
                </div>
                <div class="row">
                  <div class="text-center">
                    <label class="radio-inline"><input type="radio" name="report" value="print" checked>Print</label>
                    <label class="radio-inline"><input type="radio" name="report" value="excel">Excel</label>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="text-center">
                    <button type="submit" class="btn green">Simpan</button>
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <span id="data-laporan-stok"
    data-form-token="{{ csrf_token() }}"
    data-route-kategory="{{ route('dataKategoryRow') }}"
    data-route-group="{{ route('dataGroupRow') }}"
    data-route-merek="{{ route('dataMerekRow') }}"
    data-route-supplier="{{ route('dataSupplierRow') }}">
    </span>
    <div class="modal" id="modal-laporan-stok" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Laporan
            </h4>
          </div>
          <div class="modal-body form">
            <form action="{{route('penjualanStok')}}" class="form-horizontal form-laporan-stok" role="form" method="post" target="_blank">
              {{ csrf_field() }}
              <div class="form-body">
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" name="start_date" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-2 text-center">
                      <h4>S/d</h4>
                    </div>
                    <div class="col-md-5">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" name="end_date" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Kategory</label>
                  <div class="col-md-9">
                    <select id="data-ktg" class="form-control" data-live-search="true" name="ktg_kode">
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Group</label>
                  <div class="col-md-9">
                    <select id="data-grp" class="form-control" data-live-search="true" name="grp_kode">
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Merek</label>
                  <div class="col-md-9">
                    <select id="data-mrk" class="form-control" data-live-search="true" name="mrk_kode">
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Supplier</label>
                  <div class="col-md-9">
                    <select id="data-spl" class="form-control" data-live-search="true" name="spl_kode">
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Tipe PPN</label>
                  <div class="col-md-9">
                    <select class="form-control" name="ppn" id="ppn">
                      <option value="all" selected>All</option>
                      <option value="10">PPN</option>
                      <option value="0">NON PPN</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="text-center">
                    <label class="radio-inline"><input type="radio" name="report" value="print" checked>Print</label>
                    <label class="radio-inline"><input type="radio" name="report" value="excel">Excel</label>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="text-center">
                    <button type="submit" class="btn green">Simpan</button>
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <span id="data-laporan-customer"
    data-form-token="{{ csrf_token() }}"
    data-route-customer="{{ route('dataCustomerRow') }}">
    </span>

    <div class="modal" id="modal-laporan-customer" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Laporan
            </h4>
          </div>
          <div class="modal-body form">
            <form action="" class="form-horizontal form-laporan-customer" role="form" method="post" target="_blank">
              {{ csrf_field() }}
              <div class="form-body">
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" name="start_date" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-2 text-center">
                      <h4>S/d</h4>
                    </div>
                    <div class="col-md-5">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" name="end_date" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Customer</label>
                  <div class="col-md-9">
                    <select id="data-cus" class="form-control" data-live-search="true" name="cus_kode">
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="text-center">
                    <label class="radio-inline"><input type="radio" name="report" value="print" checked>Print</label>
                    <label class="radio-inline"><input type="radio" name="report" value="excel">Excel</label>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="text-center">
                    <button type="submit" class="btn green">Simpan</button>
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


  <span id="base-data" data-base-url="{{ url('/') }}"></span>

    <!--[if lt IE 9]>
    <script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/ie8.fix.min.js') }}"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
    type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"
    type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}"
    type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/layouts/layout3/scripts/layout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout3/scripts/demo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/chartjs/Chart.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.29/dist/sweetalert2.min.js"></script>
    <script>
      const app = new Vue({
        el: '#header_notification_bar',
        data: {
          orders: {},
          total:''
        },
        mounted() {
          this.getOrders();
          this.listen();
        },
        methods: {
          getOrders() {
            axios.get('/api/order/notif')
            .then((response) => {
              this.orders = response.data.order
              this.total = _.keys(this.orders).length
            })
            .catch(function (error) {
              // console.log(error);
            });
          },
          listen() {
            // Echo.channel('orders')
            // .listen('NewOrder', (response) => {
            //   this.orders.unshift(response.order)
            //   this.total = _.keys(this.orders).length
            // })

            Echo.private(`App.Models.mUser.{{auth()->user()->user_kode}}`)
            .notification((notification) => {
              // console.log(notification);
              this.orders.unshift(notification.order);
              this.total = _.keys(this.orders).length;

              var audio = document.getElementById("notification_sound");

              var playPromise = audio.play();

              if (playPromise !== undefined) {
                playPromise.then(_ => {
                })
                .catch(error => {
                  // console.log(error);
                });
              }

              if ($('#header_notification_bar').hasClass("show") || $('#header_notification_bar').hasClass("open")) {
              }
              else {
                $('#header_notification_bar').children('a').trigger('click');
              }

              const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000
              });

              Toast.fire({
                type: 'warning',
                title: 'Ada Online Order Baru '+notification.order.order_kode+', dari '+notification.order.karyawan.kry_nama
              })
              // swal({
              //   title: 'Perhatian',
              //   text: 'Ada Online Order Baru '+notification.order.order_kode+', dari '+notification.order.karyawan.kry_nama,
              //   type: 'warning'
              // });
            });
          }
        }
      })
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
  @yield('js')
</body>
</html>
