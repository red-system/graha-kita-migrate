<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailHargaCustomer extends Model
{
  public $incrementing = false;
  protected $table = 'tb_detail_harga_customer';
  protected $primaryKey = 'det_harga_cus_kode';
  public $timestamps = false;

}
