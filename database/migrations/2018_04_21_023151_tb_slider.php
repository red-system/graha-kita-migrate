<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_slider', function (Blueprint $table) {
           $table->increments('sldr_kode');
           $table->date('sldr_tgl')->nullable();
           $table->string('sldr_deskripsi')->nullable();
           $table->text('sldr_img')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_slider');
     }
}
