<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDeliveryOrder extends Model
{
  public $incrementing = false;
  protected $table = 'tb_delivery_order';
  protected $primaryKey = 'sjt_kode';
  // public $timestamps = false;

  public function detail()
  {
    return $this->hasMany('App\Models\mDeliveryOrderDetail', 'do_kode');
  }

  public function langsung()
  {
    return $this->belongsTo('App\Models\mPenjualanLangsung', 'no_faktur', 'pl_no_faktur')->select('pl_no_faktur', 'cus_kode');
  }

  public function titipan()
  {
    return $this->belongsTo('App\Models\mPenjualanTitipan', 'no_faktur', 'pt_no_faktur')->select('pt_no_faktur', 'cus_kode');
  }
}
