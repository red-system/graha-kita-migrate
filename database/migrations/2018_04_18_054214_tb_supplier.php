<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_supplier', function (Blueprint $table) {
         $table->increments('spl_kode');
         $table->string('spl_nama')->nullable();
         $table->string('spl_alamat')->nullable();
         $table->string('spl_telp')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_supplier');
     }
}
