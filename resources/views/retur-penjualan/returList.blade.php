@php
use App\Models\mDeliveryOrder;
@endphp

@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1_1" data-toggle="tab"> Penjualan Langsung </a>
                </li>
                <li>
                  <a href="#tab_1_2" data-toggle="tab"> Penjualan Titipan </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active in" id="tab_1_1">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                    <thead>
                      <tr class="">
                        <th width="10" style="font-size:10px"> No </th>
                        <th style="font-size:10px"> No Faktur </th>
                        <th style="font-size:10px"> Tanggal </th>
                        <th style="font-size:10px"> Jenis Pembayaran </th>
                        <th style="font-size:10px"> Pelanggan </th>
                        <th style="font-size:10px"> Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($dataList as $row)
                        {{-- @if (mDeliveryOrder::where('no_faktur', $row->pl_no_faktur)->exists()) --}}
                          <tr>
                            <td style="font-size:10px"> {{ $no++ }}. </td>
                            <td style="font-size:10px"> {{ $row->pl_no_faktur }} </td>
                            <td style="font-size:10px"> {{ $row->pl_tgl }} </td>
                            <td style="font-size:10px"> {{ $row->pl_transaksi }} </td>
                            <td style="font-size:10px"> {{ $row->cus_nama }} </td>
                            <td style="font-size:10px">
                              <div class="btn-group btn-group-xs">
                                <a type="button" class="btn btn-success" href="{{ route('ReturPenjualanEdit', ['kode'=>$row->pl_no_faktur, 'type'=>'langsung']) }}">
                                  <span class="icon-pencil"></span> Retur
                                </a>
                              </div>
                            </td>
                          </tr>
                        {{-- @endif --}}
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane" id="tab_1_2">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                    <thead>
                      <tr class="">
                        <th width="10" style="font-size:10px"> No </th>
                        <th style="font-size:10px"> No Faktur </th>
                        <th style="font-size:10px"> Tanggal </th>
                        <th style="font-size:10px"> Jenis Pembayaran </th>
                        <th style="font-size:10px"> Pelanggan </th>
                        <th style="font-size:10px"> Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($dataList_PT as $row)
                        <tr>
                          <td style="font-size:10px"> {{ $no_2++ }}. </td>
                          <td style="font-size:10px"> {{ $row->pt_no_faktur }} </td>
                          <td style="font-size:10px"> {{ $row->pt_tgl }} </td>
                          <td style="font-size:10px"> {{ $row->pt_transaksi }} </td>
                          <td style="font-size:10px"> {{ $row->cus_nama }} </td>
                          <td style="font-size:10px">
                            {{-- @if (mDeliveryOrder::where('no_faktur', $row->pt_no_faktur)->exists()) --}}
                              <div class="btn-group btn-group-xs">
                                <a type="button" class="btn btn-success" href="{{ route('ReturPenjualanEdit', ['kode'=>$row->pt_no_faktur, 'type'=>'titipan']) }}">
                                  <span class="icon-pencil"></span> Retur
                                </a>
                              </div>
                            {{-- @endif --}}
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
