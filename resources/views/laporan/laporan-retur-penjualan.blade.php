<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Retur Penjualan</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <div class="portlet light ">
            {{-- <h4>Penjualan Langsung</h4> --}}
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Customer</th>
                  <th style="font-size:12px">Pelaksana</th>
                  <th style="font-size:12px">Nama Barang</th>
                  <th style="font-size:12px">Alasan</th>
                  <th style="font-size:12px">Keterangan</th>
                  <th style="font-size:12px">Ongkos Angkut</th>
                  <th style="font-size:12px">Potongan Penjualan</th>
                  <th style="font-size:12px">Potongan Piutang</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $row)
                  @php
                  $no_2 = 0;
                  @endphp
                  @foreach($row->detail as $rowDet)
                    <tr>
                      @if ($no_2 < 1)
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $no++ }}. </td>
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->no_faktur }} </td>
                        @php
                          $kode_P = substr($row->no_faktur, 7, 3);
                        @endphp
                        @if ($kode_P == 'PLG')
                          <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->penjualanL->cus_nama }} </td>
                        @else
                          <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->penjualanT->cus_nama }} </td>
                        @endif
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->pelaksana }} </td>
                      @endif
                      <td style="font-size:12px"> {{ $rowDet->brg_nama }} </td>
                      @if ($no_2 < 1)
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->alasan }} </td>
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->keterangan }} </td>
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->ongkos_angkut, 2, "." ,",") }} </td>
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->potongan_penjualan, 2, "." ,",") }} </td>
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->potongan_piutang, 2, "." ,",") }} </td>
                        <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->total_retur, 2, "." ,",") }} </td>
                      @endif
                    </tr>
                    @php
                    $no_2++;
                    @endphp
                  @endforeach
                @endforeach
                <tr>
                  <td colspan="9" align="center" style="font-weight:bold">Total</td>
                  <td style="font-weight:bold" align="right">{{number_format($grand_total, 2, "." ,",")}}</td>
                </tr>
                {{-- <tfoot> --}}
              {{-- </tfoot> --}}
              </tbody>
            </table>
            {{-- <br>
            <br>
            <h4>Penjualan Titipan</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Sales Person</th>
                  <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Ongkos Angkut</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataPT as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no_2++ }}. </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $kode_PT.$row->pt_no_faktur }} </td>
                    <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                    <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                    <td style="font-size:12px"> {{ number_format($row->pt_disc_nom, 0, "." ,".")  }} </td>
                    <td style="font-size:12px"> {{ number_format($row->pt_ongkos_angkut, 0, "." ,".")  }} </td>
                    <td style="font-size:12px"> {{ number_format($row->grand_total, 0, "." ,".")  }} </td>
                  </tr>
                @endforeach
                <tfoot>
                  <tr>
                    <td colspan="5" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold">{{number_format($DiscPT, 0, "." ,".") }}</td>
                    <td style="font-weight:bold">{{number_format($AngkutPT, 0, "." ,".") }}</td>
                    <td style="font-weight:bold">{{number_format($TotalPT, 0, "." ,".") }}</td>
                  </tr>
              </tfoot>
              </tbody>
            </table> --}}
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
