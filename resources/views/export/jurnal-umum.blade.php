<table>
  <tr>
    <td>Jurnal Umum</td>
  </tr>
  <tr>
    <td>{{date('d M Y', strtotime($start_date))}} - {{date('d M Y', strtotime($end_date))}}</td>
  </tr>
</table>
<table>
  <thead>
    <tr>
      <th>No</th>
      <th>Tanggal</th>
      <th>No Bukti</th>
      <th>Keterangan</th>
      <th>No Akun</th>
      <th>Debet</th>
      <th>Kredit</th>
      <th>Catatan</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1; $total_debet=0;$total_kredit=0;?>
    @foreach($jurnal as $jmu)
    <tr>
      <td> {{ $no++ }}. </td>
      <td> {{ date('d M Y', strtotime($jmu->jmu_tanggal)) }} </td>
      <td><center> {{ $jmu->no_invoice }} </center></td>
      <td colspan="5"> {{ $jmu->jmu_keterangan }}</td>
    </tr>
    <?php $this_ju_debet = 0; $this_ju_kredit=0;?>
    @foreach($jmu->transaksi as $trs)
    <tr>                                
      <td></td>
      <td></td>
      <td></td>                             
      <td <?php if($trs->trs_jenis_transaksi=='kredit') echo 'align="right"';?>>   {{ $trs->trs_nama_rekening }}</td>
      <td <?php if($trs->trs_jenis_transaksi=='kredit') echo 'align="right"';?>>{{ $trs->trs_kode_rekening }}</td>
      <td align="right">{{ number_format($trs->trs_debet,2) }} </td>
      <td align="right">{{ number_format($trs->trs_kredit,2) }} </td>
      <td>{{ $trs->trs_catatan }} </td>
    </tr>
    <?php
      $total_debet += $trs->trs_debet;
      $total_kredit += $trs->trs_kredit;
    ?>
    <?php
                                                $this_ju_debet += $trs->trs_debet;
                                                $this_ju_kredit += $trs->trs_kredit;
                                                $total_debet += $trs->trs_debet;
                                                $total_kredit += $trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>                                
                                                <td></td>
                                                <td></td>
                                                <td></td>                                
                                                <td style="font-size:11px;"> </td>
                                                <td style="font-size:11px;"></td>
                                                <td style="font-size:11px;font-weight: bold;" align="right">   {{ number_format($this_ju_debet,2) }} </td>
                                                <td style="font-size:11px;font-weight: bold;" align="right">   {{ number_format($this_ju_kredit,2) }} </td>
                                                <td style="font-size:11px;">@if($this_ju_debet==$this_ju_kredit)
                                                    balance
                                                    @else
                                                    not balance
                                                    @endif  {{number_format($this_ju_debet-$this_ju_kredit,2)}}</td>
                                                
                                                <td>
                                                    
                                                </td>                                 
                                            </tr>
                                            <tr>
                                                <td colspan="8"></td>
                                            </tr>
  @endforeach
</tbody>
<tfoot>
  <tr class="">
    <th></th>
    <th colspan="4" align="center"><strong>TOTAL</strong></th>
    <th><strong>{{number_format($total_debet,2)}}</strong></th>
    <th><strong>{{number_format($total_kredit,2)}}</strong></th>
      @if($debet_jml==$kredit_jml || $total_debet==0 && $total_kredit==0)
      <th>
        Status : <font color="green">Balance</font>
      </th>
      @endif
      @if($debet_jml!=$kredit_jml)
      <th>
        Status : <font color="red">Not Balance {{$total_debet-$total_kredit}}</font>
      </th>
      @endif
    </tr>
  </tfoot>
</table>
