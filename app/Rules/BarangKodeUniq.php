<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\Models\mBarang;

class BarangKodeUniq implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $where = [
            'brg_kode'=>$value
        ];
        $check = mBarang::where($where)->count();
        return $check == 0 ? TRUE :  FALSE;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Kode Barang sudah Ada';
    }
}
