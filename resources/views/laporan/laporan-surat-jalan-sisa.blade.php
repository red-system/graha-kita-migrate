<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Sisa Surat Jalan</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          {{-- <h4>Penjualan Langsung</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th width="10">No </th>
                  <th>Tanggal</th>
                  <th>No. Faktur</th>
                  <th>Barang Barkode</th>
                  <th>Nama Barang</th>
                  <th>Customer</th>
                  <th>QTY</th>
                  <th>STN</th>
                </tr>
              </thead>
                <tbody>
                  @foreach($dataPL as $row)
                    @foreach ($row->detail as $key)
                      @if ($key->qty_akhir != 0)
                        <tr>
                          <td> {{ $no++ }}. </td>
                          <td> {{ date('Y-m-d', strtotime($row->sjl_tgl)) }} </td>
                          <td> {{ $row->pl_no_faktur }} </td>
                          <td> {{ $key->barang['brg_barcode'] }} </td>
                          <td> {{ $key->barang['brg_nama'] }} </td>
                          <td> {{ $row->customer['cus_nama'] }} </td>
                          <td> {{ $key->qty_akhir }} </td>
                          <td> {{ $key->barang->satuan['stn_nama'] }} </td>
                        </tr>
                      @endif
                    @endforeach
                  @endforeach
              </tbody>
            </table>
          </div>
          <br> --}}
          <h4>Penjualan Titipan</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th width="10">No </th>
                  <th>Tanggal</th>
                  <th>No. Faktur</th>
                  <th>Barang Barkode</th>
                  <th>Nama Barang</th>
                  <th>Customer</th>
                  <th>QTY</th>
                  <th>STN</th>
                </tr>
              </thead>
                <tbody>
                  @foreach($dataPT as $row)
                    @foreach ($row->detail as $key)
                      @if ($key->qty_akhir != 0)
                        <tr>
                          <td> {{ $no_2++ }}. </td>
                          <td> {{ date('Y-m-d', strtotime($row->sjt_tgl)) }} </td>
                          <td> {{ $row->pt_no_faktur }} </td>
                          <td> {{ $key->barang['brg_barcode'] }} </td>
                          <td> {{ $key->barang['brg_nama'] }} </td>
                          <td> {{ $row->customer['cus_nama'] }} </td>
                          <td> {{ $key->qty_akhir }} </td>
                          <td> {{ $key->barang->satuan['stn_nama'] }} </td>
                        </tr>
                      @endif
                    @endforeach
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
