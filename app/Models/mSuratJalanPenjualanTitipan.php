<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mSuratJalanPenjualanTitipan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_surat_jalan_penjualan_titipan';
  protected $primaryKey = 'sjt_kode';
  // public $timestamps = false;

  public function faktur()
  {
    return $this->belongsTo('App\Models\mPenjualanTitipan', 'pt_no_faktur')->select('pt_no_faktur', 'cus_kode');
  }

  public function det_sj()
  {
    return $this->belongsTo('App\Models\mDetailSuratJalanPT', 'sjt_kode', 'sjt_kode');
  }
}
