<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <table width="100%" border="0">
      <thead>
        <tr>
          <td colspan="8">
            <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118</p>
          </td>
        </tr>
        <tr>
          <td colspan="5"> <p><b>D/O Barang</b></p> </td>
          <td> <p>No. Ftr</p> </td>
          <td colspan="2"> <p>: {{$faktur['kode_bukti_id']}}</p> </td>
        </tr>
        <tr>
          <td colspan="5"> <p><b>Untuk</b></p> </td>
          <td> <p>No. D.O.</p> </td>
          <td colspan="2"> <p>: {{$faktur['do']}}</p> </td>
        </tr>
        <tr>
          <td colspan="5"></td>

          <td> <p>No. S.J.</p> </td>
          <td colspan="2"> <p>: {{$faktur['sj']}}</p> </td>
        </tr>
        <tr>
          <td colspan="5"></td>

          <td> <p>Tgl.</p> </td>
          <td colspan="2"> <p>: {{$faktur['tglPrint']}}</p> </td>
        </tr>
        <tr>
          <td colspan="5"></td>

          <td> <p>Yth.</p> </td>
          <td colspan="2"> <p>: {{$faktur->cus_nama['cus_nama']}}</p> </td>
        </tr>
        <tr>
          <td colspan="8"><br></td>
        </tr>
        <br>
        <tr>
          <th> <p>No</p> </th>
          <th colspan="2"> <p>Kode Stock</p> </th>
          <th colspan="2"> <p>Nama Stock</p> </th>
          <th> <p>Banyak</p> </th>
          <th> <p>Satuan</p> </th>
          <th> <p>Pack</p> </th>
        </tr>
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
      </thead>
      <tbody>
        @foreach ($faktur->detail as $key)
          <tr>
            <td align="center"><p>{{$faktur->no++}}</p></td>
            <td align="center" colspan="2"><p>{{$key->brg_barcode}}</p></td>
            <td align="center" colspan="2"><p>{{$key->brg_nama}}</p></td>
            <td align="center"><p>{{number_format($key->dikirim, 2, "." ,",")}}</p></td>
            <td align="center"><p>{{$key->stn_nama}}</p></td>
            <td align="center"><p>-</p></td>
          </tr>
        @endforeach
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
        <tr>
          <td colspan="8">
            <p>D/O ini berlaku 10 (sepuluh) hari, mulai tanggal pengeluaran D/O ini</p>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="8">
            <br>
          </td>
        </tr>

          <tr>
            <td align="center" colspan="3">
              <b><p>Yang Menerima D/O</p></b>
            </td>

            <td align="center" colspan="3">
              <b><p>Dari : ANGSA KUSUMA INDAH</p></b>
            </td>

            <td align="center" colspan="3">
              <b><p>Penerima Barang</p></b>
            </td>
          </tr>
          <tr>
            <td colspan="3">
              <br>
              <br>
              <br>
            </td>

            <td colspan="3">
              <br>
              <br>
              <br>
            </td>

            <td colspan="3">
              <br>
              <br>
              <br>
            </td>
          </tr>
          <tr>
            <td align="center" colspan="3">
              <p>(......................................)</p>
            </td>

            <td align="center" colspan="3">
              <p>(......................................)</p>
            </td>

            <td align="center" colspan="3">
              <p>(......................................)</p>
            </td>
          </tr>
      </tfoot>
    </table>
  </body>
</html>
