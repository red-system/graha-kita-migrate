@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script>
  <script src="{{ asset('js/barang.js') }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          {{-- <div class="portlet light "> --}}
            <div class="portlet light">
              <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                <i class="fa fa-plus"></i> Tambah Barang
              </button>
              <div class="btn-group-md pull-right">
                <button class="btn btn-success btn-print" data-toggle="modal" href="#modal-print">
                  <i class="glyphicon glyphicon-print"></i> Print to PDF
                </button>
                <button class="btn btn-success btn-print" data-toggle="modal" href="#modal-print-excel">
                  <i class="glyphicon glyphicon-print"></i> Print to Excel
                </button>
              </div>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th style="font-size:10px"> No </th>
                    <th style="font-size:10px"> Kode Barang </th>
                    <th style="font-size:10px"> Nama Barang </th>
                    <th style="font-size:10px"> Satuan </th>
                    <th style="font-size:10px"> Kategory </th>
                    <th style="font-size:10px"> Group Stok </th>
                    <th style="font-size:10px"> Merek </th>
                    <th style="font-size:10px"> H.Beli Akhir </th>
                    <th style="font-size:10px"> H.Beli Max </th>
                    <th style="font-size:10px"> H.Beli + PPN </th>
                    <th style="font-size:10px"> HPP </th>
                    <th style="font-size:10px"> H.Jual Retail </th>
                    <th style="font-size:10px"> H.Jual Partai </th>
                    {{-- <th style="font-size:10px"> Margin </th> --}}
                    <th style="font-size:10px"> QOH </th>
                    <th style="font-size:10px"> Aktif </th>
                    {{-- <th style="font-size:10px"> Supplier </th> --}}
                    <th style="font-size:10px"> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td style="font-size:10px"> {{ $no++ }}. </td>
                      <td style="font-size:10px"> {{ $row['brg_barcode'] }} </td>
                      <td style="font-size:10px; white-space: nowrap"> {{ $row->brg_nama }} </td>
                      <td style="font-size:10px"> {{ $row->satuan['stn_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->kategory['ktg_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->group['grp_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->merek['mrk_nama'] }} </td>
                      <td style="font-size:10px"> {{number_format($row->brg_harga_beli_terakhir, 2, "," ,".")}}</td>
                      <td style="font-size:10px"> {{number_format($row->brg_harga_beli_tertinggi, 2, "," ,".")}}</td>
                      <td style="font-size:10px"> {{number_format($row->hargaPPN, 2, "," ,".")}}</td>
                      <td style="font-size:10px"> {{number_format($row->brg_hpp, 2, "," ,".")}}</td>
                      <td style="font-size:10px"> {{number_format($row->brg_harga_jual_eceran, 2, "," ,".")}}</td>
                      <td style="font-size:10px"> {{number_format($row->brg_harga_jual_partai, 2, "," ,".")}}</td>
                      {{-- <td style="font-size:10px"> {{ $row->margin }} </td> --}}
                      <td style="font-size:10px"> {{ $row->QOH }} </td>
                      <td style="font-size:10px; white-space: nowrap"> {{ $row->brg_status }} </td>
                      {{-- <td style="font-size:10px"> {{ $row->supplier['spl_nama'] }} </td> --}}
                      <td style="font-size:10px; white-space: nowrap">
                        <div class="btn-group-xs">
                          <button class="btn btn-info btn-edit-stok" data-href="{{ route('stokbarangEdit', ['kode'=>$row->brg_kode, 'kode_gudang'=>$row->gdg_kode]) }}">
                            <span class="icon-pencil"></span> Stok
                          </button>
                          <button class="btn btn-success btn-edit" data-href="{{ route('barangEdit', ['kode'=>$row->brg_kode]) }}">
                            <span class="icon-pencil"></span> Edit
                          </button>
                          <button class="btn btn-danger btn-delete" data-href="{{ route('barangDelete', ['kode'=>$row->brg_kode]) }}">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          {{-- </div> --}}
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Barang
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{ route('barangInsert') }}" class="form-horizontal form-send-barang" role="form" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Kategory</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="ktg_kode">
                        @foreach($k_product as $kategory)
                          <option value="{{ $kategory->ktg_kode }}">{{ $kategory->ktg_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Group</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="grp_kode">
                        @foreach($g_product as $group)
                          <option value="{{ $group->grp_kode }}">{{ $group->grp_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Merk</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="mrk_kode">
                        @foreach($merek as $merk)
                          <option value="{{ $merk->mrk_kode }}">{{ $merk->mrk_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Gudang</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="gdg_kode">
                        @foreach($gudang as $gdg)
                          <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Kode Barang</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_barcode" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Nama</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_nama" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Satuan</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="stn_kode">
                        @foreach($satuan as $stn)
                          <option value="{{ $stn->stn_kode }}">{{ $stn->stn_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Product Image</label>
                    <div class="col-md-9">
                      <img id="imgScr" src="{{asset('img/icon/no_image.png')}}" alt="image" width="100">
                      <div id="message"></div>
                      <input type="file" min="0" class="form-control" name="brg_product_img" id="imgBtn">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Maximum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_maximum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Minimum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_minimum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Terakhir</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_terakhir">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Tertinggi</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_tertinggi">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">HPP</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_hpp">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Jual Eceran</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_eceran">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">harga Jual Partai</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_partai">
                    </div>
                  </div>
                  {{-- <div class="form-group">
                    <label class="col-md-3 control-label">Supplier</label>
                    <div class="col-md-9">
                      <select class="form-control" name="spl_kode">
                        @foreach($supplier as $spl)
                          <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div> --}}
                  <div class="form-group">
                    <label class="col-md-3 control-label">PPN</label>
                    <div class="col-md-8">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_ppn_dari_supplier_persen">
                    </div>
                    <label class="control-label">%</label>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Status</label>
                    <div class="col-md-2">
                      <input type="checkbox" class="form-control" name="brg_status" value="Aktif">
                    </div>
                    <label class="control-label">Aktif</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  <div class="modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Barang
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send-barang-edit" role="form" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Kategory</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="ktg_kode">
                        @foreach($k_product as $kategory)
                          <option value="{{ $kategory->ktg_kode }}">{{ $kategory->ktg_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Group</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="grp_kode">
                        @foreach($g_product as $group)
                          <option value="{{ $group->grp_kode }}">{{ $group->grp_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Merk</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="mrk_kode">
                        @foreach($merek as $merk)
                          <option value="{{ $merk->mrk_kode }}">{{ $merk->mrk_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Gudang</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="gdg_kode">
                        @foreach($gudang as $gdg)
                          <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Kode Barang</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_barcode" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Nama</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_nama" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Satuan</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="stn_kode">
                        @foreach($satuan as $stn)
                          <option value="{{ $stn->stn_kode }}">{{ $stn->stn_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Product Image</label>
                    <div class="col-md-9">
                      <img id="imgScrEdit" alt="image" width="100">
                      <div id="messageEdit"></div>
                      <input type="file" min="0" class="form-control" name="brg_product_img" id="imgBtnEdit">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Maximum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_maximum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Minimum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_minimum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Terakhir</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_terakhir">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Tertinggi</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_tertinggi">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">HPP</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_hpp">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Jual Eceran</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_eceran">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">harga Jual Partai</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_partai">
                    </div>
                  </div>
                  {{-- <div class="form-group">
                    <label class="col-md-3 control-label">Supplier</label>
                    <div class="col-md-9">
                      <select class="form-control" name="spl_kode">
                        @foreach($supplier as $spl)
                          <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div> --}}
                  <div class="form-group">
                    <label class="col-md-3 control-label">PPN</label>
                    <div class="col-md-8">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_ppn_dari_supplier_persen">
                    </div>
                    <label class="control-label">%</label>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Status</label>
                    <div class="col-md-2">
                      <input type="checkbox" class="form-control" name="brg_status" value="Aktif">
                    </div>
                    <label class="control-label">Aktif</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal-edit-stok" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-stok" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">No Seri</label>
                <div class="col-md-9">
                  <input id="stok-brg_kode" type="hidden" name="brg_kode" class="form-control" readonly>
                  {{-- <input type="hidden" name="gdg_kode" class="form-control" readonly> --}}
                  <input id="stok-brg_no_seri" type="text" name="brg_no_seri" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">QTY</label>
                <div class="col-md-9">
                  <input id="stok-stok" type="number" min="0" class="form-control" name="stok" value="0" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Stok HPP</label>
                <div class="col-md-9">
                  <input id="stok-stk_hpp" type="number" min="0" name="stk_hpp" class="form-control" value="0" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Gudang</label>
                <div class="col-md-9">
                  <select id="stok-gdg_kode" class="form-control" name="gdg_kode" required>
                    @foreach($gudang as $gdg)
                      <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select id="stok-spl_kode" class="form-control selectpicker" required data-live-search="true" name="spl_kode" required>
                    @foreach($supplier as $spl)
                      <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <br>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                  <tr class="">
                    <th> No Seri </th>
                    <th> QTY </th>
                    <th> Gudang </th>
                    <th> Supplier </th>
                    <th> Action </th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="button" class="btn green stk_btn_save">Simpan</button>
                  {{-- <button type="submit" class="btn green">Simpan</button> --}}
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-print" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Print
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{route('barangPrint')}}" class="form-horizontal" role="form" method="post" target="_blank">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="type">
                    <option value="General">General</option>
                    <option value="Detail">Detail</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kategory</label>
                <div class="col-md-9">
                  <select name="ktg_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    @foreach($k_product as $kategory)
                      <option value="{{ $kategory->ktg_kode }}">{{ $kategory->ktg_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Group</label>
                <div class="col-md-9">
                  <select name="grp_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    @foreach($g_product as $group)
                      <option value="{{ $group->grp_kode }}">{{ $group->grp_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Merek</label>
                <div class="col-md-9">
                  <select name="mrk_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    @foreach($merek as $merk)
                      <option value="{{ $merk->mrk_kode }}">{{ $merk->mrk_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select name="spl_kode" class="form-control selectpicker" required data-live-search="true">
                  <option value="0">---Semua---</option>
                    @foreach($supplier as $spl)
                      <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-print-excel" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Print
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{route('barangPrintExcel')}}" class="form-horizontal" role="form" method="post" target="_blank">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Kategory</label>
                <div class="col-md-9">
                  <select name="ktg_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    @foreach($k_product as $kategory)
                      <option value="{{ $kategory->ktg_kode }}">{{ $kategory->ktg_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Group</label>
                <div class="col-md-9">
                  <select name="grp_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    @foreach($g_product as $group)
                      <option value="{{ $group->grp_kode }}">{{ $group->grp_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Merek</label>
                <div class="col-md-9">
                  <select name="mrk_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    @foreach($merek as $merk)
                      <option value="{{ $merk->mrk_kode }}">{{ $merk->mrk_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select name="spl_kode" class="form-control selectpicker" required data-live-search="true">
                  <option value="0">---Semua---</option>
                    @foreach($supplier as $spl)
                      <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-stok_sample" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Stok Sample
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{route('stokbarangStokSample')}}" class="form-horizontal" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Qty</label>
                <div class="col-md-9">
                  <input class="form-control" type="hidden" name="stk_kode_sample">
                  <input class="form-control" type="hidden" name="brg_no_seri_sample">
                  <input class="form-control" type="hidden" name="brg_kode_sample">
                  <input class="form-control" type="hidden" name="brg_barcode_sample">
                  <input class="form-control" type="hidden" name="gdg_kode_sample">
                  <input class="form-control" type="hidden" name="spl_kode_sample">
                  <input class="form-control" type="number" min="0" name="qty_sample" value="0">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
