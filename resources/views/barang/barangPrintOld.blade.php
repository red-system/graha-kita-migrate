<html>
  <head>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4>Laporan Barang</h4>
          <h4>Tanggal {{$time}}</h4>
          <br>
          <div class="portlet light">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px"> No Seri </th>
                  <th style="font-size:12px"> Kode Barang </th>
                  <th style="font-size:12px"> Nama Barang </th>
                  <th style="font-size:12px"> Satuan </th>
                  <th style="font-size:12px"> Kategory </th>
                  <th style="font-size:12px"> Group Stok </th>
                  <th style="font-size:12px"> Merek </th>
                  <th style="font-size:12px"> H.Beli Akhir </th>
                  <th style="font-size:12px"> H.Beli Max </th>
                  <th style="font-size:12px"> H.Beli + PPN </th>
                  <th style="font-size:12px"> HPP </th>
                  <th style="font-size:12px"> H.Jual Retail </th>
                  <th style="font-size:12px"> H.Jual Partai </th>
                  <th style="font-size:12px"> Margin </th>
                  <th style="font-size:12px"> QOH </th>
                  <th style="font-size:12px"> Aktif </th>
                  <th style="font-size:12px"> Supplier </th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataList as $row)
                  @foreach ($row->stok_gudang as $key)
                    <tr>
                      <td style="font-size:12px"> {{ $no++ }}. </td>
                      <td style="font-size:12px"> {{ $key['brg_seri'] }} </td>
                      <td style="font-size:12px"> {{ $row['brg_barcode'] }} </td>
                      <td style="font-size:12px"> {{ $row->brg_nama }} </td>
                      <td style="font-size:12px"> {{ $row->satuan['stn_nama'] }} </td>
                      <td style="font-size:12px"> {{ $row->kategory['ktg_nama'] }} </td>
                      <td style="font-size:12px"> {{ $row->group['grp_nama'] }} </td>
                      <td style="font-size:12px"> {{ $row->merek['mrk_nama'] }} </td>
                      <td style="font-size:12px"> {{ $row->brg_harga_beli_terakhir }} </td>
                      <td style="font-size:12px"> {{ $row->brg_harga_beli_tertinggi }} </td>
                      <td style="font-size:12px"> {{ $row->hargaPPN }} </td>
                      <td style="font-size:12px"> {{ $row->brg_hpp }} </td>
                      <td style="font-size:12px"> {{ $row->brg_harga_jual_eceran }} </td>
                      <td style="font-size:12px"> {{ $row->brg_harga_jual_partai }} </td>
                      <td style="font-size:12px"> {{ $row->margin }} </td>
                      <td style="font-size:12px"> {{ $row->QOH }} </td>
                      <td style="font-size:12px"> {{ $row->brg_status }} </td>
                      <td style="font-size:12px"> {{ $row->supplier['spl_nama'] }} </td>
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
