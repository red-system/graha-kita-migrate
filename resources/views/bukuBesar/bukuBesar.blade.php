@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />   
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <form action="{{ route('filterByKodePerkiraan') }}" class="form-send" role="form" method="post">
                                    {{ csrf_field() }}
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-2" style="padding-top: 10px;">Filter by : </label>
                                                <div class="col-md-6">
                                                    <select class="form-control input-medium" name="master_id" data-placeholder="Kode Perkiraan">
                                                        <option value="0" <?php if($kode_perkiraan=='0'){echo 'selected';}?>>-All Kode Perkiraan-</option>
                                                        @foreach($perkiraan as $pkr)
                                                        <option value="{{$pkr->master_id}}" <?php if($kode_perkiraan=='{{$pkr->master_id}}') echo 'selected';?>>{{$pkr->mst_kode_rekening}} - {{$pkr->mst_nama_rekening}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="col-md-4">
                                                <input type="hidden" name="bulan" value="{{$bulan}}">
                                                <input type="hidden" name="tahun" value="{{$tahun_periode}}">
                                                <button type="submit" class="btn green"><span><i class="fa fa-search"></i></span></button>                                                    
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-4" style="padding-top: 15px;" align="right">
                                    <a class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode" >
                                        Pilih Period
                                    </a>
                                    <a type="button" class="btn btn-danger" href="{{route('printBukuBesar', ['bulan'=>$bulan, 'tahun'=>$tahun_periode, 'master_id'=>$kode_perkiraan])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                </div>
                            </div>
                                                                   
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h1><center>Buku Besar</center></h1>
                                        @if($bulan=='1')
                                            <h2><center>Periode Januari {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='2')
                                            <h2><center>Periode Pebruari {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='3')
                                            <h2><center>Periode Maret {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='4')
                                            <h2><center>Periode April {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='5')
                                            <h2><center>Periode Mei {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='6')
                                            <h2><center>Periode Juni {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='7')
                                            <h2><center>Periode Juli {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='8')
                                            <h2><center>Periode Agustus {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='9')
                                            <h2><center>Periode September {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='10')
                                            <h2><center>Periode Oktober {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='11')
                                            <h2><center>Periode November {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='12')
                                            <h2><center>Periode Desember {{$tahun_periode}}</center></h2> 
                                        @endif
                                    </div>

                                    @if($jml_detailPerkiraan == 0)
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="success">
                                                <th><center> Tanggal </center></th>
                                                <th><center> No Bukti </center></th>
                                                <th><center> Keterangan </center></th>
                                                <th><center> Debet </center></th>
                                                <th><center> Kredit </center></th>
                                                <th><center> Saldo()</center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                            <tr>
                                                <td colspan="6" align="center">Belum Ada Data</td>                                            
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="5" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>0</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    @endif
                                    @if($jml_detailPerkiraan > 0)
                                    @foreach($detailPerkiraan as $pkr)
                                    <hr>
                                    <br>
                                    <table width="100%" id="{{$pkr->perkiraan->mst_nama_rekening}}">
                                        <tbody>
                                        <tr>
                                            <td width="50%"><h5><strong>Perkiraan : {{$pkr->perkiraan->mst_nama_rekening}}</strong></h5></td>
                                            <td align="right" width="50%"><h5><strong>Kode Rek : {{$pkr->perkiraan->mst_kode_rekening}}</strong></h5></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="success">
                                                <th><center> Tanggal </center></th>
                                                <th><center> No Bukti </center></th>
                                                <th><center> Keterangan </center></th>
                                                <th><center> Debet </center></th>
                                                <th><center> Kredit </center></th>
                                                @if($pkr->perkiraan->mst_normal == 'kredit')
                                                <th><center> Saldo(Kredit)</center></th>
                                                @endif
                                                @if($pkr->perkiraan->mst_normal == 'debet')
                                                <th><center> Saldo(Debet)</center></th>
                                                @endif
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <td>1/{{$bulan}}/{{$tahun_periode}}</td>
                                                <td></td>
                                                <td colspan="3">Saldo awal</td>
                                                @if($pkr->perkiraan->mst_normal == 'kredit')
                                                <td align="right">{{number_format($pkr->msd_awal_kredit)}}</td>
                                                @endif
                                                @if($pkr->perkiraan->mst_normal == 'debet')
                                                <td align="right">{{number_format($pkr->msd_awal_debet)}}</td>
                                                @endif                                                
                                            </tr>
                                            <?php
                                                if($pkr->perkiraan->mst_normal == 'kredit'){
                                                    $saldo_awal=$pkr->msd_awal_kredit;
                                                    // $saldo_akhir=$saldo_awal+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                }
                                                if($pkr->perkiraan->mst_normal == 'debet'){
                                                    $saldo_awal=$pkr->msd_awal_debet;
                                                    // $saldo_akhir=$saldo_awal-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                }
                                            ?>
                                            @foreach($pkr->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi)
                                            <?php
                                                $total=0;
                                                if($pkr->perkiraan->mst_normal == 'kredit'){
                                                    // $saldo_awal=$pkr->msd_awal_kredit;
                                                    $saldo_awal=$saldo_awal+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                }
                                                if($pkr->perkiraan->mst_normal == 'debet'){
                                                    // $saldo_awal=$pkr->msd_awal_debet;
                                                    $saldo_awal=$saldo_awal-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                }
                                                $total=$total+$saldo_awal;                                                
                                            ?>
                                            <tr>
                                                <td>{{ date('d M Y', strtotime($transaksi->jurnalUmum->jmu_tanggal)) }}</td>
                                                <td>{{ $transaksi->jurnalUmum->no_invoice }}</td>
                                                <td>{{ $transaksi->trs_catatan }}</td>
                                                <td align="right">{{ number_format($transaksi->trs_debet) }}</td>
                                                <td align="right">{{ number_format($transaksi->trs_kredit) }}</td>
                                                <td align="right">{{number_format($saldo_awal)}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="5" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($saldo_awal)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    @endforeach
                                    @endif
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeBukuBesar') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Bulan</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-inline input-medium" name="bulan" required>
                                    <option value="">Pilih Bulan</option>
                                    @foreach($tanggal as $tgl)
                                    <option value="{{$tgl}}">{{$tgl}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Tahun</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-inline input-medium" name="tahun" required>
                                    <option value="">--Pilih Tahun--</option>
                                    @foreach($tahun as $thn)
                                    <option value="{{$thn}}">{{$thn}}</option>
                                    @endforeach
                                </select>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah-jurnal-umum" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Tambah Jurnal Umum
                </h4>
            </div>
            <div class="modal-body form-horizontal">
                <form action="" class="form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <div class="form-group">
                        <label class="col-md-3">Tanggal Transaksi</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-inline input-medium">
                                    <option value="">--Pilih Tanggal--</option>
                                    @foreach($tanggal as $tgl)
                                    <option value="{{$tgl}}">{{$tgl}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Kode Bukti</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="no_bg_cek">
                            </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Keterangan</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="keterangan">
                            </div>
                      </div>
                      <div class="form-group">
                         <div class="form-group">
                      <div class="col-md-4" style="margin-top: -20px">
                        <br>
                          <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal"> 
                            <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                          </button>
                      </div>
                  </div>
                      </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                    <thead>
                        <tr>
                            <th>Kode Perkiraan</th>
                            <th>Jenis Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Tipe Arus Kas</th>
                            <th>Catatan</th>
                            <th>Menu</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>

                    
                    
                </form>
            </div>
        </div>
    </div>
</div>


@stop