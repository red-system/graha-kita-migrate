@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        {{-- <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                            <i class="fa fa-plus"></i> Tambah Stok
                        </button>
                        <br /><br /> --}}
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="">
                                    <th width="10" style="font-size:10px"> No </th>
                                    <th style="font-size:10px"> Tanggal </th>
                                    <th style="font-size:10px"> No Faktur </th>
                                    <th style="font-size:10px"> Person </th>
                                    <th style="font-size:10px"> Keterangan </th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($dataList as $row)
                                <tr>
                                  <td style="font-size:10px"> {{ $no++ }}. </td>
                                  <td style="font-size:10px"> {{ $row->tgl }} </td>
                                  <td style="font-size:10px"> {{ $row->no_faktur }} </td>
                                  <td style="font-size:10px"> {{ $row->person }} </td>
                                  <td style="font-size:10px"> {{ $row->keterangan}} </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Tambah Stok
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('stokGlobalInsert.detail') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <input type="hidden" class="form-control" name="brg_kode" value="{{$id}}">
                      <div class="form-group">
                        <label class="col-md-3 control-label">Seri</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="brg_no_seri">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Harga Satuan</label>
                        <div class="col-md-9">
                          <input type="number" min="0" value="0" class="form-control" name="stk_hpp">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Stok</label>
                        <div class="col-md-9">
                          <input type="number" min="0" value="0" class="form-control" name="stok">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Supplier</label>
                        <div class="col-md-9">
                          <select class="form-control" name="spl_kode">
                            @foreach($supplier as $spl)
                              <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-pencil"></i> Edit Stok
                </h4>
            </div>
            <div class="modal-body form">
                <form action="" class="form-horizontal form-send" role="form" method="put">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <input type="hidden" class="form-control" name="brg_kode" value="{{$id}}">
                      <div class="form-group">
                        <label class="col-md-3 control-label">Seri</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="brg_no_seri">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Harga Satuan</label>
                        <div class="col-md-9">
                          <input type="number" min="0" value="0" class="form-control" name="stk_hpp">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Stok</label>
                        <div class="col-md-9">
                          <input type="number" min="0" value="0" class="form-control" name="stok">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 control-label">Supplier</label>
                        <div class="col-md-9">
                          <select class="form-control" name="spl_kode">
                            @foreach($supplier as $spl)
                              <option value="{{ $spl->spl_kode }}">{{ $spl->spl_nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> --}}
@stop
