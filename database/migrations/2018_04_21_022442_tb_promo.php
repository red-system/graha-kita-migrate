<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tb_promo', function (Blueprint $table) {
          $table->increments('prm_kode')->nullable();
          $table->date('prm_tgl')->nullable();
          $table->string('prm_judul')->nullable();
          $table->text('prm_deskripsi')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_promo');
    }
}
