<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Summary Stok</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr>
                  <th> Kode Barang </th>
                  <th> Nama Barang </th>
                  <th> Satuan </th>
                  <th> Kategory </th>
                  <th> Group Stok </th>
                  <th> Merek </th>
                  <th> Lokasi </th>
                  <th> Total In </th>
                  <th> Total Out </th>
                  <th> Last Stok </th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataList as $row)
                  <tr>
                    <td> {{ $row->brg_barcode }} </td>
                    <td> {{ $row->brg_nama }} </td>
                    <td> {{ $row->stn_nama }} </td>
                    <td> {{ $row->ktg_nama }} </td>
                    <td> {{ $row->grp_nama }} </td>
                    <td> {{ $row->mrk_nama }} </td>
                    <td> {{ $row->gdg_nama }} </td>
                    @if ($row->stokIn == null)
                      <td> - </td>
                    @else
                      <td> {{ $row->stokIn }} </td>
                    @endif
                    @if ($row->stokOut == null)
                      <td> - </td>
                    @else
                      <td> {{ $row->stokOut }} </td>
                    @endif
                    <td> {{ $row->QOH }} </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
