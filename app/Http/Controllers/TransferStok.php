<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;
use App\Helpers\Main;
use App\Models\mBarang;
use App\Models\mStok;
use App\Models\mGudang;
use App\Models\mArusStok;
use App\Models\mTransferStok;
use App\Models\mDetailTransferStok;
use App\Models\mSatuan;

use Validator;

class TransferStok extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Transfer Stok';
      $this->kodeLabel = 'barang';
  }

  protected function createID() {
    $kode = $this->main->kodeLabel('transferStok');
    $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'.'.$kode.'.';
    // $lastorder = mTransferStok::where(\DB::raw('LEFT(trf_kode, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('RIGHT(trf_kode, 4) as no_order')->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mTransferStok::where('trf_kode', 'like', '%'.$preffix.'%')->max('trf_kode');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no.$user;
  }

  function rules($request) {
      $rules = [
          'stok' => 'required',
          'gdg_kode' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function index() {

      ini_set("memory_limit", "-1");
      ini_set('MAX_EXECUTION_TIME', -1);

      $breadcrumb = [
          'Transfer Stok'=>route('transferStokList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'transfer_stok';
      $data['title'] = $this->title;
      $data['dataList'] = mBarang::all();
      foreach ($data['dataList'] as &$barang) {
        $total=0;
        $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
        foreach ($barang['stok_gudang'] as $gdg) {
          $total += $gdg->stok;
          $gdg->gudang;
        }
        $barang['QOH'] = $total;
        $barang['kategory'] = $barang->kategoryProduct;
        $barang['group'] = $barang->groupStok;
        $merek = $barang->merek;
        $satuan = $barang->satuan;
        $supplier = $barang->supplier;
      }
      $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
                        ->get();
      $kodeBarang = $this->main->kodeLabel('barang');
      $data['gudang'] = mGudang::all();
      $data['barang'] = $barang;
      $data['kodeBarang'] = $kodeBarang;
      // $data['kodeTransfer'] = $this->main->kodeLabel('transferStok');
      $data['history'] = mTransferStok::orderBy('trf_tgl', 'desc')->get();
      foreach ($data['history'] as &$key) {
        $key->gudang;
      }
      return view('transfer-stok/transferStokList', $data);
  }

  function detail($brg_kode='') {
    $breadcrumb = [
      'Detail Transfer Stok'=>route('transferStokDetail', ['kode'=>$brg_kode]),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'transfer_stok';
    $data['title'] = $this->title;
    $data['dataList'] = mBarang::where('brg_kode', $brg_kode)->get();
    foreach ($data['dataList'] as &$barang) {
      $total=0;
      $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
      foreach ($barang['stok_gudang'] as $gdg) {
        $total += $gdg->stok;
        $gdg->gudang;
      }
      $barang['QOH'] = $total;
      $barang['kategory'] = $barang->kategoryProduct;
      $barang['group'] = $barang->groupStok;
      $merek = $barang->merek;
      $satuan = $barang->satuan;
      $supplier = $barang->supplier;
    }
    $data['gudang'] = mGudang::all();

    // return $data;
    return view('transfer-stok/detail-transferStokList', $data);
  }

  function edit($brg_kode='', $gdg_kode='', $stok='') {
      $response = [
          'action'=>route('transferStokUpdate', ['kode'=>$brg_kode, 'kode_gudang'=>$gdg_kode]),
          'field'=>mBarang::find($brg_kode),
          'stok' =>$stok
      ];
      return $response;
  }

  function update(Request $request, $brg_kode, $gdg_kode) {
      $this->rules($request->all());
      $time = Carbon::now();

      // $barang = mBarang::where('brg_kode', $brg_kode)->first();
      // $barang->brg_nama = $request->brg_nama;
      // $barang->save();

      $stok = mStok::where('brg_kode', $brg_kode)->where('gdg_kode', $gdg_kode)->first();
      $stok->stok = ($stok->stok - $request->stok);
      $stok->save();

      $stokTransfer = mStok::where('brg_kode', $brg_kode)->where('gdg_kode', $request->gdg_kode)->first();
      if ($stokTransfer != null) {
        $stokTransfer->stok = ($stokTransfer->stok + $request->stok);
        $stokTransfer->save();
      } else {
        $newStok = new mStok();
        $newStok->brg_kode = $brg_kode;
        $newStok->gdg_kode = $request->gdg_kode;
        $newStok->stok = $request->stok;
        $newStok->save();
      }
      return [
          'redirect'=>route('transferStokDetail', ['kode'=>$brg_kode])
      ];
  }

  function barangRow(Request $request) {
      $brg_kode = $request->input('brg_kode');
      $brg = mBarang::leftJoin('tb_satuan', 'tb_satuan.stn_kode', '=', 'tb_barang.stn_kode')
                    ->leftJoin('tb_gudang', 'tb_gudang.gdg_kode', '=', 'tb_barang.gdg_kode')
                    ->where('brg_barcode', $brg_kode)
                    ->first();

      $stk_gdg = mStok::where('brg_kode', $brg->brg_kode)->groupBy('brg_no_seri')->get();
      $seri = '<option value="0">Pilih No Seri</option>';

      foreach ($stk_gdg as $stk) {
        $seri .= '<option value="'.$stk->brg_no_seri.'">'.$stk->brg_no_seri.'</option>';
      }

      $response = [
          'nama'=>$brg->brg_nama,
          'satuan'=>$brg->stn_nama,
          'stok'=>$seri,
      ];

      return $response;
  }

  function gudangRow(Request $request) {
      $brg_kode = $request->input('brg_kode');
      $brg_no_seri = $request->input('brg_no_seri');
      $brg = mBarang::where('brg_barcode', $brg_kode)->first();
      $stk_gdg = mStok::leftJoin('tb_gudang', 'tb_gudang.gdg_kode', '=', 'tb_stok.gdg_kode')
      ->where('brg_kode', $brg->brg_kode)
      ->where('brg_no_seri', $brg_no_seri)
      // ->groupBy('tb_stok.gdg_kode')
      ->get();
      $gudang = '<option value="0">Pilih Gudang</option>';
      foreach ($stk_gdg as $stk) {
        $gudang .= '<option value="'.$stk->gdg_kode.'">'.$stk->gdg_nama.'</option>';
      }

      $response = [
          'gudang'=>$gudang,
      ];

      return $response;
  }

  function stokRow(Request $request) {
      $brg_kode = $request->input('brg_kode');
      $brg_no_seri = $request->input('brg_no_seri');
      $gdg_kode = $request->input('gdg_kode');
      $brg = mBarang::where('brg_barcode', $brg_kode)->first();

      $stk_gdg = mStok::select('stok')
      ->where('brg_kode', $brg->brg_kode)
      ->where('brg_no_seri', $brg_no_seri)
      ->where('gdg_kode', $gdg_kode)
      ->first();

      // $response = [
      //     'stok'=>$stk_gdg,
      // ];

      return $stk_gdg;
  }

  function insert(Request $request) {
    // return $request;
    $id = $this->createID();
    //transfer
    $trf_tgl = $request->input('trf_tgl');
    $trf_tujuan = $request->input('gdg_kode');
    $trf_keterangan = $request->input('trf_keterangan');
    $transfer = new mTransferStok();
    $transfer->trf_kode = $id;
    $transfer->trf_tgl = $trf_tgl;
    $transfer->trf_tujuan = $trf_tujuan;
    $transfer->trf_keterangan = $trf_keterangan;
    $transfer->save();

    //detail_transfer
    $brg_kode = $request->input('brg_kode');
    $stok_input = $request->input('stok');
    $brg_no_seri = $request->input('brg_no_seri');
    $asal_gudang = $request->input('asal_gudang');

    foreach ($brg_kode as $key => $value) {
      // $brg = mBarang::where('brg_barcode', $brg_kode[$key])->first();
      $transferDetail = new mDetailTransferStok();
      // $transferDetail->brg_kode = $brg->brg_kode;
      $transferDetail->brg_kode = $brg_kode[$key];
      $transferDetail->trf_kode = $id;
      $transferDetail->brg_no_seri = $brg_no_seri[$key];
      // $gdg = mBarang::where('brg_kode', $brg_kode[$key])->select('gdg_kode')->first();
      // $transferDetail->trf_det_asal = $gdg->gdg_kode;
      $transferDetail->trf_det_asal = $asal_gudang[$key];
      $transferDetail->trf_det_stok = $stok_input[$key];
      $transferDetail->save();

      //Stok
      $stok = mStok::where('brg_kode', $brg_kode[$key])
      ->where('gdg_kode', $asal_gudang[$key])
      ->where('brg_no_seri', $brg_no_seri[$key])
      ->first();
      $stok->stok = ($stok->stok - $stok_input[$key]);
      $stok->save();

      //transfer
      $stokTransfer = mStok::where('brg_kode', $brg_kode[$key])
      ->where('gdg_kode', $trf_tujuan)
      ->where('brg_no_seri', $brg_no_seri[$key])
      ->first();
      if ($stokTransfer != null) {
        $stokTransfer->stok = ($stokTransfer->stok + $stok_input[$key]);
        $stokTransfer->save();
      } else {
        $newStok = new mStok();
        $newStok->brg_kode = $brg_kode[$key];
        $newStok->gdg_kode = $trf_tujuan;
        $newStok->stok = $stok_input[$key];
        $newStok->spl_kode = $stok->spl_kode;
        $newStok->stk_hpp = 0;
        $newStok->brg_no_seri = $brg_no_seri[$key];
        $newStok->save();
      }

      $total=0;
      $data = mStok::where('brg_kode', $brg_kode[$key])->get();
      foreach ($data as $gdg) {
        $total += $gdg->stok;
      }
      $QOH = $total;

      $time = Carbon::now();
      $arusStok = new mArusStok();
      $arusStok->ars_stok_date = $time;
      $arusStok->brg_kode = $brg_kode[$key];
      $arusStok->stok_in = 0;
      $arusStok->stok_out = $stok_input[$key];
      $arusStok->stok_prev = $QOH;
      $arusStok->gdg_kode = $asal_gudang[$key];
      $arusStok->keterangan = 'transfer gudang '.$id;
      $arusStok->save();

      $arusStok = new mArusStok();
      $arusStok->ars_stok_date = $time;
      $arusStok->brg_kode = $brg_kode[$key];
      $arusStok->stok_in = $stok_input[$key];
      $arusStok->stok_out = 0;
      $arusStok->stok_prev = $QOH;
      $arusStok->gdg_kode = $trf_tujuan;
      $arusStok->keterangan = 'transfer gudang '.$id;
      $arusStok->save();
    }

      return [
          'redirect'=>route('transferStokList')
      ];
  }

  function getTransferDetail($trf_kode='') {
      $response = mDetailTransferStok::where('trf_kode', $trf_kode)->get();
      foreach ($response as &$keyGudang) {
        $keyGudang->gudang;
        $barang = $keyGudang->barang;
        // $transfer = $response->transferStok;
        $barang['satuan'] = mSatuan::where('stn_kode', $barang->stn_kode)->select('stn_nama')->first();
        // $stok = mStok::where('brg_kode', $barang->brg_kode)->select('stok')->get();
        // $total=0;
        // foreach ($stok as $stk) {
        //   $total += $stk->stok;
        // }
        // $barang['QOH'] = $total;
      }

      return $response;
  }

  function TransferPrint($trf_kode='') {
      $breadcrumb = [
          'Transfer Stok'=>route('transferStokList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'transfer_stok';
      $data['title'] = $this->title;
      $data['dataTransfer'] = mTransferStok::where('trf_kode', $trf_kode)->first();
      $data['dataTransfer']->gudang;
      $data['dataList'] = mDetailTransferStok::where('trf_kode', $trf_kode)->get();
      foreach ($data['dataList'] as &$keyGudang) {
        $keyGudang->gudang;
        $barang = $keyGudang->barang;
        $barang['satuan'] = mSatuan::where('stn_kode', $barang->stn_kode)->select('stn_nama')->first();
        // $stok = mStok::where('brg_kode', $barang->brg_kode)->select('stok')->get();
        // $total=0;
        // foreach ($stok as $stk) {
        //   $total += $stk->stok;
        // }
        // $barang['QOH'] = $total;
      }
      // return $data;
      return view('transfer-stok/trasferStokPrint', $data);

      // $pdf = PDF::loadView('transfer-stok.trasferStokPrint', $data)
      //             ->setPaper('a4', 'landscape');
      // return $pdf->stream();
  }
}
