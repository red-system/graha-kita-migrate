<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbJenisPembiayaanKendaraan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_jenis_pembiayaan_kendaraan', function (Blueprint $table) {
         $table->increments('jpk_kode');
         $table->string('jpk_nama')->nullable();
         $table->integer('coa_biaya')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_jenis_pembiayaan_kendaraan');
     }
}
