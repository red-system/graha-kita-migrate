<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mGudang;
use App\Models\mBarang;
use Validator,
    DB,
    View;

class DetailPenjualanLangsung extends Controller {

  private $main;
  private $title;
  private $kodeLabel;
  private $kodeLabelText;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penjualan Langsung Detail';
    $this->kodeLabel = 'penjualanLangsung';
    $this->kodeLabelText = $this->main->kodeLabel($this->kodeLabel);
  }

  function index($pl_no_faktur = '') {
    $breadcrumb = [
      'Penjualan Langsung' => route('penjualanLangsungList'),
      $this->kodeLabelText.$pl_no_faktur => route('penjualanLangsungList'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'penjualan_langsung';
    $data['title'] = $this->title;
    $data['dataList'] = mDetailPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)
                                                ->orderBy('detail_pl_kode', 'ASC')
                                                ->get();
    $data['kodeGudang'] = $this->main->kodeLabel('gudang');
    $data['kodeBarang'] = $this->main->kodeLabel('barang');
    $data['gudang'] = mGudang::orderBy('gdg_nama', 'ASC')->get();
    $data['pl_no_faktur'] = $pl_no_faktur;
    $data['barang'] = mBarang::get();

    return view('detailPenjualanLangsung/detailPenjualanLangsungList', $data);
  }

  function rules($request) {
    $rules = [
      'gdg_kode' => 'required',
        'brg_kode' => 'required',
        'satuan' => 'required',
        'harga_jual' => 'required',
        'disc' => 'required',
        'disc_nom' => 'required',
        'harga_net' => 'required',
        'qty' => 'required',
        'terkirim' => 'required',
        'total' => 'required',
    ];
    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    mDetailPenjualanLangsung::insert($data);
    return [
      'redirect' => route('penjualanLangsungList')
    ];
  }

    function edit($detail_pl_kode='') {
        $response = [
            'action'=>route('detailPenjualanLangsungUpdate', ['kode'=>$detail_pl_kode]),
            'field'=>mDetailPenjualanLangsung::find($detail_pl_kode)
        ];
        return $response;
    }

  function update(Request $request, $detail_pl_kode) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    //$data['pl_tgl'] = date('Y-m-d H:i:s');
    mDetailPenjualanLangsung::where(['detail_pl_kode'=>$detail_pl_kode])->update($data);
    return [
      'redirect' => route('detailPenjualanLangsungList')
    ];
  }


  function delete($detail_pl_kode) {
    mDetailPenjualanLangsung::where('detail_pl_kode', $detail_pl_kode)->delete();
  }

}
