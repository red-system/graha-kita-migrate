<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Penjualan Stock Langsung</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <div class="portlet light ">
            @foreach($data as $row)
              @php
                $no=1;
              @endphp
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr>
                  <td colspan="9"><h5><b>Nama Pelanggan : {{$row->cus_nama}}</b></h5></td>
                </tr>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Barang Barkode</th>
                  <th style="font-size:12px">Nama Barang</th>
                  <th style="font-size:12px">QTY</th>
                  <th style="font-size:12px">STN</th>
                  <th style="font-size:12px">Harga</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
                <tbody>
                  @foreach ($row->penjualan as $key)
                    @foreach ($key->detail as $keyDet)
                      <tr>
                        <td style="font-size:12px"> {{ $no++ }}. </td>
                        <td style="font-size:12px"> {{ date('Y-m-d', strtotime($key->pl_tgl)) }} </td>
                        <td style="font-size:12px"> {{ $key->pl_no_faktur }} </td>
                        <td style="font-size:12px"> {{ $keyDet->barang['brg_barcode'] }} </td>
                        <td style="font-size:12px"> {{ $keyDet->barang['brg_nama'] }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($keyDet->qty, 2, "." ,",") }} </td>
                        <td style="font-size:12px"> {{ $keyDet->satuanJ['stn_nama'] }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($keyDet->harga_net, 2, "." ,",") }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($keyDet->total, 2, "." ,",") }} </td>
                      </tr>
                    @endforeach
                  @endforeach
                    <tr>
                      <td colspan="8" align="right" style="font-weight:bold">Total Penjualan Ke {{$row->cus_nama}}</td>
                      <td style="font-weight:bold" align="right">{{number_format($row->grand_total_cus, 2, "." ,",")}}</td>
                    </tr>
                @endforeach
              </tbody>
              {{-- <tfoot> --}}
                <tr>
                  <td colspan="8" align="right" style="font-weight:bold">Grand Total</td>
                  <td style="font-weight:bold" align="right">{{number_format($grand_total, 2, "." ,",")}}</td>
                </tr>
              {{-- </tfoot> --}}
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
