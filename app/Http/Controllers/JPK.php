<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mJPK;
use App\Models\mPerkiraan;

use Validator;

class JPK extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Biaya Operasional';
      $this->kodeLabel = 'jpk';
  }

  function index() {
      $breadcrumb = [
          'JPK'=>route('jpkList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'biaya_operasional';
      $data['title'] = $this->title;
      $dataJPK = mJPK::get();
      foreach ($dataJPK as $key) {
        $key->perkiraan;
      }
      $data['master'] = mPerkiraan::all();
      $data['dataList'] = $dataJPK;
      // return $dataJPK;
      return view('jpk/jpkList', $data);
  }

  function rules($request) {
      $rules = [
          'jpk_nama' => 'required',
          'coa_biaya' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mJPK::insert($request->except('_token'));
      return [
          'redirect'=>route('jpkList')
      ];
  }

  function edit($jpk_kode='') {
      $response = [
          'action'=>route('jpkUpdate', ['kode'=>$jpk_kode]),
          'field'=>mJPK::find($jpk_kode)
      ];
      return $response;
  }

  function update(Request $request, $jpk_kode) {
      $this->rules($request->all());
      mJPK::where('jpk_kode', $jpk_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('jpkList')
      ];
  }

  function delete($jpk_kode) {
      mJPK::where('jpk_kode', $jpk_kode)->delete();
  }
}
