<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mWilayah extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];
  
  public $incrementing = false;
  protected $table = 'tb_wilayah';
  protected $primaryKey = 'wlh_kode';
  public $timestamps = false;

  public function provinsi()
  {
    return $this->belongsTo('App\Models\mProvinsi', 'prov_kode');
  }
}
