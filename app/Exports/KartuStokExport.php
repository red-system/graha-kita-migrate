<?php

namespace App\Exports;

use App\Models\mArusStok;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class KartuStokExport implements FromView
{
    private $start;
    private $end;
    private $gudang;
    private $brg_kode;

    public function __construct($start, $end, $gudang, $brg_kode)
    {
      $this->start = $start;
      $this->end = $end;
      $this->gudang = $gudang;
      $this->brg_kode = $brg_kode;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;
      $gudang = $this->gudang;
      $brg_kode = $this->brg_kode;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      if ($gudang != 'all') {
        $list = mArusStok::leftJoin('tb_barang', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
        ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
        ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
        ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
        ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
        ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
        ->select('ars_stok_kode', 'ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama', 'stok_prev')
        ->where('tb_arus_stok.gdg_kode', $gudang)
        ->where('ars_stok_date', '>=', $start)
        ->where('ars_stok_date', '<=', $end)
        ->orderBy('ars_stok_kode', 'desc')
        ->get();
      }
      else {
        $list = mArusStok::leftJoin('tb_barang', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
        ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
        ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
        ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
        ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
        ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
        ->select('ars_stok_kode', 'ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama', 'stok_prev')
        ->where('ars_stok_date', '>=', $start)
        ->where('ars_stok_date', '<=', $end)
        ->orderBy('ars_stok_kode', 'asc')
        ->get();
      }

      if ($brg_kode != 'all') {
        $data['dataList'] = $list->filter(function ($value, $key) use($brg_kode){
          return $value->brg_kode == $brg_kode;
        });

        $data['dataList']->all();
      }
      else {
        $data['dataList'] = $list;
      }

      return view('export.kartu-stok', $data);
    }
}
