<html moznomarginboxes mozdisallowselectionprint>
  <head>
    <!-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> -->
     <title>Jurnal Umum - Graha Kita</title>
     <!-- <h2 style=”text-align:justify;”>   
            <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
        </h2> -->
  </head>
  <body>
  <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Tahoma;font-size:10px;padding:3px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;}


    </style>
    <div class="container-fluid">
        <h2 style="text-align:justify;font-family: Tahoma;">   
            <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
        </h2>
        <hr>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4 style="font-family: Tahoma"><center>Jurnal Umum</center></h4>
            <h5 style="font-family: Tahoma"><center>{{date('d M Y', strtotime($bulan))}} s/d {{date('d M Y', strtotime($tahun_periode))}}</center></h5>   
          </div>
          <br>
          <div class="portlet light ">
            <table class="tg" width="100%">
                <thead>
                    <tr class="">
                        <th width="5%"><center> No </center></th>
                        <th width="10%"><center> Tanggal </center></th>
                        <th width="15%"><center> No Bukti </center></th>
                        <th width="20%"><center> Keterangan </center></th>
                        <th width="10%"><center> No Akun </center></th>
                        <th width="10%"><center> Debet </center></th>
                        <th width="10%"><center> Kredit </center></th>
                        <th width="15%"><center> Catatan </center></th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach($jurnalUmum as $jmu)
                    <tr>
                        <td align="center"> {{ $no++ }}. </td>
                        <td> {{ date('d M Y', strtotime($jmu->jmu_tanggal)) }} </td>
                        <td><center> {{ $jmu->no_invoice }} </center></td>
                        <td colspan="5"> {{ $jmu->jmu_keterangan }}</td>
                    </tr>
                    @foreach($jmu->transaksi as $trs)
                    <tr>                                
                        <td></td>
                        <td></td>
                        <td></td>               
                        <td <?php if($trs->trs_jenis_transaksi=='kredit') echo 'align="right"';?>>   {{ $trs->trs_nama_rekening }}</td>
                        <td <?php if($trs->trs_jenis_transaksi=='kredit') echo 'align="right"';?>>   {{ $trs->trs_kode_rekening }}</td>
                        <td align="right" >   {{ number_format($trs->trs_debet) }} </td>
                        <td align="right" >   {{ number_format($trs->trs_kredit) }} </td>
                        <td>   {{ $trs->trs_catatan }} </td> 
                    </tr>
                    @endforeach
                    <!-- <tr>
                        <td colspan="8"></td>
                    </tr> -->
                @endforeach
                    <tr class="">
                        <th width="10"></th>
                        <th colspan="4" align="center"><h4><center><strong> TOTAL </strong></center></h4></th>
                        <th> <h4><center><strong>{{number_format($jml_debet)}} </strong></center></h4></th>
                        <th > <h4><center><strong>{{number_format($jml_kredit)}} </th>
                        @if($jml_debet==$jml_kredit && $jml_debet>0 && $jml_kredit>0)
                        <th>
                            <h4><strong>Status : <font color="green">Balance</font></strong></h4>
                        </th>
                        @endif
                        @if($jml_debet!=$jml_kredit)
                        <th>
                            <h4><strong>Status : <font color="red">Not Balance</font></strong></h4>
                        </th>
                        @endif
                        @if($jml_debet==0 && $jml_kredit==0)
                        <th>
                            <h4><strong>Status : <font color="red"></font></strong></h4>
                        </th>
                        @endif
                    </tr>
                    
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
