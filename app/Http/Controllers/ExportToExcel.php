<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Exports\BarangExport;
use App\Exports\AllPenjualanKasirExport;
use App\Exports\PenjualanLangsungExport;
use App\Exports\RekapPenjualanLangsungExport;
use App\Exports\PenjualanTitipanExport;
use App\Exports\RekapPenjualanTitipanExport;
use App\Exports\SuratJalanSisaExport;
use App\Exports\SuratJalanDetailExport;
use App\Exports\ReturPenjualanExport;
use App\Exports\OmsetSalesRekapExport;
use App\Exports\OmsetSalesDetailExport;
use App\Exports\PenjualanStokExport;
use App\Exports\JurnalUmumExport;
use App\Exports\BukuBesarExport;
use App\Exports\RugiLabaExport;
use App\Exports\RugiLabaPerBarangExport;
use App\Exports\UmurPiutangExport;
use App\Exports\UmurHutangExport;
use App\Exports\LaporanPembelianExport;
use App\Exports\LapHutangExport;
use App\Exports\LapPiutangExport;
use App\Exports\KartuHutangExport;
use App\Exports\KartuPiutangExport;
use App\Exports\SummaryStokExport;
use App\Exports\KartuStokExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExportToExcel extends Controller
{
  public function barangPrintExcel(Request $request)
  {
    $time = Carbon::now();
    return Excel::download(new BarangExport($request), 'barang-'.$time.'.xlsx');
  }

  public function all_penjualan_kasir($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new AllPenjualanKasirExport($start, $end), 'all_penjualan_kasir-'.$time.'.xlsx');
  }

  public function viewPenjualanLangsung($start='', $end='', $cus_kode='')
  {
    $time = Carbon::now();
    return Excel::download(new PenjualanLangsungExport($start, $end, $cus_kode), 'penjualan-langsung-'.$time.'.xlsx');
  }

  public function viewRekapPenjualanLangsung($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new RekapPenjualanLangsungExport($start, $end), 'rekap-penjualan-langsung-'.$time.'.xlsx');
  }

  public function viewPenjualanTitipan($start='', $end='', $cus_kode='')
  {
    $time = Carbon::now();
    return Excel::download(new PenjualanTitipanExport($start, $end, $cus_kode), 'penjualan-titipan-'.$time.'.xlsx');
  }

  public function viewRekapPenjualanTitipan($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new RekapPenjualanTitipanExport($start, $end), 'rekap-penjualan-titipan-'.$time.'.xlsx');
  }

  public function viewSuratJalanSisa($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new SuratJalanSisaExport($start, $end), 'sj-sisa-'.$time.'.xlsx');
  }

  public function viewSuratJalanDetail($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new SuratJalanDetailExport($start, $end), 'sj-detail-'.$time.'.xlsx');
  }

  public function viewReturPenjualan($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new ReturPenjualanExport($start, $end), 'retur-penjualan-'.$time.'.xlsx');
  }

  public function omset_sales_rekap($start='', $end='', $mrk_kode='')
  {
    $time = Carbon::now();
    return Excel::download(new OmsetSalesRekapExport($start, $end, $mrk_kode), 'omset-sales-rekap-'.$time.'.xlsx');
  }

  public function omset_sales_detail($start='', $end='', $mrk_kode='')
  {
    $time = Carbon::now();
    return Excel::download(new OmsetSalesDetailExport($start, $end, $mrk_kode), 'omset-sales-detail-'.$time.'.xlsx');
  }

  public function PenjualanStok($request)
  {
    $time = Carbon::now();
    return Excel::download(new PenjualanStokExport($request), 'penjualan-stok-'.$time.'.xlsx');
  }

  public function jurnalPrintExcel(Request $request)
  {
    $time = Carbon::now();
    return Excel::download(new JurnalUmumExport($request), 'jurnal-umum-'.$request->start_date.' - '.$request->end_date.'.xlsx');
  }

  public function bukuBesarPrintExcel(Request $request)
  {
    $time = Carbon::now();
    return Excel::download(new BukuBesarExport($request), 'buku-besar-'.$request->start_date.' - '.$request->end_date.'.xlsx');
  }

  public function rugiLabaPrintExcel(Request $request)
  {
    $time = Carbon::now();
    return Excel::download(new RugiLabaExport($request), 'rugi-laba-'.$request->start_date.' - '.$request->end_date.'.xlsx');
  }

  public function rugiLabaPerbarangPrintExcel(Request $request)
  {
    $time = Carbon::now();
    return Excel::download(new RugiLabaPerBarangExport($request), 'rugi-laba-per-barang-'.$request->start_date.' - '.$request->end_date.'.xlsx');
  }

  public function umurPiutangPrintExcel(Request $request)
  {
    $time = Carbon::now();
    return Excel::download(new UmurPiutangExport($request), 'umur-piutang-'.$request->start_date.' - '.$request->end_date.'.xlsx');
  }

  public function umurHutangPrintExcel(Request $request)
  {
    $time = Carbon::now();
    return Excel::download(new UmurHutangExport($request), 'umur-hutang-'.$request->start_date.' - '.$request->end_date.'.xlsx');
  }

  public function lapPembelian($start_date='',$end_date='',$spl_id='',$mrk='',$grp='',$tipe='')
  {
    $time = Carbon::now();

    return Excel::download(new LaporanPembelianExport($start_date,$end_date,$spl_id,$mrk,$grp,$tipe), $tipe.'-'.$start_date.' - '.$end_date.'.xlsx');

  }

  public function lapHutang($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new LapHutangExport($start, $end), 'laporan-hutang-'.$start.'-'.$end.'.xlsx');
  }

  public function lap_piutang($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new LapPiutangExport($start, $end), 'laporan-piutang-'.$start.'-'.$end.'.xlsx');
  }

  public function kartuHutang($kode='',$start_date='',$end_date='', $coa='')
  {
    $time = Carbon::now();
    return Excel::download(new KartuHutangExport($kode,$start_date,$end_date, $coa), 'kartu-hutang-'.$start_date.'-'.$end_date.'.xlsx');
  }

  public function kartuPiutang($kode='',$start_date='',$end_date='', $coa='')
  {
    $time = Carbon::now();
    return Excel::download(new KartuPiutangExport($kode,$start_date,$end_date, $coa), 'kartu-piutang-'.$start_date.'-'.$end_date.'.xlsx');
  }

  public function SummaryStok($start='', $end='')
  {
    $time = Carbon::now();
    return Excel::download(new SummaryStokExport($start, $end), 'summary-stok-'.$time.'.xlsx');
  }

  public function KartuStok($start='', $end='', $gudang='', $brg_kode='')
  {
    $time = Carbon::now();
    return Excel::download(new KartuStokExport($start, $end, $gudang, $brg_kode), 'kartu-stok-'.$time.'.xlsx');
  }
}
