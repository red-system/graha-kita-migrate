<html moznomarginboxes mozdisallowselectionprint>
    <head>
    <title>Penyusutan Asset {{$thn}}</title>
    <!-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> -->
    </head>
    <body>
    <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                <h2><center><strong>Laporan Penyusutan Asset</strong></center></h2>
                        <h3><center><strong>Tahun {{$thn}}</strong></center></h3>
          </div>
          <br>
          <div class="portlet light ">
          <table class="tg">
                                    <thead>
                                        <tr class="success">
                                            <th rowspan="2" align="center"> No </th>
                                            <th rowspan="2"> Keterangan</th>
                                            <th rowspan="2" width="5%"><center> Jumlah </center></th>
                                            <th rowspan="2" width="5%"> Thn Perolehan </th>
                                            <th rowspan="2" width="5%"> Nilai Perolehan </th>
                                            <th rowspan="2" width="5%">  Nilai Penyusutan </th>
                                            <th colspan="13"><center> Penyusutan {{$thn}}</center></th>
                                            <th rowspan="2" width="5%"> Akumulasi Penyusutan </th>
                                            <th rowspan="2" width="5%"> Nilai Buku </th>
                                        </tr>
                                        <tr class="info">
                                            <th>Thn Lalu</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>Mei</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Ags</th>
                                            <th>Sep</th>
                                            <th>Okt</th>
                                            <th>Nov</th>
                                            <th>Des</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($kategoriAsset as $ktgAsset)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td colspan="20">{{$ktgAsset->ka_nama}}</td>
                                        </tr>
                                        @foreach($ktgAsset->Asset as $asset)
                                        <tr>
                                            <td></td>
                                            <td>{{$asset->nama}}</td>
                                            <td><center>{{$asset->qty}}</center></td>
                                            <td>{{ date('M Y', strtotime($asset->tanggal_beli)) }}</td>
                                            <td>{{ number_format($asset->harga_beli)}}</td>
                                            <td> {{ number_format($asset->beban_perbulan)}} </td>
                                            <td>{{number_format($asset->penyusutanAsset->where('tahun', '<',$thn)->sum('penyusutan_perbulan'),2)}}</td>
                                            @for($i=1;$i<=12;$i++)
                                            <td>{{number_format($asset->penyusutanAsset->where('bulan', '=',$i)->where('tahun', '=',$thn)->sum('penyusutan_perbulan'),2)}}</td>
                                            @endfor
                                            
                                            <td> {{ number_format($asset->akumulasi_beban)}} </td>
                                            <td> {{ number_format($asset->nilai_buku)}} </td>
                                        </tr>
                                        @endforeach
                                        @endforeach
                                    </tbody>
                                </table> 
          </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
