<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDetailSuratJalanPenjualanTitipan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_detail_surat_jalan_penjualan_titipan', function (Blueprint $table) {
         $table->increments('det_sjt_kode');
         $table->integer('sjl_kode')->nullable();
         $table->integer('brg_kode')->nullable();
         $table->integer('qty')->nullable();
         $table->integer('dikirim')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_detail_surat_jalan_penjualan_titipan');
     }
}
