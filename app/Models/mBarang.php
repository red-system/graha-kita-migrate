<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mBarang extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  public $incrementing = false;
  protected $table = 'tb_barang';
  protected $primaryKey = 'brg_kode';
  public $timestamps = false;

  protected $fillable = [];

  public function kategoryProduct()
  {
    return $this->belongsTo('App\Models\mKategoryProduct', 'ktg_kode');
  }

  public function groupStok()
  {
    return $this->belongsTo('App\Models\mGroupStok', 'grp_kode');
  }

  public function merek()
  {
    return $this->belongsTo('App\Models\mMerek', 'mrk_kode');
  }

  public function satuan()
  {
    return $this->belongsTo('App\Models\mSatuan', 'stn_kode');
  }

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode');
  }

  public function stok()
  {
    return $this->hasMany('App\Models\mStok', 'brg_kode');
  }

  public function arus_stok()
  {
    return $this->hasMany('App\Models\mArusStok', 'brg_kode');
  }

  public function hargaCustomer()
  {
    return $this->hasMany('App\Models\mHargaCustomer', 'brg_kode');
  }

  public function detailPL()
  {
    return $this->hasMany('App\Models\mDetailPenjualanLangsung', 'brg_kode');
  }

  public function gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'gdg_kode');
  }

  // public function stokGudang1()
  // {
  //   return $this->hasManyThrough('App\Models\mStok', 'App\Models\mGudang');
  // }
  //
  // public function stokGudang()
  // {
  //   return $this->hasManyThrough(
  //     'App\Models\mStok',
  //     'App\Models\mGudang',
  //     'gdg_kode',
  //     'brg_kode',
  //     'brg_kode',
  //     'gdg_kode'
  //   );
  // }
}
