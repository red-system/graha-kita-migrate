<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustomerForget;
use App\Models\mCustomer;
use App\Models\mTypeCustomer;
use Validator;

class API_Customer extends Controller
{
  function index() {
    $data = mCustomer::select('cus_kode', 'cus_nama', 'cus_alamat', 'cus_telp', 'cus_tipe')->get();
    foreach ($data as $key => $value) {
      $value->typecus;
    }

    return response()->json([
        'customer' => $data
    ]);
  }

  function type_cus() {
    $data = mTypeCustomer::select('type_cus_kode', 'type_cus_nama')->get();

    return response()->json([
        'tipe_cus' => $data
    ]);
  }

  function getCustomer($id='') {
    $data = mCustomer::select('cus_kode', 'cus_nama', 'cus_alamat', 'cus_telp', 'cus_tipe', 'kry_kode')->where('kry_kode', $id)->get();
    foreach ($data as $key => $value) {
      $value->Karyawan;
    }

    return response()->json([
        'customer' => $data
    ]);
  }

  public $successStatus = 200;

  public function login(){
      if(Auth::guard('web_customer')->attempt(['username' => request('username'), 'password' => request('password')])){
          $user = Auth::guard('web_customer')->user();
          $user->Karyawan;
          $success=  $user->createToken('Grahakita')->accessToken;
          return response()->json(['success' => true, 'token' => $success, 'customer' => $user], $this->successStatus);
      }
      else{
        return response()->json(['success' => false, 'Message' => 'Pastikan Username dan Password Benar'], $this->successStatus);
        // return response()->json(['error'=>'Unauthorised'], 401);
      }
  }

  public function register(Request $request){
      $validator = Validator::make($request->all(), [
          // 'name' => 'required',
          // 'email' => 'required|email',
          // 'password' => 'required',
          // 'c_password' => 'required|same:password',
          'username' => 'required',
          'password' => 'required',
          'c_password' => 'required|same:password',
      ]);
      if ($validator->fails()) {
          return response()->json(['error'=>$validator->errors()], 401);
      }
      $input = $request->all();
      $input['password'] = bcrypt($input['password']);
      $user = mCustomer::create($input);
      $success['token'] =  $user->createToken('Grahakita')->accessToken;
      $success['username'] =  $user->username;
      return response()->json(['success'=>$success], $this->successStatus);
  }

  public function details(){
      $user = Auth::user();
      return response()->json(['success' => $user], $this->successStatus);
  }

  public function logout(){
    $user = Auth::guard('web_customer')->logout();
    return response()->json(['success' => $user], $this->successStatus);
  }

  function store(Request $request) {
    \DB::beginTransaction();
    try {
      $customer = new mCustomer([
        'cus_nama' => $request->input('cus_nama'),
        'cus_alamat' => $request->input('cus_alamat'),
        'cus_telp' => $request->input('cus_telp'),
        'cus_tipe' => $request->input('cus_tipe'),
        'cus_potongan' => $request->input('cus_potongan'),
        'kry_kode' => $request->input('kry_kode'),
        'email' => $request->input('email'),
        'username' => $request->input('username'),
        'password' => bcrypt($request->input('password'))
      ]);
      $customer->save();

      \DB::commit();

      return response()->json([
        'success' => true,
        'message' => 'Data Berhasil Disimpan',
      ]);
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  //   // return $request;
  //   $validator = Validator::make($request->all(), [
  //       // 'name' => 'required',
  //       // 'email' => 'required|email',
  //       // 'password' => 'required',
  //       // 'c_password' => 'required|same:password',
  //       'username' => 'unique:tb_customer,username',
  //       // 'password' => 'required',
  //       // 'c_password' => 'required|same:password',
  //   ]);
  //
  //   if ($validator->fails()) {
  //     return response()->json([
  //       'success' => false,
  //       'message' => 'Username Sudah Digunakan',
  //     ]);
  //   }
  //   else {
  //     $customer = new mCustomer([
  //       'cus_nama' => $request->input('cus_nama'),
  //       'cus_alamat' => $request->input('cus_alamat'),
  //       'cus_telp' => $request->input('cus_telp'),
  //       'cus_tipe' => $request->input('cus_tipe'),
  //       'cus_potongan' => $request->input('cus_potongan'),
  //       'kry_kode' => $request->input('kry_kode'),
  //       'email' => $request->input('email'),
  //       'username' => $request->input('username'),
  //       'password' => bcrypt($request->input('password'))
  //     ]);
  //     $customer->save();
  //
  //     return response()->json([
  //       'success' => true,
  //       'message' => 'Data Berhasil Disimpan',
  //     ]);
  //   }
  }

  function forget(Request $request) {
    // $customer = mCustomer::all();
    $customer = mCustomer::where('username', $request->username)->where('email', $request->email)->first();
    // return $customer;
    if ($customer != null) {
      // Mail::to($request->email)->send(new CustomerForget($customer));
      return route('api.forgot.email', $request);
      // return 'true';
    }
    else {
      return 'false';
    }
  }
}
