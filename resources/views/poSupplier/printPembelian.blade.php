<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>Laporan Pembelian Supplier</title>
    </head>
    <body>
        <style type="text/css">
            @media print and (width: 24cm) and (height: 14cm) {
                @page {
                   margin: 10cm;
                }
            }
            .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
            .tt td{font-family:Arial;font-size:9px;padding:1px 1px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
            .tt th{font-family:Arial;font-size:9px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#333;background-color:#f0f0f0;}
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:2px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:bold;padding:2px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <div class="row">
                <h3><center>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</center></h3>
                <hr size="1px">
                <div class="row">
                    <!-- <div class="col-md-12"> -->
                        <table width="100%">
                            <tr>
                                <td style="font-size: 11px;padding-top: 0px;" width="30%"></td>
                                <td style="font-size: 11px;padding-top: 0px;" width="30%"></td>
                                <td align="left" style="font-size: 11px;padding-top: 0px;" width="20%">No. Faktur</td>
                                <td align="left" style="font-size: 11px;padding-top: 0px;" width="20%">: {{$pembelian->no_pembelian}}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;padding-top: 0px;" width="30%"></td>
                                <td style="font-size: 11px;padding-top: 0px;" width="30%"></td>
                                <td align="left" style="font-size: 11px;padding-top: 0px;" width="20%">Tgl. Beli</td>
                                <td align="left" style="font-size: 11px;padding-top: 0px;" width="20%">: {{date('d M Y', strtotime($pembelian->ps_tgl))}}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;padding-top: 0px;" width="30%"></td>
                                <td style="font-size: 11px;padding-top: 0px;" width="30%"></td>
                                <td align="left" style="font-size: 11px;padding-top: 0px;" width="20%">No. Inv. Spl</td>
                                <td align="left" style="font-size: 11px;padding-top: 0px;" width="20%">: {{$pembelian->no_invoice}}</td>
                            </tr>
                            <tr>
                                
                                <td align="left" style="font-size: 12px;padding-left: : 50px;" width="25%">Supplier</td>
                                <td align="left" style="font-size: 12px;padding-top: 0px;" width="25%">: {{$pembelian->supplier->spl_nama}}</td>
                            </tr>                            
                        </table>
                    <!-- </div> -->
                </div>
                <hr size="1px">
                <div class="row">
                    <div class="col-xs-12">
                        <table class="tg table table-bordered">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px"><center> No </center></th>
                                                <th style="font-size: 12px"><center> Nama Barang </center></th>
                                                <th style="font-size: 12px"><center> No Seri </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Satuan </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> QTY </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Harga </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Total </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pembelian->detailPembelian as $detail)   
                                            <tr>
                                                <td style="font-size: 11px"><center> {{ $no++ }} </center></td>
                                                <td style="font-size: 11px"> {{ $detail->nama_barang }} </td>
                                                <td style="font-size: 11px"> {{ $detail->stok->brg_no_seri }} </td>
                                                <td style="font-size: 11px"><center> {{ $detail->satuans->stn_nama }} <center></td>
                                                <td style="font-size: 11px"><center> {{ $detail->qty }} </center></td>
                                                <td style="font-size: 11px"> {{ number_format($detail->harga_net) }} </td>
                                                <td style="font-size: 11px" align="right"> {{ number_format($detail->total) }} </td>
                                            </tr>                                            
                                        @endforeach
                                                                                        
                                        </tbody>
                        </table>
                        <br>
                        <table class="tt">
                            <tr>
                                                <td width="90%" style="font-size: 11px" colspan="6" align="right"><strong>Subtotal</strong></td>
                                                <td width="10% style="font-size: 11px" align="right"><strong>{{number_format($pembelian->ps_subtotal)}}</strong></td>
                                            </tr>
                                            @if($pembelian->ps_ppn_nom > 0)
                                            <tr>
                                                <td width="90% style="font-size: 11px" colspan="6" align="right">Diskon</td>
                                                <td width="10% style="font-size: 11px" align="right">{{$pembelian->ps_disc}}% - {{number_format($pembelian->ps_disc_nom)}}</td>
                                            </tr>
                                            @endif
                                            @if($pembelian->ps_disc_nom > 0)
                                            <tr>
                                                <td width="90% style="font-size: 11px" colspan="6" align="right">PPN</td>
                                                <td width="10% style="font-size: 11px" align="right">{{$pembelian->ps_ppn}}% - {{number_format($pembelian->ps_ppn_nom)}}</td>
                                            </tr>
                                            @endif
                                            @if($pembelian->biaya_lain > 0)
                                            <tr>
                                                <td width="90% style="font-size: 11px" colspan="6" align="right">Biaya Lain-Lain</td>
                                                <td width="10% style="font-size: 11px" align="right">{{number_format($pembelian->biaya_lain)}}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td width="90% style="font-size: 11px" colspan="6" align="right">Grand Total</td>
                                                <td width="10% style="font-size: 11px" align="right">{{number_format($pembelian->grand_total)}}</td>
                                            </tr>
                        </table>                                 
                    </div>
                </div>
                <br>
                <!-- <div class="row">
                        <table width="100%">
                            <tr>
                                <td style="font-size: 12px" width="40%">n.b : </td>
                                <td style="font-size: 12px" width="20%"></td>
                                <td style="font-size: 12px" width="40%" align="center">Denpasar</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px" width="40%">- </td>
                                <td style="font-size: 12px" width="20%"></td>
                                <td style="font-size: 12px" width="40%" align="center">GrahaKita 88</td>
                            </tr>
                        </table>
                </div> -->
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
