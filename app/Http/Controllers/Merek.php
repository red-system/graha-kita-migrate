<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mMerek;

use Validator;

class Merek extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main = new Main();
        $this->title = 'Merek';
        $this->kodeLabel = 'merek';
    }

    function index() {
        $breadcrumb = [
            'Merek'=>route('merekList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'merek';
        $data['title'] = $this->title;
        $data['dataList'] = mMerek::all();
        return view('merek/merekList', $data);
    }

    function rules($request) {
        $rules = [
            'mrk_nama' => 'required',
            'mrk_description' => 'required',
        ];
        $customeMessage = [
            'required'=>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        $this->rules($request->all());
        mMerek::insert($request->except('_token'));
        return [
            'redirect'=>route('merekList')
        ];
    }

    function edit($mrk_kode='') {
        $response = [
            'action'=>route('merekUpdate', ['kode'=>$mrk_kode]),
            'field'=>mMerek::find($mrk_kode)
        ];
        return $response;
    }

    function update(Request $request, $mrk_kode) {
        $this->rules($request->all());
        mMerek::where('mrk_kode', $mrk_kode)->update($request->except('_token'));
        return [
            'redirect'=>route('merekList')
        ];
    }

    function delete($mrk_kode) {
        mMerek::where('mrk_kode', $mrk_kode)->delete();
    }
}
