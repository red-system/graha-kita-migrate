<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mProvinsi;

use Validator;

class Provinsi extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Provinsi';
      $this->kodeLabel = 'provinsi';
  }

  function index() {
      $breadcrumb = [
          'Provinsi'=>route('provinsiList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'provinsi';
      $data['title'] = $this->title;
      $data['dataList'] = mProvinsi::all();
      return view('provinsi/provinsiList', $data);
  }

  function rules($request) {
      $rules = [
          'prov_nama' => 'required',
          'prov_description' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mProvinsi::insert($request->except('_token'));
      return [
          'redirect'=>route('provinsiList')
      ];
  }

  function edit($prov_kode='') {
      $response = [
          'action'=>route('provinsiUpdate', ['kode'=>$prov_kode]),
          'field'=>mProvinsi::find($prov_kode)
      ];
      return $response;
  }

  function update(Request $request, $prov_kode) {
      $this->rules($request->all());
      mProvinsi::where('prov_kode', $prov_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('provinsiList')
      ];
  }

  function delete($prov_kode) {
      mProvinsi::where('prov_kode', $prov_kode)->delete();
  }
}
