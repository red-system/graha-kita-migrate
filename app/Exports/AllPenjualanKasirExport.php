<?php

namespace App\Exports;

use App\Models\mPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use App\Models\mReturPenjualan;
use App\Models\mReturPenjualanDetail;

// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class AllPenjualanKasirExport implements FromCollection
class AllPenjualanKasirExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    // private $request;
    private $start;
    private $end;

    // public function __construct($request)
    // {
    //     $this->request = $request;
    // }

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      // $request = $this->request;
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;
      $data['no_3'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $disc_PL=0;
      $angkut_PL=0;
      $total_PL=0;
      $total_PL_cash=0;
      $total_PL_kembalian_uang=0;
      $total_PL_transfer=0;
      $total_PL_cek=0;
      $total_PL_edc=0;
      $total_PL_piutang=0;
      $rowPL = mPenjualanLangsung::where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->get();
      foreach ($rowPL as &$tr) {
        $tr->customer;
        $tr->karyawan;
        $disc_PL += $tr->pl_disc_nom;
        $angkut_PL += $tr->pl_ongkos_angkut;
        $total_PL_cash += $tr->cash;
        $total_PL_kembalian_uang += $tr->kembalian_uang;
        $total_PL_transfer += $tr->transfer;
        $total_PL_cek += $tr->cek_bg;
        $total_PL_edc += $tr->edc;
        $total_PL_piutang += $tr->piutang;
      }

      $disc_PT=0;
      $angkut_PT=0;
      $total_PT=0;
      $total_PT_cash=0;
      $total_PT_kembalian_uang=0;
      $total_PT_transfer=0;
      $total_PT_cek=0;
      $total_PT_edc=0;
      $total_PT_piutang=0;
      $total_PT=0;

      $rowPT = mPenjualanTitipan::where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end)->get();
      foreach ($rowPT as &$tr) {
        $tr->customer;
        $tr->karyawan;
        $disc_PT += $tr->pt_disc_nom;
        $angkut_PT += $tr->pt_ongkos_angkut;
        $total_PT_cash += $tr->cash;
        $total_PT_kembalian_uang += $tr->kembalian_uang;
        $total_PT_transfer += $tr->transfer;
        $total_PT_cek += $tr->cek_bg;
        $total_PT_edc += $tr->edc;
        $total_PT_piutang += $tr->piutang;
      }

      $disc_retur=0;
      $angkut_retur=0;
      $total_retur=0;
      $total_retur_cash=0;
      $total_retur_kembalian_uang=0;
      $total_retur_transfer=0;
      $total_retur_cek=0;
      $total_retur_edc=0;
      $total_retur_piutang=0;
      $total_retur=0;

      $rowRetur = mReturPenjualan::where('tgl_pengembalian', '>=', $start)->where('tgl_pengembalian', '<=', $end)->get();;
      foreach ($rowRetur as &$tr) {
        // $tr->customer;
        // $tr->karyawan;
        // $disc_retur += $tr->pt_disc_nom;
        // $angkut_retur += $tr->pt_ongkos_angkut;
        $total_retur_cash += $tr->cash;
        $total_retur_kembalian_uang += $tr->kembalian_uang;
        $total_retur_transfer += $tr->transfer;
        $total_retur_cek += $tr->cek_bg;
        $total_retur_edc += $tr->edc;
        $total_retur_piutang += $tr->piutang;
      }

      //Data Penjualan Langsung
      $data['dataPL'] = $rowPL;
      $data['DiscPL'] = $disc_PL;
      $data['AngkutPL'] = $angkut_PL;
      // $data['TotalPLCash'] = $total_PL_cash;
      $data['TotalPLCash'] = $total_PL_cash - $total_PL_kembalian_uang;
      $data['TotalPLTransfer'] = $total_PL_transfer;
      $data['TotalPLCek'] = $total_PL_cek;
      $data['TotalPLEdc'] = $total_PL_edc;
      $data['TotalPLPiutang'] = $total_PL_piutang;
      $data['TotalPL'] = ($angkut_PL + ($total_PL_cash - $total_PL_kembalian_uang) + $total_PL_transfer + $total_PL_cek + $total_PL_edc + $total_PL_piutang);
      $data['TotalPenjualanL'] = ($angkut_PL + ($total_PL_cash - $total_PL_kembalian_uang) + $total_PL_transfer + $total_PL_cek + $total_PL_edc + $total_PL_piutang);

      //Data Penjualan Titipan
      $data['dataPT'] = $rowPT;
      $data['DiscPT'] = $disc_PT;
      $data['AngkutPT'] = $angkut_PT;
      // $data['TotalPTCash'] = $total_PT_cash;
      $data['TotalPTCash'] = $total_PT_cash - $total_PT_kembalian_uang;
      $data['TotalPTTransfer'] = $total_PT_transfer;
      $data['TotalPTCek'] = $total_PT_cek;
      $data['TotalPTEdc'] = $total_PT_edc;
      $data['TotalPTPiutang'] = $total_PT_piutang;
      $data['TotalPT'] = ($angkut_PT + ($total_PT_cash - $total_PT_kembalian_uang) + $total_PT_transfer + $total_PT_cek + $total_PT_edc + $total_PT_piutang);
      $data['TotalPenjualanT'] = ($angkut_PT + ($total_PT_cash - $total_PT_kembalian_uang) + $total_PT_transfer + $total_PT_cek + $total_PT_edc + $total_PT_piutang);
      // return $data;

      $data['dataRetur'] = $rowRetur;

      $data['TotalReturCash'] = $total_retur_cash - $total_retur_kembalian_uang;
      $data['TotalReturTransfer'] = $total_retur_transfer;
      $data['TotalReturCek'] = $total_retur_cek;
      $data['TotalReturEdc'] = $total_retur_edc;
      $data['TotalReturPiutang'] = $total_retur_piutang;
      $data['TotalRetur'] = (($total_retur_cash - $total_retur_kembalian_uang) + $total_retur_transfer + $total_retur_cek + $total_retur_edc + $total_retur_piutang);
      $data['TotalPenjualanRetur'] = (($total_retur_cash - $total_retur_kembalian_uang) + $total_retur_transfer + $total_retur_cek + $total_retur_edc + $total_retur_piutang);

      return view('export.AllPenjualanKasir', $data);
    }
}
