<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>{{$title}}</title>
    </head>
    <body>
        <style type="text/css">
            .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
            .tt td{font-family:Tahoma;font-size:13px;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#fff;}
            .tt th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#f0f0f0;}
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Tahoma;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000000;color:#000000;}
            .tg th{font-family:Tahoma;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000000;color:#000000;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;}

            @media print {
                html, body {
                display: block;
                font-family: "Tahoma";
                margin: 0px 0px 0px 0px;
                }

                /*@page {
                  size: Faktur Besar;
                }*/
                #footer {
                  position: fixed;
                  bottom: 0;
                }
                /*#header {
                  position: fixed;
                  top: 0;
                }*/
            }
        </style>
        <div class="container-fluid">
            <div class="row">
                <div class="row">
                    <div class="col-md-6">
                        <table width="100%" class="tt">
                            <tr>
                                <td width="50%">GRAHAKITA 18</td>
                                <td width="50%">DENPASAR, {{date('d-m-Y')}}</td>
                            </tr>
                            <tr>
                                <td width="50%">JALAN GATSU BARAT NO 88 A</td>
                                <td width="50%">{{$piutangPelanggan->customers->cus_nama}}</td>
                            </tr>
                            <tr>
                                <td>Telp:0361-416088</td>
                                <td width="50%">{{$piutangPelanggan->customers->cus_alamat}}</td>
                            </tr>                  
                        </table>
                    </div>
                    <!-- <div class="col-md-6">
                        <table width="100%" class="tt">
                            <tr>
                                <td width="75%">GRAHAKITA 18</td>
                            </tr>
                            <tr>
                                <td>JALAN GATSU BARAT NO 88 A</td>
                            </tr>
                            <tr>
                                <td>Telp:0361-416088</td>
                            </tr>                  
                        </table>
                    </div> -->
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <h6 style="font-family: Tahoma;text-align: center;font-weight: bold;">TANDA TERIMA</h6>
                        <table class="tg">
                            <thead>
                                <tr>
                                    <th> NO</th>
                                    <th> INVOICE </th>
                                    <th> TANGGAL INVOICE </th>
                                    <th> TANGGAL JATUH TEMPO </th>
                                    <th> NOMINAL</th>
                                    <th> KETERANGAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>{{$piutangPelanggan->pp_no_faktur}}</td>
                                    @if($kode_piutang=='PLG')
                                    <td>{{date('d/m/Y', strtotime($piutangPelanggan->tgl_piutang))}}</td>
                                    @else
                                    <td>{{date('d/m/Y', strtotime($piutangPelanggan->tgl_piutang))}}</td>
                                    @endif
                                    <td>{{date('d/m/Y', strtotime($piutangPelanggan->pp_jatuh_tempo))}}</td>
                                    <td>{{number_format($piutangPelanggan->pp_amount,2)}}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>                             
                    </div>
                </div>
                <br>
                <div class="row">
                    <table width="100%" class="tt" id="footer">
                        <tr>
                            <td colspan="5">
                                <hr style="border-style: solid;" size="1px">
                            </td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td align="center" width="40%">Yang Menerima,</td>
                            <td align="center" width="40%">Yang Menyerahkan,</td>
                            <td width="5%"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td align="center" width="40%"><br><br><br>(........................)</td>
                            <td align="center" width="40%"><br><br><br>(........................)</td>
                            <td width="5%"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td colspan="2">NB. PEMBAYARAN DENGAN TRANSFER ATAU GIRO HARUS DIATAS NAMAKAN PT ANGSA KUSUMA INDAH BCA NO. A/C 770-501-1818</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
