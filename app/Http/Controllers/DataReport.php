<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem as File;
use App\Helpers\Main;
use App\Models\mBarang;
use App\Models\mKategoryProduct;
use App\Models\mGroupStok;
use App\Models\mMerek;
use App\Models\mSatuan;
use App\Models\mSupplier;
use App\Models\mGudang;
use App\Models\mStok;
use App\Models\mArusStok;
use App\Models\mStokSample;
use App\Rules\BarangKodeUniq;

use Validator;

class DataReport extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Report Data Barang & Harga';
      $this->kodeLabel = 'barang';
  }

  function barang() {
      $breadcrumb = [
          'Report Data Barang & Harga'=>route('DataReport.barang'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'barang_dan_harga';
      $data['title'] = $this->title;

      return view('data-report/barang', $data);
  }

  function sales() {
      $breadcrumb = [
          'Report Data Barang & Harga'=>route('DataReport.sales'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'barang_dan_harga';
      $data['title'] = $this->title;

      return view('data-report/sales', $data);
  }

  function supplier() {
      $breadcrumb = [
          'Report Data Barang & Harga'=>route('DataReport.supplier'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'barang_dan_harga';
      $data['title'] = $this->title;

      return view('data-report/supplier', $data);
  }

  public function DataTableBarangSales()
  {
    $data = mBarang::select('tb_barang.brg_kode', 'brg_barcode', 'brg_nama', 'ktg_nama', 'mrk_nama', 'stn_nama', 'grp_nama',
    'brg_harga_beli_terakhir', 'brg_ppn_dari_supplier_persen', 'brg_harga_beli_tertinggi',
    'brg_hpp', 'brg_harga_jual_eceran', 'brg_harga_jual_partai', 'brg_status', \DB::raw('sum(stok) as QOH'), \DB::raw('sum(stok_titipan) as QOH_titipan'), \DB::raw('brg_harga_beli_ppn AS hargaPPN'))
    ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
    ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
    ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
    ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
    ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
    ->groupBy('tb_barang.brg_kode')
    ->get();

    $no=1;
    foreach ($data as &$barang) {
      $barang['no'] = $no++;
      if ($barang->brg_ppn_dari_supplier_persen != 0 || $barang->brg_ppn_dari_supplier_persen != null) {
        $barang['HargaPPN'] = $barang->brg_harga_jual_partai + ($barang->brg_harga_jual_partai * ($barang->brg_ppn_dari_supplier_persen/100));
      }
      else {
        $barang['HargaPPN'] = $barang->brg_harga_jual_partai;
      }
    }

    return Datatables::of($data)->make(true);
  }
}
