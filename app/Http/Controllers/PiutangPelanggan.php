<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mPiutangPelanggan;
use App\Models\mPiutangLain;
use App\Models\mPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use App\Models\mPerkiraan;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mCustomer;
use App\Models\mKaryawan;
use App\Models\mCheque;
use PDF;
use Validator;

class PiutangPelanggan extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main         = new Main();
        $this->title        = 'List Piutang Pelanggan';
        $this->kodeLabel    = 'piutangPelanggan';
    }

    function index() {
        $breadcrumb                 = [
            'Piutang Pelanggan'     => route('piutangPelanggan'),
            'Daftar'                => ''
        ];
        $data                                   = $this->main->data($breadcrumb, $this->kodeLabel);
        //$data['kodeKaryawan']     = $this->main->kodeLabel('karyawan');
        $data['kodeFakturPenjualan']            = $this->main->kodeLabel('penjualanLangsung');
        $data['kodePenjualanTitipan']           = $this->main->kodeLabel('penjualanTitipan');
        $data['menu']                           = 'piutangpelanggan';
        $data['title']                          = $this->title;
        // $data['dataList']                       = mPiutangPelanggan::where('pp_no_faktur','like','%'.$data['kodeFakturPenjualan'].'%')->get();
        // $data['titipan']                        = mPiutangPelanggan::where('pp_no_faktur','like','%'.$data['kodePenjualanTitipan'].'%')->get();
        $data['dataList']                       = mPiutangPelanggan::where('tipe_penjualan','like','%'.$data['kodeFakturPenjualan'].'%')->where('pp_sisa_amount','!=',0)->get();
        $data['titipan']                        = mPiutangPelanggan::where('tipe_penjualan','like','%'.$data['kodePenjualanTitipan'].'%')->where('pp_sisa_amount','!=',0)->get();
        $data['perkiraan']                      = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode                                   = $this->main->kodeLabel('bayar');
        $data['no_transaksi']                   = $this->main->getNoTransaksiPayment($kode);
        return view('piutang/piutangPelangganList', $data);
    }

    public function print($pp_invoice='',$sisa='',$payment=''){
        $breadcrumb                 = [
            'Piutang Pelanggan'     => route('piutangPelanggan'),
            'Daftar'                => route('piutangPelanggan'),
            'Print'                 => ''
        ];
        $data['kodeFakturPenjualan']        = $this->main->kodeLabel('penjualanLangsung');
        $data['kodePenjualanTitipan']       = $this->main->kodeLabel('penjualanTitipan');
        $data                               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']                       = 'piutangpelanggan';
        $data['title']                      = $this->title;
        $data['piutangPelanggan']           = mPiutangPelanggan::find($pp_invoice);
        $kode_piutang                       = substr($data['piutangPelanggan']->pp_no_faktur,7,3);
        if($kode_piutang=='PLG'){
            $pl_no_faktur                       = $data['piutangPelanggan']->penjualan_langsung->pl_no_faktur;
            $data['detailPenjualanLangsung']    = mPenjualanLangsung::where('pl_no_faktur',$pl_no_faktur)->get();
        }
        if($kode_piutang=='PTP'){
            $pl_no_faktur                       = $data['piutangPelanggan']->penjualan_titipin->pt_no_faktur;
            $data['detailPenjualanLangsung']    = mPenjualanTitipan::where('pt_no_faktur',$pl_no_faktur)->get();
        }
        
        $amount                             = $data['piutangPelanggan']->pp_amount;
        $data['terbayar']                   = $payment;
        $data['terbilang']                  = $this->main->spellNumberInIndonesian($data['terbayar']);
        $data['sisa']                       = $sisa;
        

        return view('piutang/piutangPelangganPdf',$data);
    }

    public function cetak($pp_invoice=''){
        $breadcrumb                 = [
            'Piutang Pelanggan'     => route('piutangPelanggan'),
            'Daftar'                => route('piutangPelanggan'),
            'Print'                 => ''
        ];
        $data['kodeFakturPenjualan']        = $this->main->kodeLabel('penjualanLangsung');
        $data['kodePenjualanTitipan']       = $this->main->kodeLabel('penjualanTitipan');
        $data                               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']                       = 'piutangpelanggan';
        $data['title']                      = $this->title;
        $data['piutangPelanggan']           = mPiutangPelanggan::find($pp_invoice);
        $data['kode_piutang']                       = substr($data['piutangPelanggan']->pp_no_faktur,7,3);
        if($data['kode_piutang']=='PLG'){
            $pl_no_faktur                       = $data['piutangPelanggan']->penjualan_langsung->pl_no_faktur;
            $data['detailPenjualanLangsung']    = mPenjualanLangsung::where('pl_no_faktur',$pl_no_faktur)->get();
        }
        if($data['kode_piutang']=='PTP'){
            $pl_no_faktur                       = $data['piutangPelanggan']->penjualan_titipin->pt_no_faktur;
            $data['detailPenjualanLangsung']    = mPenjualanTitipan::where('pt_no_faktur',$pl_no_faktur)->get();
        }
        
        $amount                             = $data['piutangPelanggan']->pp_amount;
        $data['terbayar']                   = 2000000;
        $data['terbilang']                  = $this->main->spellNumberInIndonesian(2000000);
        $data['sisa']                       = 200000;
        

        return view('piutang/printPiutangPelanggan',$data);
    }



    public function downloadPdf($pp_invoice=''){
        $data['piutangPelanggan']           = mPiutangPelanggan::find($pp_invoice)->first();
        $pl_no_faktur                       = $data['piutangPelanggan']->penjualan_langsung->pl_no_faktur;
        $data['detailPenjualanLangsung']    = mPenjualanLangsung::where('pl_no_faktur',$pl_no_faktur)->get();
        $pdf                                = PDF::loadView('piutang.piutangPelangganPrintPdf', $data);
        return $pdf->stream('invoice.pdf');

    }

    function rules($request) {
        $rules          = [
            'master_id'     => 'required',
            'payment'       => 'required',
            'payment_total' => 'required',
            'keterangan'    => 'required',
            'setor'         => 'required',
        ];

        $customeMessage = [
            'required'      => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    public function insert(Request $request){
        $this->rules($request->all());

        \DB::beginTransaction();
        try {
            $date_db            = date('Y-m-d H:i:s');
            // Jurnal Umum
          //$kode_bukti_id = $request->input('kode_bukti_id');

          // Transaksi
            $master_id          = $request->input('master_id');
            $payment            = $request->input('payment');
            $charge             = $request->input('charge');
            $payment_total      = $request->input('payment_total');
            $no_check_bg        = $request->input('no_cek_bg');
            $tgl_pencairan      = $request->input('tgl_pencairan');
            $setor              = $request->input('setor');
            $keterangan         = $request->input('keterangan');
            $tipe_arus_kas      = "Operasi";
            $kode_perkiraan     = $request->input('kode_perkiraan');
            $pp_invoice         = $request->input('pp_invoice');
            $no_faktur          = $request->input('no_piutang_pelanggan');
            $cus_kode           = $request->input('cus_kode');
            $cus                = mCustomer::find($cus_kode);
            $cus_nama           = $cus->cus_nama;
            $data['kodeCustomer'] = $this->main->kodeLabel('customer');
            $no_transaksi       = $request->input('no_transaksi');
            $tgl_transaksi      = $request->input('tgl_transaksi');
            $nama_bank      = $request->input('nama_bank');


          // General
            $year               = date('Y', strtotime($tgl_transaksi));
            $month              = date('m', strtotime($tgl_transaksi));
            $day                = date('d', strtotime($tgl_transaksi));
            $where              = [
                'jmu_year'      => $year,
                'jmu_month'     => $month,
                'jmu_day'       => $day
            ];

            $jmu_no             = mJurnalUmum::where($where)
                                ->orderBy('jmu_no','DESC')
                                ->select('jmu_no')
                                ->limit(1);

            if($jmu_no->count() == 0) {
                $jmu_no = 1;
            } else {
                $jmu_no = $jmu_no->first()->jmu_no + 1;
            }


            $jurnal_umum_id     = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                ->limit(1)
                                ->first();
            $jurnal_umum_id     = $jurnal_umum_id['jurnal_umum_id'] + 1;

            $data_jurnal            = [
                'jurnal_umum_id'    => $jurnal_umum_id,
                'no_invoice'        => $no_transaksi,
                'id_pel'            => $data['kodeCustomer'].$cus->cus_kode,
                'jmu_tanggal'       => $tgl_transaksi,
                'jmu_no'            => $jmu_no,
                'jmu_year'          => $year,
                'jmu_month'         => $month,
                'jmu_day'           => $day,
                'jmu_keterangan'    => 'Pembayaran Piutang Pelanggan : '.$cus_nama.' No Faktur Piutang : '.$no_faktur,
                'jmu_date_insert'   => $date_db,
                'jmu_date_update'   => $date_db,
                'reference_number'  => $no_faktur
            ];
            mJurnalUmum::insert($data_jurnal);

            $pp_amountAwal          = mPiutangPelanggan::where('pp_invoice',$pp_invoice)->first();
            $pp_amount              = $pp_amountAwal['pp_sisa_amount'];
            $total_payment          = 0;

            foreach($master_id as $key => $val) {
                $master                 = mPerkiraan::find($master_id[$key]);
                $trs_kode_rekening      = $master->mst_kode_rekening;
                $trs_nama_rekening      = $master->mst_nama_rekening;

                $data_transaksi             = [
                    'jurnal_umum_id'        => $jurnal_umum_id,
                    'master_id'             => $master_id[$key],
                    'trs_jenis_transaksi'   => 'debet',
                    'trs_debet'             => $payment[$key],
                    'trs_kredit'            => 0,
                    'user_id'               => 0,
                    'trs_year'              => $year,
                    'trs_month'             => $month,
                    'trs_kode_rekening'     => $trs_kode_rekening,
                    'trs_nama_rekening'     => $trs_nama_rekening,
                    'trs_tipe_arus_kas'     => $tipe_arus_kas,
                    'trs_catatan'           => $keterangan[$key],
                    'trs_date_insert'       => $date_db,
                    'trs_date_update'       => $date_db,
                    'trs_charge'            => $charge[$key],
                    'trs_no_check_bg'       => $no_check_bg[$key],
                    'trs_tgl_pencairan'     => $tgl_pencairan[$key],
                    'trs_setor'             => $setor[$key],
                    'tgl_transaksi'         => $tgl_transaksi
                ];
                mTransaksi::insert($data_transaksi);

                
                $total_payment      = $total_payment + $payment[$key];
                $pp_amount          = $pp_amount - $payment[$key];
                $angka_pp_amount    = number_format($pp_amount,2);
                    
                if($angka_pp_amount == '0.00'){
                    mPiutangPelanggan::where('pp_invoice', $pp_invoice)->update(['pp_sisa_amount'=>$pp_amount,'pp_status'=>'Lunas']);
                }else{
                    mPiutangPelanggan::where('pp_invoice', $pp_invoice)->update(['pp_sisa_amount'=>$pp_amount]);
                }
                //$terbayar = $payment[$key];
                $terbayar = $total_payment;

                if($trs_kode_rekening=='1303'){
                    $piutang_cek            = new mCheque([
                        'no_bg_cek'             => $no_check_bg[$key],
                        'no_invoice'            => $no_faktur,
                        'tgl_cek'               => $tgl_transaksi,
                        'tgl_pencairan'         => date('Y-m-d', strtotime($tgl_pencairan[$key])),
                        'cek_amount'            => $payment[$key],
                        'sisa'                  => $payment[$key],
                        // 'nama_bank'             => $nama_bank[$key],
                        'cek_dari'             => $cus_kode,
                        'cek_keterangan'        => $keterangan[$key],
                        'created_at'            => date('Y-m-d'),
                        'created_by'            => auth()->user()->Karyawan->kry_nama,
                        // 'updated_at'            => date('Y-m-d'),
                        // 'updated_by'            => auth()->user()->Karyawan->kry_nama
                    ]);
        
                    $piutang_cek->save();
                }

            }

            $master_piutang                = mPerkiraan::where('mst_kode_rekening', $kode_perkiraan)->first();
            $master_id_piutang             = $master_piutang->master_id;
            $trs_kode_rekening_piutang     = $master_piutang->mst_kode_rekening;
            $trs_nama_rekening_piutang     = $master_piutang->mst_nama_rekening;

            $data_transaksi_piutang    = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_piutang,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $total_payment,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_piutang,
                'trs_nama_rekening'   => $trs_nama_rekening_piutang,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => "Pembayaran Piutang",
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => $total_payment,
                'tgl_transaksi'         => $tgl_transaksi
            ];
            mTransaksi::insert($data_transaksi_piutang);

            \DB::commit();

            return [
              'redirect'=>route('piutangPelanggan')
            ];
        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
        }

        
    }

    function edit($pp_invoice='') {
        $response       = [
            'action'    => route('gudangUpdate', ['kode'=>$pp_invoice]),
            'field'     => mPiutangPelanggan::select('pp_amount')->where('pp_invoice',$pp_invoice)
        ];
        return $response;
    }

    function kartuPiutang($kode='1',$start_date='',$end_date='', $coa='' ,$tipe=''){
        $breadcrumb             = [
            'Hutang/Piutang'        => '',
            'Kartu Hutang Supplier' =>route('kartuHutangSupplier')
        ];
        $data                   = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['kodeCust']       = $this->main->kodeLabel('customer');
        $data['kodeKry']        = $this->main->kodeLabel('karyawan');
        $data['menu']           = 'hutang_piutang';
        $data['title']          = 'Kartu Piutang';
        $data['start_date']     = date('Y-m-d');
        $data['end_date']       = date('Y-m-d');
        $data['coa']            = '1301';
        $data['user_id']        = 'all';
        $data['id_tipe']        = $data['kodeCust'];
        $data['dataList']       = mCustomer::all();
        if($kode!=''){
            if($kode!='all'){
                $data['user_id'] = $kode;
                $data['dataList']       = mCustomer::where('cus_kode',$kode)->get();
            }
        }
        if($coa!=''){
            $data['coa']        = $coa;
        }
        if($data['coa']=='1501' || $data['coa']=='1502'){
            $data['id_tipe']    = $data['kodeKry'];
            $data['dataList']   = mKaryawan::all();
            if($kode!=''){
                if($kode!='all'){
                    $data['user_id'] = $kode;
                    $data['dataList']       = mKaryawan::where('kry_kode',$kode)->get();
                }
            }
        }
        if($start_date!='' || $end_date!=''){
            $data['start_date'] = $start_date;
            $data['end_date']   = $end_date;
        }
        
        foreach ($data['dataList'] as $user) {
            if($data['id_tipe']==$data['kodeCust']){
                $data['begining_balance'][$data['kodeCust'].$user->cus_kode] = 0;            

                $trs = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('trs_tipe_arus_kas','!=','Saldo Awal')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeCust'].$user->cus_kode)->where('trs_kode_rekening',$data['coa'])->whereBetween('tb_ac_jurnal_umum.jmu_tanggal',[$data['start_date'],$data['end_date']]);

                $query              = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeCust'].$user->cus_kode)->where('trs_kode_rekening',$data['coa'])->where('tb_ac_transaksi.tgl_transaksi','<',$data['start_date']);
                
                $debet              = $query->sum('trs_debet');
                $kredit             = $query->sum('trs_kredit');
                $data['begining_balance'][$data['kodeCust'].$user->cus_kode] = $debet-$kredit;
                $data['jml_detail'][$data['kodeCust'].$user->cus_kode]   = $trs->count();
                $data['trs'][$data['kodeCust'].$user->cus_kode] = $trs->get();
            }else{
                $data['begining_balance'][$data['kodeKry'].$user->kry_kode] = 0;            

                $trs = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('trs_tipe_arus_kas','!=','Saldo Awal')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeKry'].$user->kry_kode)->where('trs_kode_rekening',$data['coa'])->whereBetween('tb_ac_jurnal_umum.jmu_tanggal',[$data['start_date'],$data['end_date']]);

                $query              = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeKry'].$user->kry_kode)->where('trs_kode_rekening',$data['coa'])->where('tb_ac_transaksi.tgl_transaksi','<',$data['start_date']);
                
                $debet              = $query->sum('trs_debet');
                $kredit             = $query->sum('trs_kredit');
                $data['begining_balance'][$data['kodeKry'].$user->kry_kode] = $debet-$kredit;
                $data['jml_detail'][$data['kodeKry'].$user->kry_kode]   = $trs->count();
                $data['trs'][$data['kodeKry'].$user->kry_kode] = $trs->get();
            }            
        }
        $data['customer']       = mCustomer::all();
        $data['karyawan']       = mKaryawan::all();
        $data['kode_coa']       = [
            '1301' => 'Piutang Dagang',
            '1302' => 'Piutang EDC',
            '1303' => 'Piutang Cek/Bg',
            '1305' => 'Piutang Transfer',
            '1308' => 'Piutang Lain-lain',
            '1501' => 'Piutang Pemilik Saham',
            '1502' => 'Piutang Karyawan'
        ];

        if($tipe=='print'){
            return view('piutang/printKartuPiutangSupplier', $data);
        }else{
            return view('piutang/kartuPiutang', $data);
        }
        
        
    }

    function getData($kode='') {
        // $response = [
        //     'action'=>route('piutangPelangganInsert'),
        //     'field'=>mPiutangPelanggan::find($kode)
        // ];
        // return $response;
        $data['piutang'] = mPiutangPelanggan::find($kode);
        $data['perkiraan']                      = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode                                   = $this->main->kodeLabel('bayar');
        $data['no_transaksi']                   = $this->main->getNoTransaksiPayment($kode);
        return view('piutang/payment-piutang-pelanggan', $data)->render();
    }

    function pilihPeriodeKartuPiutang(Request $request){
        $start_sate     = $request->input('start_date');
        $end_date       = $request->input('end_date');
        $user_id        = $request->input('user');
        $coa            = $request->input('coa');

        return [
          'redirect'=>route('getkartuPiutangPelanggan', ['kode'=>$user_id,'start_date'=>$start_sate, 'end_date'=>$end_date, 'coa'=>$coa]),
        ];
    }

    function umur_piutang($start_date='',$end_date='', $coa='', $cus='', $tipe=''){
        $breadcrumb         = [
            'Hutang/Piutang'        => '',
            'Laporan'               => '',
            'Umur Piutang'           => route('umurPiutang'),
        ];
        $data                   = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']           = 'hutangsuplier';
        $data['title']          = 'Umur Piutang';
        
        $data['kodeSupplier']   = $this->main->kodeLabel('supplier');
        $data['start_date']     = date('Y-m-d');
        $data['end_date']       = date('Y-m-d');
        $data['coa']            = 0;
        $data['cus']            = 0;
        $data['nama_cus']       = 'All Customer';
        $data['kode_coa']       = [
            '1301' => 'Piutang Dagang',
            '1302' => 'Piutang EDC',
            '1305' => 'Piutang Transfer',
            '1308' => 'Piutang Lain-Lain',
            '1303' => 'Piutang Cek/Bg',
            '1502' => 'Piutang Karyawan',
        ];
        $data['customer']       = mCustomer::all();
        $data['data']           = mPiutangPelanggan::where('pp_status','belum dibayar');
        $data['data_pl']        = mPiutangLain::where('pl_status','Belum Bayar');
        $data['data_pc']        = mCheque::where('sisa','>',0);
        if($start_date!='' && $end_date!=''){
            $data['start_date']     = $start_date;
            $data['end_date']       = $end_date;
        }
        if($coa!=0){
            $data['coa']            = $coa;
            $data['data']           = $data['data']->where('kode_perkiraan', $coa);
            $data['data_pl']        = $data['data_pl']->where('kode_perkiraan', $coa);
            
            // if($coa!='1303'){
            //     $data['data_pc']        = $data['data_pc'];
            // }
        }
        if($cus!=0){
            $data['cus']            = $cus;
            $data['data']           = $data['data']->where('cus_kode', $cus);

            $cust                   = mCustomer::find($cus);
            $data['data_pl']        = $data['data_pl']->where('pl_dari', $cus);
            $data['data_pc']        = $data['data_pc']->where('cek_dari', $cus);
            $data['nama_cus']       = $cust->cus_nama;
        }
        $data['data']           = $data['data']->get();
        $data['data_pl']        = $data['data_pl']->get();
        if($data['coa']==0 || $data['coa']=='1303'){
            $data['data_pc']        = $data['data_pc']->get();
        }
        if($tipe=='print'){
            return view('piutang/printUmurPiutang', $data);
        }else{
            return view('piutang/umurPiutang', $data);
        }
        
    }

    function periode(Request $request) {
        $start_sate     = $request->input('start_date');
        $end_date       = $request->input('end_date');
        $coa            = $request->input('kode_coa');
        $cus            = $request->input('cust');

        return [
           'redirect'=>route('getUmurPiutang', ['start_date'=>$start_sate, 'end_date'=>$end_date, 'coa'=>$coa, 'cus'=>$cus]),
        ];
    }
}
