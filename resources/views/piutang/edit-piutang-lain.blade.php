<?php
 
use App\Models\mWoSupplier;
use App\Models\mCustomer;
use App\Models\mKaryawan;

$customer = mCustomer::all();
$karyawan = mKaryawan::all();
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Piutang Lain-Lain</h4>
</div>
<div class="modal-body">
    <form id="form-edit-piutang" action="{{ route('piutangLainUpdate', ['kode'=>$id_piutang]) }}" class="form-horizontal" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="form-body col-md-12">
                <div class="form-group">
                    <label class="col-md-3 control-label">Tanggal</label>
                    <div class="col-md-8">
                        <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="pl_tgl" value="{{$piutang->pl_tgl}}" />
                        <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="hidden" name="id_piutang" value="{{$id_piutang}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">No Piutang</label>
                    <div class="col-md-8">
                        <input class="form-control form-control-inliner" value="{{$piutang->pl_invoice}}" size="16" type="text" name="no_piutang_lain" readonly="readonly" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Jatuh Tempo</label>
                    <div class="col-md-8">
                        <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="pl_jatuh_tempo" value="{{$piutang->pl_jatuh_tempo}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Nama</label>
                    <div class="col-md-5">
                        <input class="form-control" type="text" name="kode_pel" value="{{$piutang->kry_nama}}" readonly="readonly" />
                    </div>
                    <div class="col-md-3">                                    
                        <select class="form-control select2" name="pl_dari" required="required" data-placeholder="Dari">
                            <option value=""></option>
                            @foreach($customer as $cus)
                                <option value="{{$cus->cus_kode}}" kode="{{$kode_customer}}" <?php if($piutang->id_tipe=='CUS' && $piutang->pl_dari==$kry->kry_kode) echo 'selected';?>>{{$cus->cus_nama}}</option>
                            @endforeach
                            @foreach($karyawan as $kry)
                            <option value="{{$kry->kry_kode}}" kode="{{$kode_karyawan}}" <?php if($piutang->id_tipe=='KYW' && $piutang->pl_dari==$kry->kry_kode) echo 'selected';?>>{{$kode_karyawan.$kry->kry_kode}} - {{$kry->kry_nama}}</option>
                            @endforeach
                        </select>
                        <input class="form-control" type="hidden" name="kode_pel" value="{{$piutang->id_tipe}}" />
                        <input class="form-control" type="hidden" name="id_pel" value="{{$piutang->pl_dari}}" />                                    
                        <!-- <input type="text" class="form-control" name="pl_dari"> -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Total</label>
                    <div class="col-md-8">
                        <input id="amount" type="number" class="form-control" name="pl_amount" step="0.01" value="{{$piutang->pl_amount}}">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="pl_keterangan">{{$piutang->pl_keterangan}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Kode Rekening Debet</label>
                    <div class="col-md-8">
                        <select class="form-control select2" name="kode_perkiraan" data-placeholder="Kode Rekening">
                            <option value="">--Pilih Kode Rekening--</option>
                            @foreach($kode_piutang as $key=>$value)
                            <option value="{{ $key }}" <?php if($piutang->kode_perkiraan==$key) echo "selected";?>>{{ $key.' - '.$value }}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-8">
                                    <!-- <select class="form-control" name="pl_status">
                                        <option value="Belum Bayar">Belum Bayar</option>
                                        <option value="Lunas">Lunas</option>
                                    </select> -->
                        <input type="hidden" name="pl_status" value="Belum Bayar">
                    </div>
                </div> 
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="form-body">
                <div class="form-group col-md-offset-1">
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success btn-row-coa-plus" data-toggle="modal"> 
                            <span class="fa fa-plus"></span> COA Kredit
                        </button>
                    </div>
                </div>
                <div class="col-md-11">
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-coa">
                        <thead>
                            <tr>
                                <th>Kode Perkiraan</th>
                                <th>Kredit</th>
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($jurnal as $jrn)
                            <tr>
                                <td>
                                    <select name="coa[]" class="form-control" data-live-search="true">
                                        @foreach($perkiraan as $r)
                                        <option value="{{ $r->master_id }}" <?php if($jrn->master_id==$r->master_id) echo "selected";?>>{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                        @endforeach
                                    </select>
                                </td>                                            
                                <td class="kredit">
                                    <input type="hidden" name="debet[]" class="form-control" value="{{$jrn->trs_debet}}" step="0.01">
                                    <input type="number" name="kredit[]" class="form-control" value="{{$jrn->trs_kredit}}" step="0.01">
                                </td>
                                <td class="total_kredit">
                                    <input type="number" name="total_kredit[]" class="form-control" value="{{$jrn->trs_kredit}}" step="0.01">
                                </td>
                                <td>
                                    <a class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-coa">Hapus</a>
                                </td>
                            </tr>
                        @endforeach         
                        </tbody>
                    </table>
                    <table class="table-row-coa hide">
                        <tbody>
                            <tr>
                                <td>
                                    <select name="coa[]" class="form-control" data-live-search="true">
                                        @foreach($perkiraan as $r)
                                        <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                        @endforeach
                                    </select>
                                </td>                                            
                                <td class="kredit">
                                    <input type="hidden" name="debet[]" class="form-control" value="0" step="0.01">
                                    <input type="number" name="kredit[]" class="form-control" value="0" step="0.01">
                                </td>
                                <td class="total_kredit">
                                    <input type="number" name="total_kredit[]" class="form-control" value="0" step="0.01">
                                </td>
                                <td>
                                    <a class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-coa">Hapus</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>                            
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <input id="sisa" type="hidden" class="form-control" name="sisa" step="0.01" value="0">
                    <button type="submit" class="btn green" id="btn-submit-edit-piutang">Simpan</button>
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

            $('#form-edit-piutang').submit(function(e) {
                e.preventDefault();
                var ini = $(this);
                
                $('#btn-submit-edit-piutang').attr('disabled', true);
                var sisa = $('[name="sisa"]').val();
                if(sisa > 0) {
                    swal({
                        title: 'Perhatian',
                        text: 'Data Belum Balance',
                        type: 'error'
                    });
                    $('#btn-submit-edit-piutang').attr('disabled', false);
                }else{
                    $.ajax({
                      url: ini.attr('action'),
                      type: ini.attr('method'),
                      data: ini.serialize(),
                      success: function(data) {
                          if(data.redirect) {
                              window.location.href = data.redirect;
                          }
                      },
                      error: function(request, status, error) {
                        swal({
                          title: 'Perhatian',
                          text: 'Data Gagal Disimpan!',
                          type: 'error'
                        });

                        // var json = JSON.parse(request.responseText);
                        // $('.form-group').removeClass('has-error');
                        // $('.help-block').remove();
                        // $.each(json.errors, function(key, value) {
                        //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                        //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                        // });
                      }
                  });

                }

                return false;
            });
            
        });
</script>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->