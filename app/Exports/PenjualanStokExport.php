<?php

namespace App\Exports;

use App\Models\mPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PenjualanStokExport implements FromView
{
  private $request;

  public function __construct($request)
  {
      $this->request = $request;
  }

    public function view(): View
    {
      $request = $this->request;

      $data['no'] = 1;
      $data['no_2'] = 1;
      $data['no_3'] = 1;
      $data['no_4'] = 1;

      $start = date('Y-m-d', strtotime($request->start_date));
      $end = date('Y-m-d', strtotime($request->end_date));

      $data['start'] = $start;
      $data['end'] = $end;

      $rowQB = mPenjualanLangsung::query();
      if ($request->ktg_kode != 0) {
        $rowQB->whereHas('detail_PL.barang', function ($query) use($request) {
          $query->where('ktg_kode', $request->ktg_kode);
        });
      }
      if ($request->grp_kode != 0) {
        $rowQB->whereHas('detail_PL.barang', function ($query) use($request) {
          $query->where('grp_kode', $request->grp_kode);
        });
      }
      if ($request->mrk_kode != 0) {
        $rowQB->whereHas('detail_PL.barang', function ($query) use($request) {
          $query->where('mrk_kode', $request->mrk_kode);
        });
      }
      if ($request->spl_kode != 0) {
        $rowQB->whereHas('detail_PL.barang.stok', function ($query) use($request) {
          $query->where('spl_kode', $request->spl_kode);
        });
      }

      $rowPL =  $rowQB
      ->select('tb_penjualan_langsung.pl_no_faktur', 'pl_tgl', 'cus_nama', 'nama_barang',
       'qty', 'harga_net', 'harga_jual', 'tb_detail_penjualan_langsung.disc_nom',
       'tb_detail_penjualan_langsung.ppn_nom', 'tb_detail_penjualan_langsung.ppn',
       'tb_detail_penjualan_langsung.brg_hpp', 'tb_detail_penjualan_langsung.total',
       'satuan', 'mrk_kode', 'grp_kode', 'spl_kode')
      ->leftJoin('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->orderBy('pl_tgl')
      ->get();

      if ($request->ktg_kode != 0) {
        $rowPL = $rowPL->where('ktg_kode', $request->ktg_kode);
      }
      if ($request->grp_kode != 0) {
        $rowPL = $rowPL->where('grp_kode', $request->grp_kode);
      }
      if ($request->mrk_kode != 0) {
        $rowPL = $rowPL->where('mrk_kode', $request->mrk_kode);
      }
      if ($request->spl_kode != 0) {
        $rowPL = $rowPL->where('spl_kode', $request->spl_kode);
      }
      if ($request->ppn != 0) {
        $rowPL = $rowPL->where('ppn', $request->ppn);
      }

      $sub_total=0;
      $grand_total=0;
      $total_dic=0;
      $total_komisi=0;
      $hpp=0;
      foreach ($rowPL as &$key) {
        $sub_total += $key->total;
        $total_dic += $key->disc_nom;
        $hpp += $key->brg_hpp;
      }
      $grand_total = $sub_total - $total_dic;

      $data['sub_total'] = $sub_total;
      $data['grand_total'] = $grand_total;
      $data['total_komisi'] = $total_komisi;
      $data['HPP'] = $hpp;

      $rowQBPT = mPenjualanTitipan::query();
      if ($request->ktg_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang', function ($query) use($request) {
          $query->where('ktg_kode', $request->ktg_kode);
        });
      }
      if ($request->grp_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang', function ($query) use($request) {
          $query->where('grp_kode', $request->grp_kode);
        });
      }
      if ($request->mrk_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang', function ($query) use($request) {
          $query->where('mrk_kode', $request->mrk_kode);
        });
      }
      if ($request->spl_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang.stok', function ($query) use($request) {
          $query->where('spl_kode', $request->spl_kode);
        });
      }

      $rowPT =  $rowQBPT
      ->select('tb_penjualan_titipan.pt_no_faktur', 'pt_tgl', 'cus_nama', 'nama_barang',
       'qty', 'harga_net', 'harga_jual', 'tb_detail_penjualan_titipan.disc_nom',
       'tb_detail_penjualan_titipan.ppn_nom', 'tb_detail_penjualan_titipan.ppn',
       'tb_detail_penjualan_titipan.brg_hpp', 'tb_detail_penjualan_titipan.total',
       'satuan', 'mrk_kode', 'grp_kode', 'spl_kode')
      ->leftJoin('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->orderBy('pt_tgl')
      ->get();

      if ($request->ktg_kode != 0) {
        $rowPT = $rowPT->where('ktg_kode', $request->ktg_kode);
      }
      if ($request->grp_kode != 0) {
        $rowPT = $rowPT->where('grp_kode', $request->grp_kode);
      }
      if ($request->mrk_kode != 0) {
        $rowPT = $rowPT->where('mrk_kode', $request->mrk_kode);
      }
      if ($request->spl_kode != 0) {
        $rowPT = $rowPT->where('spl_kode', $request->spl_kode);
      }
      if ($request->ppn != 0) {
        $rowPT = $rowPT->where('ppn', $request->ppn);
      }

      $sub_totalPT=0;
      $grand_totalPT=0;
      $total_dicPT=0;
      $total_komisiPT=0;
      $hppPT=0;
      foreach ($rowPT as &$key) {
        $sub_totalPT += $key->total;
        $total_dicPT += $key->disc_nom;
        $hppPT += $key->brg_hpp;
      }
      $grand_totalPT = $sub_totalPT - $total_dicPT;

      $data['sub_totalPT'] = $sub_totalPT;
      $data['grand_totalPT'] = $grand_totalPT;
      $data['total_komisiPT'] = $total_komisiPT;
      $data['HPPPT'] = $hppPT;

      $data['dataPL'] = $rowPL;
      $data['dataPT'] = $rowPT;
      $data['type_ppn'] = $request->ppn;

      return view('export.laporan-penjualan-all-stok', $data);
    }
}
