<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mStokSample extends Model
{
  public $incrementing = false;
  protected $table = 'tb_stok_sample';
  protected $primaryKey = 'stok_sample_kode';
  public $timestamps = false;

  public function barang()
  {
    // return $this->belongsTo('App\Models\mBarang', 'brg_kode')->select('brg_kode', 'mrk_kode', 'ktg_kode', 'stn_kode', 'grp_kode','brg_barcode', 'brg_nama');
    return $this->belongsTo('App\Models\mBarang', 'brg_kode');
  }

  public function gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'gdg_kode')->select('gdg_kode', 'gdg_nama');
  }

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode')->select('spl_kode', 'spl_nama');
  }

  public function detail()
  {
    return $this->hasMany('App\Models\mDetailStokSample', 'stok_sample_kode');
  }
}
