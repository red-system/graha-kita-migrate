<?php

namespace App\Exports;

use App\Models\mBarang;
use App\Models\mStok;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class BarangExport implements FromCollection
class BarangExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
      $request = $this->request;

      $rowQB = mBarang::query();
      if ($request->ktg_kode != 0) {
        $rowQB->whereHas('kategoryProduct', function ($query) use($request) {
          $query->where('ktg_kode', $request->ktg_kode);
        });
      }
      if ($request->grp_kode != 0) {
        $rowQB->whereHas('groupStok', function ($query) use($request) {
          $query->where('grp_kode', $request->grp_kode);
        });
      }
      if ($request->mrk_kode != 0) {
        $rowQB->whereHas('merek', function ($query) use($request) {
          $query->where('mrk_kode', $request->mrk_kode);
        });
      }
      if ($request->spl_kode != 0) {
        $rowQB->whereHas('stok', function ($query) use($request) {
          $query->where('spl_kode', $request->spl_kode);
        });
      }

      $data =  $rowQB->get();

      foreach ($data as &$barang) {
        $total=0;
        $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
        foreach ($barang['stok_gudang'] as $gdg) {
          $total += $gdg->stok;
          $gdg->gudang;
        }
        $barang['QOH'] = $total;
        $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen/100));
        $barang['hargaPPN'] = $hargaPPN;
        if ($hargaPPN != 0) {
          $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN)*100;
          $barang['margin'] = number_format($margin, 2) ;
        }
        else {
          $barang['margin'] = number_format(0, 2) ;
        }
        $barang['kategory'] = $barang->kategoryProduct;
        $barang['group'] = $barang->groupStok;
        $merek = $barang->merek;
        $satuan = $barang->satuan;
        $supplier = $barang->supplier;
        unset($barang->kategoryProduct);
        unset($barang->groupStok);
      }

      // $data = mBarang::get();
      // foreach ($data as $key => $value) {
      //   $value->kategoryProduct;
      //   $value->groupStok;
      //   $value->merek;
      //   $value->satuan;
      //   $value->supplier;
      // }

      return view('export.barang', [
        'barang' => $data
      ]);
    }
}
