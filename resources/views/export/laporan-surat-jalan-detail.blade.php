<table>
  <thead>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Tgl. SJ</th>
      <th style="font-size:12px">No. SJ</th>
      <th style="font-size:12px">Gudang</th>
      <th style="font-size:12px">No. Invoice</th>
      <th style="font-size:12px">Barang Barkode</th>
      <th style="font-size:12px">Nama Barang</th>
      <th style="font-size:12px">QTY</th>
      <th style="font-size:12px">STN</th>
      <th style="font-size:12px">Total HPP</th>
      <th style="font-size:12px">Total Harga Jual</th>
    </tr>
  </thead>
    <tbody>
      @foreach($dataPT as $row)
        @foreach ($row->detail as $key)
          <tr>
            <td style="font-size:12px"> {{ $no_2++ }}. </td>
            <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->sjt_tgl)) }} </td>
            <td style="font-size:12px"> {{ $row->sjt_kode }} </td>
            <td style="font-size:12px"> {{ $key->gdg['gdg_nama'] }} </td>
            <td style="font-size:12px"> {{ $row->pt_no_faktur }} </td>
            <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
            <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
            <td style="font-size:12px" align="right"> {{ number_format($key->qty, 2, "." ,",") }} </td>
            <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
            <td style="font-size:12px" align="right"> {{ number_format($key->hpp, 2, "." ,",") }} </td>
            <td style="font-size:12px" align="right"> {{ number_format($key->total_harga_jual, 2, "." ,",") }} </td>
          </tr>
        @endforeach
      @endforeach
  </tbody>
</table>
