<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan All Penjualan Kasir</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <div class="portlet light ">
            <h4>Penjualan Langsung</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th width="10%"style="font-size:12px">Tanggal</th>
                  <th width="10%"style="font-size:12px">No. Invoice</th>
                  <th width="10%"style="font-size:12px">Nama</th>
                  <th width="10%"style="font-size:12px">Kasir</th>
                  <th width="10%"style="font-size:12px">Discount</th>
                  <th width="10%"style="font-size:12px">Ongkos Angkut</th>
                  <th width="10%"style="font-size:12px">Cash</th>
                  <th width="10%"style="font-size:12px">Transfer</th>
                  <th width="10%"style="font-size:12px">Cek/BG</th>
                  <th width="10%" style="font-size:12px">Piutang EDC (Debit & CC)</th>
                  <th width="10%"style="font-size:12px">Piutang Dagang</th>
                  {{-- <th style="font-size:12px">Total</th> --}}
                </tr>
              </thead>
              <tbody>
                @foreach($dataPL as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no++ }}. </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->pl_no_faktur }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->customer['cus_nama'] }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->karyawan['kry_nama'] }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->pl_disc_nom, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->pl_ongkos_angkut, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format(($row->cash - $row->kembalian_uang), 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->transfer, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->cek_bg, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->edc, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->piutang, 2, "." ,",") }} </td>
                    {{-- <td style="font-size:12px"> {{ number_format($row->grand_total, 2, "." ,",") }} </td> --}}
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="right" style="font-weight:bold">Total</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPL, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($DiscPL, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($AngkutPL, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPLCash, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPLTransfer, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPLCek, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPLEdc, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPLPiutang, 2, "." ,",")}}</td>
                  </tr>
                  <tr>
                    <td colspan="4" align="right" style="font-weight:bold">Total Penjualan</td>
                    <td colspan="2" align="right" style="font-weight:bold">{{number_format($TotalPenjualanL, 2, "." ,",")}}</td>
                  </tr>
              {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <br>
            <h4>Penjualan Titipan</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th width="10%" style="font-size:12px">Tanggal</th>
                  <th width="10%" style="font-size:12px">No. Invoice</th>
                  <th width="10%" style="font-size:12px">Nama</th>
                  <th width="10%" style="font-size:12px">Kasir</th>
                  <th width="10%" style="font-size:12px">Discount</th>
                  <th width="10%" style="font-size:12px">Ongkos Angkut</th>
                  <th width="10%" style="font-size:12px">Cash</th>
                  <th width="10%" style="font-size:12px">Transfer</th>
                  <th width="10%" style="font-size:12px">Cek/BG</th>
                  <th width="10%" style="font-size:12px">Piutang EDC (Debit & CC)</th>
                  <th width="10%" style="font-size:12px">Piutang Dagang</th>
                  {{-- <th style="font-size:12px">Total</th> --}}
                </tr>
              </thead>
              <tbody>
                @foreach($dataPT as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no_2++ }}. </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->pt_no_faktur }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->customer['cus_nama'] }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->karyawan['kry_nama'] }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->pt_disc_nom, 2, "." ,",")  }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->pt_ongkos_angkut, 2, "." ,",")  }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format(($row->cash - $row->kembalian_uang), 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->transfer, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->cek_bg, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->edc, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->piutang, 2, "." ,",") }} </td>
                    {{-- <td style="font-size:12px"> {{ number_format($row->grand_total, 2, "." ,",")  }} </td> --}}
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="right" style="font-weight:bold">Total</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPT, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($DiscPT, 2, "." ,",") }}</td>
                    <td align="right" style="font-weight:bold">{{number_format($AngkutPT, 2, "." ,",") }}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPTCash, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPTTransfer, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPTCek, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPTEdc, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalPTPiutang, 2, "." ,",")}}</td>
                  </tr>
                  <tr>
                    <td colspan="4" align="right" style="font-weight:bold">Total Penjualan</td>
                    <td colspan="2" align="right" style="font-weight:bold">{{number_format($TotalPenjualanT, 2, "." ,",")}}</td>
                  </tr>
              {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <br>
            <h4>Retur Penjualan</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th width="10%" style="font-size:12px">Tanggal</th>
                  <th width="10%" style="font-size:12px">No. Invoice</th>
                  <th width="10%" style="font-size:12px">No. Retur</th>
                  {{-- <th width="10%" style="font-size:12px">Nama</th> --}}
                  {{-- <th width="10%" style="font-size:12px">Kasir</th> --}}
                  {{-- <th width="10%" style="font-size:12px">Discount</th> --}}
                  {{-- <th width="10%" style="font-size:12px">Ongkos Angkut</th> --}}
                  <th width="10%" style="font-size:12px">Cash</th>
                  <th width="10%" style="font-size:12px">Transfer</th>
                  <th width="10%" style="font-size:12px">Cek/BG</th>
                  <th width="10%" style="font-size:12px">Piutang EDC (Debit & CC)</th>
                  <th width="10%" style="font-size:12px">Piutang Dagang</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataRetur as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no_3++ }}. </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ date('Y-m-d', strtotime($row->tgl_pengembalian)) }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->no_faktur }} </td>
                    <td style="font-size:12px; white-space: nowrap;"> {{ $row->no_retur_penjualan }} </td>
                    {{-- <td style="font-size:12px; white-space: nowrap;"> {{ $row->customer['cus_nama'] }} </td> --}}
                    {{-- <td style="font-size:12px; white-space: nowrap;"> {{ $row->karyawan['kry_nama'] }} </td> --}}
                    {{-- <td align="right" style="font-size:12px"> {{ number_format($row->pt_disc_nom, 2, "." ,",")  }} </td> --}}
                    {{-- <td align="right" style="font-size:12px"> {{ number_format($row->pt_ongkos_angkut, 2, "." ,",")  }} </td> --}}
                    <td align="right" style="font-size:12px"> {{ number_format(($row->cash - $row->kembalian_uang), 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->transfer, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->cek_bg, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->edc, 2, "." ,",") }} </td>
                    <td align="right" style="font-size:12px"> {{ number_format($row->piutang, 2, "." ,",") }} </td>
                  </tr>
                @endforeach
                  <tr>
                    <td colspan="3" align="right" style="font-weight:bold">Total</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalRetur, 2, "." ,",")}}</td>
                    {{-- <td align="right" style="font-weight:bold">{{number_format($DiscRetur, 2, "." ,",") }}</td> --}}
                    {{-- <td align="right" style="font-weight:bold">{{number_format($AngkutRetur, 2, "." ,",") }}</td> --}}
                    <td align="right" style="font-weight:bold">{{number_format($TotalReturCash, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalReturTransfer, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalReturCek, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalReturEdc, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($TotalReturPiutang, 2, "." ,",")}}</td>
                  </tr>
                  <tr>
                    <td colspan="4" align="right" style="font-weight:bold">Total Penjualan</td>
                    <td colspan="2" align="right" style="font-weight:bold">{{number_format($TotalPenjualanRetur, 2, "." ,",")}}</td>
                  </tr>
              </tbody>
            </table>
            <br>
            <br>
            <h4>Grand Total</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th width="10%" style="font-size:12px">Total Discount</th>
                  <th width="10%" style="font-size:12px">Total Ongkos Angkut</th>
                  <th width="10%" style="font-size:12px">Total Cash</th>
                  <th width="10%" style="font-size:12px">Total Transfer</th>
                  <th width="10%" style="font-size:12px">Total Cek/BG</th>
                  <th width="10%" style="font-size:12px">Total Piutang EDC (Debit & CC)</th>
                  <th width="10%" style="font-size:12px">Total Piutang Dagang</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td align="right" style="font-weight:bold">{{number_format($DiscPL + $DiscPT, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format($AngkutPL + $AngkutPT, 2, "." ,",") }}</td>
                    <td align="right" style="font-weight:bold">{{number_format(($TotalPLCash + $TotalPTCash) - $TotalReturCash, 2, "." ,",") }}</td>
                    <td align="right" style="font-weight:bold">{{number_format(($TotalPLTransfer + $TotalPTTransfer) - $TotalReturTransfer, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format(($TotalPLCek + $TotalPTCek) - $TotalReturCek, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format(($TotalPLEdc + $TotalPTEdc) - $TotalReturEdc, 2, "." ,",")}}</td>
                    <td align="right" style="font-weight:bold">{{number_format(($TotalPLPiutang + $TotalPTPiutang) - $TotalReturPiutang, 2, "." ,",")}}</td>
                  </tr>
                  <tr>
                    <td colspan="4" align="right" style="font-weight:bold">Grand Total Penjualan</td>
                    <td colspan="2" align="right" style="font-weight:bold">{{number_format($TotalPenjualanL+$TotalPenjualanT-$TotalPenjualanRetur, 2, "." ,",")}}</td>
                  </tr>
              </tbody>
            </table>
            {{-- <table class="table table-bordered table-hover table-header-fixed">
              <tbody>
                <tr>
                  <td align="right" style="font-weight:bold">Grand Total Penjualan</td>
                  <td align="right" style="font-weight:bold">{{number_format($TotalPenjualanL+$TotalPenjualanT-$TotalPenjualanRetur, 2, "." ,",")}}</td>
                </tr>
              </tbody>
            </table> --}}
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
{{-- <script type="text/javascript">
  window.printDiv('printMe');
</script> --}}
