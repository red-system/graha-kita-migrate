<table>
  <thead>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Nama Dept.</th>
      <th style="font-size:12px">Barang Barkode</th>
      <th style="font-size:12px">Nama Barang</th>
      <th style="font-size:12px">QTY</th>
      <th style="font-size:12px">STN</th>
      <th style="font-size:12px">Harga</th>
      <th style="font-size:12px">Total</th>
    </tr>
  </thead>
    <tbody>
      @foreach($dataPL as $row)
        <tr>
          <td style="font-size:12px"> {{ $no++ }}. </td>
          <td style="font-size:12px"> {{ $row->gdg['gdg_nama'] }} </td>
          <td style="font-size:12px"> {{ $row->barang['brg_barcode'] }} </td>
          <td style="font-size:12px"> {{ $row->barang['brg_nama'] }} </td>
          <td style="font-size:12px" align="right"> {{ number_format($row->brg_qty, 2, "." ,",") }} </td>
          <td style="font-size:12px"> {{ $row->barang->satuan['stn_nama'] }} </td>
          <td style="font-size:12px" align="right"> {{ number_format($row->harga_net, 2, "." ,",") }} </td>
          <td style="font-size:12px" align="right"> {{ number_format($row->total, 2, "." ,",") }} </td>
        </tr>
      @endforeach
      <tr>
        <td colspan="7" align="right" style="font-weight:bold">Grand Total</td>
        <td style="font-weight:bold" align="right">{{number_format($grand_total, 2, "." ,",")}}</td>
      </tr>
  </tbody>
</table>
