@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript">
    // $('.input-daterange').datepicker({
    //   todayBtn:'linked',
    //   format: "yyyy-mm-dd",
    //   autoclose: true
    // });

    $('#search-date').click(function() {
      var href = "{{route('summaryStokRange')}}";
      var start = $('#start_date').val();
      var end = $('#end_date').val();
      var table = $('#sample_2').DataTable();
      table.clear();

      $('#sample_2').DataTable({
        destroy : true,
        processing: true,
        serverSide: true,
        ajax : {
          url: href,
          type: 'POST',
          data: { start_date: start, end_date: end , _token: "{{ csrf_token() }}" },
          dataSrc : ''
        },
        columns: [
          { data: null, render: function ( data, type, row ) {
            return data.brg_barcode;
          } },
          { data: "brg_nama" },
          { data: "stn_nama" },
          { data: "ktg_nama" },
          { data: "grp_nama" },
          { data: "mrk_nama" },
          { data: "gdg_nama" },
          { data: null, render: function ( data, type, row ) {
            if (data.stokIn == null) {
              return '-';
            }
            else {
              return data.stokIn;
            }
          } },
          { data: null, render: function ( data, type, row ) {
            if (data.stokOut == null) {
              return '-';
            }
            else {
              return data.stokOut;
            }
          } },
          { data: "QOH" },
        ]
      });
    });

    $('.btn-cetak').click(function() {
      var href = "{{route('SummaryStok.print')}}";
      var type = $(this).data('type');
      var start = $('#start_date').val();
      var end = $('#end_date').val();

      $.ajax({
        url: href,
        type: 'POST',
        data: {
          report: type,
          start_date: start,
          end_date: end,
          _token: "{{ csrf_token() }}"
        },
          success: function(data) {
            // console.log(respond);
            // window.location.href = data.redirect;
            window.open(data.redirect,'_blank');
          },
          error: function(request, status, error) {
            // console.log(error);
          }
        });
    });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <div class="row">
                {{-- <div class="input-daterange"> --}}
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                      <input type="date" name="start_date" id="start_date" class="form-control">
                    </div>
                  </div>
                  <div class="col-xs-1 text-center">
                    <h4>S/d</h4>
                  </div>
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                      <input type="date" name="end_date" id="end_date" class="form-control">
                    </div>
                  </div>

                  <div class="btn-group-md pull-right">
                    <button class="btn btn-success btn-cetak" data-type="print">
                      <i class="glyphicon glyphicon-print"></i> Print to PDF
                    </button>
                    <button class="btn btn-success btn-cetak" data-type="excel">
                      <i class="glyphicon glyphicon-print"></i> Print to Excel
                    </button>
                  </div>
                {{-- </div> --}}
                <div class="col-md-2">
                  <button type="button" name="button" id="search-date" class="btn btn-info">View</button>
                </div>
              </div>
              <br>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                  <tr class="">
                    <th> Kode Barang </th>
                    <th> Nama Barang </th>
                    <th> Satuan </th>
                    <th> Kategory </th>
                    <th> Group Stok </th>
                    <th> Merek </th>
                    <th> Lokasi </th>
                    <th> Total In </th>
                    <th> Total Out </th>
                    <th> Last Stok </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td> {{ $row->brg_barcode }} </td>
                      <td> {{ $row->brg_nama }} </td>
                      <td> {{ $row->stn_nama }} </td>
                      <td> {{ $row->ktg_nama }} </td>
                      <td> {{ $row->grp_nama }} </td>
                      <td> {{ $row->mrk_nama }} </td>
                      <td> {{ $row->gdg_nama }} </td>
                      @if ($row->stokIn == null)
                        <td> - </td>
                      @else
                        <td> {{ $row->stokIn }} </td>
                      @endif
                      @if ($row->stokOut == null)
                        <td> - </td>
                      @else
                        <td> {{ $row->stokOut }} </td>
                      @endif
                      {{-- <td> {{ $row->stokIn }} </td>
                      <td> {{ $row->stokOut }} </td> --}}
                      <td> {{ $row->QOH }} </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
