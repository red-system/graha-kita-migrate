@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<!-- <script src="{{ asset('assets/global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" type="text/javascript"></script> -->
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/poSupplier.js') }}" type="text/javascript"></script>
@stop

@section('body')

<span id="data-back"
  data-kode-customer="{{ $kodeSupplier }}"
  data-form-token="{{ csrf_token() }}"
  data-route-po-supplier-barang-row="{{ route('poSupplierBarangRow') }}"
  data-route-po-supplier-hitung-kredit="{{ route('poSupplierHitungWaktuKredit') }}"
</span>

<form class="form-send" action="{{ route('poSupplierInsert') }}" method="post">
{{ csrf_field() }}
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-12">Supplier</label>
                      <div class="col-xs-3">
                        <input type="text" class="form-control" placeholder="Kode" name="spl_kode_label" value="{{ $kodeSupplier }}" disabled>
                      </div>
                      <div class="col-xs-4">
                        <select name="spl_kode" class="form-control select2" data-placeholder="Pilih Supplier" required>
                          <option value="0">Supplier</option>
                          @foreach($supplier as $r)
                            <option value="{{ $r->spl_kode }}"
                                    data-alamat="{{ $r->spl_alamat }}">{{ $r->spl_nama }}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-xs-2">
                        <button type="button" class="btn btn-success btn-block" data-toggle="modal" href="#modal-supplier">
                          <span class="glyphicon glyphicon-search"></span>
                        </button>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Alamat</label><br />
                      <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                          : <span class="spl_alamat">-</span>
                      </div>
                  </div>
                  <div class="form-group">
                    <!-- <label class="col-md-{{ $col_label }}">Alamat</label><br /> -->
                      <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                        <br>
                          <!-- <a href="#modal-add-item" data-toggle="modal" data-target="#modal-add-item" class="btn btn-primary"></a> -->
                          <button type="button" class="btn btn-primary" data-toggle="modal" href="#modal-supplier"> Add Item
                            <!-- <span class="glyphicon glyphicon-plus"></span> -->
                          </button>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">Tanggal</label>
                        <div class="col-md-4">
                            <input type="text" name="tgl_po" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{date('Y-m-d')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">No PO</label>
                        <div class="col-md-4">
                            <input type="text" name="no_po" class="form-control" value="POS{{$pl_no_po_next}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">No Bukti</label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" placeholder="No Bukti" name="no_bukti_faktur" id="brg_barcode" required>
                          </div>
                    </div>
                  <!-- <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">Transaksi</label>
                      <div class="col-md-{{ $col_form }}">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" class="form-control" name="pl_transaksi" id="optionsRadios22" value="cash" checked=""> Cash
                            <span></span>
                          </label>
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" class="form-control" name="pl_transaksi" id="optionsRadios23" value="kredit"> Kredit
                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div> -->
                </div>
              </div>
            </div>
            
        
            
            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-po-supplier-detail">
              <thead>
                <tr class="">
                  <th> Kode </th>
                  <th> Nama Barang</th>
                  <th> No. Seri </th>
                  <th> Gudang </th>
                  <th> Harga Beli </th>
                  <th> PPN(%) </th>
                  <th> Disc(%) </th>
                  <th> Disc. Nom </th>
                  <th> Harga Net </th>
                  <th> Qty </th>
                  <th> Satuan </th>
                  <th> Total </th>
                  <th> Keterangan</th>
                  <th> Aksi </th>
                </tr>
              </thead>
              <tbody>
              <!-- <tr>
                <td colspan="14">
                  <button type="button" class="btn btn-success btn-block btn-row-plus"><span class="fa fa-plus"></span> Tambah Data Barang</button>
                </td>
              </tr> -->
              </tbody>
            </table>
            <table class="table-row-data hide">
              <tbody class="form-group">
                <tr>
                  <td class="brg_kode">                             
                    <select name="brg_kode[]" class="form-control table brg-kode" data-placeholder="Kode Barang">
                      <option value=""></option>
                      @foreach($barang as $brg)
                      <option value="{{ $brg->brg_kode }}"
                                data-brg-nama="{{ $brg->brg_nama }}"
                                data-stn-kode="{{ $brg->stn_kode }}"
                                data-harga-beli="{{ $brg->harga_beli_terakhir }}">{{ $brg->brg_kode }}
                      </option>
                      @endforeach
                    </select>
                    
                </td>
                <td class="brg_nama">
                  <input type="text" name="brg_nama[]" class="form-control"> 
                </td>
                <td class="brg_no_seri">
                  <select name="brg_no_seri[]" class="form-control" data-placeholder="No Seri">
                    <option value=""></option>
                  </select>  
                </td>
                <td class="gudang">                
                  <select name="gudang[]" class="form-control" data-placeholder="Pilih Gudang">
                    <option value=""></option>
                    @foreach($gudang as $gdg)
                    <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                    @endforeach
                  </select>                  
                </td>
                <td class="harga-beli">
                  <input type="number" class="form-control" name="harga_beli[]" min="0" readonly>
                </td>
                <td class="ppn">
                  <input type="number" class="form-control" name="ppn[]" min="0" value="0">
                </td>
                <td class="disc">
                  <input type="number" class="form-control" name="disc[]" min="0" value="0" >
                </td>
                <td class="disc-nom">
                  <input type="number" class="form-control" name="disc_nom[]" value="0" readonly>
                </td>
                <td class="harga-net">
                  <input type="number" class="form-control" name="harga_net[]" value="0" readonly>
                </td>
                <td class="qty">
                  <input type="number" class="form-control" name="qty[]" value="0" min="0">
                </td>
                <td class="satuan">
                  <input type="text" class="form-control" name="satuan[]" readonly>
                  <input type="hidden" class="form-control" name="satuan_kode[]">
                </td>
                <td class="total">
                  <input type="number" class="form-control" name="total[]" value="0" min="0" readonly>
                </td>
                <td class="ket">
                  <textarea class="form-control" name="ket[]"></textarea>
                </td>
                <td class="btn-delete">
                  <button type="button" class="btn btn-danger btn-row-delete"><span class="fa fa-trash"></span></button>
                </td>
              </tr>
              </tbody>
            </table>

            <hr />
            <div class="row">
              <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Catatan</label>
                        <div  class="col-md-{{ $col_form }}">
                          <textarea class="form-control" name="pl_catatan"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Tanggal Kirim</label>
                      <div class="col-md-{{ $col_form }}">
                        <input type="text" name="tgl_kirim" class="form-control date-picker" data-date-format="yyyy-mm-dd">
                      </div>
                  </div>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal">
                <div class="form-body form-kredit hide">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Lama Kredit</label>
                      <div class="col-md-{{ $col_label }}">
                      <input type="text" name="pl_lama_kredit" class="form-control">
                      </div>
                      <div class="col-md-{{ $col_label }}">
                      <select class="form-control" name="pl_waktu">
                          <option value="-Pilih Waktu-"></option>                          
                          <option value="hari">Hari</option>
                          <option value="bulan">Bulan</option>
                          <option value="tahun">Tahun</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Jatuh Tempo</label>
                      <div class="col-md-{{ $col_form }}">
                        <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" readonly>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Sub Total</label>
                      <div class="col-md-{{ $col_form }}">
                          <input type="text" class="form-control" name="pl_subtotal" value="0" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Disc</label>
                      <div  class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="pl_disc" value="0">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Ppn</label>
                      <div  class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="pl_ppn" value="0">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Ongkos Angkut</label>
                      <div  class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="pl_ongkos_angkut" value="0">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}"l>Grand Total</label>
                    <div   class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="grand_total" value="0" readonly>
                    </div>
                  </div>
                  <div class="form-action">
                    <button type="submit" class="btn btn-success btn-lg btn-block">SAVE</button>
                    <a href="{{ route('poSupplierDaftar') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
</form>

  <div class="modal draggable-modal" id="modal-supplier" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Supplier </h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Supplier</th>
                <th>Alamat Supplier</th>
                <th>No Telepon</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($supplier as $r)
              <tr>
                <td>{{ $no++ }}.</td>
                <td>{{ $r->spl_nama }}</td>
                <td>{{ $r->spl_alamat }}</td>
                <td>{{ $r->spl_telp }}</td>
                <td>
                  <button class="btn btn-success btn-pilih-supplier"
                          data-spl-kode="{{ $r->spl_kode }}"
                          data-spl-nama="{{ $r->spl_nama }}"
                          data-spl-alamat="{{ $r->spl_alamat }}"
                          data-spl-telp="{{ $r->spl_telp }}">Pilih</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div
@stop