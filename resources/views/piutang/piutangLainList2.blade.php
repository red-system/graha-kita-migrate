<?php
    use App\Models\mCustomer;

    $customer = mCustomer::all();
?>
@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-payment-hl').click(function() {
                var href = $(this).data('href');
                $.ajax({
                    url: href,
                    success: function(data) {
                        $.each(data.field, function(field, value) {
                            $('#modalPaymentPiutangLain [name="'+field+'"]').val(value);

                            if(field=='pl_amount'){
                                const formatter = new Intl.NumberFormat()
                                var balance = formatter.format(value); // "$1,000.00"
                                // $('#balance').html('Rp. '+balance);

                                // $('.nominal-grand-total').html('Rp. '+balance);
                                $('.nominal-grand-total').html('Rp. '+balance);
                                $('.nominal-sisa').html('Rp. '+balance);
                                $('#modalPaymentPiutangLain [name="amount"]').val(value);
                            }

                        });
                        $('#modalPaymentPiutangLain form').attr('action', data.action);
                        $('#modalPaymentPiutangLain').modal('show');
                    }
                });
            });

            
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                            <i class="fa fa-plus"></i> New
                        </button>
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="" align="center">
                                    <th width="10"> No </th>
                                    <th> Invoice </th>
                                    <th> Jatuh Tempo</th>
                                    <th> Dari </th>
                                    <th> Amount </th>
                                    <th> Keterangan </th>                                    
                                    <th> Status </th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dataList as $piutang)
                            <tr>
                                <td align="center"> {{ $no++ }}. </td>
                                <td> {{ $piutang->pl_invoice }} </td>
                                <td> {{ date('d M Y',strtotime($piutang->pl_jatuh_tempo)) }} </td>
                                <td> {{ $piutang->pl_dari }} </td>
                                <td align="right"> Rp {{ number_format($piutang->pl_amount) }} </td>                                
                                <td> {{ $piutang->pl_keterangan}}</td>
                                <td> {{ $piutang->pl_status}}</td>
                                <td>
                                    <!-- <div class="btn-group btn-group-xs"> -->
                                    <button class="btn btn-success btn-edit btn-xs" data-href="{{ route('piutangLainEdit', ['kode'=>$piutang->pl_invoice]) }}">
                                            <span class="icon-pencil"></span>
                                    </button>
                                    <button class="btn btn-danger btn-delete btn-xs" data-href="{{ route('piutangLainDelete', ['kode'=>$piutang->pl_invoice]) }}">
                                            <span class="icon-trash"></span>
                                    </button>
                                    <a class="btn btn-success btn-xs" href="{{ route('piutangLainInvoice', ['kode'=>$piutang->id]) }}">
                                            <span class="fa fa-print"></span>
                                    </a>
                                    <button class="btn btn-danger btn-payment-hl btn-xs" data-href="{{ route('getPiutangLain', ['kode'=>$piutang->id]) }}">
                                            <span class="fa fa-money"></span>
                                    </button>
                                    <!-- </div> -->
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Penerimaan Piutang Lain-Lain
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('piutangLainInsert') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-body col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">No Piutang</label>
                                <div class="col-md-8">
                                    <input class="form-control form-control-inliner" value="{{$next_no_piutang_lain}}" size="16" type="text" name="no_piutang_lain" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jatuh Tempo</label>
                                <div class="col-md-8">
                                    <input class="form-control date-picker" size="16" type="text" name="pl_jatuh_tempo" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Dari</label>
                                <div class="col-md-8">                                    
                                    <select class="form-control select2" name="pl_dari" required="required" data-placeholder="Dari">
                                        <option value=""></option>
                                        @foreach($customer as $cus)
                                        <option value="{{$cus->cus_nama}}">{{$cus->cus_nama}}</option>
                                        @endforeach
                                    </select>                                    
                                    <!-- <input type="text" class="form-control" name="pl_dari"> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Amount</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="pl_amount">
                                </div>
                            </div>
                        </div>
                        <div class="form-body col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Keterangan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="pl_keterangan"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Kode Rekening</label>
                                <div class="col-md-8">
                                    <select class="form-control select2" name="kode_perkiraan" data-placeholder="Kode Rekening">
                                        <option value="">--Pilih Kode Rekening--</option>
                                        @foreach($perkiraan as $r)
                                            <option value="{{ $r->mst_kode_rekening }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="pl_status">
                                        <option value="Belum Bayar">Belum Bayar</option>
                                        <option value="Lunas">Lunas</option>
                                    </select>
                                </div>
                            </div>                        
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="form-body">
                            <div class="form-group col-md-offset-1">
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-row-transaksi-plus" data-toggle="modal"> 
                                        <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                                   </button>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <table class="table table-striped table-bordered table-hover table-header-fixed table-data-transaksi">
                                    <thead>
                                        <tr>
                                            <th>Kode Perkiraan</th>
                                            <th>Jenis Transaksi</th>
                                            <th>Debet</th>
                                            <th>Kredit</th>
                                            <th>Tipe Arus Kas</th>
                                            <th>Catatan</th>
                                            <th>Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                    
                                    </tbody>
                                </table>
                                <table class="table-row-transaksi hide">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <select name="master_id[]" class="form-control">
                                                    @foreach($perkiraan as $r)
                                                    <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="jenis_transaksi">
                                                <select name="jenis_transaksi[]" class="form-control">
                                                    <option value="debet">Debet</option>
                                                    <option value="kredit">Kredit</option>
                                                </select>
                                                      <!-- <input type="number" name="jenis_transaksi[]" class="form-control" value="0"> -->
                                            </td>
                                            <td class="debet">
                                                <input type="number" name="debet[]" class="form-control" value="0">
                                            </td>
                                            <td class="kredit">
                                                <input type="number" name="kredit[]" class="form-control" value="0" readonly="readonly">
                                            </td>
                                            <td class="tipe_arus_kas">
                                                <select name="tipe_arus_kas[]" class="form-control">
                                                    <option value="operasi">Operasi</option>
                                                    <option value="pendanaan">Pendanaan</option>             
                                                    <option value="investasi">Investasi</option>
                                                </select>
                                            </td>
                                            <td class="catatan">
                                                <textarea class="form-control" name="catatan[]"></textarea>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-transaksi">Hapus</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-pencil"></i> Edit Piutang Lain-Lain
                </h4>
            </div>
            <div class="modal-body form">
                <form action="" class="form-horizontal form-send" role="form" method="put">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jatuh Tempo</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inline input-medium date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="pl_jatuh_tempo" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dari</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pl_dari">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Amount</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pl_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="pl_keterangan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="pl_status">
                                    <option value="Belum Bayar">Belum Bayar</option>
                                    <option value="Lunas">Lunas</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modalPaymentPiutangLain" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
                <form class="form-send form-horizontal" action="" method="post" role="form">
                {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3">No Invoice</label>
                            <div class="col-md-4">
                                <input type="text" name="pl_invoice" class="form-control" value="0">
                                <input type="hidden" name="id" class="form-control" value="0">
                                <input type="hidden" name="kode_perkiraan" class="form-control" value="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Nama</label>
                            <div class="col-md-4">
                                <input type="text" name="pl_dari" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4" style="margin-top: -20px">
                                <br>
                                <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal"> 
                                    <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                                </button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                        <thead>
                            <tr>
                                <th>Kode Perkiraan</th>
                                <th>Payment</th>
                                <th>Total</th>
                                <th>Keterangan</th>
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                    <br >
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h1>Grand Total</h1>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <h1 class="nominal-grand-total" name="nominal-grand-total" id="nominal-grand-total"></h1>
                            <input type="hidden" name="amount">
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <h1>Sisa</h1>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <h1 class="nominal-sisa">0</h1>
                                <input type="hidden" name="amount_sisa">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4 col-md-offset-8">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success btn-lg">SAVE</button>
                                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <table class="table-row-payment hide">
        <tbody>
          <tr>
            <td>
              <select name="master_id[]" class="form-control selectpickerx" data-live-search="true">
                @foreach($perkiraan as $r)
                  <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                  @endforeach
              </select>
            </td>
            <td class="payment">
              <input type="number" name="payment[]" class="form-control" value="0">
            </td>
            
            <td class="payment_total">
              <input type="number" name="payment_total[]" class="form-control" value="0" readonly>
            </td>
            
            <td>
              <input type="hidden" name="setor[]" class="form-control" value="0">
              <input type="text" name="keterangan[]" class="form-control">
            </td>
            
            <td>
              <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
            </td>
          </tr>
        </tbody>
    </table>
</div>
</form>
@stop