<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\mSlider;

class API_Slider extends Controller
{
  function index() {
    $data = mSlider::all();

    return response()->json([
        'slider' => $data
    ]);
  }
}
