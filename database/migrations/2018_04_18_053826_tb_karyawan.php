<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbKaryawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_karyawan', function (Blueprint $table) {
         $table->increments('kry_kode');
         $table->string('kry_nama')->nullable();
         $table->string('kry_alamat')->nullable();
         $table->string('kry_telp')->nullable();
         $table->string('kry_posisi')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_karyawan');
     }
}
