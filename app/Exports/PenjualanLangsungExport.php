<?php

namespace App\Exports;

use App\Models\mCustomer;
use App\Models\mPenjualanLangsung;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PenjualanLangsungExport implements FromView
{
    private $start;
    private $end;
    private $cus_kode;

    public function __construct($start, $end, $cus_kode)
    {
      $this->start = $start;
      $this->end = $end;
      $this->cus_kode = $cus_kode;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;
      $cus_kode = $this->cus_kode;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      if ($cus_kode != 'all') {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanLangsung', function($q) use($start, $end){
          $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
        })
        ->where('cus_kode', $cus_kode)
        ->get();
      }
      else {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanLangsung', function($q) use($start, $end){
          $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
        })->get();
      }

      $grand_total=0;
      foreach ($row as $key) {
        $grand_total_cus=0;
        $key['penjualan'] = mPenjualanLangsung::where('cus_kode', $key->cus_kode)
        ->where('pl_tgl', '>=', $start)
        ->where('pl_tgl', '<=', $end)
        ->get();
        // $key['penjualan'] = $key->PenjualanLangsung;
        foreach ($key['penjualan'] as $keyPenjualan) {
          $grand_total_cus+=$keyPenjualan->pl_subtotal;
          $grand_total+=$keyPenjualan->pl_subtotal;
          $keyPenjualan['detail'] = $keyPenjualan->detail_PL;
          // foreach ($keyPenjualan['detail'] as $keyDet) {
          //   $keyDet->barang->satuan;
          // }
          unset($keyPenjualan->detail_PL);
        }
        $key['grand_total_cus'] = $grand_total_cus;
        unset($key->PenjualanLangsung);
      }

      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      return view('export.laporan-penjualan-langsung', $data);
    }
}
