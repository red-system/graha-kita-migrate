<table>
  <thead>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">No. Faktur</th>
      <th style="font-size:12px">Customer</th>
      <th style="font-size:12px">Pelaksana</th>
      <th style="font-size:12px">Nama Barang</th>
      <th style="font-size:12px">Alasan</th>
      <th style="font-size:12px">Keterangan</th>
      <th style="font-size:12px">Ongkos Angkut</th>
      <th style="font-size:12px">Potongan Penjualan</th>
      <th style="font-size:12px">Potongan Piutang</th>
      <th style="font-size:12px">Total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $row)
      @php
      $no_2 = 0;
      @endphp
      @foreach($row->detail as $rowDet)
        <tr>
          @if ($no_2 < 1)
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $no++ }}. </td>
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->no_faktur }} </td>
            @php
              $kode_P = substr($row->no_faktur, 7, 3);
            @endphp
            @if ($kode_P == 'PLG')
              <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->penjualanL->cus_nama }} </td>
            @else
              <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->penjualanT->cus_nama }} </td>
            @endif
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->pelaksana }} </td>
          @endif
          <td style="font-size:12px"> {{ $rowDet->brg_nama }} </td>
          @if ($no_2 < 1)
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->alasan }} </td>
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: center;"> {{ $row->keterangan }} </td>
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->ongkos_angkut, 2, "." ,",") }} </td>
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->potongan_penjualan, 2, "." ,",") }} </td>
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->potongan_piutang, 2, "." ,",") }} </td>
            <td rowspan="{{$row->span}}" style="font-size:12px; vertical-align: middle; text-align: right;"> {{ number_format($row->total_retur, 2, "." ,",") }} </td>
          @endif
        </tr>
        @php
        $no_2++;
        @endphp
      @endforeach
    @endforeach
    <tr>
      <td colspan="9" align="center" style="font-weight:bold">Total</td>
      <td style="font-weight:bold" align="right">{{number_format($grand_total, 2, "." ,",")}}</td>
    </tr>
  </tbody>
</table>
