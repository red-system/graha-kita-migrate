<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPromo extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_promo';
  protected $primaryKey = 'prm_kode';
  public $timestamps = false;
}
