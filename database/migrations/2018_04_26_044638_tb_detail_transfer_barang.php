<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDetailTransferBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_detail_transfer_barang', function (Blueprint $table) {
           $table->increments('trf_det_kode');
           $table->integer('brg_kode')->nullable();
           $table->integer('trf_det_asal')->nullable();
           $table->integer('trf_kode')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_detail_transfer_barang');
     }
}
