@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
  <script type="text/javascript">
  $('#sample_3').on( 'draw.dt', function () {
    $('.btn-detail-sales').click(function() {
      var href = $(this).data('href');
      // .find(':selected')
      var mrk_kode = $("#mrk_kode").find(':selected').val();
      var link = href+'/'+mrk_kode;
      window.location.href = link;
    });
  });

    $('#search-date').click(function() {
      var start = $('#start_date').val();
      var end = $('#end_date').val();
      var mrk_kode = $('#mrk_kode').val();

      var href = "{{route('salesRangePL')}}";
      var l = window.location;
      var base_url = l.protocol + "//" + l.host + "/";
      var table = $('#sample_3').DataTable();
      var no = 1;
      table.clear();

      $('#sample_3').DataTable({
        destroy : true,
        processing: true,
        serverSide: true,
        ajax : {
          url: href,
          type: 'POST',
          data: {
            start_date: start,
            end_date: end,
            mrk_kode: mrk_kode,
            _token: "{{ csrf_token() }}"
          },
          dataSrc : ''
        },
        columns: [
          {
            data: null, render: function ( data, type, row )
            {
              return no++;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.kry_nama;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return parseFloat(data.qty_jual).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return parseFloat(data.total_jual).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return parseFloat(data.qty_retur).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return parseFloat(data.total_retur).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return '<a class="btn btn-primary btn-detail btn-icon btn-detail-sales" type="button" data-href="'+base_url+'laporan/view-sales/'+data.kry_kode+'/transaksi'+'/'+start+'/'+end+'"><i class="icon-eye"></i> View</a>';
            }
          },
        ]
      });

      // var href = "{{route('salesRangePT')}}";
      // var l = window.location;
      // var base_url = l.protocol + "//" + l.host + "/";
      // var table2 = $('#sample_4').DataTable();
      // var no_2 = 1;
      // table2.clear();
      //
      // $('#sample_4').DataTable({
      //   destroy : true,
      //   processing: true,
      //   serverSide: true,
      //   ajax : {
      //     url: href,
      //     type: 'POST',
      //     data: { start_date: start, end_date: end , _token: "{{ csrf_token() }}" },
      //     dataSrc : ''
      //   },
      //   columns: [
      //     {
      //       data: null, render: function ( data, type, row )
      //       {
      //         return no_2++;
      //       }
      //     },
      //     {
      //       data: null, render: function ( data, type, row )
      //       {
      //         return data.kry_nama;
      //       }
      //     },
      //     {
      //       data: null, render: function ( data, type, row )
      //       {
      //         return data.qty_jual;
      //       }
      //     },
      //     {
      //       data: null, render: function ( data, type, row )
      //       {
      //         return parseFloat(data.total_jual).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      //       }
      //     },
      //     {
      //       data: null, render: function ( data, type, row )
      //       {
      //         return '<a class="btn btn-primary btn-detail btn-icon" type="button" href="'+base_url+'laporan/view-sales/'+data.kry_kode+'/transaksi'+'/'+start+'/'+end+'"><i class="icon-eye"></i> View</a>';
      //       }
      //     },
      //   ]
      // });
    });

    $('#print').click(function() {
      var href = "{{route('omsetSalesRekapView')}}";
      var type_report = $("input[name='report']:checked").val();
      var start = $('#start_date').val();
      var end = $('#end_date').val();
      $.ajax({
          url: href,
          type: 'POST',
          data: {
            start_date: start,
            end_date: end,
            report: type_report,
            _token: "{{ csrf_token() }}" },
          success: function(data) {
            // console.log(respond);
            // window.location.href = data.redirect;
            window.open(data.redirect,'_blank');
          },
          error: function(request, status, error) {
            // console.log(error);
          }
      });
      return false;
    });

    $('#print-modal').click(function() {
      var type = $('#type').val();
      if (type == 'Rekap') {
        var href = "{{route('omsetSalesRekapView')}}";
      }
      else {
        var href = "{{route('omsetSalesDetailView')}}";
      }
      var start = $('#start_date').val();
      var end = $('#end_date').val();
      var type_report = $("input[name='report']:checked").val();
      var mrk_kode = $("#mrk_kode").find(':selected').val();

      $.ajax({
          url: href,
          type: 'POST',
          data: {
            start_date: start,
            end_date: end,
            report: type_report,
            mrk_kode: mrk_kode,
            _token: "{{ csrf_token() }}" },
          success: function(data) {
            // console.log(respond);
            // window.location.href = data.redirect;
            window.open(data.redirect,'_blank');
          },
          error: function(request, status, error) {
            // console.log(error);
          }
      });
      return false;
    });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <div class="row">
                {{-- <div class="input-daterange"> --}}
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                      <input type="date" name="start_date" id="start_date" class="form-control">
                    </div>
                  </div>
                  <div class="col-xs-1 text-center">
                    <h4>S/d</h4>
                  </div>
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                      <input type="date" name="end_date" id="end_date" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <select class="form-control selectpicker" data-live-search="true" name="mrk_kode" id="mrk_kode">
                      <option value="all" selected>Pilih Semua Merek</option>
                      @foreach ($merek as $key => $value)
                        <option value="{{$value->mrk_kode}}">{{$value->mrk_nama}}</option>
                      @endforeach
                    </select>
                  </div>
                {{-- </div> --}}
                <div class="col-md-2">
                  <button type="button" name="button" id="search-date" class="btn btn-info"><i class="glyphicon glyphicon-search"></i> Search</button>
                  {{-- <button type="button" name="button" id="print" class="btn btn-info"><i class="glyphicon glyphicon-print"></i> Print</button> --}}
                  <button class="btn btn-success pull-right btn-print" data-toggle="modal" href="#modal-print">
                    <i class="glyphicon glyphicon-print"></i> Print
                  </button>
                </div>
              </div>
              <br>
              <br>
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1_1" data-toggle="tab"> Penjualan </a>
                </li>
                {{-- <li>
                  <a href="#tab_1_2" data-toggle="tab"> Penjualan Titipan </a>
                </li> --}}
              </ul>
              <div class="tab-content">
                <div class="tab-pane active in" id="tab_1_1">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
                    <thead>
                      <tr class="">
                        <th width="10"> No </th>
                        <th> Nama Sales </th>
                        <th> Qty Jual </th>
                        <th> Rp. Jual </th>
                        <th> Qty Retur </th>
                        <th> Rp. Retur </th>
                        <th> Action </th>
                      </tr>
                    </thead>
                  </table>
                </div>
                {{-- <div class="tab-pane" id="tab_1_2">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_4">
                    <thead>
                      <tr class="">
                        <th width="10"> No </th>
                        <th> Nama Sales </th>
                        <th> Qty Jual </th>
                        <th> Rp. Jual </th>
                        <th> Action </th>
                      </tr>
                    </thead>
                  </table>
                </div> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-print" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="#" class="form-horizontal" role="form" method="post" target="_blank">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="type" id="type">
                    <option value="Rekap">Rekap</option>
                    <option value="Detail">Detail</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="text-center">
                  <label class="radio-inline"><input type="radio" name="report" value="print" checked>Print</label>
                  <label class="radio-inline"><input type="radio" name="report" value="excel">Excel</label>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="button" class="btn green" id="print-modal">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
