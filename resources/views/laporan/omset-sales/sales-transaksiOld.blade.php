@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th width="10"> No </th>
                    <th> Tanggal </th>
                    <th> No. Faktur </th>
                    <th> Pelanggan </th>
                    <th> Qty Jual </th>
                    <th> Rp. Jual </th>
                    <th> Terbayar </th>
                    <th> Sisa Bayar </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    @foreach ($row['penjualan'] as $keyTR)
                      <tr>
                        <td> {{ $no++ }}. </td>
                        <td> {{ date('Y-m-d', strtotime($keyTR->pl_tgl)) }} </td>
                        <td> {{ $kode.$keyTR->pl_no_faktur }} </td>
                        <td> {{ $keyTR->customer['cus_nama'] }} </td>
                        <td> {{ number_format($keyTR->qty_jual, 0, "." ,".") }} </td>
                        <td> {{ number_format($keyTR->total_jual, 0, "." ,".") }} </td>
                        <td> {{ number_format($keyTR->terbayar, 0, "." ,".") }} </td>
                        <td> {{ number_format($keyTR->pp_amount, 0, "." ,".") }} </td>
                        <td class="text-center">
                          <a class="btn btn-primary btn-detail btn-icon" type="button" href="{{ route('viewSalesDetail', ['kode'=>$keyTR->pl_no_faktur]) }}"><i class="icon-eye"></i> View</a>
                        </td>
                      </tr>
                    @endforeach
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
