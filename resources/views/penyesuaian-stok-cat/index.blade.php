@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/.css') }}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <style media="screen">
  td.wrapok {
    white-space:nowrap
  }

  td.fontsize{
    font-size:10px
  }
  </style>
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>

  <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/penyesuaianSC.js') }}" type="text/javascript"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script type="text/javascript">
  var l = window.location;
  var base_url = l.protocol + "//" + l.host + "/";

  $('#table_2').DataTable({
    destroy : true,
    processing: true,
    serverSide: true,
    ajax: base_url+'penyesuaian-stok-cat/history',
    columns: [
      {data: 'pco_kode', class:"wrapok fontsize"},
      {data: 'pco_tgl', class:"wrapok fontsize"},
      {data: 'hpp_total', class:"fontsize", render: function ( data, type, row ) {
        return numeral(data).format('0,0[.]00');
        }
      },
      { data: null, orderable: false, searchable: false, render: function ( data, type, row ) {
        return '<div style="font-size:10px; white-space: nowrap" class="btn-group-xs"> <a class="btn btn-info" href="'+base_url+'penyesuaian-stok-cat/'+data.pco_kode+'/detail"><span class="icon-pencil"></span> Detail</a></div>';
        }
      },
    ]
  });
  </script>
@stop

@section('body')
  <span id="data-back"
  data-form-token="{{ csrf_token() }}"
  data-route-penjualan-langsung-barang-row="{{ route('penjualanLangsungBarangRow') }}"
  data-route-gudang-row="{{ route('penjualanLangsungGudangRow') }}"
  data-route-stok-row="{{ route('penjualanLangsungStokRow') }}"></span>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Penyesuaian </a>
              </li>
              <li>
                <a href="#tab_1_2" data-toggle="tab"> History Penyesuaian </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active in" id="tab_1_1">
                <form class="form-send-penjualan" action="{{route('penyesuaianSC.store')}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-body">
                        <div class="form-group">
                          <label class="col-md-3">Kode Penyesuaian Cat Oplosan</label>
                          <div class="col-md-9">
                            <input type="text" name="kode_bukti_id" class="form-control" value="{{$new_id}}" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-2">
                      <button type="button" class="btn btn-success btn-block btn-row-plus">
                        <span class="fa fa-plus"></span> Tambah Data
                      </button>
                    </div>
                  </div>
                  <br>
                  <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data ">
                    <thead>
                      <tr class="">
                        <th width="14%"> Barcode </th>
                        <th width="10%"> Nama </th>
                        <th width="10%"> Nomer Seri </th>
                        <th width="10%"> Gudang </th>
                        <th class="hide"> Satuan </th>
                        <th class="hide"> Barang HPP </th>
                        <th width="6%"> Qty </th>
                        <th width="8%"> Aksi </th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>

                  <div class="row">
                    <div class="col-md-4 col-md-offset-8 col-xs-12 col-sm-4 form-horizontal">
                      <div class="form-group">
                        <label class="col-md-3">Total HPP</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="all_total_hpp" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-body">
                        <div class="form-group">
                          <button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="modal">SAVE</button>
                          {{-- <a href="#" class="btn btn-warning btn-lg btn-block">Batal</a> --}}
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="tab-pane" id="tab_1_2">
                <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_2">
                  <thead>
                    <tr class="">
                      <th>Kode</th>
                      <th>Tanggal</th>
                      <th>Total</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal bs-modal-lg" id="modal-barang" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-smpl2"></button>
          <h4 class="modal-title"> Daftar Barang </h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_6">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Barkode</th>
                <th>Nama Barang</th>
                <th>Satuan</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
              @foreach($barang as $r)
                <tr>
                  <td>{{ $no++ }}.</td>
                  <td>{{ $kodeBarang.$r->brg_kode }}</td>
                  <td>{{ $r->brg_barcode }}</td>
                  <td>{{ $r->brg_nama }}</td>
                  <td>{{ $r->stn_nama}}</td>
                  <td style="white-space: nowrap">
                    <div class="btn-group-md">
                      <button class="btn btn-info btn-stok"
                      data-href="{{ route('penjualanTitipanGetStok', ['kode'=>$r->brg_kode]) }}">
                      <span class="icon-eye"></span> Lihat Stok
                    </button>
                    <button class="btn btn-success btn-pilih-barang"
                    data-brg-kode="{{ $r->brg_kode }}"
                    data-brg-barkode="{{ $r->brg_barcode }}"
                    data-brg-nama="{{ $r->brg_nama }}">
                    <span class="icon-plus"></span> Pilih Barang
                  </button>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<table class="table-row-data hide" id="table-data-barang">
  <tbody>
    <tr>
      <td class="brg_kode">
        <div class="form-inline input-group">
          <input type="text" name="brg_barcode[]" class="form-control" required="">
          <input type="hidden" name="brg_kode[]" class="form-control" required="">
          <span class="input-group-btn">
            <button type="button" class="btn btn-success btn-modal-barang" data-toggle="modal">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>
        </div>
      </td>
      <td class="nama">
        <input type="text" class="form-control" name="nama[]" readonly>
      </td>
      <td class="brg_no_seri">
        <select name="brg_no_seri[]" class="form-control" data-placeholder="Pilih No Seri" required="">
        </select>
      </td>
      <td class="gdg_kode">
        <select name="gdg_kode[]" class="form-control" data-placeholder="Pilih Gudang" required="">
        </select>
      </td>
      <td class="satuan hide">
        <input type="text" name="satuan[]" class="form-control" readonly>
      </td>
      <td class="brg_hpp hide">
        <input type="number" class="form-control" name="brg_hpp[]" value="0" min="0" readonly>
      </td>
      <td class="brg_hpp_total hide">
        <input type="number" class="form-control" name="brg_hpp_total[]" value="0" min="0" readonly>
      </td>
      <td class="qty">
        <input type="number" class="form-control" name="qty[]" value="0" min="0" required>
      </td>
      <td>
        <button type="button" class="btn btn-danger btn-row-delete">Delete</button>
      </td>
    </tr>
  </tbody>
</table>

<div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-blue-steel bg-font-blue-steel">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          Stok
        </h4>
      </div>
      <div class="modal-body form">
        <div class="form-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
            <thead>
              <tr class="">
                <th> No Seri </th>
                <th> QTY </th>
                <th> Titipan </th>
                <th> Gudang </th>
                <th> Supplier </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
