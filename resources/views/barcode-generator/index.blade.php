<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <style>
    @media print {
      html, body {
      display: block;
      font-family: "Tahoma";
      margin: 0px 0px 0px 0px;
      }

      p {
        font-size: 12px;
        padding: 0 !important;
        margin: 0 !important;
      }

      #div-barcode{
        display: flex;
        justify-content: space-between;
        padding-top: 1.5mm;
        padding-right: 1.5mm;
        padding-bottom: 1.5mm;
        padding-left: 1.5mm;
      }

      .barcode{
        display: flex;
        justify-content: space-between;
        padding-top: 1.5mm;
        padding-right: 1.5mm;
        padding-bottom: 1.5mm;
        padding-left: 1.5mm;
      }

      .footer {
        position: sticky;
        bottom: 0;
      }

      @page {
        size: 105mm 28mm;
      }
    }
    p {
      font-size: 12px;
      padding: 0 !important;
      margin: 0 !important;
    }

    #div-barcode{
      display: flex;
      justify-content: space-between;
      padding-top: 1.5mm;
      padding-right: 1.5mm;
      padding-bottom: 1.5mm;
      padding-left: 1.5mm;
    }

    .barcode{
      display: flex;
      justify-content: space-between;
      padding-top: 1.5mm;
      padding-right: 1.5mm;
      padding-bottom: 1.5mm;
      padding-left: 1.5mm;
    }

    .footer {
      position: sticky;
      bottom: 0;
    }
    </style>

  </head>
  <body>
    <div id="div-barcode" class="container text-center" style="border: 0px solid; width: 114mm;">
      @if ($barang != null)
        {{-- @for ($i=0; $i < 2; $i++) --}}
          <div id="bar1" class="barcode" style="border: 0px solid; width: 50mm; height: 25mm; display: inline-block;">
            <div id="barcode" style="display: block">
              <img src="data:image/png; base64, {{DNS1D::getBarcodePNG($barang->brg_barcode, 'C128A')}}" style="max-width: 100%;" alt="barcode"/>
            </div>
            <div id="brg_barcode" style="display: block;">
              <p style="text-align: center;">{{$barang->brg_barcode}}</p>
            </div>
            <div id="brg_nama" style="display: block; max-height: 50%;">
              <p style="text-align: center;">{{$barang->brg_nama}}</p>
            </div>
            <div class="footer">
              <div id="harga1" style="position: absolute;">
                {{-- <P><b>Rp {{number_format($barang->brg_harga_jual_eceran, 2, "," ,".")}}</b></P> --}}
                <P><b>Rp {{number_format((($barang->brg_harga_jual_eceran * ($barang->brg_ppn_dari_supplier_persen/100)) + $barang->brg_harga_jual_eceran), 2, "," ,".")}}</b></P>
              </div>
              <div id="logo1" style="position: absolute;">
                <p><b>GK18</b></p>
              </div>
            </div>
          </div>

          <div id="bar2" class="barcode" style="border: 0px solid; width: 50mm; height: 25mm; display: inline-block;">
            <div id="barcode" style="display: block">
              <img src="data:image/png; base64, {{DNS1D::getBarcodePNG($barang->brg_barcode, 'C128A')}}" style="max-width: 100%;" alt="barcode"/>
            </div>
            <div id="brg_barcode" style="display: block;">
              <p style="text-align: center;">{{$barang->brg_barcode}}</p>
            </div>
            <div id="brg_nama" style="display: block; max-height: 50%;">
              <p style="text-align: center;">{{$barang->brg_nama}}</p>
            </div>
            <div class="footer">
              <div id="harga2" style="position: absolute;">
                {{-- <P><b>Rp {{number_format($barang->brg_harga_jual_eceran, 2, "," ,".")}}</b></P> --}}
                <P><b>Rp {{number_format((($barang->brg_harga_jual_eceran * ($barang->brg_ppn_dari_supplier_persen/100)) + $barang->brg_harga_jual_eceran), 2, "," ,".")}}</b></P>
              </div>
              <div id="logo2" style="position: absolute;">
                <p><b>GK18</b></p>
              </div>
            </div>
          </div>
        {{-- @endfor --}}
      @else
        DATA TIDAK DITEMUKAN
        {{-- <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('11', 'C39')}}" alt="barcode"/> --}}
      @endif
    </div>
  </body>
</html>

<script type="text/javascript">
  var box1 = document.getElementById('bar1');
  var harga1 = document.getElementById('harga1');
  var logo1 = document.getElementById('logo1');
  harga1.style.top = (box1.offsetHeight - harga1.getBoundingClientRect().top - 12) + 'px';
  harga1.style.left = '0' + 'px';
  logo1.style.top = (box1.offsetHeight - logo1.getBoundingClientRect().top - 12) + 'px';
  logo1.style.right = '0' + 'px';

  var box2 = document.getElementById('bar2');
  var harga2 = document.getElementById('harga2');
  var logo2 = document.getElementById('logo2');
  harga2.style.top = (box2.offsetHeight - harga2.getBoundingClientRect().top - 12) + 'px';
  harga2.style.left = '0' + 'px';
  logo2.style.top = (box2.offsetHeight - logo2.getBoundingClientRect().top - 12) + 'px';
  logo2.style.right = '0' + 'px';
  window.print();
</script>
