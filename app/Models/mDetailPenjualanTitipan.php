<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailPenjualanTitipan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_detail_penjualan_titipan';
  protected $primaryKey = 'detail_pt_kode';
  // public $timestamps = false;

  public function PenjualanTitipan()
  {
    return $this->belongsTo('App\Models\mPenjualanTitipan', 'pt_no_faktur');
  }

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode')->select('brg_kode', 'brg_barcode', 'brg_nama', 'stn_kode');
  }

  public function PT_tgl()
  {
    return $this->belongsTo('App\Models\mPenjualanTitipan', 'pt_no_faktur')->select('pt_no_faktur', 'pt_tgl');
  }

  public function gdg()
  {
    return $this->belongsTo('App\Models\mGudang', 'gudang', 'gdg_kode')->select('gdg_kode', 'gdg_nama');
  }

  public function satuanJ()
  {
    return $this->belongsTo('App\Models\mSatuan', 'satuan', 'stn_kode')->select('stn_kode', 'stn_nama');
  }
}
