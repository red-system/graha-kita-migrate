<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAcKodeBukti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_ac_kode_bukti', function (Blueprint $table) {
           $table->increments('kode_bukti_id');
           $table->string('kbt_kode_nama')->nullable();
           $table->string('kbt_keterangan')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_ac_kode_bukti');
     }
}
