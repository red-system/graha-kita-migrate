@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />   
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.date-picker').datepicker()
            .on('changeDate', function(ev){                 
                $('.date-picker').datepicker('hide');
            });
            
        });
    </script>
    
@stop

@section('body')
<style type="text/css">
    .table .head{font-size: 13px;font-family: Tahoma;text-align: center;vertical-align: middle !important;padding-top: 1px;font-weight: bold;}
    .table td{font-size: 12px;font-family: Tahoma;padding-top: 1px;}
</style>
<div class="page-content-inner">
    <div class="mt-content-body">        
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="col-md-6 col-xs-6">
                        <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                Pilih Periode
                        </a>
                        <a style="font-size: 11px" type="button" class="btn btn-danger" href="{{route('printUmurPiutang',['start_date'=>$start_date,'end_date'=>$end_date, 'coa'=>$coa, 'cus'=>$cus, 'tipe'=>'print'])}}" target="_blank">
                            <span><i class="fa fa-print"></i></span> Print
                        </a>
                        <a style="font-size: 12px;" class="btn btn-info excel-btn" data-toggle="modal" type="button" href="#export-excel" >
                            Excel
                        </a>
                    </div>
                    <div class="portlet light">
                        <?php $akhir_tgl=$end_date;?>
                        <h3 style="font-family: Tahoma"><center>Umur Piutang<center></h3>
                        <h4 style="font-family: Tahoma"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y',strtotime($end_date))}}<?php $tgl_akhir=strtotime($end_date)?></center></h4>
                        <br /><br />
                        <h6 style="font-family: Tahoma;font-weight: bold">Kode Perkiraan : @if($coa!=0) {{$coa}} - {{$kode_coa[$coa]}}@else All @endif | Atas Nama : {{$nama_cus}}</h6>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr class="success">
                                    <td rowspan="3" class="head"> No </td>
                                    <td rowspan="3" class="head"> Nama Customer</td>
                                    <td rowspan="3" class="head"> Tgl Inv</td>
                                    <td rowspan="3" class="head"> No Inv </td>
                                    <td rowspan="3" class="head"> Tempo </td>
                                    <td rowspan="3" class="head"> Tgl Jth Temp </td>
                                    <td colspan="5" class="head">Umur Piutang</td>
                                </tr>
                                <tr class="success">
                                    <td colspan="2" class="head">Belum Jatuh Tempo</td>
                                    <td colspan="3" class="head">Sudah Jatuh Tempo</td>
                                </tr>
                                <tr class="success">
                                    <td class="head">1-30 days</td>
                                    <td class="head">30-60 days</td>
                                    <td class="head">1-30 days</td>
                                    <td class="head">30-60 days</td>
                                    <td class="head">60-90 days</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $ttl_a=0;$ttl_b=0;$ttl_c=0;$ttl_d=0;$ttl_e=0;?>
                            @foreach($data as $piutang)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$piutang->customers->cus_nama}}</td>
                                    @if($piutang->tipe_penjualan=='plg')
                                    <td>{{date('d M Y', strtotime($piutang->tgl_piutang))}}</td>
                                    <?php $tgl_jual=$piutang->tgl_piutang;?>
                                    @else
                                    <td>{{date('d M Y', strtotime($piutang->tgl_piutang))}}</td>
                                    <?php $tgl_jual=$piutang->tgl_piutang;?>
                                    @endif
                                    <td>{{$piutang->pp_no_faktur}}</td>
                                    <?php

                                        $tgl_beli = new DateTime($tgl_jual);
                                        $jth_tmp = new DateTime($piutang->pp_jatuh_tempo);
                                        $difference = $tgl_beli->diff($jth_tmp);
                                        $tmp = $difference->days;
                                        // $today = date('Y-m-d');
                                        $today = $akhir_tgl;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';

                                        if(strtotime($piutang->pp_jatuh_tempo)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli = new DateTime($tgl_jual);
                                            $today = new DateTime($today);
                                            $difference = $tgl_beli->diff($today);
                                            $days = $difference->days;
                                            if($days<=30){
                                                $aa         = $piutang->pp_sisa_amount;
                                                $a=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $piutang->pp_sisa_amount;
                                                $b=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }                                            
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp = new DateTime($piutang->pp_jatuh_tempo);
                                            $end_date = new DateTime($today);
                                            $difference = $jth_tmp->diff($end_date);
                                            $days = $difference->days;
                                            if($days<=30){
                                                $cc         = $piutang->pp_sisa_amount;
                                                $c=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $piutang->pp_sisa_amount;
                                                $d=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $piutang->pp_sisa_amount;
                                                $e=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }                                        
                                    ?>
                                    <td>{{$tmp}} days</td>
                                    <td>{{date('d M Y', strtotime($piutang->pp_jatuh_tempo))}}</td>
                                    <!--belum jatuh tempo-->                                    
                                    <td>{{$a}}</td>
                                    <td>{{$b}}</td>
                                    <!--sudah jatuh tempo-->
                                    <td>{{$c}}</td>
                                    <td>{{$d}}</td>
                                    <td>{{$e}}</td>
                                </tr>
                            @endforeach
                            @foreach($data_pl as $hl)
                                <tr>
                                    <td>{{$no++}}</td>
                                    @if($hl->id_tipe=='CUS')
                                    <td> {{ $hl->customer->cus_nama }} </td>
                                    @elseif($hl->id_tipe=='KYW')
                                    <td> {{ $hl->karyawan->kry_nama }} </td>
                                    @endif
                                    <td>{{date('d M Y', strtotime($hl->pl_tgl))}}</td>
                                    <td>{{$hl->pl_invoice}}</td>
                                    <?php
                                        $tgl_beli           = new DateTime($hl->pl_tgl);
                                        $jth_tmp            = new DateTime($hl->pl_jatuh_tempo);
                                        $difference         = $tgl_beli->diff($jth_tmp);
                                        $tmp                = $difference->days;
                                        // $today = date('Y-m-d');
                                        // $today              = $end_date;
                                        $today              = $akhir_tgl;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';

                                        if(strtotime($hl->pl_jatuh_tempo)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli       = new DateTime($hl->pl_tgl);
                                            $today          = new DateTime($today);
                                            $difference     = $tgl_beli->diff($today);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $aa         = $hl->pl_sisa_amount;
                                                $a          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $hl->pl_sisa_amount;
                                                $b          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp        = new DateTime($hl->pl_jatuh_tempo);
                                            $end_date       = new DateTime($today);
                                            $difference     = $jth_tmp->diff($end_date);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $cc         = $hl->pl_sisa_amount;
                                                $c          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $hl->pl_sisa_amount;
                                                $d          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $hl->pl_sisa_amount;
                                                $e          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }
                                    ?>
                                    <td>{{$tmp}} days</td>
                                    <td>{{date('d M Y', strtotime($hl->pl_jatuh_tempo))}}</td>
                                    <!--belum jatuh tempo--> 
                                    <td>{{$a}}</td>
                                    <td>{{$b}}</td>
                                    <!--sudah jatuh tempo-->
                                    <td>{{$c}}</td>
                                    <td>{{$d}}</td>
                                    <td>{{$e}}</td>
                                </tr>
                                @endforeach
                                @foreach($data_pc as $hc)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td> {{ $hc->customer->cus_nama }} </td>
                                    <td>{{date('d M Y', strtotime($hc->tgl_cek))}}</td>
                                    <td>{{$hc->no_bg_cek}}</td>
                                    <?php
                                        $tgl_beli           = new DateTime($hc->tgl_cek);
                                        $jth_tmp            = new DateTime($hc->tgl_pencairan);
                                        $difference         = $tgl_beli->diff($jth_tmp);
                                        $tmp                = $difference->days;
                                        // $today = date('Y-m-d');
                                        // $today              = $end_date;
                                        $today              = $akhir_tgl;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';

                                        if(strtotime($hc->tgl_pencairan)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli       = new DateTime($hc->tgl_cek);
                                            $today          = new DateTime($today);
                                            $difference     = $tgl_beli->diff($today);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $aa         = $hc->sisa;
                                                $a          = number_format($hc->sisa,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $hc->sisa;
                                                $b          = number_format($hc->sisa,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp        = new DateTime($hc->tgl_pencairan);
                                            $end_date       = new DateTime($today);
                                            $difference     = $jth_tmp->diff($end_date);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $cc         = $hc->sisa;
                                                $c          = number_format($hc->sisa,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $hc->sisa;
                                                $d          = number_format($hc->sisa,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $hc->sisa;
                                                $e          = number_format($hc->sisa,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }
                                    ?>
                                    <td>{{$tmp}} days</td>
                                    <td>{{date('d M Y', strtotime($hc->tgl_pencairan))}}</td>
                                    <!--belum jatuh tempo--> 
                                    <td>{{$a}}</td>
                                    <td>{{$b}}</td>
                                    <!--sudah jatuh tempo-->
                                    <td>{{$c}}</td>
                                    <td>{{$d}}</td>
                                    <td>{{$e}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="6" style="font-size: 13px;font-weight: bold;">Total</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_a,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_b,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_c,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_d,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_e,2)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    Date
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('periodeUmurPiutang') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">COA</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="kode_coa">
                                    <option value="0">All</option>
                                    <?php foreach ($kode_coa as $key => $value) {?>
                                    <option value="{{$key}}" <?php if($coa==$key) echo 'selected';?>>{{$key}} - {{$value}}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">Customer</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="cust">
                                    <option value="0">All</option>
                                    <?php foreach ($customer as $custom) {?>
                                    <option value="{{$custom->cus_kode}}" <?php if($cus==$custom->cus_kode) echo 'selected';?>>{{$custom->cus_nama}}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date_pembelian" value="{{date('Y-m-d', strtotime($start_date))}}" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date_pembelian" value="{{date('Y-m-d')}}"/>
                                <input type="hidden" name="tipe_laporan" value="rekapStock">

                            </div>
                        </div>
                        
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-4">
                                <button type="submit" class="btn green col-md-6">Search</button>
                                <button type="button" class="btn default col-md-6" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="export-excel" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('umurPiutangPrintExcel') }}" class="form-horizontal" role="form" method="post"  target="_blank">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">COA</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="kode_coa">
                                    <option value="0">All</option>
                                    <?php foreach ($kode_coa as $key => $value) {?>
                                    <option value="{{$key}}" <?php if($coa==$key) echo 'selected';?> >{{$key}} - {{$value}}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">Customer</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="cust">
                                    <option value="0">All</option>
                                    <?php foreach ($customer as $custom) {?>
                                    <option value="{{$custom->cus_kode}}" <?php if($cus==$custom->cus_kode) echo 'selected';?>>{{$custom->cus_nama}}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="{{$akhir_tgl}}"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                                <input type="hidden" name="tipe_laporan" value="rekapPembelian">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
