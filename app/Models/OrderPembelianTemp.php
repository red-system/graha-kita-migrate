<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPembelianTemp extends Model
{
    //
    //public $incrementing = false;
  	protected $table = 'tb_order_pembelian_temp';
  	protected $primaryKey = 'id_order_pembelian_temp';
  	//public $timestamps = false;

  	protected $fillable = [
      'kode',
  		'no_po',
  		'gdg_kode',
  		'brg_barcode',
  		'brg_kode',
  		'brg_nama',
  		'stock_id',
  		'satuan',
  		'satuan_kode',
  		'harga_beli',
  		'ppn',
  		'disc',
  		'disc_nom',
  		'harga_net',
  		'qty',
  		'total',
  		'keterangan',
			'sub_total',
			'no_seri',
      'id_detail_po',
      'ppn_nom',
  	];

    public function barang(){
      return $this->belongsTo(mBarang::class,'brg_kode','brg_kode');
    }

    public function stok(){
      return $this->belongsTo(mStok::class,'stock_id','stk_kode');

    }

    public function gudangs(){
      return $this->belongsTo(mGudang::class,'gdg_kode','gdg_kode');

    }


}
