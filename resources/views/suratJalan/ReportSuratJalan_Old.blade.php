<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <div>
      <div>
        <div>
          <table width="100%">
            <tr>
              <td>
                <img src="http://grahakita.ganesh-demo.online/img/icon/aki.png" width="70">
              </td>
              <td colspan="3">
                <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088 Fax.(0361)418933</p>
              </td>
            </tr>
          </table>
          <div style="float: left; width: 60%;">
            <table width="100%" border="0">
              <tr>
                <td width="20%"> <p>Dikirim Kepada</p> </td>
              </tr>
              <tr>
                <td width="20%"> <p>Nama</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur->cus_nama['cus_nama']}}</p> </td>
              </tr>
              <tr>
                <td width="20%"> <p>Alamat</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['cus_alamat']}}</p> </td>
              </tr>
            </table>
          </div>

          <div style="float: right; width: 40%;">
            <table width="100%" border="0">
              <tr>
                <td width="15%"> <p>No. Ftr</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['kode_bukti_id']}}</p> </td>
              </tr>
              <tr>
                <td width="20%"> <p>No. D.O.</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['do']}}</p> </td>
              </tr>
              <tr>
                <td width="20%"> <p>No. S.J.</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['sj']}}</p> </td>
              </tr>
              <tr>
                <td width="20%"> <p>Tgl. S.J.</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['tglPrint']}}</p> </td>
              </tr>
            </table>
          </div>

          <div class="index" style="clear: both; margin: 0pt; padding: 0pt;">
            {{-- <h5 align="center">Surat Jalan</h5> --}}
            <p align="center"><b>Surat Jalan</b></p>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <th> <p>No</p> </th>
                  <th> <p>Kode Stock</p> </th>
                  <th> <p>Nama Stock</p> </th>
                  <th> <p>Banyak</p> </th>
                  <th> <p>Satuan</p> </th>
                  <th> <p>Pack</p> </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($faktur->detail as $key)
                  <tr>
                    <td><p>{{$faktur->no++}}</p></td>
                    <td><p>{{$key->brg_barcode}}</p></td>
                    <td><p>{{$key->brg_nama}}</p></td>
                    <td><p>{{number_format($key->dikirim, 0, "." ,".")}}</p></td>
                    <td><p>{{$key->stn_nama}}</p></td>
                    <td><p>-</p></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="keterangan" style="clear: both; margin: 0pt; padding: 0pt;">
            <table width="20%" border="0">
              <tr>
                <td> <p>Cheker</p> </td>
                <td> <p>:</p> </td>
                <td> <p>{{$faktur->chekerPrint['kry_nama']}}</p> </td>
                <td> | </td>
                <td> <p>Supir</p> </td>
                <td> <p>:</p> </td>
                <td> <p>{{$faktur->supirPrint['kry_nama']}}</p> </td>
              </tr>
            </table>
          </div>
          <br>
          <div style="float: left; width: 50%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Pengirim</h5> --}}
                    <b><p>Pengirim</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style="float: left; width: 50%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Penerima</h5> --}}
                    <b><p>Penerima</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </body>
</html>
