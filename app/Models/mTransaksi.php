<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mTransaksi extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_ac_transaksi';
  protected $primaryKey = 'transaksi_id';
  public $timestamps = false;

  public function jurnalUmum(){
    return $this->belongsTo(mJurnalUmum::class,'jurnal_umum_id','jurnal_umum_id');
  }
  
  public function perkiraan(){
    return $this->belongsTo(mPerkiraan::class,'master_id','master_id');
  }


}
