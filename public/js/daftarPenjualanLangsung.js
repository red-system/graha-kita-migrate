$(document).ready(function () {
  $('.form-send-penjualan').submit(function(e) {
      e.preventDefault();
      var ini = $(this);

      $.ajax({
          url: ini.attr('action'),
          type: ini.attr('method'),
          data: ini.serialize(),
          success: function(data) {
              if(data.redirect) {
                  window.location.href = data.redirect;
              }
          },
          error: function(request, status, error) {
            swal({
              title: 'Perhatian',
              text: 'Data Gagal Disimpan!',
              type: 'error'
            });
              // var json = JSON.parse(request.responseText);
              // $('.form-group').removeClass('has-error');
              // $('.help-block').remove();
              // $.each(json.errors, function(key, value) {
              //     $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
              //     $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
              // });
          }
      });
      return false;
  });

  $('[name="qty_retur[]"]').keyup(function() {
    var total = 0;
    var qty_retur = $(this).val();
    var harga_net = $(this).parents('td').siblings('td').children('input[name="harga_net[]"]').val();
    total = parseInt(qty_retur) * parseInt(harga_net);
    var total_retur_detail = $(this).parents('td').siblings('td').children('input[name="total_retur_detail[]"]').val(total);

    subtotal();
    grand_total();
  });

  $('[name="disc"]').keyup(function () {
    grand_total();
  });

  $('[name="ppn"]').keyup(function () {
    grand_total();
  });

  $('[name="ongkos_angkut"]').keyup(function () {
    grand_total();
  });

  function subtotal() {
    var total = 0;
    $('[name="total_retur_detail[]"]').each(function(key, val) {
      var val = $(this).val();
      total = parseInt(total) + parseInt(val);
    });
    $('[name="subtotal"]').val(total);
  }

  function grand_total() {
    var total = 0;
    var subtotal = $('[name="subtotal"]').val();
    var disc = $('[name="disc"]').val();
    var ppn = $('[name="ppn"]').val();
    var ongkos_angkut = $('[name="ongkos_angkut"]').val();
    var kal_disc = (parseInt(subtotal) * (parseInt(disc)/100));
    var kal_ppn = (parseInt(subtotal) * (parseInt(ppn)/100));

    total = parseInt(subtotal) - parseInt(kal_disc) + parseInt(kal_ppn) + parseInt(ongkos_angkut);

    $('[name="grand_total"]').val(total);
  }

});
