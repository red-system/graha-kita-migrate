<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mJurnalUmum extends Model
{
  //public $incrementing = false;
  protected $table = 'tb_ac_jurnal_umum';
  protected $primaryKey = 'jurnal_umum_id';
  public $timestamps = false;

  public function transaksi(){
    return $this->hasMany(mTransaksi::class,'jurnal_umum_id','jurnal_umum_id')->orderBy('trs_jenis_transaksi','asc');
  }

  // public function trs_buku_besar(){
  //   return $this->hasMany(mTransaksi::class,'jurnal_umum_id','jurnal_umum_id')->orderBy('jmu_tanggal','asc');
  // }


}
