<?php
    use App\Models\mSupplier;

    $suppliers = mSupplier::all();
?>

@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
    <script src="{{ asset('js/jquery.maskMoney.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {


            $("#amount").maskMoney({ thousands:',', decimal:'.', affixesStay: false, precision: 2});
            $("#edit_amount").maskMoney({ thousands:',', decimal:'.', affixesStay: false, precision: 2});
            $('[name="hl_jatuh_tempo"],[name="hl_tgl"]').datepicker()
            .on('changeDate', function(ev){                 
                $('[name="hl_jatuh_tempo"],[name="hl_tgl"]').datepicker('hide');
            });
            
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                            <i class="fa fa-plus"></i> New
                        </button>
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="" align="center">
                                    <th width="10" style="font-size: 12px;"> No </th>
                                    <th style="font-size: 12px;"> No Hutang </th>
                                    <th style="font-size: 12px;"> Jatuh Tempo</th>
                                    <th style="font-size: 12px;"> Dari </th>
                                    <th style="font-size: 12px;"> Total </th>
                                    <th style="font-size: 12px;"> Keterangan </th>
                                    <th style="font-size: 12px;"> Status </th>
                                    <th style="font-size: 12px;"> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dataList as $hutang)
                            <tr>
                                <td style="font-size: 11px;"> {{ $no++ }}. </td>
                                <td style="font-size: 11px;"> {{ $hutang->no_hutang_lain }} </td>
                                <td style="font-size: 11px;"> {{ date('d M Y',strtotime($hutang->hl_jatuh_tempo)) }} </td>
                                <td style="font-size: 11px;"> {{ $hutang->spl->spl_nama }} </td>
                                <td style="font-size: 11px;"> Rp. {{ number_format($hutang->hl_sisa_amount,2) }} </td>
                                <td style="font-size: 11px;"> {{ $hutang->hs_keterangan}}</td>
                                <td style="font-size: 11px;">{{ $hutang->hs_status}}</td>
                                <td>
                                    <!-- <div class="btn-group btn-group-xs"> -->
                                        @if($hutang->hl_sisa_amount==$hutang->hl_amount)
                                        <button class="btn btn-success btn-edit btn-xs" data-href="{{ route('hutangLainEdit', ['kode'=>$hutang->hl_kode]) }}">
                                            <span class="icon-pencil"></span>
                                        </button>
                                        @endif
                                        @if($hutang->hl_sisa_amount>0)
                                        <a class="btn btn-danger btn-payment-hutang-supplier btn-xs" href="{{ route('get-hutang-lain', ['kode'=>$hutang->hl_kode]) }}" data-target="#modalPaymentHutang" data-toggle="modal">
                                            <span class="fa fa-money"></span>
                                        </a>
                                        @endif
                                        <a class="btn btn-primary btn-xs" href="{{ route('show-history-hutang', ['kode'=>$hutang->hl_kode]) }}" data-target="#viewDetailProduksi" data-toggle="modal">
                                            <span class="fa fa-bars"></span>
                                        </a>
                                    <!-- </div> -->
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="viewDetailProduksi" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                    
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modalPaymentHutang" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">                    
                <span> &nbsp;&nbsp;Loading... </span>                
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Penerimaan Hutang Lain-Lain
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('hutangLainInsert') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal</label>
                            <div class="col-md-9">
                                <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="hl_tgl" id="hl_tgl" value="{{date('Y-m-d')}}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">No Hutang</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inliner" value="{{$next_no_hutang_lain}}" size="16" type="text" name="no_hutang_lain" readonly="readonly" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jatuh Tempo</label>
                            <div class="col-md-9">
                                <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="hl_jatuh_tempo" id="hl_jatuh_tempo"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama</label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="hl_dari" required="required" data-placeholder="Dari">
                                    <option value=""></option>
                                    @foreach($suppliers as $spl)
                                    <option value="{{$spl->spl_kode}}">{{$spl->spl_nama}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="hl_dari"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Total</label>
                            <div class="col-md-9">
                                <input id="amount" type="text" class="form-control" name="hl_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="hs_keterangan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Akun Kredit</label>
                            <div class="col-md-9">
                                <select name="kode_perkiraan" class="form-control select2" data-placeholder="Kode Rekening">
                                    <option value="">--Pilih Kode Rekening--</option>
                                    <option value="2104">Hutang Biaya</option>
                                    <option value="2107">Hutang Lain-lain</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Akun Debet</label>
                            <div class="col-md-9">
                                <select name="akun_debet" class="form-control select2" data-placeholder="Kode Rekening">
                                    <option value="">--Pilih Kode Rekening--</option>
                                    @foreach($perkiraan as $r)
                                    <option value="{{ $r->mst_kode_rekening }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" class="form-control" name="hs_status" value="Belum Bayar">
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="hs_status">
                                    <option value="Belum Bayar">Belum Bayar</option>
                                    <option value="Lunas">Lunas</option>
                                </select>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-pencil"></i> Edit Hutang Lain-Lain
                </h4>
            </div>
            <div class="modal-body form">
                <form action="" class="form-horizontal form-send" role="form" method="put">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal</label>
                            <div class="col-md-9">
                                <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="hl_tgl" id="hl_tgl" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">No Hutang</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inliner" value="" size="16" type="text" name="no_hutang_lain" readonly="readonly" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jatuh Tempo</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inline date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="hl_jatuh_tempo" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama</label>
                            <div class="col-md-9">
                                <select class="form-control" name="hl_dari" required="required" data-placeholder="Dari">
                                    <option value=""></option>
                                    @foreach($suppliers as $spl)
                                    <option value="{{$spl->spl_kode}}">{{$spl->spl_nama}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="hl_dari"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Total</label>
                            <div class="col-md-9">
                                <input id="edit_amount" type="text" class="form-control" name="hl_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="hs_keterangan"></textarea>
                                <input type="hidden" class="form-control" name="hs_status">
                                <input type="hidden" class="form-control" name="kode_perkiraan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Akun Kredit</label>
                            <div class="col-md-9">
                                <select name="kode_perkiraan" class="form-control" data-placeholder="Kode Rekening">
                                    <option value="">--Pilih Kode Rekening--</option>
                                    <option value="2104">2104 - Hutang Biaya</option>
                                    <option value="2107">2107 - Hutang Lain-lain</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Akun Debet</label>
                            <div class="col-md-9">
                                <select class="form-control" name="akun_debet" required="required" data-placeholder="kode_perkiraan">
                                    <option value=""></option>
                                    @foreach($perkiraan as $pkr)
                                    <option value="{{$pkr->mst_kode_rekening}}">{{$pkr->mst_kode_rekening}} - {{$pkr->mst_nama_rekening}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="hl_dari"> -->
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="hs_status">
                                    <option value="Belum Bayar">Belum Bayar</option>
                                    <option value="Lunas">Lunas</option>
                                </select>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
