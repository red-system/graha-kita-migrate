<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbStok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_stok', function (Blueprint $table) {
         $table->increments('stk_kode');
         $table->integer('brg_kode')->nullable();
         $table->integer('gdg_kode')->nullable();
         $table->integer('stok')->nullable();
         $table->integer('stk_hpp')->nullable();
         $table->string('brg_no_seri')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_stok');
     }
}
