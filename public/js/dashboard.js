$(document).ready(function(){

  var base_url = $('#base-data').data('base-url');

  $('#dashboard-search').click(function() {
    var route_dashboard_chart = $('#data-back').data('route-dashboard-chart');
    var route_dashboard_chart_kategori = $('#data-back').data('route-dashboard-chart-kategori');
    var route_dashboard_chart_hourly = $('#data-back').data('route-dashboard-chart-hourly');
    var route_dashboard_chart_weekly = $('#data-back').data('route-dashboard-chart-weekly');
    var route_dashboard_chart_daily = $('#data-back').data('route-dashboard-chart-daily');
    var route_dashboard_chart_monthly = $('#data-back').data('route-dashboard-chart-monthly');

    var start = $('#dashboard-start').val();
    var end = $('#dashboard-end').val();
    var token = $('#data-back').data('form-token');
    var data_send = {
      start_date: start,
      end_date: end,
      _token: token
    };

    var total = $('#total');
    var sales_summary = $('#sales_summary');
    var net_profit = $('#net_profit');
    var transaction = $('#transaction');
    // var avg = $('#avg');

    var cash = $('#cash');
    var credit = $('#credit');
    var EDC = $('#EDC');
    var BG = $('#BG');
    var TRF = $('#TRF');
    var TTL = $('#TTL');
    var NET = $('#NET');
    // var kas = $('#kas');
    // var bank = $('#bank');
    var m_data = $('#m_data');

    $.ajax({
      url: route_dashboard_chart,
      type: 'POST',
      data: data_send,
      success: function(data) {
        total.html(data.total);
        sales_summary.html(data.gross);
        net_profit.html(data.net);
        transaction.html(data.transaksi);
        // avg.html(data.avg);

        cash.html(data.cash);
        credit.html(data.credit);
        EDC.html(data.EDC);
        BG.html(data.BG);
        TRF.html(data.TRF);
        TTL.html(data.TTL);
        NET.html(data.NET_PRO);
        // kas.html(data.kas);
        // bank.html(data.bank);
        m_data.html(data.m_data);
        // console.log(data);
      }
    });

    $.ajax({
      url: route_dashboard_chart_kategori,
      type: 'POST',
      data: data_send,
      success: function(data) {
        $('#sample_2').DataTable({
          destroy   : true,
          processing: true,
          serverSide: true,
          searching : false,
          paging    : false,
          ordering  : false,
          info      : false,
          ajax : {
            url: route_dashboard_chart_kategori,
            type: 'POST',
            data: data_send,
            dataSrc : ''
          },
          columns: [
            { data: "C_name" },
            {
              data: null, render: function ( data, type, row )
              {
                return parseFloat(data.jumlah).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
              }
            },
            // { data: "jumlah" },
          ]
        });

        var category = [];
        var total = [];
        var coloR = [];

        for (var i in data) {
          category.push(data[i].C_name);
          total.push(data[i].jumlah);
          coloR.push(getRandomColor());
        }
        var chartData = {
          labels: category,
          datasets: [{
            label: 'Category',
            backgroundColor: coloR,
            borderColor: 'rgba(200, 200, 200, 0.75)',
            hoverBorderColor: 'rgba(200, 200, 200, 1)',
            data: total
          }]
        };

        $('#product_category').remove();
        $('#div-product_category').append('<canvas id="product_category"><canvas>');
        canvas = document.querySelector('#product_category');

        // var product_category = $("#product_category");
        var kategoriChartPost = new Chart(canvas, {
          type: 'pie',
          data: chartData,
          options: {
            maintainAspectRatio: false,
          }
        })
      },
      error: function(data) {
        // console.log(data);
      },
    });

    $.ajax({
      url: route_dashboard_chart_monthly,
      type: 'POST',
      data: data_send,
      success: function(data) {
        var Labels_monthly_gross = [];
        var jumlah_monthly_grossPL = [];
        var jumlah_monthly_grossPT = [];

        for (var i in data.label) {
          Labels_monthly_gross.push(data.label[i]);
        }
        for (var i in data.PL) {
          jumlah_monthly_grossPL.push(data.PL[i].total);
        }
        for (var i in data.PT) {
          jumlah_monthly_grossPT.push(data.PT[i].total);
        }
        var chartData = {
          labels: Labels_monthly_gross,
          datasets: [{
            label: 'Penjualan Langsung',
            backgroundColor: getRandomColor(),
            borderColor: [],
            data: jumlah_monthly_grossPL,
            borderWidth: 2
          },
          {
            label: 'Penjualan Titipan',
            backgroundColor: getRandomColor(),
            borderColor: [],
            data: jumlah_monthly_grossPT,
            borderWidth: 2
          }]
        };

        $('#monthly_gross').remove();
        $('#div-monthly_gross').append('<canvas id="monthly_gross"><canvas>');
        canvas = document.querySelector('#monthly_gross');

        // var monthly_gross = $("#monthly_gross");
        var dailyChartPost = new Chart(canvas, {
          type: 'bar',
          data: chartData,
          options: {
            maintainAspectRatio: false,
            scales: {
              xAxes: [{
                time: {
                  unit: 'day'
                }
              }],
              yAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }]
            },
            legend: { display: true },
            title: {
              display: false,
              text: 'Monthly Gross'
            }
          }
        })
      },
      error: function(data) {
        // console.log(data);
      },
    });

    // $.ajax({
    //   url: route_dashboard_chart_daily,
    //   type: 'POST',
    //   data: data_send,
    //   success: function(data) {
    //     var Labels_daily_gross = [];
    //     var jumlah_daily_grossPL = [];
    //     var jumlah_daily_grossPT = [];
    //
    //     for (var i in data.label) {
    //       Labels_daily_gross.push(data.label[i].tgl);
    //     }
    //     for (var i in data.PL) {
    //       jumlah_daily_grossPL.push(data.PL[i].total);
    //     }
    //     for (var i in data.PT) {
    //       jumlah_daily_grossPT.push(data.PT[i].total);
    //     }
    //     var chartData = {
    //       labels: Labels_daily_gross,
    //       datasets: [{
    //         label: 'Penjualan Langsung',
    //         backgroundColor: getRandomColor(),
    //         borderColor: [],
    //         data: jumlah_daily_grossPL,
    //         borderWidth: 2
    //       },
    //       {
    //         label: 'Penjualan Titipan',
    //         backgroundColor: getRandomColor(),
    //         borderColor: [],
    //         data: jumlah_daily_grossPT,
    //         borderWidth: 2
    //       }]
    //     };
    //
    //     $('#daily_gross').remove();
    //     $('#div-daily_gross').append('<canvas id="daily_gross"><canvas>');
    //     canvas = document.querySelector('#daily_gross');
    //
    //     // var daily_gross = $("#daily_gross");
    //     var dailyChartPost = new Chart(canvas, {
    //       type: 'bar',
    //       data: chartData,
    //       options: {
    //         maintainAspectRatio: false,
    //         scales: {
    //           xAxes: [{
    //             time: {
    //               unit: 'day'
    //             }
    //           }],
    //           yAxes: [{
    //             ticks: {
    //               beginAtZero:true
    //             }
    //           }]
    //         },
    //         legend: { display: true },
    //         title: {
    //           display: false,
    //           text: 'Daily Gross'
    //         }
    //       }
    //     })
    //   },
    //   error: function(data) {
    //     // console.log(data);
    //   },
    // });
    //
    // $.ajax({
    //   url: route_dashboard_chart_weekly,
    //   type: 'POST',
    //   data: data_send,
    //   success: function(data) {
    //     var Labels_weekly_gross = [];
    //     var jumlah_weekly_grossPL = [];
    //     var jumlah_weekly_grossPT = [];
    //
    //     for (var i in data.label) {
    //       Labels_weekly_gross.push(data.label[i].tgl);
    //     }
    //     for (var i in data.PL) {
    //       jumlah_weekly_grossPL.push(data.PL[i].total);
    //     }
    //     for (var i in data.PT) {
    //       jumlah_weekly_grossPT.push(data.PT[i].total);
    //     }
    //
    //     var chartData = {
    //       labels: Labels_weekly_gross,
    //       datasets: [{
    //         label: 'Penjualan Langsung',
    //         backgroundColor: getRandomColor(),
    //         borderColor: [],
    //         data: jumlah_weekly_grossPL,
    //         borderWidth: 2
    //       },
    //       {
    //         label: 'Penjualan Titipan',
    //         backgroundColor: getRandomColor(),
    //         borderColor: [],
    //         data: jumlah_weekly_grossPT,
    //         borderWidth: 2
    //       }]
    //     };
    //
    //     $('#weekly_gross').remove();
    //     $('#div-weekly_gross').append('<canvas id="weekly_gross"><canvas>');
    //     canvas = document.querySelector('#weekly_gross');
    //
    //     // var weekly_gross = $("#weekly_gross");
    //     var weeklyChartPost = new Chart(canvas, {
    //       type: 'bar',
    //       data: chartData,
    //       options: {
    //         scales: {
    //           xAxes: [{
    //             time: {
    //               unit: 'day'
    //             }
    //           }],
    //           yAxes: [{
    //             ticks: {
    //               beginAtZero:true
    //             }
    //           }]
    //         },
    //         legend: { display: true },
    //         title: {
    //           display: false,
    //           text: 'Weekly Gross'
    //         }
    //       }
    //     })
    //   },
    //   error: function(data) {
    //     // console.log(data);
    //   },
    // });
    //
    // $.ajax({
    //   url: route_dashboard_chart_hourly,
    //   type: 'POST',
    //   data: data_send,
    //   success: function(data) {
    //     var Labels_hourly_gross = [];
    //     var jumlah_hourly_grossPL = [];
    //     var jumlah_hourly_grossPT = [];
    //
    //     for (var i in data.label) {
    //       Labels_hourly_gross.push(data.label[i].det_tgl);
    //     }
    //     for (var i in data.PL) {
    //       jumlah_hourly_grossPL.push(data.PL[i].total);
    //     }
    //     for (var i in data.PT) {
    //       jumlah_hourly_grossPT.push(data.PT[i].total);
    //     }
    //
    //     var chartData = {
    //       labels: Labels_hourly_gross,
    //       datasets: [{
    //         label: 'Penjualan Langsung',
    //         backgroundColor: getRandomColor(),
    //         borderColor: [],
    //         data: jumlah_hourly_grossPL,
    //         borderWidth: 2
    //       },
    //       {
    //         label: 'Penjualan Titipan',
    //         backgroundColor: getRandomColor(),
    //         borderColor: [],
    //         data: jumlah_hourly_grossPT,
    //         borderWidth: 2
    //       }]
    //     };
    //
    //     $('#hourly_gross').remove();
    //     $('#div-hourly_gross').append('<canvas id="hourly_gross"><canvas>');
    //     canvas = document.querySelector('#hourly_gross');
    //
    //     // var hourly_gross = $("#hourly_gross");
    //     var hourlyChartPost = new Chart(canvas, {
    //       type: 'bar',
    //       data: chartData,
    //       options: {
    //         scales: {
    //           xAxes: [{
    //             time: {
    //               unit: 'hour'
    //             }
    //           }],
    //           // xAxes: [{
    //           //   type: 'time',
    //           //   time: {
    //           //     displayFormats: {
    //           //       quarter: 'H:mm'
    //           //     }
    //           //   }
    //           // }],
    //           yAxes: [{
    //             ticks: {
    //               beginAtZero:true
    //             }
    //           }]
    //         },
    //         legend: { display: true },
    //         title: {
    //           display: false,
    //           text: 'Hourly Gross'
    //         }
    //       }
    //     })
    //   },
    //   error: function(data) {
    //     // console.log(data);
    //   },
    // });

  }); //END

  // Random Color
  function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  $.ajax({
    url: base_url+"/dashboard/top_category",
    method: "GET",
    success: function(data) {
      var category = [];
      var total = [];
      var coloR = [];

      // var dynamicColors = function() {
      //    var r = Math.floor(Math.random() * 255);
      //    var g = Math.floor(Math.random() * 255);
      //    var b = Math.floor(Math.random() * 255);
      //    return "rgb(" + r + "," + g + "," + b + ")";
      // };

      for (var i in data) {
        category.push(data[i].C_name);
        total.push(data[i].jumlah);
        coloR.push(getRandomColor());
      }
      var chartData = {
        labels: category,
        datasets: [{
          label: 'Category',
          backgroundColor: coloR,
          borderColor: 'rgba(200, 200, 200, 0.75)',
          hoverBorderColor: 'rgba(200, 200, 200, 1)',
          data: total
        }]
      };

      var product_category = $("#product_category");
      var kategoriChartGet = new Chart(product_category, {
        type: 'pie',
        data: chartData,
        options: {
          maintainAspectRatio: false,
        }
      })
    },
    error: function(data) {
      // console.log(data);
    },
  });

  $('#sample_2').DataTable({
    destroy   : true,
    processing: true,
    serverSide: true,
    searching : false,
    paging    : false,
    ordering  : false,
    info      : false,
    ajax : {
      url: base_url+"/dashboard/top_category",
      type: 'GET',
      dataSrc : ''
    },
    columns: [
      { data: "C_name" },
      {
        data: null, render: function ( data, type, row )
        {
          return parseFloat(data.jumlah).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        }
      },
      // { data: "jumlah" },
    ]
  });

  var url_daily_gross = base_url+'/dashboard/daily';
  var Labels_daily_gross = new Array();
  var jumlah_daily_grossPL = new Array();
  var jumlah_daily_grossPT = new Array();
  $.get(url_daily_gross, function(response){
    // response.forEach(function(data){
    response.label.forEach(function(label) {
      Labels_daily_gross.push(label.tgl);
    })
    response.PL.forEach(function(langsung) {
      jumlah_daily_grossPL.push(langsung.total);
    })
    response.PT.forEach(function(titipan) {
      jumlah_daily_grossPT.push(titipan.total);
    })
    // });
    var daily_gross = document.getElementById("daily_gross").getContext('2d');
    var dailyChartGet = new Chart(daily_gross, {
      type: 'bar',
      data: {
        labels: Labels_daily_gross,
        datasets: [{
          label: 'Penjualan Langsung',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_daily_grossPL,
          borderWidth: 2
        },
        {
          label: 'Penjualan Titipan',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_daily_grossPT,
          borderWidth: 2
        }]
      },
      options: {
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            time: {
              unit: 'day'
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: { display: true },
        title: {
          display: false,
          text: 'Daily Gross'
        }
      }
    });
  });

  var url_monthly_gross = base_url+'/dashboard/monthly';
  var Labels_monthly_gross = new Array();
  var jumlah_monthly_grossPL = new Array();
  var jumlah_monthly_grossPT = new Array();
  $.get(url_monthly_gross, function(response){
    // response.forEach(function(data){
    response.label.forEach(function(label) {
      Labels_monthly_gross.push(label);
    })
    response.PL.forEach(function(langsung) {
      jumlah_monthly_grossPL.push(langsung.total);
    })
    response.PT.forEach(function(titipan) {
      jumlah_monthly_grossPT.push(titipan.total);
    })
    // });
    var monthly_gross = document.getElementById("monthly_gross").getContext('2d');
    var monthlyChartGet = new Chart(monthly_gross, {
      type: 'bar',
      data: {
        labels: Labels_monthly_gross,
        datasets: [{
          label: 'Penjualan Langsung',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_monthly_grossPL,
          borderWidth: 2
        },
        {
          label: 'Penjualan Titipan',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_monthly_grossPT,
          borderWidth: 2
        }]
      },
      options: {
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            time: {
              unit: 'day'
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: { display: true },
        title: {
          display: false,
          text: 'Monthly Gross'
        }
      }
    });
  });

  var url_weekly_gross = base_url+'/dashboard/weekly';
  var Labels_weekly_gross = new Array();
  var jumlah_weekly_grossPL = new Array();
  var jumlah_weekly_grossPT = new Array();
  $.get(url_weekly_gross, function(response){
    // response.forEach(function(data){
    response.label.forEach(function(label) {
      Labels_weekly_gross.push(label.tgl);
    })
    response.PL.forEach(function(langsung) {
      jumlah_weekly_grossPL.push(langsung.total);
    })
    response.PT.forEach(function(titipan) {
      jumlah_weekly_grossPT.push(titipan.total);
    })
    // });
    var weekly_gross = document.getElementById("weekly_gross").getContext('2d');
    var weeklyChartGet = new Chart(weekly_gross, {
      type: 'bar',
      data: {
        labels: Labels_weekly_gross,
        datasets: [{
          label: 'Penjualan Langsung',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_weekly_grossPL,
          borderWidth: 2
        },
        {
          label: 'Penjualan Titipan',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_weekly_grossPT,
          borderWidth: 2
        }]
      },
      options: {
        scales: {
          xAxes: [{
            time: {
              unit: 'day'
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: { display: true },
        title: {
          display: false,
          text: 'Weekly Gross'
        }
      }
    });
  });

  var url_hourly_gross = base_url+'/dashboard/hourly';
  var Labels_hourly_gross = new Array();
  var jumlah_hourly_grossPL = new Array();
  var jumlah_hourly_grossPT = new Array();
  $.get(url_hourly_gross, function(response){
    // response.forEach(function(data){
    response.label.forEach(function(label) {
      Labels_hourly_gross.push(label.det_tgl);
    })
    response.PL.forEach(function(langsung) {
      jumlah_hourly_grossPL.push(langsung.total);
    })
    response.PT.forEach(function(titipan) {
      jumlah_hourly_grossPT.push(titipan.total);
    })
    // });
    var hourly_gross = document.getElementById("hourly_gross").getContext('2d');
    var hourlyChartGet = new Chart(hourly_gross, {
      type: 'bar',
      data: {
        labels: Labels_hourly_gross,
        datasets: [{
          label: 'Penjualan Langsung',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_hourly_grossPL,
          borderWidth: 2
        },
        {
          label: 'Penjualan Titipan',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_hourly_grossPT,
          borderWidth: 2
        }]
      },
      options: {
        scales: {
          xAxes: [{
            time: {
              unit: 'hour'
            }
          }],
          // xAxes: [{
          //   type: 'time',
          //   time: {
          //     displayFormats: {
          //       quarter: 'H:mm'
          //     }
          //   }
          // }],
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: { display: true },
        title: {
          display: false,
          text: 'Hourly Gross'
        }
      }
    });
  });

}); //END
