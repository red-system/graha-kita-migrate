// function readURL(input) {
//   if (input.files && input.files[0]) {
//     var reader = new FileReader();
//     reader.onload = function(e) {
//       $('#imgScr').attr('src', e.target.result);
//     }
//     reader.readAsDataURL(input.files[0]);
//   }
// }
// $("#imgBtn").change(function() {
//   readURL(this);
// });

$(document).ready(function (e) {
  function ID(ini, ktg, grp, mrk) {
    var href = $('#data-back').data('href_create_id');
    var init = ini;
    var _token = $('[name="_token"]').val();

    var data_send = {
      ktg_kode  : ktg,
      grp_kode  : grp,
      mrk_kode  : mrk,
      _token    : _token,
    };

    $.ajax({
      url: href,
      type: 'POST',
      data: data_send,
      success:function(data) {
        // var kode = init.parents('div').siblings('div.kode').children('div.col-md-9').children('input[name="brg_kode"]').val(data.kode);
        $('input[name="brg_kode"]').val(data.kode);
        $('input[name="brg_barcode"]').val(data.barcode);
      }
    });
  }

  $('select[name="ktg_kode"]').change(function() {
    var ini = $(this);
    var ktg = ini.val();
    var grp = ini.parents('div').siblings('div.grp').children('div.col-md-9').find(':selected').val();
    var mrk = ini.parents('div').siblings('div.mrk').children('div.col-md-9').find(':selected').val();
    ID(ini, ktg, grp, mrk);

    // var com = 'Ktg : '+ktg+' Grp : '+grp+' Mrk : '+mrk;
    // console.log(com);
    // var kode = ini.parents('div').siblings('div.kode').children('div.col-md-9').children('input[name="brg_kode"]').val(com).trigger('change');
    // barcode();
  });

  $('select[name="grp_kode"]').change(function() {
    var ini = $(this);
    var ktg = ini.parents('div').siblings('div.ktg').children('div.col-md-9').find(':selected').val();
    var grp = ini.val();
    var mrk = ini.parents('div').siblings('div.mrk').children('div.col-md-9').find(':selected').val();
    ID(ini, ktg, grp, mrk);

    // console.log('Ktg : '+ktg+' Grp : '+grp+' Mrk : '+mrk);
  });

  $('select[name="mrk_kode"]').change(function() {
    var ini = $(this);
    var ktg = ini.parents('div').siblings('div.ktg').children('div.col-md-9').find(':selected').val();
    var grp = ini.parents('div').siblings('div.grp').children('div.col-md-9').find(':selected').val();
    var mrk = ini.val();
    ID(ini, ktg, grp, mrk);

    // console.log('Ktg : '+ktg+' Grp : '+grp+' Mrk : '+mrk);
  });


  // Function to preview image after validation
  $(function() {
    $("#imgBtn").change(function() {
      $("#message").empty(); // To remove the previous error message
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#imgScr').attr('src', "{{asset('img/icon/no_image.png')}}");
        $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
        return false;
      }
      else
      {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoaded(e) {
    $('#imgScr').attr('src', e.target.result);
    $('#imgScr').attr('width', '100px');
    $('#imgScr').attr('height', '100px');
  };

  $(function() {
    $("#imgBtnEdit").change(function() {
      $("#messageEdit").empty(); // To remove the previous error message
      var file = this.files[0];
      var imagefile = file.type;
      var match= ["image/jpeg","image/png","image/jpg"];
      if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
      {
        $('#imgScrEdit').attr('src', "{{asset('img/icon/no_image.png')}}");
        $("#messageEdit").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
        return false;
      }
      else
      {
        var reader = new FileReader();
        reader.onload = imageIsLoadedEdit;
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
  function imageIsLoadedEdit(e) {
    $('#imgScrEdit').attr('src', e.target.result);
    $('#imgScrEdit').attr('width', '100px');
    $('#imgScrEdit').attr('height', '100px');
  };

  var table = $('#sample_x').DataTable();
  $('#sample_x').on( 'draw.dt', function () {
    $('.btn-print-barcode').click(function() {
      var href = $(this).data('href');
      window.open(href);
    });

    $('.btn-edit-stok').click(function() {
      var href = $(this).data('href');
      var href2 = href.replace("edit", "get");
      var l = window.location;
      var base_url = l.protocol + "//" + l.host + "/";

      $.ajax({
        url: href,
        success: function(data) {
          $.each(data.field, function(field, value) {
            if (field == 'brg_kode') {
              $('#modal-edit-stok [name="brg_kode"]').val(value);
            }
            else if (field == 'spl_kode') {
              $('#modal-edit-stok [name="spl_kode"]').val(value);
            }
            else {
              $('#modal-edit-stok [name="gdg_kode"]').val(value);
            }
          });
          $('#modal-edit-stok form').attr('action', data.action);
          $('#modal-edit-stok').modal('show');
        }
      });

      $('#sample_2').DataTable({
        destroy : true,
        ajax : {
          url : href2,
          dataSrc : ''
        },
        columns: [
          { data: "brg_no_seri" },
          { data: "QOH" },
          { data: "Titipan" },
          { data: "gdg_nama" },
          { data: "spl_nama" },
          { data: null, render: function ( data, type, row ) {
            return '<div class="btn-group-xs"> <a class="btn btn-danger btn-delete-new btn-icon" type="button" data-id="' + data.stk_kode + '" data-href="'+base_url+'barang/stok/delete/'+data.stk_kode+'"><i class="icon-trash"></i> Delete</a> <a class="btn btn-info btn-icon btn-stok-sample" type="button" data-stk_kode="' + data.stk_kode + '" data-brg_no_seri="' + data.brg_no_seri + '" data-brg_kode="' + data.brg_kode + '" data-gdg_kode="' + data.gdg_kode + '" data-spl_kode="' + data.spl_kode + '" data-qoh="' + data.QOH + '"><i class="icon-plus"></i> Stok Sample</a> <a class="btn btn-info btn-icon btn-stok-reject" type="button" data-stk_kode="' + data.stk_kode + '" data-qoh="' + data.QOH + '"><i class="icon-plus"></i> Stok Reject</a></div>';
            }
          },
        ]
      });
      $('#sample_2').on( 'draw.dt', function () {
        $('.btn-stok-sample').click(function() {
          // var ini = $(this);
          // $('#modal-stok_sample').data('stok', ini).modal('show');
          var stk_kode = $(this).data('stk_kode');
          var brg_no_seri = $(this).data('brg_no_seri');
          var brg_kode = $(this).data('brg_kode');
          var gdg_kode = $(this).data('gdg_kode');
          var spl_kode = $(this).data('spl_kode');

          $('#modal-stok_sample').modal('show');
          $('[name="stk_kode_sample"]').val(stk_kode);
          $('[name="brg_no_seri_sample"]').val(brg_no_seri);
          $('[name="brg_kode_sample"]').val(brg_kode);
          $('[name="gdg_kode_sample"]').val(gdg_kode);
          $('[name="spl_kode_sample"]').val(spl_kode);

          var row_qty = $('input[name="qty_sample"]');
          var row_qty_val = $(this).data('qoh');

          if (row_qty_val != null) {
            row_qty.off('input');
            row_qty.on('input', function () {
              var value = $(this).val();
              $(this).val(Math.max(Math.min(value, row_qty_val), 0));
              // if ((value !== '') && (value.indexOf('.') === -1)) {
                // }
              });
            }
        });

        $('.btn-stok-reject').click(function() {
          var stk_kode = $(this).data('stk_kode');

          $('#modal-stok_reject').modal('show');
          $('[name="stk_kode_reject"]').val(stk_kode);

          var row_qty = $('input[name="qty_reject"]');
          var row_qty_val = $(this).data('qoh');

          if (row_qty_val != null) {
            row_qty.off('input');
            row_qty.on('input', function () {
              var value = $(this).val();
              $(this).val(Math.max(Math.min(value, row_qty_val), 0));
              // if ((value !== '') && (value.indexOf('.') === -1)) {
              // }
            });
          }
        });

        $('.btn-delete-new').click(function() {
          var ini = $(this);
          swal({
            title: 'Perhatian',
            text: 'Yakin ? Data akan dihapus permanen',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText:'Yakin',
            cancelButtonText:'Batal'
          }).then((result) => {
            if (result.value) {
              $.ajax({
                url: ini.data('href'),
                success: function(data) {
                  ini.parents('tr').fadeOut('fast', function(){
                    ini.parents('tr').remove();
                  });
                }
              });

              swal(
                'Deleted!',
                'Data berhasil dihapus.',
                'success'
              )
            } else if (
              // Read more about handling dismissals
              result.dismiss === swal.DismissReason.cancel
            ) {
              swal(
                'Cancelled',
                'Data batal dihapus',
                'error'
              )
            }
          })
        });
      });
    });

    $('.btn-delete-new').click(function() {
      var ini = $(this);
      swal({
        title: 'Perhatian',
        text: 'Yakin ? Data akan dihapus permanen',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText:'Yakin',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: ini.data('href'),
            success: function(data) {
              ini.parents('tr').fadeOut('fast', function(){
                ini.parents('tr').remove();
              });
            }
          });

          swal(
            'Deleted!',
            'Data berhasil dihapus.',
            'success'
          )
        } else if (
          // Read more about handling dismissals
          result.dismiss === swal.DismissReason.cancel
        ) {
          swal(
            'Cancelled',
            'Data batal dihapus',
            'error'
          )
        }
      })
    });

    $('.btn-edit').click(function() {
        var href = $(this).data('href');
        $('#modal-edit [id="imgBtnEdit"]').val('');
        $.ajax({
            url: href,
            success: function(data) {
                $.each(data.field, function(field, value) {
                    if($('#modal-edit [name="'+field+'"]').is('select')) {
                        //$('#modal-edit [name="'+field+'"]').val(value);
                        // $('#modal-edit [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                        $('#modal-edit [name="'+field+'"]').selectpicker('val', value);
                    }
                    else if ($('#modal-edit [name="'+field+'"]').is(':checkbox')) {
                      if (value.trim() == 'Aktif') {
                        $('#modal-edit [name="'+field+'"]').attr('checked', true);
                      }
                    }
                    else if (field == 'brg_product_img') {
                      if ($('#modal-edit [id="imgScrEdit"]').is('img')) {
                        if (value != null ) {
                          var l = window.location;
                          var base_url = l.protocol + "//" + l.host + "/";
                          // console.log(base_url);
                          $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + value);
                        }
                        else {
                          var l = window.location;
                          var base_url = l.protocol + "//" + l.host + "/";
                          $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + "img/icon/no_image.png");
                        }
                      }
                    }
                    else {
                        $('#modal-edit [name="'+field+'"]').val(value);
                    }
                });
                $('#modal-edit form').attr('action', data.action);
                $('#modal-edit').modal('show');
            }
        });
    });

    $('.btn-stok-titipan').click(function () {
      var href = $(this).data('href');
      var href2 = href.replace("edit", "getStokTitipan");
      var l = window.location;
      var base_url = l.protocol + "//" + l.host + "/";

      $.ajax({
        url: href,
        success: function (data) {
          $.each(data.field, function (field, value) {
            if (field == 'brg_kode') {
              $('#modal-stok-titipan [name="brg_kode"]').val(value);
            }
            else {
              $('#modal-stok-titipan [name="gdg_kode"]').val(value);
            }
          });
          $('#modal-stok-titipan form').attr('action', data.action);
          $('#modal-stok-titipan').modal('show');
        }
      });

      $('#stok_titipan').DataTable({
        destroy: true,
        ajax: {
          url: href2,
          dataSrc: ''
        },
        columns: [
          { data: "pt_no_faktur" },
          { data: "cus_nama" },
          { data: "brg_kode" },
          { data: "nama_barang" },
          { data: "brg_no_seri" },
          { data: "qty" },
          { data: "terkirim" },

          // { data: null, render: function ( data, type, row ) {
          //   return '<div class="btn-group-xs"> <a class="btn btn-danger btn-delete-new btn-icon" type="button" data-id="' + data.stk_kode + '" data-href="'+base_url+'barang/stok/delete/'+data.stk_kode+'"><i class="icon-trash"></i> Delete</a> <a class="btn btn-info btn-icon btn-stok-sample" type="button" data-stk_kode="' + data.stk_kode + '" data-brg_no_seri="' + data.brg_no_seri + '" data-brg_kode="' + data.brg_kode + '" data-gdg_kode="' + data.gdg_kode + '" data-spl_kode="' + data.spl_kode + '" data-qoh="' + data.QOH + '"><i class="icon-plus"></i> Stok Sample</a> <a class="btn btn-info btn-icon btn-stok-reject" type="button" data-stk_kode="' + data.stk_kode + '" data-qoh="' + data.QOH + '"><i class="icon-plus"></i> Stok Reject</a></div>';
          //   }
          // },
        ]
      });

      $('#stok_titipan').on('draw.dt', function () {
        $('.btn-stok-sample').click(function () {
          // var ini = $(this);
          // $('#modal-stok_sample').data('stok', ini).modal('show');
          var stk_kode = $(this).data('stk_kode');
          var brg_no_seri = $(this).data('brg_no_seri');
          var brg_kode = $(this).data('brg_kode');
          var gdg_kode = $(this).data('gdg_kode');
          var spl_kode = $(this).data('spl_kode');

          $('#modal-stok_sample').modal('show');
          $('[name="stk_kode_sample"]').val(stk_kode);
          $('[name="brg_no_seri_sample"]').val(brg_no_seri);
          $('[name="brg_kode_sample"]').val(brg_kode);
          $('[name="gdg_kode_sample"]').val(gdg_kode);
          $('[name="spl_kode_sample"]').val(spl_kode);

          var row_qty = $('input[name="qty_sample"]');
          var row_qty_val = $(this).data('qoh');

          if (row_qty_val != null) {
            row_qty.off('input');
            row_qty.on('input', function () {
              var value = $(this).val();
              $(this).val(Math.max(Math.min(value, row_qty_val), 0));
              // if ((value !== '') && (value.indexOf('.') === -1)) {
              // }
            });
          }
        });

        $('.btn-stok-reject').click(function () {
          var stk_kode = $(this).data('stk_kode');

          $('#modal-stok_reject').modal('show');
          $('[name="stk_kode_reject"]').val(stk_kode);

          var row_qty = $('input[name="qty_reject"]');
          var row_qty_val = $(this).data('qoh');

          if (row_qty_val != null) {
            row_qty.off('input');
            row_qty.on('input', function () {
              var value = $(this).val();
              $(this).val(Math.max(Math.min(value, row_qty_val), 0));
              // if ((value !== '') && (value.indexOf('.') === -1)) {
              // }
            });
          }
        });

        $('.btn-delete-new').click(function () {
          var ini = $(this);
          swal({
            title: 'Perhatian',
            text: 'Yakin ? Data akan dihapus permanen',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yakin',
            cancelButtonText: 'Batal'
          }).then((result) => {
            if (result.value) {
              $.ajax({
                url: ini.data('href'),
                success: function (data) {
                  ini.parents('tr').fadeOut('fast', function () {
                    ini.parents('tr').remove();
                  });
                }
              });

              swal(
                'Deleted!',
                'Data berhasil dihapus.',
                'success'
              )
            } else if (
              // Read more about handling dismissals
              result.dismiss === swal.DismissReason.cancel
            ) {
              swal(
                'Cancelled',
                'Data batal dihapus',
                'error'
              )
            }
          })
        });
      });
    }); 

  });

  $('.btn-modal-tambah-barang').click(function() {
    $('#modal-tambah').modal('show');
    $("#form-tambah")[0].reset();
  });

  $('.form-send-barang').submit(function (e) {
    e.preventDefault();

    var form = $('.form-send-barang');
    var formData = new FormData(form[0]);
    $.ajax({
      url: form.attr('action'),
      type: 'POST',
      data: formData,
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: function (data) {
        $('#modal-tambah').modal('hide');

        swal({
          title: 'Perhatian',
          text: 'Data Berhasil Disimpan',
          type: 'success'
        });
        table.ajax.reload();

        // if(data.redirect) {
        //     window.location.href = data.redirect;
        // }
        // window.location.href = data.redirect;

        $("#form-tambah")[0].reset();
      },
      error: function(request, status, error) {
        swal({
          title: 'Perhatian',
          text: 'Data Gagal Disimpan!',
          type: 'error'
        });

        var json = JSON.parse(request.responseText);
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.each(json.errors, function(key, value) {
          $('.form-send-barang [name="'+key+'"]').parents('.form-group').addClass('has-error');
          $('.form-send-barang [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
        });
      },
    });
    return false;
  });

  $('.form-send-barang-edit').submit(function (e) {
    e.preventDefault();

    var form = $('.form-send-barang-edit');
    var formData = new FormData(form[0]);
    $.ajax({
      url: form.attr('action'),
      type: 'POST',
      data: formData,
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: function (data) {
        $('#modal-edit').modal('hide');

        swal({
          title: 'Perhatian',
          text: 'Data Berhasil Disimpan',
          type: 'success'
        });
        table.ajax.reload();

        // if(data.redirect) {
        //     window.location.href = data.redirect;
        // }
        // window.location.href = data.redirect;
      },
      error: function(request, status, error) {
        swal({
          title: 'Perhatian',
          text: 'Data Gagal Disimpan!',
          type: 'error'
        });

        var json = JSON.parse(request.responseText);
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.each(json.errors, function(key, value) {
          $('.form-send-barang [name="'+key+'"]').parents('.form-group').addClass('has-error');
          $('.form-send-barang [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
        });
      },
    });
    return false;
  });

  $('.btn-edit-stok').click(function() {
    var href = $(this).data('href');
    var href2 = href.replace("edit", "get");
    var l = window.location;
    var base_url = l.protocol + "//" + l.host + "/";

    $.ajax({
      url: href,
      success: function(data) {
        $.each(data.field, function(field, value) {
          if (field == 'brg_kode') {
            $('#modal-edit-stok [name="brg_kode"]').val(value);
          }
          else {
            $('#modal-edit-stok [name="gdg_kode"]').val(value);
          }
        });
        $('#modal-edit-stok form').attr('action', data.action);
        $('#modal-edit-stok').modal('show');
      }
    });

    $('#sample_2').DataTable({
      destroy : true,
      ajax : {
        url : href2,
        dataSrc : ''
      },
      columns: [
        { data: "brg_no_seri" },
        { data: "QOH" },
        { data: "Titipan" },
        { data: "gdg_nama" },
        { data: "spl_nama" },
        { data: null, render: function ( data, type, row ) {
          return '<div class="btn-group-xs"> <a class="btn btn-danger btn-delete-new btn-icon" type="button" data-id="' + data.stk_kode + '" data-href="'+base_url+'barang/stok/delete/'+data.stk_kode+'"><i class="icon-trash"></i> Delete</a> <a class="btn btn-info btn-icon btn-stok-sample" type="button" data-stk_kode="' + data.stk_kode + '" data-brg_no_seri="' + data.brg_no_seri + '" data-brg_kode="' + data.brg_kode + '" data-gdg_kode="' + data.gdg_kode + '" data-spl_kode="' + data.spl_kode + '" data-qoh="' + data.QOH + '"><i class="icon-plus"></i> Stok Sample</a> <a class="btn btn-info btn-icon btn-stok-reject" type="button" data-stk_kode="' + data.stk_kode + '" data-qoh="' + data.QOH + '"><i class="icon-plus"></i> Stok Reject</a></div>';
          }
        },
      ]
    });
    $('#sample_2').on( 'draw.dt', function () {
      $('.btn-stok-sample').click(function() {
        // var ini = $(this);
        // $('#modal-stok_sample').data('stok', ini).modal('show');
        var stk_kode = $(this).data('stk_kode');
        var brg_no_seri = $(this).data('brg_no_seri');
        var brg_kode = $(this).data('brg_kode');
        var gdg_kode = $(this).data('gdg_kode');
        var spl_kode = $(this).data('spl_kode');

        $('#modal-stok_sample').modal('show');
        $('[name="stk_kode_sample"]').val(stk_kode);
        $('[name="brg_no_seri_sample"]').val(brg_no_seri);
        $('[name="brg_kode_sample"]').val(brg_kode);
        $('[name="gdg_kode_sample"]').val(gdg_kode);
        $('[name="spl_kode_sample"]').val(spl_kode);

        var row_qty = $('input[name="qty_sample"]');
        var row_qty_val = $(this).data('qoh');

        if (row_qty_val != null) {
          row_qty.off('input');
          row_qty.on('input', function () {
            var value = $(this).val();
            $(this).val(Math.max(Math.min(value, row_qty_val), 0));
            // if ((value !== '') && (value.indexOf('.') === -1)) {
              // }
            });
          }
      });

      $('.btn-stok-reject').click(function() {
        var stk_kode = $(this).data('stk_kode');

        $('#modal-stok_reject').modal('show');
        $('[name="stk_kode_reject"]').val(stk_kode);

        var row_qty = $('input[name="qty_reject"]');
        var row_qty_val = $(this).data('qoh');

        if (row_qty_val != null) {
          row_qty.off('input');
          row_qty.on('input', function () {
            var value = $(this).val();
            $(this).val(Math.max(Math.min(value, row_qty_val), 0));
            // if ((value !== '') && (value.indexOf('.') === -1)) {
              // }
            });
          }
      });

      $('.btn-delete-new').click(function() {
        var ini = $(this);
        swal({
          title: 'Perhatian',
          text: 'Yakin ? Data akan dihapus permanen',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText:'Yakin',
          cancelButtonText:'Batal'
        }).then((result) => {
          if (result.value) {
            $.ajax({
              url: ini.data('href'),
              success: function(data) {
                ini.parents('tr').fadeOut('fast', function(){
                  ini.parents('tr').remove();
                });
              }
            });

            swal(
              'Deleted!',
              'Data berhasil dihapus.',
              'success'
            )
          } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
          ) {
            swal(
              'Cancelled',
              'Data batal dihapus',
              'error'
            )
          }
        })
      });
    });
  });

  // function barcode() {
  //   $('input[name="brg_barcode"]').val();
  //
  //   $('input[name="brg_kode"]').change(function() {
  //   // $('input[name="brg_kode"]').keyup(function() {
  //     var brg_kode = $(this).val();
  //     var array = [];
  //     var replace = null;
  //     var barcode = null;
  //
  //     var split = brg_kode.split(".");
  //     for (var i = 0; i < split.length; i++) {
  //       if (i != split.length-1) {
  //         replace = split[i].replace(/0/g, "");
  //       }
  //       else {
  //         replace = split[i];
  //       }
  //       array.push(replace);
  //     }
  //     barcode = array.join('');
  //
  //     $('input[name="brg_barcode"]').val(barcode);
  //   });
  // }
  // barcode();
});

$('select[name="group_type"]').on('change', function() {
  var stateID = $(this).val();
  if(stateID) {
    $.ajax({
      url: '/barang/get/'+stateID,
      type: "GET",
      dataType: "json",
      success:function(data) {
        $('select[name="type"]').empty();
        $.each(data, function(key, value) {
          $('select[name="type"]').append('<option value="'+ key +'">'+ value +'</option>');
        });
      }
    });
  }else{
    $('select[name="type"]').empty();
  }
});

$('.stk_btn_save').click(function() {
  var url = $('#modal-edit-stok form').attr('action');

  var field = ['brg_kode', 'brg_no_seri', 'stok', 'stk_hpp', 'gdg_kode', 'spl_kode'];

  for (var i = 0; i < field.length; i++) {
    var x = document.forms["stok-form"][field[i]].value;
    if (x == "") {
      alert("Tolong Isi Data Yang Kosong Terlebih Dahulu");
      return false;
    }
  }

  var brg_kode = $('#stok-brg_kode').val();
  var brg_no_seri = $('#stok-brg_no_seri').val();
  var stok = $('#stok-stok').val();
  var stk_hpp = $('#stok-stk_hpp').val();
  var gdg_kode = $('#stok-gdg_kode').val();
  var spl_kode = $('#stok-spl_kode').val();
  var _token = $('[name="_token"]').val();

  var data_send = {
    brg_kode:brg_kode,
    brg_no_seri:brg_no_seri,
    stok:stok,
    stk_hpp:stk_hpp,
    gdg_kode:gdg_kode,
    spl_kode:spl_kode,
    _token: _token,
  };

  $.ajax({
    url: url,
    type: 'POST',
    data: data_send,
    success:function(data) {
      $('#modal-edit-stok').modal('hide');
      $('#modal-stok_sample').modal('hide');
      const toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      toast({
        type: 'success',
        title: 'Item telah Ditambahkan'
      })

      $('#sample_x').DataTable().ajax.reload();
    }
  });
});

$('.form-stok-sample').submit(function(e) {
  e.preventDefault();
  var ini = $(this);
  $.ajax({
    url: ini.attr('action'),
    type: ini.attr('method'),
    data: ini.serialize(),
    success: function(data) {
      $('#modal-edit-stok').modal('hide');
      $('#modal-stok_sample').modal('hide');
      $('#modal-stok_reject').modal('hide');
      const toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      toast({
        type: 'success',
        title: 'Item telah Ditambahkan'
      })
    },
    error: function(request, status, error) {
      swal({
        title: 'Perhatian',
        text: 'Data Gagal Disimpan!',
        type: 'error'
      });
    }
  });
  return false;
});
