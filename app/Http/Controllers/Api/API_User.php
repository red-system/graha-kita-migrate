<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\mUser;
use Validator;

class API_User extends Controller
{
    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            $user = Auth::user();
            $user->Karyawan;
            $success =  $user->createToken('Grahakita')->accessToken;
            return response()->json(['success' => true, 'token' => $success, 'user' => $user], $this->successStatus);
        }
        else{
          return response()->json(['success' => false, 'Message' => 'Pastikan Username dan Password Benar'], $this->successStatus);
          // return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            // 'name' => 'required',
            // 'email' => 'required|email',
            // 'password' => 'required',
            // 'c_password' => 'required|same:password',
            'username' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = mUser::create($input);
        $success['token'] =  $user->createToken('Grahakita')->accessToken;
        $success['username'] =  $user->username;
        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function details(){
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function logout(){
        $user = Auth::logout();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
