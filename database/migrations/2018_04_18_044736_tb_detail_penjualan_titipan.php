<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDetailPenjualanTitipan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_detail_penjualan_titipan', function (Blueprint $table) {
         $table->increments('detail_pt_kode');
         $table->integer('pt_no_faktur')->nullable();
         $table->integer('gudang')->nullable();
         $table->integer('brg_kode')->nullable();
         $table->string('nama_barang')->nullable();
         $table->integer('satuan')->nullable();
         $table->integer('harga_jual')->nullable();
         $table->integer('brg_hpp')->nullable();
         $table->float('disc')->nullable();
         $table->integer('disc_nom')->nullable();
         $table->integer('harga_net')->nullable();
         $table->integer('qty')->nullable();
         $table->integer('terkirim')->nullable();
         $table->integer('total')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_detail_penjualan_titipan');
     }
}
