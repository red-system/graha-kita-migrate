@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />   
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('[name="start_date"],[name="end_date"]').datepicker()
            .on('changeDate', function(ev){                 
                $('[name="start_date"],[name="end_date"]').datepicker('hide');
            });
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="col-md-12">
                            <div class="portlet light ">
                                <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                    Pilih Periode
                                </a>
                                <a style="font-size: 11px" type="button" class="btn btn-danger" href="{{route('printkartuPiutangPelanggan',['kode'=>$user_id, 'start_date'=>$start_date, 'end_date'=>$end_date, 'coa'=>$coa, 'tipe'=>'print'])}}" target="_blank">
                                    <span><i class="fa fa-print"></i></span> Print
                                </a>
                                <a style="font-size: 11px" type="button" class="btn btn-danger" href="{{route('kartuPiutangExcel',['kode'=>$user_id, 'start_date'=>$start_date, 'end_date'=>$end_date, 'coa'=>$coa])}}" target="_blank">
                                    <span><i class="fa fa-print"></i></span> Excel
                                </a>
                            </div>
                        </div>
                        <h4 style="font-family: Tahoma">{{$title}}</h4>
                        <h5 style="font-family: Tahoma">{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</h5>
                        @if($id_tipe==$kodeCust)
                        @foreach($dataList as $user)
                                               
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;">{{$kodeCust.$user->cus_kode}} - {{ $user->cus_nama }}</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;"> {{$coa}} - {{$kode_coa[$coa]}}</td>
                                </tr>
                                <?php 
                                    // use use App\Models\mJurnalUmum;
                                    // $data2   = mJurnalUmum::where('id_pel',$kodeSupplier.$spl->spl_kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->get();
                                ?>
                                <tr class="">
                                    <td style="font-size: 12px" width="10"> No </td>
                                    <td style="font-size: 12px"> Tanggal </td>
                                    <td style="font-size: 12px"> No Invoice</td>
                                    <td style="font-size: 12px"> Keterangan </td>
                                    <td style="font-size: 12px"> Debet </td>
                                    <td style="font-size: 12px"> Kredit </td>
                                    <td style="font-size: 12px"> Saldo </td>
                                </tr>
                                <?php $saldo = $begining_balance[$kodeCust.$user->cus_kode];?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> 1</td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px"> begining balance </td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($begining_balance[$kodeCust.$user->cus_kode],2) }} </td>
                                    <td style="font-size: 11px;text-align: right;"></td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($saldo,2) }} </td>
                                </tr>
                                <?php $no=2;$ttl_kredit=0;$ttl_debet=$begining_balance[$kodeCust.$user->cus_kode];?>
                                @if($jml_detail[$kodeCust.$user->cus_kode]>0)
                                @foreach($trs[$kodeCust.$user->cus_kode] as $spl_trs)
                                <?php 
                                    $saldo = $saldo+$spl_trs->trs_debet-$spl_trs->trs_kredit;
                                    $ttl_debet+=$spl_trs->trs_debet;
                                    $ttl_kredit+=$spl_trs->trs_kredit
                                ?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> {{ $no++ }}. </td>
                                    <td style="font-size: 11px"> {{ date('d M Y', strtotime($spl_trs->jmu_tanggal)) }} </td>
                                    <td style="font-size: 11px"> {{ $spl_trs->no_invoice }} </td>
                                    <td style="font-size: 11px"> {{ $spl_trs->jmu_keterangan }} </td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($spl_trs->trs_debet,2) }} </td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($spl_trs->trs_kredit,2) }} </td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($saldo,2) }} </td>                                
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td style="font-size: 12px;font-weight: bold;" align="left" colspan="4">Sub Total Account</td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left">{{number_format($ttl_debet,2)}}</td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left">{{number_format($ttl_kredit,2)}}</td>
                                </tr>
                                
                            </tbody>
                        </table>
                        
                        @endforeach
                        @elseif($id_tipe==$kodeKry)
                        @foreach($dataList as $user)
                        
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;">{{$kodeKry.$user->kry_kode}} - {{ $user->kry_nama }}</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;"> {{$coa}} - {{$kode_coa[$coa]}}</td>
                                </tr>
                                <?php 
                                    // use use App\Models\mJurnalUmum;
                                    // $data2   = mJurnalUmum::where('id_pel',$kodeSupplier.$spl->spl_kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->get();
                                ?>
                                <tr class="">
                                    <td style="font-size: 12px" width="10"> No </td>
                                    <td style="font-size: 12px"> Tanggal </td>
                                    <td style="font-size: 12px"> No Invoice</td>
                                    <td style="font-size: 12px"> Keterangan </td>
                                    <td style="font-size: 12px"> Debet </td>
                                    <td style="font-size: 12px"> Kredit </td>
                                    <td style="font-size: 12px"> Saldo </td>
                                </tr>
                                <?php $saldo = $begining_balance[$kodeKry.$user->kry_kode];?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> 1</td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px"> begining balance </td>
                                    <td style="font-size: 11px;text-align: right;">{{ number_format($begining_balance[$kodeKry.$user->kry_kode],2) }}</td>
                                    <td style="font-size: 11px;text-align: right;"></td>
                                    <td style="font-size: 11px;text-align: right;">{{ number_format($saldo,2) }} </td>
                                </tr>
                                <?php $no=2;$ttl_kredit=0;$ttl_debet=$begining_balance[$kodeKry.$user->kry_kode];?>
                                @if($jml_detail[$kodeKry.$user->kry_kode]>0)
                                @foreach($trs[$kodeKry.$user->kry_kode] as $spl_trs)
                                <?php 
                                    $saldo = $saldo+$spl_trs->trs_debet-$spl_trs->trs_kredit;
                                    $ttl_debet+=$spl_trs->trs_debet;
                                    $ttl_kredit+=$spl_trs->trs_kredit
                                ?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> {{ $no++ }}. </td>
                                    <td style="font-size: 11px"> {{ date('d M Y', strtotime($spl_trs->jmu_tanggal)) }} </td>
                                    <td style="font-size: 11px"> {{ $spl_trs->no_invoice }} </td>
                                    <td style="font-size: 11px"> {{ $spl_trs->jmu_keterangan }} </td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($spl_trs->trs_debet,2) }} </td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($spl_trs->trs_kredit,2) }} </td>
                                    <td style="font-size: 11px;text-align: right;"> {{ number_format($saldo,2) }} </td>                                
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td style="font-size: 12px;font-weight: bold;" align="left" colspan="4">Sub Total Account</td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left">{{number_format($ttl_debet,2)}}</td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left">{{number_format($ttl_kredit,2)}}</td>
                                </tr>
                                
                            </tbody>
                        </table>
                        
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-clock"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeKartuPiutang') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 7px">Kode COA</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="coa">
                                    @foreach($kode_coa as $key=>$value)
                                    <option value="{{$key}}" <?php if($key==$coa) echo 'selected';?>>{{$value}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            @if($id_tipe==$kodeCust)
                            <div class="col-md-3">
                                <label style="padding-top: 7px">Customer</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="user">
                                    <option value="all"  <?php if($user_id=='all') echo 'selected';?>>All Customer</option>
                                    @foreach($customer as $cust)
                                    <option value="{{$cust->cus_kode}}" <?php if($user_id==$cust->cus_kode) echo 'selected';?>>{{$cust->cus_nama}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                            @elseif($id_tipe==$kodeKry)
                            <div class="col-md-3">
                                <label style="padding-top: 7px">Karyawan</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="user">
                                    <option value="all"  <?php if($user_id=='all') echo 'selected';?>>All Karyawan</option>
                                    @foreach($karyawan as $kry)
                                    <option value="{{$kry->kry_kode}}" <?php if($user_id==$kry->kry_kode) echo 'selected';?>>{{$kry->kry_nama}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="{{$end_date}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row col-md-offset-3">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
