<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mHutangCek extends Model
{
    //public $incrementing = false;
  	protected $table = 'tb_hutang_cek';
  	protected $primaryKey = 'id_hutang_cek';
  	//public $timestamps = false;

  	protected $guarded=[];

  

  	public function dataList() 
  	{
   	 	return static::all();
  	}

  	public function suppliers(){
	    return $this->belongsTo(mSupplier::class,'cek_untuk','spl_kode');
	 }
}
