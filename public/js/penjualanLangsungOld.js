$(document).ready(function () {
    //penjualan langsung
    $('[name="cus_kode"]').change(function () {
        var cus_kode = $(this).val();
        var cus_alamat = $(this).find(':selected').attr('data-alamat');
        var cus_tipe = $(this).find(':selected').attr('data-cus-tipe');

        set_customer(cus_kode, cus_alamat, cus_tipe);
    });

    $('[name="pl_transaksi"]').change(function () {
        var pl_transaksi = $(this).val();
        if (pl_transaksi === 'kredit') {
            $('.form-kredit').removeClass('hide');
        } else {
            $('.form-kredit').addClass('hide');
        }
    });

    $('.btn-pilih-customer').click(function() {
        var cus_kode = $(this).data('cus-kode');
        var cus_alamat = $(this).data('cus-alamat');
        var cus_tipe = $(this).data('cus-tipe');
        $('[name="cus_kode"]').val(cus_kode).trigger('change');;
        set_customer(cus_kode, cus_alamat, cus_tipe);
        $('#modal-customer').modal('hide');
    });

    $('.btn-row-plus').click(function() {
        var cus_kode = $('[name="cus_kode"]').val();
        if(cus_kode == '') {
            swal({
                title: 'Perhatian',
                text: 'Diharuskan Pilih Customer Terlebih Dahulu',
                type: 'warning'
            });
        } else {
            var row = $('.table-row-data tbody').html();
            $('.table-all-data tbody').append(row);
            select_row_barang();
            btn_row_delete();
            enter_disc();
            select_harga();
            set_qty();
            detail_third_party();
        }
    });

    $('[name="pl_disc"], [name="pl_ppn"], [name="pl_ongkos_angkut"]').keyup(function() {
        kalkulasi_subtotal();
        kalkulasi_grandtotal();
    });

    function btn_row_delete() {
        $('.btn-row-delete').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_subtotal();
            kalkulasi_grandtotal();
        });

    }

    function select_row_barang() {
        var route_penjualan_langsung_barang_row = $('#data-back').data('route-penjualan-langsung-barang-row');
        $('select[name="brg_kode[]"], select[name="brg_no_seri[]"]').change(function() {
            var brg_kode = $(this).val();
            var cus_kode = $('[name="cus_kode"]').val();
            var token = $('#data-back').data('form-token');
            var ini = $(this);
            var data_send = {
                brg_kode: brg_kode,
                cus_kode: cus_kode,
                _token: token
            };

            var nama = ini.parents('td').siblings('td.nama').children('input[name="nama[]"]');
            var satuan = ini.parents('td').siblings('td.satuan').children('input[name="satuan[]"]');
            var harga_jual = ini.parents('td').siblings('td.harga_jual').children('select[name="harga_jual[]"]');


            nama.val('');
            satuan.val('');
            harga_jual.html('');

            $.ajax({
                url: route_penjualan_langsung_barang_row,
                type: 'POST',
                data: data_send,
                success: function(data) {
                    nama.val(data.nama);
                    satuan.val(data.satuan);
                    harga_jual.html(data.harga_jual);
                }
            });
        });
    }

    function enter_disc() {
        $('[name="disc[]"]').keyup(function() {
            var form_disc_nom = $(this).parents('td').siblings('td.disc_nom').children('input');
            var form_harga_net = $(this).parents('td').siblings('td.harga_net').children('input');
            var form_total = $(this).parents('td').siblings('td.total').children('input');

            form_disc_nom.val(0);
            form_harga_net.val(0);

            var disc = $(this).val();
            var qty = $(this).parents('td').siblings('td.qty').children('input').val();
            var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('select').val();
            var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
            var harga_net = parseInt(harga_jual) - disc_nom;
            var total = parseInt(harga_net) * parseInt(qty);

            form_disc_nom.val(disc_nom);
            form_harga_net.val(harga_net);
            form_total.val(total);

            kalkulasi_subtotal();
            kalkulasi_grandtotal();

        });
    }

    function select_harga() {
        $('[name="harga_jual[]"]').change(function() {
            var disc = $(this).parents('td').siblings('td.disc').children('input').val();
            var qty = $(this).parents('td').siblings('td.qty').children('input').val();
            var harga_jual = $(this).val();
            var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
            var harga_net = (parseInt(harga_jual) - disc_nom) * parseInt(qty);
            $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
            $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net);
            $(this).parents('td').siblings('td.total').children('input').val(harga_net);

            kalkulasi_subtotal();
            kalkulasi_grandtotal();
        });
    }

    function set_qty() {
        $('[name="qty[]"]').keyup(function() {
            var qty = $(this).val();
            var harga_net = $(this).parents('td').siblings('td.harga_net').children('input').val();
            var total = parseInt(qty) * parseInt(harga_net);
            $(this).parents('td').siblings('td.total').children('input').val(total);


            kalkulasi_subtotal();
            kalkulasi_grandtotal();
        });
    }

    function set_customer(cus_kode, cus_alamat, cus_tipe) {
        var kode_customer = $('#data-back').data('kode-customer');
        $('.cus_alamat').text(cus_alamat);
        $('[name="cus_kode_label"]').val(kode_customer+cus_kode);
        $('[name="cus_tipe"]').val(cus_tipe);
    }

    function kalkulasi_subtotal() {
        var total = 0;
        $('[name="total[]"]').each(function(key, val) {
            var val = $(this).val();
            total = parseInt(total) + parseInt(val);
        });
        $('[name="pl_subtotal"]').val(total);
    }

    function kalkulasi_grandtotal() {
        var subtotal = $('[name="pl_subtotal"]').val();
        var disc = $('[name="pl_disc"]').val();
        var ppn = $('[name="pl_ppn"]').val();
        var ongkos_angkut = $('[name="pl_ongkos_angkut"]').val();

        var disc_kal = (parseInt(disc) / parseInt(100)) * parseInt(subtotal);
        var ppn_kal = (parseInt(ppn) / parseInt(100)) * parseInt(subtotal);

        var grand_total = parseInt(subtotal) - parseInt(disc_kal) + parseInt(ppn_kal) + parseInt(ongkos_angkut);

        $('[name="grand_total"]').val(grand_total);
        $('.nominal-grand-total').html(grand_total);
    }

    function detail_third_party() {
        //$('[name="gdg_kode[]"], [name="brg_kode[]"], [name="harga_jual[]"]').select2();
    }

    ////// JS PAYMENT

    $('.btn-row-payment-plus').click(function() {
        var row_payment = $('.table-row-payment tbody').html();
        $('.table-data-payment tbody').append(row_payment);

        btn_row_delete_payment();
        payment();
        set_charge();
        setor();
        payment_third_party();
    });

    function btn_row_delete_payment() {
        $('.btn-row-delete-payment').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_sisa();
        });
    }

    function payment() {
        $('[name="payment[]"]').keyup(function() {
            var payment = $(this).val();

            $(this).parents('td').siblings('td.payment_total').children('input').val(payment);

            kalkulasi_sisa();
        });
    }

    function set_charge() {
        $('[name="charge[]"]').keyup(function() {
            var charge = $(this).val();
            var payment = $(this).parents('td').siblings('td.payment').children('input').val();
            var charge_nom = (parseInt(charge)/100) * parseInt(payment);
            var total = parseInt(payment) + parseInt(charge_nom);

            $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
            $(this).parents('td').siblings('td.payment_total').children('input').val(total);

            kalkulasi_sisa();
        });
    }

    function setor() {
        $('[name="setor[]"]').keyup(function() {
            var setor = $(this).val();
            var total = $(this).parents('td').siblings('td.payment_total').children('input').val();
            var kembalian = parseInt(setor) - parseInt(total);

            $(this).parents('td').siblings('td.kembalian').html(kembalian);
        });
    }

    function kalkulasi_sisa() {
        var payment_total = 0;
        var grand_total = $('.nominal-grand-total').html();
        $('[name="payment_total[]"]').each(function(key, val) {
            var payment_total_ini = $(this).val();
            if(payment_total_ini != '') {
                payment_total = parseInt(payment_total) + parseInt(payment_total_ini);
            }
        });
        sisa = parseInt(grand_total) - parseInt(payment_total);
        $('.nominal-sisa').text(sisa);
    }

    function payment_third_party() {
        $('[name="tgl_pencairan[]"]').datepicker();
        //$('[name="master_id[]"]').select2();
    }

    //akhir dari penjualan langsung
});