<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbArusStok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tb_arus_stok', function (Blueprint $table) {
        $table->increments('ars_stok_kode');
        $table->dateTime('ars_stok_date')->nullable();
        $table->integer('brg_kode')->nullable();
        $table->integer('stok_in')->nullable();
        $table->integer('stok_out')->nullable();
        $table->text('keterangan')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('tb_arus_stok');
    }
}
