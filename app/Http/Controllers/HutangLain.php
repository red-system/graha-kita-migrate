<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mHutangCek;
use App\Models\mHutangLain;
use App\Models\mPerkiraan;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use Carbon\Carbon;

use Validator;

class HutangLain extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main       = new Main();
        $this->title      = 'List Hutang Lain-Lain';
        $this->kodeLabel  = 'hutangLain';
    }

    function index() {
        $breadcrumb             = [
            'Hutang Lain-Lain'  =>route('hutangLain'),
            'Daftar'            =>''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'hutanglain';
        $data['title']      = $this->title;
        $data['kodeHutangLain']       = $this->main->kodeLabel('hutangLain');
        //buat no faktur pembelian
        $kode_user                = auth()->user()->kode;
        $tahun                    = date('Y');
        $thn                      = substr($tahun,2,2);
        $bulan                    = date('m');
        $tgl                      = date('d');
        $no_po_last               = mHutangLain::where('no_hutang_lain','like',$thn.$bulan.$tgl.'-'.$data['kodeHutangLain'].'%')->max('no_hutang_lain');
        $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        if($lastNoUrut==0){
          $nextNoUrut                   = 0+1;
        }else{
          $nextNoUrut                   = $lastNoUrut+1;
        }    
        $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$data['kodeHutangLain'].'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
        $data['next_no_hutang_lain']    = $nextNoTransaksi;
        //buat no faktur pembelian
        // $data['dataList']   = mHutangLain::where('hs_status','Belum Bayar')->get();
        $data['dataList']   = mHutangLain::orderBy('hl_kode','DESC')->where('hl_sisa_amount','>',0)->get();
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode               = $this->main->kodeLabel('bayar');
        $data['no_transaksi'] = $this->main->getNoTransaksiPayment($kode);
        return view('hutang/hutangLainList', $data);
    }

    function rules($request) {
        $rules                  =  [
            'hl_jatuh_tempo'    => 'required',
            'hl_dari'           => 'required',
            'hl_amount'         => 'required',
            'hs_keterangan'     => 'required',
            'hs_status'         => 'required',
        ];                          
        $customeMessage = [
            'required'          => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        $this->rules($request->all());
        \DB::beginTransaction();
        try{
            $hutangLain           = new mHutangLain([
                'no_hutang_lain'  => $request->no_hutang_lain,
                'hl_jatuh_tempo'  => date('Y-m-d', strtotime($request->hl_jatuh_tempo)),
                'hl_dari'         => $request->hl_dari,
                'hl_amount'       => str_replace(',', '', $request->hl_amount),
                'hl_sisa_amount'     => str_replace(',', '', $request->hl_amount),
                'hs_keterangan'   => $request->hs_keterangan,
                'hs_status'       => $request->hs_status,
                'kode_perkiraan'  => $request->kode_perkiraan,//untuk diserver 2107
                'created_at'      => date('Y-m-d H:i:s'),
                'created_by'      => auth()->user()->karyawan->kry_nama,
                'updated_at'      => date('Y-m-d H:i:s'),
                'updated_by'      => auth()->user()->karyawan->kry_nama,
                'hl_tgl'          => date('Y-m-d', strtotime($request->hl_tgl)),
                'akun_debet'      => $request->akun_debet,
            ]);

            $hutangLain->save();

            // $hutang_lain     = mHutangLain::orderBy('hl_kode','DESC')->limit(1)->first();//ambil id hutang lain
            $hutang_lain     = $hutangLain->hl_kode;//ambil id hutang lain

            // $hutang_lain_id     = $hutang_lain['hl_kode'];
            $hutang_lain_id     = $hutang_lain;

            $jatuh_tempo               = date('Y-m-d', strtotime($request->hl_jatuh_tempo));
            $dari                      = $request->hl_dari;
            $amount                    = str_replace(',', '', $request->hl_amount);
            $keterangan                = $request->hs_keterangan;
            $status                    = $request->hs_status;
            $kode_perkiraan            = $request->kode_perkiraan;
            $akun_debet                = $request->akun_debet;
            $tipe_arus_kas             = "Operasi";
            $kode                      = $this->main->kodeLabel('supplier');

            //input kode perkiraan "Persediaan"
            $date_db          = date('Y-m-d', strtotime($request->hl_tgl));
            $year             = date('Y',strtotime($date_db));
            $month            = date('m',strtotime($date_db));
            $day              = date('d',strtotime($date_db));
            $where            = [
                'jmu_year'    => $year,
                'jmu_month'   => $month,
                'jmu_day'     => $day
            ];
            $jmu_no           = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
            if($jmu_no->count() == 0) {
                $jmu_no         = 1;
            } else {
                $jmu_no         = $jmu_no->first()->jmu_no + 1;
            }
            
            
            $jurnal_umum_id     = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                           ->limit(1)
                                            ->first();
            $jurnal_umum_id     = $jurnal_umum_id['jurnal_umum_id'] + 1;

            $data_jurnal          = [
                'jurnal_umum_id'  => $jurnal_umum_id,
                'no_invoice'      => $request->no_hutang_lain,
                'id_pel'          => $kode.$hutangLain->hl_dari,
                'jmu_tanggal'     => $date_db,
                'jmu_no'          => $jmu_no,
                'jmu_year'        => $year,
                'jmu_month'       => $month,
                'jmu_day'         => $day,
                'jmu_keterangan'  => 'Penerimaan hutang lain-lain',
                'jmu_date_insert' => date('Y-m-d'),
                'jmu_date_update' => date('Y-m-d'),
                'reference_number' => $request->no_hutang_lain,
            ];
            mJurnalUmum::insert($data_jurnal);

            //insert data akun debet ke tb_ac_transaksi
            $master_persediaan                 = mPerkiraan::where('mst_kode_rekening',$akun_debet)->first();
            $master_id_persediaan              = $master_persediaan->master_id;
            $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
            $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

            $data_transaksi_persediaan           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_persediaan,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet'           => $amount,
                'trs_kredit'          => 0,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
                'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => $keterangan,
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'tgl_transaksi'       => $date_db
            ];
            mTransaksi::insert($data_transaksi_persediaan);
            //end insert data yg dipilih ke tb_ac_transaksi

            //insert data akun kredit ke tabel tb_ac_transaksi
            $master                 = mPerkiraan::where('mst_kode_rekening',$kode_perkiraan)->first();
            $master_id              = $master->master_id;
            $trs_kode_rekening      = $master->mst_kode_rekening;
            $trs_nama_rekening      = $master->mst_nama_rekening;

            $data_transaksi_persediaan           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $amount,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening,
                'trs_nama_rekening'   => $trs_nama_rekening,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => $keterangan,
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'tgl_transaksi'       => $date_db
            ];
            mTransaksi::insert($data_transaksi_persediaan);
            //end insert data hutang lain-lain otomatis ke tabel tb_ac_transaksi


            //end input kode perkiraan "Persediaan"
            \DB::commit();
            return [
                'redirect'=>route('hutangLain')
            ];
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();    
                
        }
    }

    function edit($hl_kode='') {
        $response = [
            'action'=>route('hutangLainUpdate', ['kode'=>$hl_kode]),
            'field'=>mHutangLain::find($hl_kode)
        ];
        return $response;
    }

    function update(Request $request, $hl_kode) {
        $this->rules($request->all());
        \DB::beginTransaction();
        try{
            $this->rules($request->all());
            $hutangLain           = [
                'no_hutang_lain'  => $request->no_hutang_lain,
                'hl_jatuh_tempo'  => date('Y-m-d', strtotime($request->hl_jatuh_tempo)),
                'hl_dari'         => $request->hl_dari,
                'hl_amount'       => str_replace(',', '', $request->hl_amount),
                'hl_sisa_amount'  => str_replace(',', '', $request->hl_amount),
                // 'hl_sisa_amount'  => str_replace(',', '', $request->hl_amount),
                'hs_keterangan'   => $request->hs_keterangan,
                'hs_status'       => $request->hs_status,
                'kode_perkiraan'  => $request->kode_perkiraan,//untuk diserver 2107
                // 'created_at'      => date('Y-m-d'),
                // 'created_by'      => auth()->user()->karyawan->kry_nama,
                'updated_at'      => date('Y-m-d H:i:s'),
                'updated_by'      => auth()->user()->karyawan->kry_nama,
                'akun_debet'      => $request->akun_debet,
            ];
            mHutangLain::where('hl_kode', $hl_kode)->update($hutangLain);
            $jurnal_umum = mJurnalUmum::where('no_invoice',$request->no_hutang_lain)->firstOrFail();
            mTransaksi::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->delete();
            $amount                 = str_replace(',', '', $request->hl_amount);
            $year                   = date('Y', strtotime($request->hl_tgl));
            $month                  = date('m', strtotime($request->hl_tgl));
            $date_db                = $request->hl_tgl;
            $tipe_arus_kas          = "Operasi";
            $keterangan             = $request->hs_keterangan;
            //insert data akun debet ke tb_ac_transaksi
                $master_persediaan                 = mPerkiraan::where('mst_kode_rekening',$request->akun_debet)->first();
                $master_id_persediaan              = $master_persediaan->master_id;
                $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
                $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

                $data_transaksi_persediaan           = [
                    'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
                    'master_id'           => $master_id_persediaan,
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet'           => $amount,
                    'trs_kredit'          => 0,
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
                    'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
                    'trs_tipe_arus_kas'   => $tipe_arus_kas,
                    'trs_catatan'         => $keterangan,
                    'trs_date_insert'     => $date_db,
                    'trs_date_update'     => $date_db,
                    'trs_charge'          => 0,
                    'trs_no_check_bg'     => 0,
                    'trs_tgl_pencairan'   => date('Y-m-d'),
                    'trs_setor'           => 0,
                    'tgl_transaksi'       => $date_db
                ];
                mTransaksi::insert($data_transaksi_persediaan);
                //end insert data yg dipilih ke tb_ac_transaksi

                //insert data akun kredit otomatis ke tabel tb_ac_transaksi
                $master                 = mPerkiraan::where('mst_kode_rekening',$request->kode_perkiraan)->first();
                $master_id              = $master->master_id;
                $trs_kode_rekening      = $master->mst_kode_rekening;
                $trs_nama_rekening      = $master->mst_nama_rekening;

                $data_transaksi_persediaan           = [
                    'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
                    'master_id'           => $master_id,
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet'           => 0,
                    'trs_kredit'          => $amount,
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $trs_kode_rekening,
                    'trs_nama_rekening'   => $trs_nama_rekening,
                    'trs_tipe_arus_kas'   => $tipe_arus_kas,
                    'trs_catatan'         => $keterangan,
                    'trs_date_insert'     => $date_db,
                    'trs_date_update'     => $date_db,
                    'trs_charge'          => 0,
                    'trs_no_check_bg'     => 0,
                    'trs_tgl_pencairan'   => date('Y-m-d'),
                    'trs_setor'           => 0,
                    'tgl_transaksi'       => $date_db
                ];
                mTransaksi::insert($data_transaksi_persediaan);
                //end insert data hutang lain-lain otomatis ke tabel tb_ac_transaksi
            // mJurnalUmum::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->delete();
                \DB::commit();
            return [
                'redirect'=>route('hutangLain')
            ];
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();    
                
        }

        
    }

    /*function delete($gdg_kode) {
        mGudang::where('gdg_kode', $gdg_kode)->delete();
    }*/

    function rulesPayment($request) {
        $rules = [
            'master_id'=> 'required',
            'payment' => 'required',
            'payment_total' => 'required',
            'keterangan' => 'required',
        ];

        $customeMessage = [
            'required' => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    public function insertPayment(Request $request){
        // $this->rules($request->all());
        \DB::beginTransaction();
        try{
            $this->rulesPayment($request->all());
            $date_db          = date('Y-m-d H:i:s');
            // Jurnal Umum
            //$kode_bukti_id = $request->input('kode_bukti_id');
            
            // Transaksi
            $master_id        = $request->input('master_id');
            $payment          = $request->input('payment');
            $charge           = $request->input('charge');
            $payment_total    = $request->input('payment_total');
            $no_check_bg      = $request->input('no_cek_bg');
            $nama_bank      = $request->input('nama_bank');
            $tgl_pencairan    = $request->input('tgl_pencairan');
            $setor            = $request->input('setor');
            $keterangan       = $request->input('keterangan');
            $tipe_arus_kas    = 'Operasi';
            $hl_kode          = $request->input('hl_kode');
            $hl_kode_2          = $request->input('hl_kode_2');
            $kode_perkiraan   = $request->input('kode_perkiraan');
            $hutangLain       = mHutangLain::find($hl_kode);
            //$kode_perkiraan   = $request->input('kode_perkiraan');
            $no_transaksi      = $request->input('no_transaksi');
            $tgl_transaksi    = $request->input('tgl_transaksi');
            
            // General
            $year             = date('Y', strtotime($tgl_transaksi));
            $month            = date('m', strtotime($tgl_transaksi));
            $day              = date('d', strtotime($tgl_transaksi));
            $where            = [
                'jmu_year'    => $year,
                'jmu_month'   => $month,
                'jmu_day'     => $day
            ];
            $jmu_no           = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
            if($jmu_no->count() == 0) {
                $jmu_no         = 1;
            } else {
                $jmu_no         = $jmu_no->first()->jmu_no + 1;
            }
            
            
            $jurnal_umum_id     = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                           ->limit(1)
                                            ->first();
            $jurnal_umum_id     = $jurnal_umum_id['jurnal_umum_id'] + 1;
            
            $data_jurnal          = [
                'jurnal_umum_id'  => $jurnal_umum_id,
                // 'no_invoice'      => $hutangLain->no_hutang_lain,
                'no_invoice'      => $no_transaksi,
                'id_pel'          => 'SPL'.$hutangLain->hl_dari,
                'jmu_tanggal'     => $tgl_transaksi,
                'jmu_no'          => $jmu_no,
                'jmu_year'        => $year,
                'jmu_month'       => $month,
                'jmu_day'         => $day,
                'jmu_keterangan'  => 'Pembayaran hutang lain-lain No Invoice : '.$hutangLain->no_hutang_lain,
                'jmu_date_insert' => $date_db,
                'jmu_date_update' => $date_db,
                'reference_number' => $hutangLain->no_hutang_lain,
            ];
            mJurnalUmum::insert($data_jurnal);
            
            $hl_amountAwal        = mHutangLain::where('hl_kode',$hl_kode)->first();
            $hl_amount            = $hl_amountAwal['hl_sisa_amount'];
            $total_payment        = 0;
            
            foreach($master_id as $key=>$val) {
                $master             = mPerkiraan::find($master_id[$key]);
                $trs_kode_rekening  = $master->mst_kode_rekening;
                $trs_nama_rekening  = $master->mst_nama_rekening;
                   
            
                $data_transaksi           = [
                    'jurnal_umum_id'      => $jurnal_umum_id,
                    'master_id'           => $master_id[$key],
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet'           => 0,
                    'trs_kredit'          => $payment[$key],
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $trs_kode_rekening,
                    'trs_nama_rekening'   => $trs_nama_rekening,
                    'trs_tipe_arus_kas'   => $tipe_arus_kas,
                    'trs_catatan'         => $keterangan[$key],
                    'trs_date_insert'     => $date_db,
                    'trs_date_update'     => $date_db,
                    'trs_charge'          => $charge[$key],
                    'trs_no_check_bg'     => $no_check_bg[$key],
                    'trs_tgl_pencairan'   => $tgl_pencairan[$key],
                    'trs_setor'           => 0,
                    'tgl_transaksi'       => $tgl_transaksi
                ];

                mTransaksi::insert($data_transaksi);

                      
                $hl_amount                = $hl_amount-$payment[$key];
                $total_payment            = $total_payment + $payment[$key];
                $angka_hl_amount          = number_format($hl_amount,2);
                        
                if($angka_hl_amount=='0.00'){
                    mHutangLain::where('hl_kode', $hl_kode)->update(['hl_sisa_amount'=>$hl_amount,'hs_status'=>'Lunas']);
                }else{
                    mHutangLain::where('hl_kode', $hl_kode)->update(['hl_sisa_amount'=>$hl_amount]);
                }
                if($trs_kode_rekening=='2103'){
                        $hutang_cek            = new mHutangCek([
                            'no_cek_bg'             => $no_check_bg[$key],
                            'tgl_pencairan'         => date('Y-m-d', strtotime($tgl_pencairan[$key])),
                            'tgl_cek'               => $tgl_transaksi,
                            'total_cek'             => $payment[$key],
                            'sisa'                  => $payment[$key],
                            'nama_bank'             => $nama_bank[$key],
                            'cek_untuk'             => $hutangLain->hl_dari,
                            'keterangan_cek'        => $keterangan[$key],
                            'created_at'            => date('Y-m-d'),
                            'created_by'            => auth()->user()->Karyawan->kry_nama,
                            'updated_at'            => date('Y-m-d'),
                            'updated_by'            => auth()->user()->Karyawan->kry_nama
                        ]);
            
                        $hutang_cek->save();
                    }
            }

            $master_hutang                 = mPerkiraan::where('mst_kode_rekening', $kode_perkiraan)->first();
            $master_id_hutang              = $master_hutang->master_id;
            $trs_kode_rekening_hutang      = $master_hutang->mst_kode_rekening;
            $trs_nama_rekening_hutang      = $master_hutang->mst_nama_rekening;

            $data_transaksi_hutang    = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_hutang,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet'           => $total_payment,
                'trs_kredit'          => 0,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_hutang,
                'trs_nama_rekening'   => $trs_nama_rekening_hutang,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => "Pembayaran Hutang",
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => $total_payment,
                'tgl_transaksi'       => $tgl_transaksi
            ];
            mTransaksi::insert($data_transaksi_hutang);
            \DB::commit();
            return [
                'redirect'=>route('hutangLain')
            ];
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();    
                
        }
    }

    public function show_history($id)
    {
        $data['hutang'] = mHutangLain::where('hl_kode', $id)->firstOrFail();
        // $data['histories'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('reference_number',$data['hutang']->no_hutang_lain)->get();
        $data['histories'] = mJurnalUmum::where('reference_number',$data['hutang']->no_hutang_lain)->get();
        return view('hutang/show-history-hutang-lain', $data)->render();
    }

    function getData($kode='') {
        
        $data['hutang'] = mHutangLain::leftJoin('tb_supplier','tb_supplier.spl_kode','=','tb_hutang_lain.hl_dari')->find($kode);
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode               = $this->main->kodeLabel('bayar');
        $data['no_transaksi'] = $this->main->getNoTransaksiPayment($kode);
        return view('hutang/payment-hutang-lain', $data)->render();
    }
    
}
