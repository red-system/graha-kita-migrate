<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login/user', 'Api\API_User@login');
Route::post('register/user', 'Api\API_User@register');

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('api.forgot.email');

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details/user', 'Api\API_User@details');
	Route::post('logout/user', 'Api\API_User@logout');
});

Route::post('login/customer', 'Api\API_Customer@login');
Route::post('register/customer', 'Api\API_Customer@register');

Route::group(['middleware' => 'auth:api_customer'], function(){
	Route::post('details/customer', 'Api\API_Customer@details');
	Route::post('logout/customer', 'Api\API_Customer@logout');
});

//Data Barang & harga
Route::get('/barang', 'Api\API_Barang@index')->name('Api_barangList');
Route::post('/barang-customer', 'Api\API_Barang@customer')->name('Api_barangCustomer');
Route::post('/barang/search', 'Api\API_Barang@barangSearch');

//Customer
Route::get('/customer', 'Api\API_Customer@index')->name('Api_customerList');
Route::get('/customer-type', 'Api\API_Customer@type_cus')->name('Api_customerType');
Route::get('/customer/{id}', 'Api\API_Customer@getCustomer')->name('Api_customerGet');
Route::post('/customer', 'Api\API_Customer@store')->name('Api_customerStore');
Route::post('/customer/forget', 'Api\API_Customer@forget')->name('Api_customerForget');


//Slider
Route::get('/slider', 'Api\API_Slider@index')->name('Api_sliderList');

//promo
Route::get('/promo', 'Api\API_Promo@index')->name('Api_promoList');

//hutang
Route::get('/hutang', 'Api\API_Hutang@index')->name('Api_hutangList');
Route::get('/hutang-customer/{id}', 'Api\API_Hutang@detail_customer')->name('Api_hutangDetailCustomer');

//Order
Route::get('/order', 'Api\API_Order@index')->name('Api_orderList');
Route::get('/order/notif', 'Api\API_Order@notif')->name('Api_orderNotif');
Route::get('/order-customer/{id}', 'Api\API_Order@detail_customer')->name('Api_orderDetailCustomer');
Route::get('/order-karyawan/{id}', 'Api\API_Order@detail_karyawan')->name('Api_orderDetailKaryawan');
Route::post('/order', 'Api\API_Order@store')->name('Api_orderStore');

//Penjualan
Route::get('/penjualan', 'Api\API_Penjualan@index')->name('Api_penjualanList');
Route::get('/penjualan-customer/{id}', 'Api\API_Penjualan@detail_customer')->name('Api_penjualanDetailCustomer');
Route::get('/penjualan-karyawan/{id}', 'Api\API_Penjualan@detail_karyawan')->name('Api_penjualanDetailKaryawan');
Route::get('/best_seller_product', 'Api\API_Penjualan@best_seller_product');

Route::get('storage/{folder}/{filename}', 'StorageController@setstorage');
