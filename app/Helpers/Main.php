<?php

/**
 * Created by PhpStorm.
 * User: Mahendra Wardana Z
 * Date: 3/21/2018
 * Time: 10:30 PM
 */

namespace App\Helpers;
use App\Models\mPerkiraan;
use App\Models\mJurnalUmum;
use Session;

class Main
{
    public function data($breadcrumb=array(), $kodeLabel='') {
        $data['no'] = 1;
        $data['no_2'] = 1;
        $data['breadcrumb'] = $this->breadcrumb($breadcrumb);
        $data['menuAll'] = $this->menuAll();
        $data['kodeLabel'] = $this->kodeLabel($kodeLabel);
        $data['mainHelper'] = $this;
        return $data;
    }

    public function breadcrumb ($data) {
        $count = count($data);
        $no = 1;
        $breadcrumb = '<ul class="page-breadcrumb breadcrumb">';
        $breadcrumb .= '<li><a href="'.route('dashboardPage').'">Dashboard&nbsp</a><i class="fa fa-circle"></i> </li>';
        foreach($data as $key=>$val) {
            $breadcrumb .= '<li>';
            if($no != $count) {
                $breadcrumb .= '<a href="'.$val.'">&nbsp'.$key.'&nbsp</a> ';
                $breadcrumb .= ' <i class="fa fa-circle"></i>';
            } else {
                $breadcrumb .= '&nbsp'.$key;
            }
            $breadcrumb .= '</li>';

            $no++;
        }
        $breadcrumb .= '</ul>';

        return $breadcrumb;
    }

    public function kodeLabel($kodeLabel) {
        $kode = [
            'gudang'			=>'GDG',
            'merek'				=>'MRK',
            'kategoryStok'		=>'KTG',
            'groupStok'			=>'GPS',
            'satuan'			=>'STN',
            'wilayah'			=>'WYH',
            'provinsi'			=>'PVS',
            'supplier'			=>'SPL',
            'type_customer'		=>'TYPC',
            'customer'			=>'CUS',
            'karyawan'			=>'KYW',
            'kendaraan'			=>'KDR',
            'jpk'				=>'JPK',
            'kategoryAsset'		=>'KAS',
            'kodePerkiraan'		=>'KPR',
            'barang'			=>'BRG',
            'hargaCustomer'		=>'HRC',
            'penjualanLangsung'	=>'PLG',
            'penjualanTitipan'	=>'PTP',
            'delivery_order'	=>'DO',
            'suratJalan'		=>'DO',
            'suratJalanPL'		=>'SJPL',
            'suratJalanPT'		=>'SJPT',
            'user'				=>'USR',
            'profile'			=>'PRF',
            'slider'			=>'SLDR',
            'promo'				=>'PRM',
            'pembelianSupplier'	=>'PS',
            'hutangSuplier'		=>'HS',
            'hutangLain'		=>'HL',
            'piutangPelanggan'	=>'PP',
            'piutangLain'		=>'PL',
            'poSupplier'		=>'POS',
            'transferStok'		=>'TRF',
            'online-order'		=>'OL',
            'stok_sample'		=>'STS',
            'biaya_operasional_kendaraan'=>'BOK',
            'asset' 			=>'AT',
            'retur-penjualan'	=>'RTP',
            'returPembelian'    => 'RB',
            'wo'         		=> 'WO',
            'penyesuaianSC'		=>'PSC',
            'penyesuaian_stok'	=>'ADJ',
            'cat_oplosan'		=>'CO',
            'history_cetak_faktur'=>'HCF',
            'pencairan'         => 'PN',
            'jurnalumum'        => 'JU',
            'bayar'             => 'BR',
        ];

        return isset($kode[$kodeLabel]) ? $kode[$kodeLabel] : '';
    }

    public function menuAll() {
        $menu = [
            'master_data'=> [
                'gudang',
                'merek',
                'kategori',
                'group_stok',
                'satuan',
                'wilayah',
                'provinsi',
                'supplier',
                'type_customer',
                'customer',
                'karyawan',
                'kendaraan',
                'biaya_operasional',
                'kategori_asset',
                'kode_perikiraan'
            ],
            'inventory'=> [
                'barang_dan_harga',
                'buat_barcode',
                'harga_customer',
                'transfer_stok',
                'summary_stok',
                'kartu_stok',
                'stok_alert',
                'stok_sample',
                'retur_penjualan',
                'penyesuaian_stok_cat',
                'penyesuaian_stok',
            ],
            'transaksi'=> [
                'penjualan_langsung',
                'return_penjualan',
                'penjualan_titipan',
                'surat_jalan',
                'order_pembelian',
                'daftar_po_out',
                'pembelian',
                'return_pembelian',
                'laporan_penjualan',
                'laporan_pembelian',
                'history_cetak_faktur',
            ],
            'hutang_piutang'=> [
                'hutang_supplier',
                'hutang_pelanggan',
                'hutang_lain',
                'piutang_lain',
                'daftar_cek_bg',
                'laporan_hutang',
                'laporan_piutang'
            ],
            'accounting'=> [
                'jurnal_umum',
                'km_bm_kk',
                'asset',
                'penyusutan_asset',
                'biaya_operasional_kendaraan',
                'pemindah_bukuan',
                'laporan'
            ],
            'utilitas'=> [
                'user',
                'profile',
                'home-slider',
                'promo'
            ]
        ];

        return $menu;
    }

    function nominal($nominal) {
        return number_format($nominal);
    }

    function perkiraan_space($subKe) {
        switch($subKe) {
            case 1: $space = '&nbsp;&nbsp;'; break;
            case 2: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            case 3: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            case 4: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            default: $space = '';
        }
        return $space;
    }

    function perkiraan($year='', $month='') {
        $perkiraan = [];
        $data = mPerkiraan::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        foreach($data as $k=>$r) {
            $sub1 = mPerkiraan::where('mst_master_id', $r->master_id)->orderBy('mst_kode_rekening','ASC')->get();
            $perkiraan[$k] = $r;
            $perkiraan[$k]['sub1'] = $sub1;
            foreach($sub1 as $k1=>$r1) {
                $sub2 = mPerkiraan::where('mst_master_id', $r1->master_id)->orderBy('mst_kode_rekening','ASC')->get();
                $perkiraan[$k]['sub1'][$k1]['sub2'] = $sub2;
                foreach($sub2 as $k2=>$r2) {
                    $sub3 = mPerkiraan::where('mst_master_id', $r2->master_id)->orderBy('mst_kode_rekening','ASC')->get();
                    $perkiraan[$k]['sub1'][$k1]['sub2'][$k2]['sub3'] = $sub3;
                }
            }
        }

        return $perkiraan;
    }

    function perkiraanBiaya($year='', $month='') {
        $perkiraan = [];
        $data = mPerkiraan::where('mst_master_id',0)
        ->where('mst_kode_rekening','>',5100)
        ->where('mst_kode_rekening','<=',5191)
        ->orderBy('mst_kode_rekening','ASC')->get();
        foreach($data as $k=>$r) {
            $sub1 = mPerkiraan::where('mst_master_id', $r->master_id)->orderBy('mst_kode_rekening','ASC')->get();
            $perkiraan[$k] = $r;
            $perkiraan[$k]['sub1'] = $sub1;
            foreach($sub1 as $k1=>$r1) {
                $sub2 = mPerkiraan::where('mst_master_id', $r1->master_id)->orderBy('mst_kode_rekening','ASC')->get();
                $perkiraan[$k]['sub1'][$k1]['sub2'] = $sub2;
                foreach($sub2 as $k2=>$r2) {
                    $sub3 = mPerkiraan::where('mst_master_id', $r2->master_id)->orderBy('mst_kode_rekening','ASC')->get();
                    $perkiraan[$k]['sub1'][$k1]['sub2'][$k2]['sub3'] = $sub3;
                }
            }
        }

        return $perkiraan;
    }

    function spellNumberInIndonesian($number)
	{
		$result = "";
		$number = strval($number);
		if (!preg_match("/^[0-9]{1,15}$/", $number)) return false;
		$ones           = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan");
		$majorUnits     = array("", "ribu", "juta", "milyar", "trilyun");
		$minorUnits     = array("", "puluh", "ratus");
		$length         = strlen($number);
		$isAnyMajorUnit = false;

		for ($i = 0, $pos = $length - 1; $i < $length; $i++, $pos--) {
			if ($number{$i} != '0') {
				if ($number{$i} != '1') {
					$result .= $ones[$number{$i}].' '.$minorUnits[$pos % 3].' ';
				} else if ($pos % 3 == 1 && $number{$i + 1} != '0') {
					if ($number{$i + 1} == '1')
						$result .= "sebelas ";
					else
						$result .= $ones[$number{$i + 1}]." belas ";
					$i++; $pos--;
				} else if ($pos % 3 != 0) {
					$result .= "se".$minorUnits[$pos % 3].' ';
				} else if ($pos == 3 && !$isAnyMajorUnit) {
					$result .= "se";
				} else {
					$result .= "satu ";
				}
				$isAnyMajorUnit = true;
			}
			if ($pos % 3 == 0 && $isAnyMajorUnit) {
				$result         .= $majorUnits[$pos / 3].' ';
				$isAnyMajorUnit = false;
			}
		}
		$result = trim($result);
		if ($result == "") $result = "nol";
		return $result;
 	}

    function getNoTransaksiPayment($kode){
        //buat no faktur pembelian
        $tahun                    = date('Y');
        $thn                      = substr($tahun,2,2);
        $bulan                    = date('m');
        $tgl                      = date('d');
        $kode_user                = auth()->user()->kode;
        $no_po_last               = mJurnalUmum::where('no_invoice','like',$thn.$bulan.$tgl.'-'.$kode.'%')->max('no_invoice');
        $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        if($lastNoUrut==0){
          $nextNoUrut                   = 0+1;
        }else{
          $nextNoUrut                   = $lastNoUrut+1;
        }    
        // $nextNoTransaksi              = sprintf('%04s',$nextNoUrut).'/'.$kode.'/'.$bulan.$thn;
        $nextNoTransaksi            = $thn.$bulan.$tgl.'-'.$kode.'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
        // $data['next_no_trs']    = $nextNoTransaksi;
        $next_no_tr = $nextNoTransaksi;

        return $next_no_tr;
    }

}
