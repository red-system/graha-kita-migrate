<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="invoice">
                            <p>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</p>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <h5>Kepada :</h5>
                                    <ul class="list-unstyled">
                                        <li> Bpk/Ibu {{$piutangPelanggan->customers->cus_nama}} </li>
                                    </ul>
                                </div>
                                <div class="col-xs-4">
                                </div>
                                <div class="col-xs-4">
                                    <h5></h5>
                                    <ul class="list-unstyled">
                                        <li> No Invoice </li>
                                        <li> Tanggal Invoice : {{$piutangPelanggan->penjualan_langsung->pl_tgl}}</li>
                                        <li> No Faktur Penjualan : PLG{{$piutangPelanggan->penjualan_langsung->pl_no_faktur}}</li>
                                        <li> Tanggal Jatuh Tempo : {{$piutangPelanggan->penjualan_langsung->pl_tgl_jatuh_tempo}}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th> No</th>
                                                <th> Nama Barang </th>
                                                <th class="hidden-xs"> Harga/Sat(Rp) </th>
                                                <th class="hidden-xs"> Disc % </th>
                                                <th class="hidden-xs"> Disc. Nom </th>
                                                <th class="hidden-xs"> Qty </th>
                                                <th> Total </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($detailPenjualanLangsung as $detail)
                                            @foreach($detail->detail_penjualanLangsung as $penjualanLangsung_detail)    
                                            <tr>
                                                <td>  </td>
                                                <td> {{$penjualanLangsung_detail->nama_barang}} </td>
                                                <td class="hidden-xs"> Rp {{ number_format($penjualanLangsung_detail->harga_jual)}} </td>
                                                <td class="hidden-xs"> {{ $penjualanLangsung_detail->disc}} </td>
                                                <td class="hidden-xs"> {{ $penjualanLangsung_detail->disc_nom}} </td>
                                                <td> {{ $penjualanLangsung_detail->qty}} </td>
                                                <td> Rp. {{ number_format($penjualanLangsung_detail->total)}} </td>
                                            </tr>
                                                  @endforeach
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="col-md-12" width="100%">
                                        <tr>
                                                <td colspan="5" width="45%">Terbilang : </td>
                                                <td width="8%">Total</td>
                                                <td width="10%">Rp. {{number_format($detail->pl_subtotal)}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" rowspan="3">Keterangan</td>
                                                <td>Disc</td>
                                                <td>Rp. {{number_format($detail->pl_disc_nom)}}</td>
                                            </tr>
                                            <tr>
                                                <!-- <td colspan="5"></td> -->                                                
                                                <td>PPN 10%</td>
                                                <td>Rp. {{number_format($detail->pl_ppn_nom)}}</td>
                                            </tr>
                                            <tr>
                                                <!-- <td colspan="5"></td>  -->                                               
                                                <td><strong>Grand Total</strong></td>
                                                <td><strong>Rp. {{number_format($detail->grand_total)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">-{{$detail->pl_catatan}}</td>                                               
                                                <td><strong>Terbayar</strong></td>
                                                <td><strong>Rp. {{number_format($detail->grand_total)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5"></td>                                                
                                                <td><strong>Sisa</strong></td>
                                                <td><strong>Rp. 0</strong></td>
                                            </tr>
                                            @endforeach
                                    </table>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>