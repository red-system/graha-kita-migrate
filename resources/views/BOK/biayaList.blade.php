@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <!-- (Optional) Latest compiled and minified JavaScript translation files -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

  <script type="text/javascript">
  $(document).ready(function() {
    let today = new Date().toISOString().substr(0, 10);
    $('input[name="tanggal"]').val(today);
    $('input[name="jatuh_tempo"]').val(today);

    $('#btn-tambah').click(function () {
      $('#modal-tambah').modal('show');
      // $('#selectpicker1').selectpicker();
      // $('#selectpicker2').selectpicker();
    });

    // $('#selectpicker1').click(function () {
    //   $('#selectpicker1').selectpicker('refresh');
    //   // $('#selectpicker1').selectpicker('destroy');
    //   $('#selectpicker1').selectpicker('render');
    // });
    //
    // $('#selectpicker2').click(function () {
    //   $('#selectpicker2').selectpicker('refresh');
    //   // $('#selectpicker2').selectpicker('destroy');
    //   $('#selectpicker2').selectpicker('render');
    // });
    //
    // $('#selectpicker3').click(function () {
    //   $('#selectpicker3').selectpicker('refresh');
    //   // $('#selectpicker3').selectpicker('destroy');
    //   $('#selectpicker3').selectpicker('render');
    // });
    //
    // $('#selectpicker4').click(function () {
    //   $('#selectpicker4').selectpicker('refresh');
    //   // $('#selectpicker4').selectpicker('destroy');
    //   $('#selectpicker4').selectpicker('render');
    // });

    // $('select[name="jpk_kode"]').change(function () {
    //   var id = $(this).val();
    //   console.log(id);
    //
    //   var nama = $(this).data('nama');
    //   console.log(nama);
    //
    //   $('input[name="nama_biaya"]').val(id);
    // });

    $('select[name="jpk_kode"]').change(function () {
      var id = $(this).val();
      // console.log(id);
      // var nama = $(this).data('nama');
      // console.log(nama);
      var token = $('input[name="_token"]').val();
      var data_send = {
        jpk_kode: id,
        _token: token
      };
      $.ajax({
        url: '../BiayaNamaRow',
        type: 'POST',
        data: data_send,
        success: function(data) {
          // console.log(data);
          $('input[name="nama_biaya"]').val(data.jpk_nama);
        }
      });
    });

    var table = $('#sample_3').DataTable();

    $('#sample_3').on('draw.dt', function () {
      $('select[name="jpk_kode"]').change(function () {
        var id = $(this).val();
        // console.log(id);
        // var nama = $(this).data('nama');
        // console.log(nama);
        var token = $('input[name="_token"]').val();
        var data_send = {
          jpk_kode: id,
          _token: token
        };
        $.ajax({
          url: '../BiayaNamaRow',
          type: 'POST',
          data: data_send,
          success: function(data) {
            console.log(data);
            $('input[name="nama_biaya"]').val(data.jpk_nama);
          }
        });
      });

      $('.btn-edit').click(function() {
          var href = $(this).data('href');
          $.ajax({
              url: href,
              success: function(data) {
                  $.each(data.field, function(field, value) {
                      if($('#modal-edit [name="'+field+'"]').is('select')) {
                          //$('#modal-edit [name="'+field+'"]').val(value);
                          $('#modal-edit [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                      }
                      else if ($('#modal-edit [name="'+field+'"]').is(':checkbox')) {
                        if (value == 'Aktif') {
                          $('#modal-edit [name="'+field+'"]').attr('checked', true);
                        }
                      }
                      else if (field == 'brg_product_img') {
                        if ($('#modal-edit [id="imgScrEdit"]').is('img')) {
                          if (value != null ) {
                            var l = window.location;
                            var base_url = l.protocol + "//" + l.host + "/";
                            // console.log(base_url);
                            $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + value);
                          }
                          else {
                            var l = window.location;
                            var base_url = l.protocol + "//" + l.host + "/";
                            $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + "img/icon/no_image.png");
                          }
                        }
                      }
                      else {
                          $('#modal-edit [name="'+field+'"]').val(value);
                      }
                  });
                  $('#modal-edit form').attr('action', data.action);
                  $('#modal-edit').modal('show');
              }
          });
      });

      $('.btn-delete-new').click(function() {
        var ini = $(this);
        swal({
          title: 'Perhatian',
          text: 'Yakin ? Data akan dihapus permanen',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText:'Yakin',
          cancelButtonText:'Batal'
        }).then((result) => {
          if (result.value) {
            $.ajax({
              url: ini.data('href'),
              success: function(data) {
                ini.parents('tr').fadeOut('fast', function(){
                  ini.parents('tr').remove();
                });
              },
              error: function(request, status, error) {
                swal(
                  'Error',
                  'Data gagal dihapus',
                  'error'
                )
              },
            });
          } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
          ) {
            swal(
              'Cancelled',
              'Data batal dihapus',
              'error'
            )
          }
        })
      });

      $('.btn-edit-stok').click(function() {
        var href = $(this).data('href');
        var href2 = href.replace("edit", "get");
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/";

        $.ajax({
          url: href,
          success: function(data) {
            $.each(data.field, function(field, value) {
              if (field == 'brg_kode') {
                $('#modal-edit-stok [name="brg_kode"]').val(value);
              }
              else {
                $('#modal-edit-stok [name="gdg_kode"]').val(value);
              }
            });
            $('#modal-edit-stok form').attr('action', data.action);
            $('#modal-edit-stok').modal('show');
          }
        });

      });
    });

    $('#search-date').click(function() {
      var start = $('#start_date').val();
      var end = $('#end_date').val();
      var kendaraan_operasional_kode = $('input[name="kendaraan_operasional_kode"]').val();
      var master_id = $("#master_id").find(':selected').val();

      var href = "{{route('BiayaBOKBiayaRow')}}";
      var l = window.location;
      var base_url = l.protocol + "//" + l.host + "/";
      var table = $('#sample_3').DataTable();
      var no = 1;
      table.clear();

      $.ajax({
          url: href,
          type: 'POST',
          data: { start_date: start, end_date: end, kendaraan_operasional_kode: kendaraan_operasional_kode, master_id: master_id, _token: "{{ csrf_token() }}" },
          success: function(data) {
            total = data.total_biaya;
            $('#total').text('Rp.'+total);
            // console.log(data.total_biaya);
          },
          error: function(request, status, error) {
            // console.log(error);
          }
      });

      $('#sample_3').DataTable({
        destroy : true,
        processing: true,
        serverSide: true,
        ajax : {
          url: href,
          type: 'POST',
          data: { start_date: start, end_date: end, kendaraan_operasional_kode: kendaraan_operasional_kode, master_id: master_id, _token: "{{ csrf_token() }}" },
          dataSrc : 'data'
        },
        columns: [
          {
            data: null, render: function ( data, type, row )
            {
              return no++;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return 'JPK'+data.jpk_kode;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              if (data.spl) {
                return data.spl.spl_nama;
              }
              else {
                return '-';
              }
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.nama_biaya;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.total;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.keterangan;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.biaya.mst_kode_rekening+' - '+data.biaya.mst_nama_rekening;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.pembayaran.mst_kode_rekening+' - '+data.pembayaran.mst_nama_rekening;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.km_awal;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return data.km_akhir;
            }
          },
          {
            data: null, render: function ( data, type, row )
            {
              return '<div class="btn-group-xs"> <a class="btn btn-success btn-edit btn-icon" type="button" data-href="'+base_url+'biaya-operasional-kendaraan/'+data.biaya_kendaraan_kode+'/biaya/edit"><i class="icon-pencil"></i> Edit</a> <a class="btn btn-danger btn-delete-new btn-icon" type="button" data-href="'+base_url+'biaya-operasional-kendaraan/'+data.biaya_kendaraan_kode+'/biaya/delete"><i class="icon-trash"></i> Delete</a> </div>';
            }
          },
          ]
        });
      });
    });
  </script>

@stop
@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <div class="row">
                <div class="col-md-3">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    <input type="date" name="start_date" id="start_date" class="form-control">
                  </div>
                </div>
                <div class="col-xs-1 text-center">
                  <h4>S/d</h4>
                </div>
                <div class="col-md-3">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    <input type="date" name="end_date" id="end_date" class="form-control">
                  </div>
                </div>
                <div class="col-md-3">
                  <select class="form-control selectpicker" data-live-search="true" name="master_id" id="master_id">
                    <option value="all" selected>Pilih Semua COA</option>
                      @foreach($perkiraan as $r)
                        {{-- <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option> --}}
                        <option style="font-weight:bold" value="{{ $r->master_id }}"
                          data-master-id="{{ $r->master_id }}"
                          data-mst-kode-rekening="{{ $r->mst_kode_rekening }}"
                          data-mst-nama-rekening="{{ $r->mst_nama_rekening }}"
                          data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
                          {{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}
                        </option>
                        @if ($r['sub1'])
                          @foreach($r['sub1'] as $r1)
                            <option value="{{ $r1->master_id }}"
                              data-master-id="{{ $r1->master_id }}"
                              data-mst-kode-rekening="{{ $r1->mst_kode_rekening }}"
                              data-mst-nama-rekening="{{ $r1->mst_nama_rekening }}"
                              data-content="{{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}">
                              {{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}
                            </option>
                            @if ($r1['sub2'])
                              @foreach($r1['sub2'] as $r2)
                                <option value="{{ $r2->master_id }}"
                                  data-master-id="{{ $r2->master_id }}"
                                  data-mst-kode-rekening="{{ $r2->mst_kode_rekening }}"
                                  data-mst-nama-rekening="{{ $r2->mst_nama_rekening }}"
                                  data-content="{{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}">
                                  {{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}
                                </option>
                                @if ($r2['sub3'])
                                  @foreach($r2['sub3'] as $r3)
                                    <option value="{{ $r3->master_id }}"
                                      data-master-id="{{ $r3->master_id }}"
                                      data-mst-kode-rekening="{{ $r3->mst_kode_rekening }}"
                                      data-mst-nama-rekening="{{ $r3->mst_nama_rekening }}"
                                      data-content="{{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}">
                                      {{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}
                                    </option>
                                  @endforeach
                                @endif
                              @endforeach
                            @endif
                          @endforeach
                        @endif
                      @endforeach
                  </select>
                </div>
                <div class="col-md-2">
                  <button type="button" name="button" id="search-date" class="btn btn-info"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>
              </div>
              <br>
              <button id="btn-tambah" class="btn btn-primary" data-toggle="modal">
                <i class="fa fa-plus"></i> Tambah Biaya
              </button>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
                <thead>
                  <tr class="">
                    <th width="10"> No </th>
                    <th> Kode Biaya </th>
                    <th> Supplier </th>
                    <th> Nama Biaya </th>
                    <th> Total </th>
                    <th> Keterangan </th>
                    <th> Kode Perkiraan Biaya</th>
                    <th> Kode Perkiraan Pembayaran</th>
                    <th> KM Awal</th>
                    <th> KM Akhir</th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td> {{ $no++ }}. </td>
                      <td> {{ $jpkLabel.$row->jpk_kode }} </td>
                      <td>
                        @if ($row->spl)
                          {{ $row->spl['spl_nama'] }}
                        @else
                          -
                        @endif
                      </td>
                      <td> {{ $row->nama_biaya }} </td>
                      <td> {{ $row->total }} </td>
                      <td> {{ $row->keterangan }} </td>
                      <td> {{ $row->biaya['mst_kode_rekening'].' - '.$row->biaya['mst_nama_rekening'] }}</td>
                      <td> {{ $row->pembayaran['mst_kode_rekening'].' - '.$row->pembayaran['mst_nama_rekening'] }}</td>
                      <td> {{ $row->km_awal }} </td>
                      <td> {{ $row->km_akhir }} </td>
                      <td style="white-space: nowrap">
                        <div class="btn-group-xs">
                          <a class="btn btn-success btn-edit btn-icon" type="button" data-href="{{ route('BiayaBOKEdit', ['kode'=>$row->biaya_kendaraan_kode]) }}"><i class="icon-pencil"></i> Edit</a>
                          <a class="btn btn-danger btn-delete-new btn-icon" type="button" data-href="{{ route('BiayaBOKDelete', ['kode'=>$row->biaya_kendaraan_kode]) }}"><i class="icon-trash"></i> Delete</a>
                        </div>
                        {{-- <div class="btn-group-xs">
                          <button class="btn btn-success btn-edit" data-href="{{ route('BiayaBOKEdit', ['kode'=>$row->biaya_kendaraan_kode]) }}">
                            <span class="icon-pencil"></span> Edit
                          </button>
                          <button class="btn btn-danger btn-delete-new" data-href="{{ route('BiayaBOKDelete', ['kode'=>$row->biaya_kendaraan_kode]) }}">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div> --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal modal-tambah" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Biaya
          </h4>
        </div>
        <div class="modal-body form" style="overflow: visible;">
          <form action="{{ route('BiayaBOKInsert') }}" class="form-horizontal form-send" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Tanggal</label>
                <div class="col-md-9">
                  <input type="hidden" class="form-control" name="kendaraan_operasional_kode" value="{{$kendaraan_operasional_kode}}">
                  <input class="form-control" type="date" name="tanggal" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Jatuh Tempo</label>
                <div class="col-md-9">
                  <input class="form-control" type="date" name="jatuh_tempo" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Biaya</label>
                <div class="col-md-9">
                  <select name="jpk_kode" class="form-control selectpicker" data-live-search="true">
                    <option value="0" data-nama="-">Pilih Kode Biaya</option>
                    @foreach($jpk as $r)
                      <option value="{{ $r->jpk_kode }}" data-nama="{{ $r->jpk_nama }}">{{ $jpkLabel.$r->jpk_kode.' - '.$r->jpk_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select name="spl_kode" class="form-control selectpicker" data-live-search="true">
                    <option value="" data-nama="-">Pilih Supplier</option>
                    @foreach($spl as $r)
                      <option value="{{ $r->spl_kode }}" data-nama="{{ $r->spl_nama }}">{{ $r->spl_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Biaya</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="nama_biaya" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Total</label>
                <div class="col-md-9">
                  <input type="number" min="0" value="0" class="form-control" name="total" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Keterangan</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="keterangan" rows="4" cols="40"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Perkiraan Biaya</label>
                <div class="col-md-9">
                  <select id="selectpicker1" name="biaya_kode" class="form-control selectpicker" data-live-search="true" data-size="5">
                    @foreach($perkiraan as $r)
                      {{-- <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option> --}}
                      <option style="font-weight:bold" value="{{ $r->master_id }}"
                        data-master-id="{{ $r->master_id }}"
                        data-mst-kode-rekening="{{ $r->mst_kode_rekening }}"
                        data-mst-nama-rekening="{{ $r->mst_nama_rekening }}"
                        data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
                        {{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}
                      </option>
                      @if ($r['sub1'])
                        @foreach($r['sub1'] as $r1)
                          <option value="{{ $r1->master_id }}"
                            data-master-id="{{ $r1->master_id }}"
                            data-mst-kode-rekening="{{ $r1->mst_kode_rekening }}"
                            data-mst-nama-rekening="{{ $r1->mst_nama_rekening }}"
                            data-content="{{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}">
                            {{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}
                          </option>
                          @if ($r1['sub2'])
                            @foreach($r1['sub2'] as $r2)
                              <option value="{{ $r2->master_id }}"
                                data-master-id="{{ $r2->master_id }}"
                                data-mst-kode-rekening="{{ $r2->mst_kode_rekening }}"
                                data-mst-nama-rekening="{{ $r2->mst_nama_rekening }}"
                                data-content="{{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}">
                                {{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}
                              </option>
                              @if ($r2['sub3'])
                                @foreach($r2['sub3'] as $r3)
                                  <option value="{{ $r3->master_id }}"
                                    data-master-id="{{ $r3->master_id }}"
                                    data-mst-kode-rekening="{{ $r3->mst_kode_rekening }}"
                                    data-mst-nama-rekening="{{ $r3->mst_nama_rekening }}"
                                    data-content="{{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}">
                                    {{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}
                                  </option>
                                @endforeach
                              @endif
                            @endforeach
                          @endif
                        @endforeach
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Perkiraan Pembayaran</label>
                <div class="col-md-9">
                  <select id="selectpicker2" name="pembayaran_kode" class="form-control selectpicker" data-live-search="true" data-size="5">
                    @foreach($perkiraanP as $r)
                      {{-- <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option> --}}
                      <option style="font-weight:bold" value="{{ $r->master_id }}"
                        data-master-id="{{ $r->master_id }}"
                        data-mst-kode-rekening="{{ $r->mst_kode_rekening }}"
                        data-mst-nama-rekening="{{ $r->mst_nama_rekening }}"
                        data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
                        {{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}
                      </option>
                      @if ($r['sub1'])
                        @foreach($r['sub1'] as $r1)
                          <option value="{{ $r1->master_id }}"
                            data-master-id="{{ $r1->master_id }}"
                            data-mst-kode-rekening="{{ $r1->mst_kode_rekening }}"
                            data-mst-nama-rekening="{{ $r1->mst_nama_rekening }}"
                            data-content="{{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}">
                            {{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}
                          </option>
                          @if ($r1['sub2'])
                            @foreach($r1['sub2'] as $r2)
                              <option value="{{ $r2->master_id }}"
                                data-master-id="{{ $r2->master_id }}"
                                data-mst-kode-rekening="{{ $r2->mst_kode_rekening }}"
                                data-mst-nama-rekening="{{ $r2->mst_nama_rekening }}"
                                data-content="{{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}">
                                {{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}
                              </option>
                              @if ($r2['sub3'])
                                @foreach($r2['sub3'] as $r3)
                                  <option value="{{ $r3->master_id }}"
                                    data-master-id="{{ $r3->master_id }}"
                                    data-mst-kode-rekening="{{ $r3->mst_kode_rekening }}"
                                    data-mst-nama-rekening="{{ $r3->mst_nama_rekening }}"
                                    data-content="{{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}">
                                    {{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}
                                  </option>
                                @endforeach
                              @endif
                            @endforeach
                          @endif
                        @endforeach
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 control-label">KM Awal</label>
                  <div class="col-md-9">
                    <input type="number" min="0" value="0" class="form-control" name="km_awal" required>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 control-label">KM Akhir</label>
                  <div class="col-md-9">
                    <input type="number" min="0" value="0" class="form-control" name="km_akhir" required>
                  </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal modal-edit" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Biaya
          </h4>
        </div>
        <div class="modal-body form" style="overflow: visible;">
          <form action="" class="form-horizontal form-send" role="form" method="put">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Tanggal</label>
                <div class="col-md-9">
                  <input type="hidden" class="form-control" name="kendaraan_operasional_kode" value="{{$kendaraan_operasional_kode}}">
                  <input class="form-control" type="date" name="tanggal" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Jatuh Tempo</label>
                <div class="col-md-9">
                  <input class="form-control" type="date" name="jatuh_tempo" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Biaya</label>
                <div class="col-md-9">
                  <select name="jpk_kode" class="form-control selectpicker" data-live-search="true">
                    <option value="0" data-nama="-">Pilih Kode Biaya</option>
                    @foreach($jpk as $r)
                      <option value="{{ $r->jpk_kode }}" data-nama="{{ $r->jpk_nama }}">{{ $jpkLabel.$r->jpk_kode.' - '.$r->jpk_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select name="spl_kode" class="form-control selectpicker" data-live-search="true">
                    @foreach($spl as $r)
                      <option value="{{ $r->spl_kode }}" data-nama="{{ $r->spl_nama }}">{{ $r->spl_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Biaya</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="nama_biaya" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Total</label>
                <div class="col-md-9">
                  <input type="number" min="0" value="0" class="form-control" name="total" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Keterangan</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="keterangan" rows="4" cols="40"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Perkiraan Biaya</label>
                <div class="col-md-9">
                  <select id="selectpicker3" name="biaya_kode" class="form-control selectpicker" data-live-search="true" data-size="5">
                    @foreach($perkiraan as $r)
                      {{-- <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option> --}}
                      <option style="font-weight:bold" value="{{ $r->master_id }}"
                        data-master-id="{{ $r->master_id }}"
                        data-mst-kode-rekening="{{ $r->mst_kode_rekening }}"
                        data-mst-nama-rekening="{{ $r->mst_nama_rekening }}"
                        data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
                        {{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}
                      </option>
                      @if ($r['sub1'])
                        @foreach($r['sub1'] as $r1)
                          <option value="{{ $r1->master_id }}"
                            data-master-id="{{ $r1->master_id }}"
                            data-mst-kode-rekening="{{ $r1->mst_kode_rekening }}"
                            data-mst-nama-rekening="{{ $r1->mst_nama_rekening }}"
                            data-content="{{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}">
                            {{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}
                          </option>
                          @if ($r1['sub2'])
                            @foreach($r1['sub2'] as $r2)
                              <option value="{{ $r2->master_id }}"
                                data-master-id="{{ $r2->master_id }}"
                                data-mst-kode-rekening="{{ $r2->mst_kode_rekening }}"
                                data-mst-nama-rekening="{{ $r2->mst_nama_rekening }}"
                                data-content="{{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}">
                                {{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}
                              </option>
                              @if ($r2['sub3'])
                                @foreach($r2['sub3'] as $r3)
                                  <option value="{{ $r3->master_id }}"
                                    data-master-id="{{ $r3->master_id }}"
                                    data-mst-kode-rekening="{{ $r3->mst_kode_rekening }}"
                                    data-mst-nama-rekening="{{ $r3->mst_nama_rekening }}"
                                    data-content="{{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}">
                                    {{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}
                                  </option>
                                @endforeach
                              @endif
                            @endforeach
                          @endif
                        @endforeach
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Perkiraan Pembayaran</label>
                <div class="col-md-9">
                  <select id="selectpicker4" name="pembayaran_kode" class="form-control selectpicker" data-live-search="true" data-size="5">
                    @foreach($perkiraanP as $r)
                      {{-- <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option> --}}
                      <option style="font-weight:bold" value="{{ $r->master_id }}"
                        data-master-id="{{ $r->master_id }}"
                        data-mst-kode-rekening="{{ $r->mst_kode_rekening }}"
                        data-mst-nama-rekening="{{ $r->mst_nama_rekening }}"
                        data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
                        {{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}
                      </option>
                      @if ($r['sub1'])
                        @foreach($r['sub1'] as $r1)
                          <option value="{{ $r1->master_id }}"
                            data-master-id="{{ $r1->master_id }}"
                            data-mst-kode-rekening="{{ $r1->mst_kode_rekening }}"
                            data-mst-nama-rekening="{{ $r1->mst_nama_rekening }}"
                            data-content="{{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}">
                            {{ $r1->mst_kode_rekening.' - '.$r1->mst_nama_rekening }}
                          </option>
                          @if ($r1['sub2'])
                            @foreach($r1['sub2'] as $r2)
                              <option value="{{ $r2->master_id }}"
                                data-master-id="{{ $r2->master_id }}"
                                data-mst-kode-rekening="{{ $r2->mst_kode_rekening }}"
                                data-mst-nama-rekening="{{ $r2->mst_nama_rekening }}"
                                data-content="{{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}">
                                {{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}
                              </option>
                              @if ($r2['sub3'])
                                @foreach($r2['sub3'] as $r3)
                                  <option value="{{ $r3->master_id }}"
                                    data-master-id="{{ $r3->master_id }}"
                                    data-mst-kode-rekening="{{ $r3->mst_kode_rekening }}"
                                    data-mst-nama-rekening="{{ $r3->mst_nama_rekening }}"
                                    data-content="{{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}">
                                    {{ $r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening }}
                                  </option>
                                @endforeach
                              @endif
                            @endforeach
                          @endif
                        @endforeach
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 control-label">KM Awal</label>
                  <div class="col-md-9">
                    <input type="number" min="0" value="0" class="form-control" name="km_awal" required>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-3 control-label">KM Akhir</label>
                  <div class="col-md-9">
                    <input type="number" min="0" value="0" class="form-control" name="km_akhir" required>
                  </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="fixed">
    <div class="row">
      <div class="col-xs-3 col-md-3 col-lg-3">
        <ul class="nav nav-tabs nav-lg nav-justified no-margin no-border-radius text-center" style="border: 2px solid black;">
          <li class="bg-info">
            <div class="text-white text-uppercase" aria-expanded="true">
              <h4><b>Total</b></h4>
              <h4 id="total">RP.{{number_format($total_biaya, 2, "," ,".")}}</h4>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>

<style media="screen">
div.fixed {
  position: fixed;
  bottom: 0;
  left: 0;
}
</style>
@stop
