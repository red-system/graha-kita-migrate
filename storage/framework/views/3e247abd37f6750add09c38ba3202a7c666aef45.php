<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
  <script src="js/dashboard.js" charset="utf-8"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <span id="data-back" data-form-token="<?php echo e(csrf_token()); ?>"
  data-route-dashboard-chart="<?php echo e(route('dashboardChartData')); ?>"
  data-route-dashboard-chart-kategori="<?php echo e(route('dashboardChartKategori')); ?>"
  data-route-dashboard-chart-hourly="<?php echo e(route('dashboardChartHourly')); ?>"
  data-route-dashboard-chart-weekly="<?php echo e(route('dashboardChartWeekly')); ?>"
  data-route-dashboard-chart-daily="<?php echo e(route('dashboardChartDaily')); ?>"
  data-route-dashboard-chart-monthly="<?php echo e(route('dashboardChartMonthly')); ?>"
  ></span>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            <input type="date" name="start_date" id="dashboard-start" class="form-control">
          </div>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            <input type="date" name="end_date" id="dashboard-end" class="form-control">
          </div>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
          <div class="input-group">
            <button type="button" name="button" id="dashboard-search" class="btn btn-info">View</button>
          </div>
        </div>
      </div>
      <br>
      <h4><b>Sales Summary</b></h4>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2 ">
            <div class="display">
              <div class="number">
                <h3 class="font-purple-soft">
                  <small class="font-purple-soft">Rp.</small>
                  <span id="total" data-counter="counterup" data-value="<?php echo e(number_format($total, 0, "." ,".")); ?>"><?php echo e(number_format($total, 0, "." ,".")); ?></span>
                </h3>
                <br>
                <small>Total</small>
              </div>
            </div>
            <div class="progress-info">
              <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2 ">
            <div class="display">
              <div class="number">
                <h3 class="font-green-sharp">
                  <small class="font-green-sharp">Rp.</small>
                  <span id="sales_summary" data-counter="counterup" data-value="<?php echo e(number_format($gross, 0, "." ,".")); ?>"><?php echo e(number_format($gross, 0, "." ,".")); ?></span>
                </h3>
                <br>
                <small>Gross PROFIT</small>
              </div>
            </div>
            <div class="progress-info">
              <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2 ">
            <div class="display">
              <div class="number">
                <h3 class="font-red-haze">
                  <small class="font-red-haze">Rp.</small>
                  <span id="net_profit" data-counter="counterup" data-value="<?php echo e(number_format($net, 0, "." ,".")); ?>"><?php echo e(number_format($net, 0, "." ,".")); ?></span>
                </h3>
                <br>
                <small>Net Profit</small>
              </div>
            </div>
            <div class="progress-info">
              <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2 ">
            <div class="display">
              <div class="number">
                <h3 class="font-blue-sharp">
                  <span id="transaction" data-counter="counterup" data-value="<?php echo e($transaksi); ?>"><?php echo e($transaksi); ?></span>
                </h3>
                <br>
                <small>Number Of Transaction</small>
              </div>
            </div>
            <div class="progress-info">
              <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                </span>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h4><b>Transaction</b></h4>
          <div class="portlet light">
            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data">
              <tr>
                <td><b>Penjualan Cash</b></td>
                <td id="cash">Rp.<?php echo e(number_format($cash, 0, "." ,".")); ?></td>
              </tr>
              <tr>
                <td><b>Penjualan Credit</b></td>
                <td id="credit">Rp.<?php echo e(number_format($credit, 0, "." ,".")); ?></td>
              </tr>
              <tr>
                <td><b>EDC</b></td>
                <td id="EDC">Rp.<?php echo e(number_format($EDC, 0, "." ,".")); ?></td>
              </tr>
              <tr>
                <td><b>BG/Cheques</b></td>
                <td id="BG">Rp.<?php echo e(number_format($BG, 0, "." ,".")); ?></td>
              </tr>
              <tr>
                <td><b>Transfer</b></td>
                <td id="TRF">Rp.<?php echo e(number_format($TRF, 0, "." ,".")); ?></td>
              </tr>
              <tr>
                <td><b>Total</b></td>
                <td id="TTL">Rp.<?php echo e(number_format($TTL, 0, "." ,".")); ?></td>
              </tr>
              <tr>
                <td><b>Net Profit</b></td>
                <td id="NET">Rp.<?php echo e(number_format($net, 0, "." ,".")); ?></td>
              </tr>
              
            </table>
          </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
          <h4><b>Sales Product Category</b></h4>
          <div class="portlet light">
            <div class="row">
              <div id="div-product_category" class="chart-container col-md-6">
                <canvas id="product_category"></canvas>
              </div>
              <div id="div-tb_kategori" class="col-md-6" style="overflow-y: scroll;">
                <h4 id="m_data">Month (<?php echo e($m_start); ?>/<?php echo e($y_start); ?> - <?php echo e($m_end); ?>/<?php echo e($y_end); ?>)</h4>
                <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data" id="sample_2">
                  <thead>
                    <tr>
                      <th>Category</th>
                      <th>Sold</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <h4><b>Daily Gross Sales Amount</b></h4>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div id="div-daily_gross" class="portlet light chart-container">
            <canvas id="daily_gross"></canvas>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <h4><b>Weekly Gross Sales Amount</b></h4>
          <div id="div-weekly_gross" class="portlet light chart-container">
            <canvas id="weekly_gross"></canvas>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <h4><b>Hourly Gross Sales Amount</b></h4>
          <div id="div-hourly_gross" class="portlet light chart-container">
            <canvas id="hourly_gross"></canvas>
          </div>
        </div>
      </div>
      <h4><b>Monthly Gross Sales Amount</b></h4>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div id="div-monthly_gross" class="portlet light chart-container">
            <canvas id="monthly_gross"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>