@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.min.css" />
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#sample_1').on( 'draw.dt', function () {
      $('.btn_view_img').click(function() {
        var ini = $(this);
        var href = ini.data('href');
        var imageJQ = $("#image");
        imageJQ.attr("src", href);

        var el = document.getElementById('image');
        var viewer = new Viewer(el, {
          inline: false,
          viewed: function() {
            viewer.zoomTo(1);
          }
        });
        imageJQ.trigger( "click" );
      });
    });
  });
  </script>
@stop

@section('body')
  <div class="hidden">
    <img id="image" alt="Picture">
  </div>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th> No </th>
                    <th> Kode Order </th>
                    <th> Tanggal Order </th>
                    <th> Kode Pelanggan </th>
                    <th> Nama Pelanggan </th>
                    <th> Sales Person </th>
                    <th> Status </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td> {{ $no++ }}. </td>
                      <td> {{ $row->order_kode }} </td>
                      <td> {{ $row->order_tgl }} </td>
                      <td> {{ $kodeCustomer.$row->customer['cus_kode'] }} </td>
                      <td> {{ $row->customer['cus_nama'] }} </td>
                      <td> {{ $row->karyawan['kry_nama'] }} </td>
                      @if ($row->status == 1)
                        <td> Confirm </td>
                      @else
                        <td> Cancel </td>
                      @endif
                      <td style="font-size:12px; white-space: nowrap">
                        <div class="btn-group-xs">
                          <a href="{{ route('history-orderDetail', ['kode'=>$row->order_kode]) }}" type="button" class="btn btn-success">
                            <i class=" icon-pencil" title="Detail"></i> Detail
                          </a>
                          @if ($row->img)
                            <a data-href="{{ asset($row->img) }}" type="button" class="btn btn-info btn_view_img">
                              <i class="icon-eye" title="SP"></i> SP
                            </a>
                          @endif
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
