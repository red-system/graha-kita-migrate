<table>
  <thead>
    <tr>
      <th>Barang Kode</th>
      <th>Barcode</th>
      <th>Nama Barang</th>
      <th>Gudang</th>
      <th>Merek</th>
      <th>Kategori</th>
      <th>Satuan</th>
      <th>Group</th>
      <th>Supplier</th>
      <th>Stok Max</th>
      <th>Stok Min</th>
      <th>Harga Beli Tertinggi</th>
      <th>Harga Beli Terakhir</th>
      <th>PPN Supplier</th>
      <th>Harga Beli + PPN</th>
      <th>HPP</th>
      <th>Harga Jual Eceran</th>
      <th>Harga Jual Partai</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach($barang as $item)
      <tr>
        <td>{{ $item->brg_kode }}</td>
        <td>{{ $item->brg_barcode }}</td>
        <td>{{ $item->brg_nama }}</td>
        <td>{{ $item->gudang['gdg_nama'] }}</td>
        <td>{{ $item->merek['mrk_nama'] }}</td>
        <td>{{ $item->kategory['ktg_nama'] }}</td>
        <td>{{ $item->satuan['stn_nama'] }}</td>
        <td>{{ $item->group['grp_nama'] }}</td>
        <td>{{ $item->supplier['spl_nama'] }}</td>
        <td>{{ $item->brg_stok_maximum }}</td>
        <td>{{ $item->brg_stok_minimum }}</td>
        <td>{{ $item->brg_harga_beli_tertinggi }}</td>
        <td>{{ $item->brg_harga_beli_terakhir }}</td>
        <td>{{ $item->brg_ppn_dari_supplier_persen }}</td>
        <td>{{ $item->hargaPPN }}</td>
        <td>{{ $item->brg_hpp }}</td>
        <td>{{ $item->brg_harga_jual_eceran }}</td>
        <td>{{ $item->brg_harga_jual_partai }}</td>
        <td>{{ $item->brg_status }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
