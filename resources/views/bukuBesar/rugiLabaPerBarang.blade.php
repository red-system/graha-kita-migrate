<?php
use App\Models\mReturPenjualan;
?>
@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                <div class="col-md-6 col-xs-6">
                                    <a class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                                        Pilih Periode
                                    </a>
                                    <a type="button" class="btn btn-danger" href="{{route('printRugiLabaPerBarang', ['start_date'=>$start_date, 'end_date'=>$end_date, 'tipe'=>'print'])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                    <a class="btn btn-primary excel-btn" data-toggle="modal" type="button" href="#export-excel">
                                        Excel
                                    </a>
                                </div>
                                    
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h1><center>Laporan Rugi/Laba Per Barang</center></h1>
                                        <h2><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h2>
                                    </div>
                                                                     
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                                        <thead>
                                        <tr class="success">
                                                <th style="font-size:12px;" rowspan="2" align="center"><center> No </center></th>
                                                <th style="font-size:12px;" rowspan="2" align="center"><center> Kode Stock </center></th>
                                                <th style="font-size:12px;" rowspan="2" align="center"><center> Nama Stock </center></th>
                                                <th style="font-size:12px;" colspan="2" align="center"><center> Penjualan </center></th>
                                                <th style="font-size:12px;" colspan="2" align="center"><center> Retur </center></th>
                                                <th style="font-size:12px;" rowspan="2" align="center"><center> R/L </center></th>
                                            </tr>
                                            <tr class="success" >
                                                <th style="font-size:12px;" align="center"><center> Penjualan </center></th>
                                                <th style="font-size:12px;" align="center"><center> HPP </center></th>
                                                <th style="font-size:12px;" align="center"><center> Retur </center></th>
                                                <th style="font-size:12px;" align="center"><center> HPP </center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $total_penjualan = 0;
                                            $total_hpp_penjualan = 0;
                                            $total_retur = 0;
                                            $total_hpp_retur = 0;
                                            $total_rugi_laba = 0;
                                                
                                        ?>
                                            @foreach($barang as $brg)
                                            <?php
                                                $rugi_laba = ($item['total_jual'.$brg->brg_kode]-$item['hpp_jual'.$brg->brg_kode])-($item['total_retur'.$brg->brg_kode]-$item['hpp_retur'.$brg->brg_kode]);
                                            ?>
                                            @if($item['total_jual'.$brg->brg_kode]!=0 || $item['hpp_jual'.$brg->brg_kode]!=0 || $item['total_retur'.$brg->brg_kode]!=0 || $item['hpp_retur'.$brg->brg_kode]!=0)
                                                <tr>
                                                    <td style="font-size:11px;" align="center">{{$no++}}</td>
                                                    <td style="font-size:11px;">{{$brg->brg_kode}}</td>
                                                    <td style="font-size:11px;">{{$brg->brg_nama}}</td>
                                                    <td style="font-size:11px;">{{number_format($item['total_jual'.$brg->brg_kode],2)}}</td>
                                                    <td style="font-size:11px;">{{number_format($item['hpp_jual'.$brg->brg_kode],2)}}</td>
                                                    <td style="font-size:11px;">{{number_format($item['total_retur'.$brg->brg_kode],2)}}</td>
                                                    <td style="font-size:11px;">{{number_format($item['hpp_retur'.$brg->brg_kode],2)}}</td>
                                                    <td style="font-size:11px;">{{number_format($rugi_laba,2)}}</td>
                                                </tr>
                                                <?php
                                                    $total_penjualan        = $total_penjualan+$item['total_jual'.$brg->brg_kode];
                                                    $total_hpp_penjualan    = $total_hpp_penjualan+$item['hpp_jual'.$brg->brg_kode];
                                                    $total_retur            = $total_retur+$item['total_retur'.$brg->brg_kode];
                                                    $total_hpp_retur        = $total_hpp_retur+$item['hpp_retur'.$brg->brg_kode];
                                                    $total_rugi_laba        = $total_rugi_laba+$rugi_laba;
                                                ?>
                                                @endif
                                            @endforeach
                                            <?php
                                                    $total_penjualan        = $total_penjualan+$disc_nom;
                                                    // $total_hpp_penjualan    = $total_hpp_penjualan+$disc_nom;
                                                    // $total_retur            = $total_retur+$item['total_retur'.$brg->brg_kode];
                                                    // $total_hpp_retur        = $total_hpp_retur+$item['hpp_retur'.$brg->brg_kode];
                                                    $total_rugi_laba        = $total_rugi_laba+$disc_nom;
                                                ?>
                                            <tr>
                                                <td style="font-size:11px;" align="center">{{$no++}}</td>
                                                <td style="font-size:11px;">Total Diskon</td>
                                                <td style="font-size:11px;"></td>
                                                <td style="font-size:11px;">{{number_format($disc_nom,2)}}</td>
                                                <td style="font-size:11px;"></td>
                                                <td style="font-size:11px;"></td>
                                                <td style="font-size:11px;"></td>
                                                <td style="font-size:11px;"></td>
                                            </tr>                             
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="font-size:12px;" colspan="3">Grand Total</td>
                                                <td style="font-size:12px;" align="right">{{number_format($total_penjualan,2)}}</td>
                                                <td style="font-size:12px;" align="right">{{number_format($total_hpp_penjualan,2)}}</td>
                                                <td style="font-size:12px;" align="right">{{number_format($total_retur,2)}}</td>
                                                <td style="font-size:12px;" align="right">{{number_format($total_hpp_retur,2)}}</td>
                                                <td style="font-size:12px;" align="right">{{number_format($total_rugi_laba,2)}}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeRugiLabaPerBarang') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="export-excel" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('rugiLabaPerBarangPrintExcel') }}" class="form-horizontal" role="form" method="post"  target="_blank">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="{{$end_date}}"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop