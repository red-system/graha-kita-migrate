<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/font/style.css') }}">
  <title></title>
  <style>
  /* .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
  .tt td{font-family:Tahoma;font-size:11px;padding-top: 0px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
  .tt th{font-family:Tahoma;font-size:11px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#333;background-color:#f0f0f0;}
  .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; padding:5px;}
  .tg td{font-family:Tahoma;font-size:11px;padding:5px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;}
  .tg .tg-3wr7{font-weight:bold;font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;} */

  @media print {
    html, body {
    display: block;
    /* font-family: "Tahoma"; */
    margin: 0px 0px 0px 0px;
    }

    @page {
      size: 7.6cm;
      /* size: 5.8cm; */
    }

    /* #footer {
      position: fixed;
      bottom: 0;
    } */
  }

  p {
    font-size: 10px;
    letter-spacing: 1px;
    padding: 0.1mm;
    margin: 0.1mm;
    font-family:'FakeReceipt-Regular';
  }
  table {
    border-collapse: collapse;
    padding: 0 !important;
    margin: 0 !important;
  }
  tr td{
    padding: 0 !important;
    margin: 0 !important;
  }
  </style>
</head>
<body>
  <table id="header" width="100%" border="0">
    <thead>
      <tr>
        <td>
          <p>GRAHA KITA 18</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Jl. Gatot Subroto Barat No. 88A</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Tlp. (0361)416088</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Fax.(0361)418933</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>www.grahakita18.com</p>
        </td>
      </tr>
      <tr>
        <td colspan="4">
          <hr>
        </td>
      </tr>
      <tr>
        <td> <p>No. Ftr : {{$faktur['kode_bukti_id']}}</p> </td>
      </tr>
      <tr>
        <td> <p>Tgl : {{$faktur['tglPrint']}}</p> </td>
      </tr>
      <tr>
        <td colspan="4">
          <hr>
        </td>
      </tr>
    </thead>
  </table>

  <table id="body" width="100%" border="0">
    <thead>
      <tr>
        <th align="left" colspan="4"> <p>Nama Barang</p> </th>
      </tr>
      <tr>
        <th align="left" weight="40%"> <p>Harga (Rp.)</p> </th>
        <th align="center" weight="20%" colspan="2"> <p>Qty</p> </th>
        <th align="right" weight="40%"> <p>Total (Rp.)</p> </th>
      </tr>
      <tr>
        <td colspan="4">
          <hr>
        </td>
      </tr>
    </thead>
    <tbody>
      @foreach ($faktur->detail as $key)
        <tr>
          <td align="left" colspan="4"><p>{{$key['nama_barang']}}</p></td>
        </tr>
        <tr>
          <td weight="40%" align="left"><p>{{number_format($key['harga_net'], 2, "." ,",")}}</p></td>
          <td weight="20%" align="center" colspan="2"><p>{{number_format($key['qty'], 2, "." ,",")}}</p></td>
          <td weight="40%" align="right"><p>{{number_format($key['total'], 2, "." ,",")}}</p></td>
        </tr>
      @endforeach
      <tr>
        <td colspan="4">
          <hr>
        </td>
      </tr>
    </tbody>
  </table>

  <table id="footer" width="100%" border="0">
    <thead>
      @if ($faktur['charge_nom'] != 0)
        <tr>
          <td colspan="2">
            <p>Charge</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['charge_nom'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      @if ($faktur['pl_ongkos_angkut'] != 0)
        <tr>
          <td colspan="2">
            <p>Ongkos Angkut</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['pl_ongkos_angkut'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      @if ($faktur['pl_disc_nom'] != 0)
        <tr>
          <td colspan="2">
            <p>Discount</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['pl_disc_nom'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      <tr>
        <td colspan="2">
          <p>Grand Total</p>
        </td>
        <td>
          <p>Rp. </p>
        </td>
        <td align="right">
          <p>{{number_format($faktur['grand_total'], 2, "." ,",")}}</p>
        </td>
      </tr>

      @if ($faktur['bayar'] != 0)
        <tr>
          <td colspan="2">
            <p>Cash</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['bayar'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      @if ($faktur['transfer'] != 0)
        <tr>
          <td colspan="2">
            <p>Transfer</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['transfer'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      @if ($faktur['cek_bg'] != 0)
        <tr>
          <td colspan="2">
            <p>Cek/BG</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['cek_bg'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      @if ($faktur['edc'] != 0)
        <tr>
          <td colspan="2">
            <p>EDC</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['edc'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      @if ($faktur['piutang'] != 0)
        <tr>
          <td colspan="2">
            <p>Piutang</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['piutang'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif

      {{-- @if ($faktur['sisa_uang'] > 0)
        <tr>
          <td colspan="2">
            <p>Sisa</p>
          </td>
          <td>
            <p>Rp. </p>
          </td>
          <td align="right">
            <p>{{number_format($faktur['sisa_uang'], 2, "." ,",")}}</p>
          </td>
        </tr>
      @endif --}}

      <tr>
        <td colspan="2">
          <p>Kembalian</p>
        </td>
        <td>
          <p>Rp. </p>
        </td>
        <td align="right">
          <p>{{number_format($faktur['kembalian_uang'], 2, "." ,",")}}</p>
        </td>
      </tr>

      <tr>
        <td colspan="4">
          <br>
          <br>
        </td>
      </tr>

      <tr>
        <td align="center" colspan="4">
          <p>Barang yg telah dibeli tdk dapat dikembalikan/ditukar. Harga Telah Termasuk PPN</p>
        </td>
      </tr>
    </thead>
  </table>
</body>
</html>

<script type="text/javascript">
  window.print();
</script>
