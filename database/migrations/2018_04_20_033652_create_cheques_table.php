<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_cheques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_bg_cek');
            $table->datetime('tgl_pencairan');
            $table->integer('cek_amount');
            $table->string('cek_dari');
            $table->text('cek_keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_cheques');
    }
}
