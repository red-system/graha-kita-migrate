<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <div>
      <div>
        <div>
          <table width="100%">
            <tr>
              <td>
                <img src="http://grahakita.ganesh-demo.online/img/icon/aki.png" width="70">
              </td>
              <td colspan="3">
                <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088 Fax.(0361)418933</p>
              </td>
            </tr>
          </table>
          <div style="float: left; width: 60%;">
            <table width="100%" border="0">
              <tr>
                <td width="10%"> <p><b>D/O Barang</b></p> </td>
              </tr>
              <tr>
                <td width="10%"> <p><b>Untuk</b></p> </td>
              </tr>
            </table>
          </div>

          <div style="float: right; width: 40%;">
            <table width="100%" border="0">
              <tr>
                <td width="15%"> <p>No. Ftr</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['kode_bukti_id']}}</p> </td>
              </tr>
              <tr>
                <td width="15%"> <p>Tgl.</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['tglPrint']}}</p> </td>
              </tr>
              <tr>
                <td width="15%"> <p>Yth</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['cus_nama']}}</p> </td>
              </tr>
            </table>
          </div>

          <div class="tlp" style="clear: both; margin: 0pt; padding: 0pt;">
            <table width="30%" border="0">
              <tr>
                <td width="15%"> <p>Tlp.</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td> <p>{{$faktur['cus_telp']}}</p> </td>
              </tr>
            </table>
          </div>
          <div class="index" style="clear: both; margin: 0pt; padding: 0pt;">
            <table width="100%" border="1">
              <thead>
                <tr>
                  <th> <p>No</p> </th>
                  <th> <p>Kode Barang</p> </th>
                  <th> <p>Nama Barang</p> </th>
                  <th> <p>Pengambilan</p> </th>
                  <th> <p>Satuan</p> </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($faktur->detail as $key)
                  <tr>
                    <td><p>{{$faktur->no++}}</p></td>
                    <td><p>{{$key->barang['brg_barcode']}}</p></td>
                    <td><p>{{$key['nama_barang']}}</p></td>
                    <td><p>{{number_format($key['qty'], 0, "." ,".")}}</p></td>
                    <td><p>{{$key->satuanJ->stn_nama}}</p></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="keterangan" style="clear: both; margin: 0pt; padding: 0pt;">
            <table width="100%" border="0">
              <tr>
                <td width="100%">
                  <p>D/O ini berlaku 10 (sepuluh) hari, mulai tanggal pengeluaran D/O ini</p>
                </td>
              </tr>
            </table>
          </div>
          <br>
          <div style="float: left; width: 33%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Yang Menerima D/O</h5> --}}
                    <b><p>Yang Menerima D/O</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style="float: left; width: 33%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Dari : ANGSA KUSUMA INDAH</h5> --}}
                    <b><p>Dari : ANGSA KUSUMA INDAH</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style="float: right; width: 34%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Penerima Barang</h5> --}}
                    <b><p>Penerima Barang</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </body>
</html>
