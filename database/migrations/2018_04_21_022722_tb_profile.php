<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_profile', function (Blueprint $table) {
           $table->increments('prf_kode');
           $table->string('prf_alamat')->nullable();
           $table->string('prf_telp')->nullable();
           $table->string('prf_fax')->nullable();
           $table->text('prf_TC')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_profile');
     }
}
