<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailPoSupplier extends Model
{
  public $incrementing = false;
  protected $table = 'tb_detail_po_supplier';
  protected $primaryKey = 'detail_pok_kode';
  public $timestamps = false;
  
  
  public function gudangs(){
      return $this->belongsTo(mGudang::class,'gudang','gdg_kode');
  }

  public function satuans(){
    return $this->belongsTo(mSatuan::class,'satuan','stn_kode');
  }

  public function stok(){
    return $this->belongsTo(mStok::class,'stok_id','stk_kode');
  }

  public function brg(){
    return $this->belongsTo(mBarang::class,'brg_kode','brg_kode');
  }
  
}
