@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="col-md-6 col-xs-6">
                            <a class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                                Pilih Periode
                            </a>
                            <a type="button" class="btn btn-danger" href="{{route('printLaporanPembelian', ['start_date'=>$start_date, 'end_date'=>$end_date])}}" target="_blank">
                                <span><i class="fa fa-print"></i></span> Print
                            </a>
                        </div>
                        <div class="col-md-12">
                            <h2><center>Laporan Pembelian Supplier</center></h2>
                            <h3><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h3> 
                                            
                        </div>
                        <br /><br />
                        <!-- <div class="col-md-10">                         -->
                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                <thead>
                                    <tr class="success">
                                        <th width="10"> No </th>
                                        <th> Tanggal</th>
                                        <th> No Invoice </th>
                                        <th> Barcode </th>
                                        <th> Nama Barang </th>
                                        <th> Qty </th>
                                        <th> Satuan </th>
                                        <th> Harga </th>
                                        <th> Total </th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($jml_pembelian == 0)
                                    <tr>
                                        <td colspan="9"  align="center">No Data Available</td>
                                    </tr>
                                @endif
                                @if($jml_pembelian > 0)
                                @foreach($dataList as $poS)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td colspan="8"><strong> {{ $poS->supplier->spl_nama }} </strong></td>
                                </tr>
                                    @foreach($poS->detailPembelian as $detail)
                                    <tr>
                                        <td></td>
                                        <td>{{ date('d M Y', strtotime($poS->ps_tgl)) }}</td>
                                        <td>{{$poS->no_invoice}}</td>
                                        <td>{{$detail->brg->brg_barcode}}</td>
                                        <td>{{$detail->brg->brg_nama}}</td>
                                        <td>{{$detail->qty}}</td>
                                        <td>{{$detail->satuans->stn_nama}}</td>
                                        <td>{{number_format($detail->harga_net)}}</td>
                                        <td>{{number_format($detail->total)}}</td>
                                    </tr>
                                    @endforeach
                                <tr>
                                    <td align="right" colspan="8"><strong>Grand Total</strong></td>
                                    <td><strong>{{number_format($poS->grand_total)}}</strong></td>
                                </tr>
                                @endforeach
                                @endif                                
                                </tbody>
                            </table>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodePembelian') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
