<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailSuratJalanPL extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_detail_surat_jalan_penjualan_langsung';
  protected $primaryKey = 'det_sjl_kode';
  // public $timestamps = false;

  public function SuratJalan()
  {
    return $this->belongsTo('App\Models\mSuratJalanPL', 'sjl_kode');
  }

  public function Barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode');
  }

  public function Gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'gdg_kode');
  }
}
