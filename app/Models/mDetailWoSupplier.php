<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailWoSupplier extends Model
{
  public $incrementing = false;
  protected $table     = 'tb_detail_wo';
  protected $primaryKey = 'id_detail_wo';
  public $timestamps   = false;

  protected $guarded =[];
  
  
  public function gudangs(){
      return $this->belongsTo(mGudang::class,'gudang','gdg_kode');
  }

  public function satuans(){
    return $this->belongsTo(mSatuan::class,'satuan','stn_kode');
  }

  public function stok(){
    return $this->belongsTo(mStok::class,'stock_id','stk_kode');
  }

  public function brg(){
    return $this->belongsTo(mBarang::class,'brg_kode','brg_kode');
  }

  public function detail_po(){
    return $this->belongsTo(mDetailPoSupplier::class,'id_detail_po','detail_pok_kode');
  }
  
}
