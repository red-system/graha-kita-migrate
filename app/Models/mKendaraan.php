<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mKendaraan extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  public $incrementing = false;
  protected $table = 'tb_kendaraan';
  protected $primaryKey = 'kdn_kode';
  public $timestamps = false;
}
