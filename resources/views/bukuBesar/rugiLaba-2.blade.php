@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                <div class="col-md-6 col-xs-6">
                                    <a class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                                        Pilih Periode
                                    </a>
                                    <a type="button" class="btn btn-danger" href="{{route('printRugiLaba', ['bulan'=>$bulan, 'tahun'=>$tahun_periode])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                </div>
                                    
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h1><center>Laporan Rugi Laba</center></h1>
                                        <h2><center>Periode {{$tanggal[$bulan]}} {{$tahun_periode}}</center></h2>
                                    </div>
                                                                     
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                                        <thead>
                                            <tr class="success" >
                                                <th rowspan="2" width="10%" align="center"><center> Kode Rekening </center></th>
                                                <th rowspan="2" width="60%" align="center"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                            <tr class="success">
                                                <th colspan="3"><center>{{$tanggal[$bulan]}}</center></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                        <?php
                                            $total_pendapatan=0;
                                        ?>
                                        @foreach($kode_pendapatan as $pendapatan)
                                            <tr >
                                                <td align="center"><strong>{{$pendapatan->mst_kode_rekening}}</strong></td>
                                                <td><strong>{{$pendapatan->mst_nama_rekening}}</strong></td>
                                                <?php
                                                    if($pendapatan->mst_normal == 'kredit'){                                                        
                                                        $total_pendapatan=$total_pendapatan+$pendapatan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($pendapatan->mst_normal == 'debet'){
                                                        $total_pendapatan=$total_pendapatan-$pendapatan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>                                                
                                                <td colspan="3" align="right">{{number_format($biaya[$pendapatan->mst_kode_rekening])}} </td>
                                            </tr>
                                            @foreach($pendapatan->childs as $pendapatanUsaha)
                                            <tr >
                                                <td align="center">{{$pendapatanUsaha->mst_kode_rekening}}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$pendapatanUsaha->mst_nama_rekening}}</td>
                                                <?php
                                                    if($pendapatanUsaha->mst_normal == 'kredit'){
                                                        $total_pendapatan=$total_pendapatan+$pendapatanUsaha->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($pendapatanUsaha->mst_normal == 'debet'){
                                                        $total_pendapatan=$total_pendapatan-$pendapatanUsaha->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                               <td colspan="3" align="right">{{number_format($biaya[$pendapatanUsaha->mst_kode_rekening])}}</td>                                                                                           
                                            </tr>
                                            @foreach($pendapatanUsaha->childs as $penjualan)
                                            <tr >
                                                <td align="center">{{$penjualan->mst_kode_rekening}}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$penjualan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($penjualan->mst_normal == 'kredit'){
                                                        $total_pendapatan=$total_pendapatan+$penjualan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($penjualan->mst_normal == 'debet'){
                                                        $total_pendapatan=$total_pendapatan-$penjualan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                               <td colspan="3" align="right">{{number_format($biaya[$penjualan->mst_kode_rekening])}}</td>                                                                                           
                                            </tr>
                                            @foreach($penjualan->childs as $penjualanChilds)
                                            <tr >
                                                <td align="center">{{$penjualanChilds->mst_kode_rekening}}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$penjualanChilds->mst_nama_rekening}}</td>
                                                <?php
                                                    if($penjualanChilds->mst_normal == 'kredit'){
                                                        $total_pendapatan=$total_pendapatan+$penjualanChilds->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($penjualanChilds->mst_normal == 'debet'){
                                                        $total_pendapatan=$total_pendapatan-$penjualanChilds->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                               <td colspan="3" align="right">{{number_format($biaya[$penjualanChilds->mst_kode_rekening])}}</td>
                                                                                                                                             
                                            </tr>
                                            @endforeach
                                            @endforeach
                                            @endforeach
                                        @endforeach
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> {{number_format($total_pendapatan)}} </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <?php
                                                $total_hpp=0;
                                            ?>
                                        @foreach($kode_hpp as $hpp)
                                            <tr >
                                                <td align="center"><strong>{{$hpp->mst_kode_rekening}}</strong></td>
                                                <td><strong>&nbsp;&nbsp;{{$hpp->mst_nama_rekening}}</strong></td><?php
                                                    if($hpp->mst_normal == 'kredit'){
                                                        $total_hpp=$total_hpp-$hpp->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($hpp->mst_normal == 'debet'){
                                                        $total_hpp=$total_hpp+$hpp->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right"><strong>{{number_format($biaya[$hpp->mst_kode_rekening])}}</strong> </td>                                                                     
                                            </tr>
                                            @foreach($hpp->childs as $hppChild)
                                            <tr >
                                                <td align="center"><strong>{{$hppChild->mst_kode_rekening}}</strong></td>
                                                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;{{$hppChild->mst_nama_rekening}}</strong></td><?php
                                                    if($hppChild->mst_normal == 'kredit'){
                                                        $total_hpp=$total_hpp-$hppChild->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($hppChild->mst_normal == 'debet'){
                                                        $total_hpp=$total_hpp+$hppChild->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right"><strong>{{number_format($biaya[$hppChild->mst_kode_rekening])}}</strong> </td>                                                                
                                            </tr>
                                            @foreach($hppChild->childs as $hppChild2)
                                            <tr >
                                                <td align="center">{{$hppChild2->mst_kode_rekening}}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$hppChild2->mst_nama_rekening}}</td>
                                                <?php
                                                    if($hppChild2->mst_normal == 'kredit'){
                                                        $total_hpp=$total_hpp-$hppChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($hppChild2->mst_normal == 'debet'){
                                                        $total_hpp=$total_hpp+$hppChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right">{{number_format($biaya[$hppChild2->mst_kode_rekening])}} </td>                                                                            
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        @endforeach
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><strong><h4> {{number_format($total_hpp)}} </h4></strong></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <tr class="info">
                                                <td colspan="2" align="center"><h3><strong> LABA (RUGI) KOTOR </strong></h3></td>
                                                <td align="right"><h3> <strong>{{number_format($total_pendapatan-$total_hpp)}}</strong> </h3></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                        <?php
                                            $total_biaya=0;
                                        ?>
                                        @foreach($kode_biaya as $biaya2)
                                            <tr >
                                                <td align="center"><strong>{{$biaya2->mst_kode_rekening}}</strong></td>
                                                <td><strong>&nbsp;&nbsp;{{$biaya2->mst_nama_rekening}}</strong></td>
                                                <?php
                                                    if($biaya2->mst_normal == 'kredit'){
                                                        $total_biaya=$total_biaya-$biaya2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($biaya2->mst_normal == 'debet'){
                                                        $total_biaya=$total_biaya+$biaya2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right"><strong>{{number_format($biaya[$biaya2->mst_kode_rekening])}}</strong> </td>                                                               
                                            </tr>
                                            @foreach($biaya2->childs as $biayaChild)
                                            <tr >
                                                <td align="center"><strong>{{$biayaChild->mst_kode_rekening}}</strong></td>
                                                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;{{$biayaChild->mst_nama_rekening}}</strong></td>
                                                <?php
                                                    if($biayaChild->mst_normal == 'kredit'){
                                                        $total_biaya=$total_biaya-$biayaChild->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($biayaChild->mst_normal == 'debet'){
                                                        $total_biaya=$total_biaya+$biayaChild->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right"><strong>{{number_format($biaya[$biayaChild->mst_kode_rekening])}}</strong> </td>                                                                                           
                                            </tr>
                                            @foreach($biayaChild->childs as $biayaChild2)
                                            <tr >
                                                <td align="center">{{$biayaChild2->mst_kode_rekening}}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$biayaChild2->mst_nama_rekening}}</td>
                                                <?php
                                                    if($biayaChild2->mst_normal == 'kredit'){
                                                        $total_biaya=$total_biaya-$biayaChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($biayaChild2->mst_normal == 'debet'){
                                                        $total_biaya=$total_biaya+$biayaChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right">{{number_format($biaya[$biayaChild2->mst_kode_rekening])}} </td>                                                                                             
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        @endforeach
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> {{number_format($total_biaya)}} </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <?php
                                                $total_pendapatan_diluar_usaha=0;
                                            ?>
                                        @foreach($kode_pendapatan_diluar_usaha as $pendapatan_diluar_usaha)
                                            <tr >
                                                <td align="center"><strong>{{$pendapatan_diluar_usaha->mst_kode_rekening}}</strong></td>
                                                <td><strong>&nbsp;&nbsp;{{$pendapatan_diluar_usaha->mst_nama_rekening}}</strong></td>
                                                <?php
                                                    if($pendapatan_diluar_usaha->mst_normal == 'kredit'){
                                                        $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha+$pendapatan_diluar_usaha->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($pendapatan_diluar_usaha->mst_normal == 'debet'){
                                                        $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha-$pendapatan_diluar_usaha->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right">{{number_format($biaya[$pendapatan_diluar_usaha->mst_kode_rekening])}} </td>                                                                                            
                                            </tr>
                                            @foreach($pendapatan_diluar_usaha->childs as $pendapatan_diluar_usahaChild)
                                            <tr >
                                                <td align="center">{{$pendapatan_diluar_usahaChild->mst_kode_rekening}}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$pendapatan_diluar_usahaChild->mst_nama_rekening}}</td>
                                                <?php
                                                    if($pendapatan_diluar_usahaChild->mst_normal == 'kredit'){
                                                        $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha+$pendapatan_diluar_usahaChild->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($pendapatan_diluar_usahaChild->mst_normal == 'debet'){
                                                        $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha-$pendapatan_diluar_usahaChild->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right">{{number_format($biaya[$pendapatan_diluar_usahaChild->mst_kode_rekening])}} </td>                                                                                             
                                            </tr>
                                            @endforeach
                                        @endforeach
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> {{number_format($total_pendapatan_diluar_usaha)}} </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <tr class="info">
                                                <td colspan="2" align="center"><h3><strong> LABA (RUGI) BERSIH </strong></h3></td>
                                                <td align="right"><h3><strong> {{number_format($total_pendapatan-$total_hpp-$total_biaya+$total_pendapatan_diluar_usaha)}} </strong></h3></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeRugiLaba') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Bulan</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-inline input-medium" name="bulan" required>
                                    <option value="">Pilih Bulan</option>
                                    @foreach($tanggal as $tgl => $value)
                                    <option value="{{$tgl}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Tahun</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-inline input-medium" name="tahun" required>
                                    <option value="">--Pilih Tahun--</option>
                                    @foreach($tahun as $thn)
                                    <option value="{{$thn}}">{{$thn}}</option>
                                    @endforeach
                                </select>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop