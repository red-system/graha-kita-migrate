<?php

namespace App\Exports;

use App\Models\mCustomer;
use App\Models\mPenjualanTitipan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PenjualanTitipanExport implements FromView
{
    private $start;
    private $end;
    private $cus_kode;

    public function __construct($start, $end, $cus_kode)
    {
      $this->start = $start;
      $this->end = $end;
      $this->cus_kode = $cus_kode;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;
      $cus_kode = $this->cus_kode;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      if ($cus_kode != 'all') {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanTitipan', function($q) use($start, $end){
          $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
        })
        ->where('cus_kode', $cus_kode)
        ->get();
      }
      else {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanTitipan', function($q) use($start, $end){
          $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
        })->get();
      }

      $grand_total=0;
      foreach ($row as $key) {
        $grand_total_cus=0;
        $key['penjualan'] = mPenjualanTitipan::where('cus_kode', $key->cus_kode)
        ->where('pt_tgl', '>=', $start)
        ->where('pt_tgl', '<=', $end)
        ->get();
        // $key['penjualan'] = $key->PenjualanTitipan;
        foreach ($key['penjualan'] as $keyPenjualan) {
          $grand_total_cus+=$keyPenjualan->pt_subtotal;
          $grand_total+=$keyPenjualan->pt_subtotal;
          $keyPenjualan['detail'] = $keyPenjualan->detail_PT;
          // foreach ($keyPenjualan['detail'] as $keyDet) {
          //   $keyDet->barang->satuan;
          // }
          unset($keyPenjualan->detail_PT);
        }
        $key['grand_total_cus'] = $grand_total_cus;
        unset($key->PenjualanTitipan);
      }

      $data['start'] = $start;
      $data['end'] = $end;

      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      return view('export.laporan-penjualan-titipan', $data);
    }
}
