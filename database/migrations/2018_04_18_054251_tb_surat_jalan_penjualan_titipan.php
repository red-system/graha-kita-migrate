<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSuratJalanPenjualanTitipan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_surat_jalan_penjualan_titipan', function (Blueprint $table) {
         $table->increments('sjt_kode');
         $table->datetime('sjt_tgl')->nullable();
         $table->integer('pt_no_faktur')->nullable();
         $table->integer('cus_kode')->nullable();
         $table->integer('sjt_sopir')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_surat_jalan_penjualan_titipan');
     }
}
