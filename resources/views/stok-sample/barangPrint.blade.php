<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Stok Sample</h4>
            <h4>Tanggal {{$time}}</h4>
          </div>
          <br>
          <div class="portlet light">
            <table class="table table-bordered table-hover table-header-fixed" id="sample_1">
              <thead>
                <tr class="">
                  <th style="font-size:10px"> No </th>
                  <th style="font-size:10px"> No Seri </th>
                  <th style="font-size:10px"> Kode Barang </th>
                  <th style="font-size:10px"> Nama Barang </th>
                  <th style="font-size:10px"> Satuan </th>
                  <th style="font-size:10px"> Kategory </th>
                  <th style="font-size:10px"> Group Stok </th>
                  <th style="font-size:10px"> Merek </th>
                  <th style="font-size:10px"> H.Beli Akhir </th>
                  <th style="font-size:10px"> H.Beli Max </th>
                  {{-- <th style="font-size:10px"> H.Beli + PPN </th> --}}
                  <th style="font-size:10px"> HPP </th>
                  <th style="font-size:10px"> H.Jual Retail </th>
                  <th style="font-size:10px"> H.Jual Partai </th>
                  {{-- <th style="font-size:10px"> Margin </th> --}}
                  <th style="font-size:10px"> QOH </th>
                  <th style="font-size:10px"> Aktif </th>
                  <th style="font-size:10px"> Supplier </th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataList as $row)
                  @foreach ($row->stok as $key)
                    <tr>
                      <td style="font-size:10px"> {{ $no++ }}. </td>
                      <td style="font-size:10px"> {{ $key['brg_no_seri'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang['brg_barcode'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang->brg_nama }} </td>
                      <td style="font-size:10px"> {{ $row->satuan['stn_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->kategory['ktg_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->group['grp_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->merek['mrk_nama'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang->brg_harga_beli_terakhir }} </td>
                      <td style="font-size:10px"> {{ $row->barang->brg_harga_beli_tertinggi }} </td>
                      {{-- <td style="font-size:10px"> {{ $row->barang->hargaPPN }} </td> --}}
                      <td style="font-size:10px"> {{ $row->barang->brg_hpp }} </td>
                      <td style="font-size:10px"> {{ $row->barang->brg_harga_jual_eceran }} </td>
                      <td style="font-size:10px"> {{ $row->barang->brg_harga_jual_partai }} </td>
                      {{-- <td style="font-size:10px"> {{ $row->margin }} </td> --}}
                      <td style="font-size:10px"> {{ $key['total'] }} </td>
                      <td style="font-size:10px"> {{ $row->barang->brg_status }} </td>
                      <td style="font-size:10px"> {{ $key['spl_nama'] }} </td>
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
