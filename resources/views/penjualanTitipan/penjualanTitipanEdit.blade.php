<form action="{{ route('penjualanTitipanUpdate', ['pt_no_faktur'=>$edit->pt_no_faktur]) }}" class="form-horizontal form-send" role="form" method="put">  
  {{ csrf_field() }}
  <div class="portlet light">
    <div class="form-horizontal" role="form">
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-2 control-label">Pelanggan</label>
              <div class="col-md-10">
                <select name="cus_kode" class="form-control select2" data-placeholder="Pilih">
                  <option value="">Pilih Pelanggan</option>
                  @foreach ($customer as $row)
                  <option value="{{ $row->cus_kode }}" data-alamat="{{ $row->cus_alamat }}" {{ $row->cus_kode == $edit->cus_kode ? 'selected':'' }}>{{ $kodeCustomer.$row->cus_kode.' - '.$row->cus_tipe.' - '.$row->cus_nama }} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label">Alamat</label>
              <div class="col-md-10" style="padding-top: 8px">
                <strong><span class="cus_alamat">{{ $customerData->cus_alamat }}</span></strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6">
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-2 control-label">No Faktur</label>
              <div class="col-md-3">
                <input type="text" class="form-control" name="pt_kode" disabled value="{{ $edit->pt_no_faktur }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label">Transaksi</label>
              <div class="col-md-10">
                <div class="mt-radio-inline">
                  <label class="mt-radio mt-radio-outline">
                    <input type="radio" name="pt_transaksi" id="optionsRadios22" value="cash" {{ $edit->pt_transaksi == 'cash' ?' checked':'' }}> Cash
                    <span></span>
                  </label>
                  <label class="mt-radio mt-radio-outline">
                    <input type="radio" name="pt_transaksi" id="optionsRadios23" value="kredit" {{ $edit->pt_transaksi == 'kredit' ?' checked':'' }}> Kredit
                    <span></span>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-md-4">
        <div class="form-horizontal" role="form">
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-4 control-label">Sales Person</label>
              <div class="col-md-8">
                <select class="form-control select2" name="pt_sales_person" data-placeholder="Pilih">
                  <option value="">Pilih </option>
                  @foreach ($karyawan as $row)
                  <option value="{{ $row->kry_kode }}" {{ $row->kry_kode == $edit->pt_sales_person ? 'selected':'' }}>{{ $kodeKaryawan.$row->kry_kode.' - '.$row->kry_nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Checker</label>
              <div class="col-md-8">
                <select class="form-control select2" name="pt_checker" data-placeholder="Pilih">
                  <option value="">Pilih </option>
                  @foreach ($karyawan as $row)
                  <option value="{{ $row->kry_kode }} " {{ $row->kry_kode == $edit->pt_checker ? 'selected':'' }}>{{ $kodeKaryawan.$row->kry_kode.' - '.$row->kry_nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Sopir</label>
              <div class="col-md-8">
                <select class="form-control select2" name="pt_sopir" data-placeholder="Pilih">
                  <option value="">Pilih </option>
                  @foreach ($karyawan as $row)
                  <option value="{{ $row->kry_kode }} " {{ $row->kry_kode == $edit->pt_sopir ? 'selected':'' }}>{{ $kodeKaryawan.$row->kry_kode.' - '.$row->kry_nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Kirim Semua</label>
              <div class="col-md-8">
                <div class="mt-radio-inline">
                  <label class="mt-radio mt-radio-outline">
                    <input type="radio" name="pt_kirim_semua" id="optionsRadios22" value="ya" {{ $edit->pt_kirim_semua == 'ya' ?' checked':'' }}> Ya
                    <span></span>
                  </label>
                  <label class="mt-radio mt-radio-outline">
                    <input type="radio" name="pt_kirim_semua" id="optionsRadios23" value="tidak"  {{ $edit->pt_kirim_semua == 'tidak' ?' checked':'' }}> Tidak
                    <span></span>
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Catatan</label>
              <div class="col-md-8">
                <textarea class="form-control" name="pt_catatan">{{ $edit->pt_catatan }}</textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-4">
        <div class="form-horizontal div-kredit {{ $edit->pt_transaksi == 'cash' ? 'hide':'' }}" role="form">
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-4 control-label">Lama Kredit</label>
              <div class="col-md-8">
                <select class="form-control" name="pt_lama_kredit">
                  @for($i=1; $i<=31; $i++)
                  <option value="{{ $i }}" {{ $i == $edit->pt_lama_kredit ? 'selected':'' }}>{{ $i }} </option>
                  @endfor
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Jatuh Tempo</label>
              <div class="col-md-8">
                <input type="text" name="pt_tgl_jatuh_tempo" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{ $edit->pt_tgl_jatuh_tempo }}">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-4">
        <div class="form-horizontal" role="form">
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-4 control-label">Sub Total</label>
              <div class="col-md-8">
                <input type="number" class="form-control" name="pt_subtotal" value="{{ $edit->pt_subtotal }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Disc</label>
              <div class="col-md-3">
                <input type="text" class="form-control" name="pt_disc" placeholder="%" value="{{ $edit->pt_disc }}">
              </div>
              <div class="col-md-5">
                <input type="number" class="form-control" name="pt_disc_nom" value="{{ $edit->pt_disc_nom }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">PPN</label>
              <div class="col-md-3">
                <input type="text" class="form-control" name="pt_ppn" placeholder="%" value="{{ $edit->pt_ppn }}">
              </div>
              <div class="col-md-5">
                <input type="number" class="form-control" name="pt_ppn_nom" value="{{ $edit->pt_ppn_nom }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Ongkos Angkut</label>
              <div class="col-md-8">
                <input type="number" class="form-control" name="pt_ongkos_angkut" value="{{ $edit->pt_ongkos_angkut }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Grand Total</label>
              <div class="col-md-8">
                <input type="number" class="form-control" name="grand_total" value="{{ $edit->grand_total }}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-success btn-lg">
                  <i class="fa fa-save"></i> Perbarui
                </button>
                <a href="{{ route('penjualanTitipanList') }}" class="btn btn-warning btn-lg">
                  <i class="fa fa-refresh"></i> Kembali
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</form>