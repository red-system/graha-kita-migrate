@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
{{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>

@stop

@section('body')

{{ csrf_field() }}
<div class="page-content-inner">
  <div class="mt-content-body">
    <div class="row">
      <div class="col-xs-12">
        <div class="portlet light ">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#tab_1_1" data-toggle="tab"> Penjualan Langsung </a>
            </li>
            <li>
              <a href="#tab_1_2" data-toggle="tab"> Penjualan Titipan </a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active in" id="tab_1_1">

              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                <tr class="">
                  <th width="10"> No </th>
                  <th> No Faktur </th>
                  <th> Kode Customer </th>
                  <th> Nama </th>
                  <th> Transaksi </th>
                  {{-- <th> Ongkos Angkut </th> --}}
                  {{-- <th> Total </th> --}}
                  <th> Kirim Semua </th>
                  <th> Batas Waktu </th>
                  <th> Aksi </th>
                </tr>
                </thead>
                <tbody>
                @foreach($penjualanLangsung as $r)
                  @if ($r->SJQTY > $r->SJterkirim)
                    <tr>
                      <td> {{ $no++ }} . </td>
                      <td> {{ $kodePL.$r->pl_no_faktur }}</td>
                      <td> {{ $kodeCustomer.$r->cus_kode }}</td>
                      @if ($r->cus_nama == null)
                        <td> Guest </td>
                      @else
                        <td> {{ $r->cus_nama }} </td>
                      @endif
                      <td> {{ $r->pl_transaksi }} </td>
                      {{-- <td> {{ $r->pl_ongkos_angkut }} </td>
                      <td> {{ number_format($r->grand_total) }} </td> --}}
                      <td> {{ $r->pl_kirim_semua }} </td>
                      <td> {{ date('Y-m-d', strtotime($r->pl_batas_kirim)) }} </td>
                      <td>
                        <div class="btn-group-vertical btn-group-sm">
                          <a href="{{ route('suratJalanDetail', ['kode'=>$r->pl_no_faktur, 'tipe'=>'langsung']) }}" class="btn btn-success" >
                            <span class="icon-pencil"></span> Buat Surat Jalan </a>
                        </div>
                      </td>
                    </tr>
                  @endif
                @endforeach
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="tab_1_2">

              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                <tr class="">
                  <th width="10"> No </th>
                  <th> No Faktur </th>
                  <th> Kode Customer </th>
                  <th> Nama </th>
                  <th> Transaksi </th>
                  {{-- <th> Ongkos Angkut </th>
                  <th> Total </th> --}}
                  <th> Aksi </th>
                </tr>
                </thead>
                <tbody>
                @foreach($penjualanTitipan as $r)
                  @if ($r->SJQTY > $r->SJterkirim)
                    <tr>
                      <td> {{ $no_2++ }} . </td>
                      <td> {{ $kodePT.$r->pt_no_faktur }}</td>
                      <td> {{ $kodeCustomer.$r->cus_kode }}</td>
                      @if ($r->cus_nama == null)
                        <td> Guest </td>
                      @else
                        <td> {{ $r->cus_nama }} </td>
                      @endif
                      <td> {{ $r->pt_transaksi }} </td>
                      {{-- <td> {{ $r->pt_ongkos_angkut }} </td>
                      <td> {{ number_format($r->grand_total) }}</td> --}}
                      <td>
                        <div class="btn-group-vertical btn-group-sm">
                          <a href="{{ route('suratJalanDetail', ['kode'=>$r->pt_no_faktur, 'tipe'=>'titipan']) }}" class="btn btn-success" >
                            <span class="icon-pencil"></span> Buat Surat Jalan </a>
                        </div>
                      </td>
                    </tr>
                  @endif
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
