<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>{{$title}}</title>
    </head>
    <body>
        <style type="text/css">
            .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
            .tt td{font-family:Tahoma;font-size:13px;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#fff;}
            .tt th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#f0f0f0;}
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Tahoma;font-size:12px;padding:4px 4px;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#000000;background-color:#fff;}
            .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:4px 4px;border-width:1px;overflow:hidden;word-break:normal;border-color:#000000;color:#333;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;}

            @media print {
                html, body {
                display: block;
                font-family: "Tahoma";
                margin: 0px 0px 0px 0px;
                }

                /*@page {
                  size: Faktur Besar;
                }*/
                #footer {
                  position: fixed;
                  bottom: 0;
                }
                /*#header {
                  position: fixed;
                  top: 0;
                }*/
            }
        </style>
        <div class="container-fluid">
            <div class="row">
                <div class="row">
                    <div class="col-md-6">
                        <table width="100%" class="tt">
                            <tr>
                                <td width="60%">W/O Barang</td>
                                <td width="20%">No. PO</td>
                                <td width="20%"> : {{$work_order->no_po}}</td>
                            </tr>
                            <tr>
                                <td>Untuk : {{$gudang->gdg_nama}}</td>
                                <td>No. SJ</td>
                                <td> : {{$work_order->no_sj}}</td>
                            </tr>
                            <tr>
                                <td> {{$gudang->gdg_alamat}}</td>
                                <td>No. W/O</td>
                                <td> : {{$work_order->no_wo}}</td>
                            </tr>
                            <tr>
                                <td>Si Pengirim : {{$poSupplier->suppliers->spl_nama}}</td>
                                <td>Tgl. W/O</td>
                                <td> : {{date('d-M-Y',strtotime($work_order->wo_tgl))}}</td>
                            </tr>                    
                        </table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <table class="tg table table-bordered">
                            <thead>
                                <tr>
                                    <td colspan="4">
                                        <hr style="border-style: solid;" size="1px">
                                    </td>
                                </tr>
                                <tr>
                                    <th> NO</th>
                                    <th> NAMA BARANG </th>
                                    <th> QTY </th>
                                    <th> SATUAN </th>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <hr style="border-style: solid;" size="1px">
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($poSupplier->detailPoSupplier->where('gudang',$id_gudang) as $detail)   
                                <tr>
                                    <td style="text-align:center"> {{ $no++ }} </td>
                                    <td> {{ strtoupper($detail->nama_barang) }} </td>
                                    <td style="text-align:center"> {{ $detail->qty }} </td>
                                    <td style="text-align:center"> {{ strtoupper($detail->satuans->stn_nama) }} </td>
                                </tr>
                            @endforeach
                            
                            </tbody>
                        </table>
                        <br>                             
                    </div>
                </div>
                <br>
                <div class="row">
                    <table width="100%" class="tt" id="footer">
                        <tr>
                            <td colspan="5">
                                <hr style="border-style: solid;" size="1px">
                            </td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td align="center" width="30%">Dibuat Oleh,</td>
                            <td align="center" width="30%">Pengirim Barang,</td>
                            <td align="center" width="30%" align="center">Penerima Barang,</td>
                            <td width="5%"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td align="center" width="30%"><br><br><br><U>....................</U></td>
                            <td align="center" width="30%"><br><br><br><U>....................</U></td>
                            <td align="center" width="30%" align="center"><br><br><br><U>....................</U></td>
                            <td width="5%"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td align="center" width="30%"></td>
                            <td colspan="2">No. Kendaraan :</td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
