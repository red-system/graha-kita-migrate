<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <table width="100%" border="0">
      <thead>
        <tr>
          <td colspan="8">
            <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118</p>
          </td>
        </tr>
        <tr>
          <td colspan="5"> <p>Dikirim Kepada</p> </td>
          <td> <p>No. Ftr</p> </td>
          <td colspan="2"> <p>: {{$faktur['kode_bukti_id']}}</p> </td>
        </tr>
        <tr>
          <td> <p>Nama</p> </td>
          <td colspan="4"> <p>: {{$faktur->cus_nama['cus_nama']}}</p> </td>

          <td> <p>No. D.O.</p> </td>
          <td colspan="2"> <p>: {{$faktur['do']}}</p> </td>
        </tr>
        <tr>
          <td> <p>Alamat</p> </td>
          <td colspan="4"> <p>: {{$faktur['cus_alamat']}}</p> </td>

          <td> <p>No. S.J.</p> </td>
          <td colspan="2"> <p>: {{$faktur['sj']}}</p> </td>
        </tr>

        <tr>
          <td colspan="5"></td>

          <td> <p>Tgl. S.J.</p> </td>
          <td colspan="2"> <p>: {{$faktur['tglPrint']}}</p> </td>
        </tr>
        <tr>
          <td><br></td>
        </tr>
        <tr>
          <td colspan="8" align="center"><p align="center"><b>Surat Jalan</b></p></td>
        </tr>
        <br>
        <tr>
          <th> <p>No</p> </th>
          <th colspan="2"> <p>Kode Stock</p> </th>
          <th colspan="2"> <p>Nama Stock</p> </th>
          <th> <p>Banyak</p> </th>
          <th> <p>Satuan</p> </th>
          <th> <p>Pack</p> </th>
        </tr>
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
      </thead>
      <tbody>
        @foreach ($faktur->detail as $key)
          <tr>
            <td align="center"><p>{{$faktur->no++}}</p></td>
            <td align="center" colspan="2"><p>{{$key->brg_barcode}}</p></td>
            <td align="center" colspan="2"><p>{{$key->brg_nama}}</p></td>
            <td align="center"><p>{{number_format($key->dikirim, 2, "." ,",")}}</p></td>
            <td align="center"><p>{{$key->stn_nama}}</p></td>
            <td align="center"><p>-</p></td>
          </tr>
        @endforeach
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="8">
            <br>
          </td>
        </tr>

          <tr>
            <td align="center" colspan="4">
              <b><p>Pengirim</p></b>
            </td>

            <td align="center" colspan="4">
              <b><p>Penerima</p></b>
            </td>
          </tr>
          <tr>
            <td colspan="4">
              <br>
              <br>
              <br>
            </td>

            <td colspan="4">
              <br>
              <br>
              <br>
            </td>
          </tr>
          <tr>
            <td align="center" colspan="4">
              <p>(......................................)</p>
            </td>

            <td align="center" colspan="4">
              <p>(......................................)</p>
            </td>
          </tr>

      </tfoot>
    </table>
  </body>
</html>
