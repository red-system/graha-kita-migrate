@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
{{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>

@stop

@section('body')

{{ csrf_field() }}
<div class="page-content-inner">
  <div class="mt-content-body">
    <div class="row">
      <div class="col-xs-12">
        <div class="portlet light ">
              <div class="row">
                <div class="col-xs-12 col-sm-5">
                  <table style="border-collapse: separate; border-spacing: 20px;">
                    <tr>
                      <td>No Faktur</td>
                      <td>:</td>
                      <td>{{ $noFaktur }}</td>
                    </tr>
                    @if ($row['cus_nama'] == null)
                      <tr>
                        <td>Pelanggan</td>
                        <td>:</td>
                        <td>Guest</td>
                      </tr>
                    @else
                      <tr>
                        <td>Pelanggan</td>
                        <td>:</td>
                        <td>{{ $row['cus_nama'] }}</td>
                      </tr>
                    @endif
                    @if ($row['cus_alamat'] == null)
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>-</td>
                      </tr>
                    @else
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $row['cus_alamat'] }}</td>
                      </tr>
                    @endif
                    @if ($row['cus_telp'] == null)
                      <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td>-</td>
                      </tr>
                    @else
                      <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td>{{ $row['cus_telp'] }}</td>
                      </tr>
                    @endif
                  </table>
                </div>
                <div class="col-xs-12 col-sm-5">
                  <table style="border-collapse: separate; border-spacing: 20px;">
                    <tr>
                      <td>Tanggal</td>
                      <td>:</td>
                      <td>{{ $tanggal }}</td>
                    </tr>
                    <tr>
                      <td>No. Surat Jalan</td>
                      <td>:</td>
                      <td>{{ $SJ }}</td>
                    </tr>
                    <tr>
                      <td>Sopir</td>
                      <td>:</td>
                      <td>{{ $sopir }}</td>
                    </tr>
                    <tr>
                      <td>Catatan</td>
                      <td>:</td>
                      <td>{{ $catatan }}</td>
                    </tr>
                  </table>
                </div>
                <div class="col-xs-12 col-sm-2">
                  <a href="{{ route('suratJalanGetSuratJalanPT',['id'=>$kode]) }}" class="btn btn-success btn-lg btn-block" target="_blank"><span class="glyphicon glyphicon-print"></span> SURAT JALAN</a>
                  <a href="{{ route('suratJalanGetDOPT',['id'=>$kode]) }}" class="btn btn-info btn-lg btn-block" target="_blank"><span class="glyphicon glyphicon-print"></span> DO</a>
                  <a href="{{ route('suratJalanDetail', ['kode'=>$row->$idNoFaktur, 'tipe'=>$tipe]) }}" class="btn btn-warning btn-lg btn-block"><span class="glyphicon glyphicon-share-alt"></span> Kembali</a>
                </div>
              </div>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed">
                <thead>
                <tr class="">
                  <th width="10"> No </th>
                  <th> Kode Gudang </th>
                  <th> Kode Barang </th>
                  <th> Barang Barkode </th>
                  <th> Nama Barang </th>
                  <th> Satuan </th>
                  <th> Dikirim </th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $row)
                  <tr>
                    <td> {{ $no++ }}. </td>
                    <td> {{ $kodeGudang.$row->gudang['gdg_kode'] }} </td>
                    <td> {{ $kodeBarang.$row->barang['brg_kode'] }} </td>
                    <td> {{ $row->barang['brg_barcode'] }} </td>
                    <td> {{ $row->barang['brg_nama'] }} </td>
                    <td> {{ $row->satuanJ->stn_nama }} </td>
                    <td> {{ $row->dikirim }} </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
