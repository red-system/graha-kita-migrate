<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailStokSample extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_detail_stok_sample';
  protected $primaryKey = 'detail_stok_sample_kode';
  // public $timestamps = false;

  public function sample()
  {
    return $this->belongsTo('App\Models\mStokSample', 'stok_sample_kode');
  }
}
