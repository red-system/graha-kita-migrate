<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAcTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_ac_transaksi', function (Blueprint $table) {
           $table->increments('transaksi_id');
           $table->integer('jurnal_umum_id')->nullable();
           $table->integer('master_id')->nullable();
           $table->string('trs_jenis_transaksi')->nullable();
           $table->bigInteger('trs_debet')->nullable();
           $table->bigInteger('trs_kredit')->nullable();
           $table->integer('user_id')->nullable();
           $table->year('trs_year')->nullable();
           $table->tinyInteger('trs_month')->nullable();
           $table->string('trs_kode_rekening')->nullable();
           $table->string('trs_nama_rekening')->nullable();
           $table->string('trs_tipe_arus_kas')->nullable();
           $table->string('trs_catatan')->nullable();
           $table->datetime('trs_date_insert')->nullable();
           $table->datetime('trs_date_update')->nullable();
           $table->string('trs_charge')->nullable();
           $table->string('trs_no_check_bg')->nullable();
           $table->date('trs_tgl_pencairan')->nullable();
           $table->bigInteger('trs_setor')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_ac_transaksi');
     }
}
