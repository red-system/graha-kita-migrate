<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Info Purchase Order</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Tgl. PO</td>
                        <td>: {{date('d M Y', strtotime($purchase->pos_tgl))}}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">No. PO</td>
                        <td>: {{$purchase->no}}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Supplier</td>
                        <td>: {{$purchase->suppliers->spl_nama}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <!-- <h4>Item Produksi</h4> -->
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th class="text-center">Nama Stock</th>
                        <th>Satuan</th>
                        <th>Qty</th>
                        <th class="text-center">Harga Beli</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody><?php $no=1;?>
                    @foreach($purchase->detailPoSupplier as $detail)
                        <tr>
                            <td style="font-size: 12px"> {{ $no++ }} </td>
                            <td style="font-size: 12px"> {{ $detail->nama_barang }} </td>
                            <td style="font-size: 12px"> {{ $detail->satuans->stn_nama }} </td>
                            <td style="font-size: 12px"> {{ number_format($detail->qty,2) }} </td>
                            <td style="font-size: 12px"> {{ number_format($detail->harga_net,2) }} </td>
                            <td style="font-size: 12px"> {{ number_format($detail->total,2)}} </td>
                        </tr>
                    @endforeach
                    
                </tbody>
                <tfoot>
                    <tr style="">
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: left;" colspan="4">nb : {{$purchase->pos_catatan}}</td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">Subtotal :</td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">{{ number_format($purchase->pos_subtotal,2)}} </td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: left;" colspan="4">Pembayaran secara {{$purchase->tipe_transaksi}} <?php if($purchase->tipe_transaksi=='credit'){ echo 'tgl jatuh tempo :'.date('d M Y', strtotime($purchase->tgl_jatuh_tempo));}?></td>                                                
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">Diskon :</td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">{{ number_format($purchase->pos_disc_nom,2)}} </td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: left;" colspan="4"></td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">PPN :</td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">{{ number_format($purchase->pos_ppn_nom,2)}} </td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: left;" colspan="4"></td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">Biaya Tambahan :</td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">{{ number_format($purchase->biaya_lain,2)}} </td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: left;" colspan="4"></td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;">Grand Total  :</td>
                        <td style="font-size: 11px;padding-top: 1px;padding-bottom: 1px;border-style: none;text-align: right;"> {{ number_format($purchase->grand_total,2)}} </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
</div>