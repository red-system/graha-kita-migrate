<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use Carbon\Carbon;
use App\Models\mBarang;
use App\Models\mStok;
use App\Models\mArusStok;

class SummaryStok extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Summary Stok';
      $this->kodeLabel = 'barang';
  }

  function index() {
      $breadcrumb = [
          'Stok Alert'=>route('summaryStokList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'summary_stok';
      $data['title'] = $this->title;
      $data['dataList'] = mBarang::has('arus_stok')
      ->leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      // ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
      ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama')
      // ->whereNotNull('ars_stok_date')
      ->groupBy('tb_barang.brg_kode')
      ->selectRaw('sum(stok_in) as stokIn')
      ->selectRaw('sum(stok_out) as stokOut')
      ->get();

      foreach ($data['dataList'] as &$barang) {
        $total = 0;
        $barangStok =  $barang->stok;
        foreach ($barangStok as $stok) {
          $total += $stok->stok;
        }
        $barang['QOH'] = $total;
      }
      // return $data;
      return view('summary-stok/summaryStokList', $data);
  }

  function range(Request $request) {
      // return $request;
      $date_start = $request->start_date;
      $date_end = $request->end_date;

      $time = Carbon::now();
      if ($request->start_date == null) {
        $start = $time;
      }
      if ($request->end_date == null) {
        $end = $time;
      }
      $start = date('Y-m-d', strtotime($date_start));
      $end = date('Y-m-d', strtotime($date_end));

      $data = mBarang::has('arus_stok')
      ->leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama')
      ->where('ars_stok_date', '>=', $start)
      ->where('ars_stok_date', '<=', $end)
      ->groupBy('tb_barang.brg_kode')
      ->selectRaw('sum(stok_in) as stokIn')
      ->selectRaw('sum(stok_out) as stokOut')
      ->get();

      // if ($request->start_date == null && $request->end_date == null) {
      //   $data = mBarang::leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      //   ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      //   ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      //   ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      //   ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      //   ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama')
      //   ->whereNotNull('ars_stok_date')
      //   ->groupBy('tb_barang.brg_kode')
      //   ->selectRaw('sum(stok_in) as stokIn')
      //   ->selectRaw('sum(stok_out) as stokOut')
      //   ->get();
      // }
      // elseif ($request->start_date == null || $request->end_date == null) {
      //   $time = Carbon::now();
      //   if ($request->start_date == null) {
      //     $start = $time;
      //   }
      //   else {
      //     $end = $time;
      //   }
      //   $data = mBarang::leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      //   ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      //   ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      //   ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      //   ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      //   ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama')
      //   ->whereNotNull('ars_stok_date')
      //   ->where('ars_stok_date', '>=', $start)
      //   ->where('ars_stok_date', '<=', $end)
      //   ->groupBy('tb_barang.brg_kode')
      //   ->selectRaw('sum(stok_in) as stokIn')
      //   ->selectRaw('sum(stok_out) as stokOut')
      //   ->get();
      // }
      // else {
      //   $data = mBarang::leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      //   ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      //   ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      //   ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      //   ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      //   ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama')
      //   ->whereNotNull('ars_stok_date')
      //   ->where('ars_stok_date', '>=', $start)
      //   ->where('ars_stok_date', '<=', $end)
      //   ->groupBy('tb_barang.brg_kode')
      //   ->selectRaw('sum(stok_in) as stokIn')
      //   ->selectRaw('sum(stok_out) as stokOut')
      //   ->get();
      // }

      foreach ($data as &$barang) {
        $total = 0;
        $barangStok =  $barang->stok;
        foreach ($barangStok as $stok) {
          $total += $stok->stok;
        }
        $barang['QOH'] = $total;
      }

      return $data;
      // return response()->json([
      //     'data' => $data
      // ]);
  }

  function viewSummaryStok(Request $request) {
    $date_start = $request->start_date;
    $date_end = $request->end_date;

    $time = Carbon::now();
    if ($request->start_date == null) {
      $start = $time;
    }
    if ($request->end_date == null) {
      $end = $time;
    }
    $start = date('Y-m-d', strtotime($date_start));
    $end = date('Y-m-d', strtotime($date_end));

    if ($request->report == 'print') {
      return [
        'redirect'=>route('summaryStokCetak', ['start'=>$start, 'end'=>$end]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.summary-stok', ['start'=>$start, 'end'=>$end]),
      ];
    }
  }

  function summary_stok_cetak($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;

    $data['start'] = $start;
    $data['end'] = $end;
    
    $data['dataList'] = mBarang::has('arus_stok')
    ->leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
    ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
    ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
    ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
    ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
    ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
    ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama')
    ->where('ars_stok_date', '>=', $start)
    ->where('ars_stok_date', '<=', $end)
    ->groupBy('tb_barang.brg_kode')
    ->selectRaw('sum(stok_in) as stokIn')
    ->selectRaw('sum(stok_out) as stokOut')
    ->get();

    foreach ($data['dataList'] as &$barang) {
      $total = 0;
      $barangStok =  $barang->stok;
      foreach ($barangStok as $stok) {
        $total += $stok->stok;
      }
      $barang['QOH'] = $total;
    }

    return view('laporan.summary-stok', $data);
  }
}
