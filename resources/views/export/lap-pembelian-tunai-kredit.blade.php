<table>
    <tr>
        <td>Laporan Pembelian Tunai Kredit</td>
    </tr>
    <tr>
        <td>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</td>
    </tr>
</table>
<table>
    <thead>
        <tr>
            <th><center> No </center></th>
            <th><center> Tanggal </center></th>
            <th><center> No Faktur </center></th>
            <th><center> No Invoice Spl. </center></th>
            <th><center> Nama </center></th>
            <th><center> Harga Beli (DPP) </center></th>
            <th><center> Diskon </center></th>
            <th><center> PPN </center></th>
            <th><center> Cash </center></th>
            <th><center> Credit </center></th>
            <th><center> Tgl Jatuh Tempo </center></th>
        </tr>
    </thead>
    <?php
        $total_cash = 0;
        $total_credit = 0;
        $total_cash_ppn = 0;
        $total_credit_ppn = 0;
        $total_cash_non_ppn = 0;
        $total_credit_non_ppn = 0;  
    ?>
    <tbody>
        @foreach($dataListPpn as $data)
        <?php $no=1;?>
        <tr>
            <td>{{$no++}}</td>
            <td>{{date('d M Y', strtotime($data->ps_tgl))}}</td>
            <td>{{$data->no_pembelian}}</td>
            <td>{{$data->no_invoice}}</td>
            <td>{{$data->supplier->spl_nama}}</td>
            <td>{{number_format($data->ps_subtotal)}}</td>
            <td>{{number_format($data->ps_disc_nom)}}</td>
            <td>{{number_format($data->ps_ppn_nom)}}</td>
            @if($data->tipe=='cash')
            <!-- <td style="font-size:11px;" align="right">{{number_format($data->ps_subtotal-$data->ps_disc_nom+$data->ps_ppn_nom)}}</td> -->
            <td>{{number_format($data->grand_total)}}</td>
            <td>-</td>
            <td>-</td>
            <?php 
            $total_cash += $data->grand_total;
            $total_cash_ppn += $data->grand_total;
            ?>
            @endif
            @if($data->tipe=='credit')
            <td>-</td>
            <!-- <td style="font-size:11px;">{{number_format($data->ps_subtotal-$data->ps_disc_nom+$data->ps_ppn_nom)}}</td> -->
            <td>{{number_format($data->grand_total)}}</td>
            <td>{{date('d M Y', strtotime($data->hutangSupplier->js_jatuh_tempo))}}</td>
            <?php 
            $total_credit += $data->grand_total;
            $total_credit_ppn += $data->grand_total;
            ?>
            @endif
        </tr> 
        @endforeach
        <tr>
            <td colspan="4"></td>
            <td><strong>Subtotal</strong></td>
            <td><strong>{{number_format($dataListPpn->sum('ps_subtotal'))}}</strong></td>
            <td><strong>{{number_format($dataListPpn->sum('ps_disc_nom'))}}</strong></td>
            <td><strong>{{number_format($dataListPpn->sum('ps_ppn_nom'))}}</strong></td>
            <td><strong>{{number_format($total_cash_ppn)}}</strong></td>
            <td><strong>{{number_format($total_credit_ppn)}}</strong></td>
            <td></td>
        </tr> 
        @foreach($dataListNonPpn as $dataCredit)
        <?php $no=1;?>
        <tr>
            <td>{{$no++}}</td>
            <td>{{date('d M Y', strtotime($dataCredit->ps_tgl))}}</td>
            <td>{{$dataCredit->no_pembelian}}</td>
            <td>{{$dataCredit->no_invoice}}</td>
            <td>{{$dataCredit->supplier->spl_nama}}</td>
            <td>{{number_format($dataCredit->ps_subtotal)}}</td>
            <td>{{number_format($dataCredit->ps_disc_nom)}}</td>
            <td>{{number_format($dataCredit->ps_ppn_nom)}}</td>
            @if($dataCredit->tipe=='cash')
            <td>{{number_format($dataCredit->grand_total)}}</td>
            <td>-</td>
            <td>-</td>
            <?php 
            $total_cash += $dataCredit->grand_total;
            $total_cash_non_ppn += $dataCredit->grand_total;
            ?>
            @endif
            @if($dataCredit->tipe=='credit')
            <td>-</td>
            <td>{{number_format($dataCredit->grand_total)}}</td>
            <td>{{date('d M Y', strtotime($dataCredit->hutangSupplier->js_jatuh_tempo))}}</td>
            <?php 
            $total_credit += $dataCredit->grand_total;
            $total_credit_non_ppn += $dataCredit->grand_total;
            ?>
            @endif
        </tr>
        @endforeach
        <tr>
            <td colspan="4"></td>
            <td><strong>Subtotal</strong></td>
            <td><strong>{{number_format($dataListNonPpn->sum('ps_subtotal'))}}</strong></td>
            <td><strong>{{number_format($dataListNonPpn->sum('ps_disc_nom'))}}</strong></td>
            <td><strong>{{number_format($dataListNonPpn->sum('ps_ppn_nom'))}}</strong></td>
            <td><strong>{{number_format($total_cash_non_ppn)}}</strong></td>
            <td><strong>{{number_format($total_credit_non_ppn)}}</strong></td>
            <td></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4"></td>
            <td><strong>Grand Total</strong></td>
            <td><strong>{{number_format($dataListNonPpn->sum('ps_subtotal')+$dataListPpn->sum('ps_subtotal'))}}</strong></td>
            <td><strong>{{number_format($dataListNonPpn->sum('ps_disc_nom')+$dataListPpn->sum('ps_disc_nom'))}}</strong></td>
            <td><strong>{{number_format($dataListNonPpn->sum('ps_ppn_nom')+$dataListPpn->sum('ps_ppn_nom'))}}</strong></td>
            <td><strong>{{number_format($total_cash)}}</strong></td>
            <td><strong>{{number_format($total_credit)}}</strong></td>
            <td></td>
        </tr>
    </tfoot>
</table>