<?php

namespace App\Exports;

use App\Models\mBarang;
use App\Models\mJurnalUmum;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class BarangExport implements FromCollection
class JurnalUmumExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
      $request = $this->request;

      $jurnalUmum = mJurnalUmum::whereBetween('jmu_tanggal', [$request->start_date, $request->end_date])->orderBy('jmu_tanggal','ASC')->get();
      $jml_debet  = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$request->start_date, $request->end_date])->sum('tb_ac_transaksi.trs_debet');
      $jml_kredit = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$request->start_date, $request->end_date])->sum('tb_ac_transaksi.trs_kredit');

      return view('export.jurnal-umum', [
        'jurnal'      => $jurnalUmum,
        'debet_jml'   => $jml_debet,
        'kredit_jml'  => $jml_kredit,
        'start_date'  => $request->start_date,
        'end_date'    => $request->end_date,
      ]);
    }
}
