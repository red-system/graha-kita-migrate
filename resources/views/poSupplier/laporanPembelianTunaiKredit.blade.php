@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />   
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                <div class="col-md-6 col-xs-6">
                                    <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                        Filter
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="{{route('printLapPembelianTunaiKedit', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl_kode])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="{{route('lapPembelianTunaiKreditExcel', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl_kode,'mrk'=>0, 'grp'=>0, 'tipe'=> 'pembelianTunaiKredit'])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Excel
                                    </a>
                                </div>
                                    
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h3><center>Laporan Pembelian Tunai Kredit</center></h3>
                                        <h4><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h4>
                                    </div>
                                                                     
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th style="font-size:12px;" align="center"><center> No </center></th>
                                                <th style="font-size:12px;" align="center"><center> Tanggal </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Faktur </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Invoice Spl. </center></th>
                                                <th style="font-size:12px;" align="center"><center> Nama </center></th>
                                                <th style="font-size:12px;" align="center"><center> Harga Beli (DPP) </center></th>
                                                <th style="font-size:12px;" align="center"><center> Diskon </center></th>
                                                <th style="font-size:12px;" align="center"><center> PPN </center></th>
                                                <th style="font-size:12px;" align="center"><center> Cash </center></th>
                                                <th style="font-size:12px;" align="center"><center> Credit </center></th>
                                                <th style="font-size:12px;" align="center"><center> Tgl Jatuh Tempo </center></th>
                                            </tr>
                                        </thead>
                                        <?php
                                            $total_cash = 0;
                                            $total_credit = 0;

                                            $total_cash_ppn = 0;
                                            $total_credit_ppn = 0;
                                            $total_cash_non_ppn = 0;
                                            $total_credit_non_ppn = 0;
                                            
                                        ?>
                                        <tbody>
                                            @foreach($dataListPpn as $data)
                                            <tr>
                                                <td style="font-size:11px;" align="center">{{$no++}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($data->ps_tgl))}}</td>
                                                <td style="font-size:11px;">{{$data->no_pembelian}}</td>
                                                <td style="font-size:11px;">{{$data->no_invoice}}</td>
                                                <td style="font-size:11px;">{{$data->supplier->spl_nama}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->ps_subtotal)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->ps_disc_nom)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->ps_ppn_nom)}}</td>
                                            @if($data->tipe=='cash')
                                                <!-- <td style="font-size:11px;" align="right">{{number_format($data->ps_subtotal-$data->ps_disc_nom+$data->ps_ppn_nom)}}</td> -->
                                                <td style="font-size:11px;" align="right">{{number_format($data->grand_total)}}</td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;">-</td>
                                                <?php 
                                                    $total_cash += $data->grand_total;
                                                    $total_cash_ppn += $data->grand_total;                                                    
                                                ?>
                                            @endif
                                            @if($data->tipe=='credit')
                                                <td style="font-size:11px;" align="right">-</td>
                                                <!-- <td style="font-size:11px;">{{number_format($data->ps_subtotal-$data->ps_disc_nom+$data->ps_ppn_nom)}}</td> -->
                                                <td style="font-size:11px;" align="right">{{number_format($data->grand_total)}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($data->hutangSupplier->js_jatuh_tempo))}}</td>
                                                <?php 
                                                    $total_credit += $data->grand_total;
                                                    $total_credit_ppn += $data->grand_total;
                                                ?>
                                            @endif
                                                
                                            </tr> 
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListPpn->sum('ps_subtotal'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListPpn->sum('ps_disc_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListPpn->sum('ps_ppn_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_cash_ppn)}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_credit_ppn)}}</strong></td>
                                                <td></td>
                                            </tr> 
                                            @foreach($dataListNonPpn as $dataCredit)
                                            <tr>
                                                <td style="font-size:11px;" align="center">{{$no++}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($dataCredit->ps_tgl))}}</td>
                                                <td style="font-size:11px;">{{$dataCredit->no_pembelian}}</td>
                                                <td style="font-size:11px;">{{$dataCredit->no_invoice}}</td>
                                                <td style="font-size:11px;">{{$dataCredit->supplier->spl_nama}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->ps_subtotal)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->ps_disc_nom)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->ps_ppn_nom)}}</td>
                                            @if($dataCredit->tipe=='cash')
                                               
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->grand_total)}}</td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;">-</td>
                                                <?php 
                                                    $total_cash += $dataCredit->grand_total;
                                                    $total_cash_non_ppn += $dataCredit->grand_total;
                                                ?>
                                            @endif
                                            @if($dataCredit->tipe=='credit')
                                                <td style="font-size:11px;" align="right">-</td>
                                                
                                                <td style="font-size:11px;" align="right">{{number_format($dataCredit->grand_total)}}</td>
                                                <td style="font-size:11px;">{{date('d M Y', strtotime($dataCredit->hutangSupplier->js_jatuh_tempo))}}</td>
                                                <?php 
                                                    $total_credit += $dataCredit->grand_total;
                                                    $total_credit_non_ppn += $dataCredit->grand_total;
                                                ?>
                                            @endif
                                                
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_subtotal'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_disc_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_ppn_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_cash_non_ppn)}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_credit_non_ppn)}}</strong></td>
                                                <td></td>
                                            </tr>                                                                                 
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Grand Total</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_subtotal')+$dataListPpn->sum('ps_subtotal'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_disc_nom')+$dataListPpn->sum('ps_disc_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($dataListNonPpn->sum('ps_ppn_nom')+$dataListPpn->sum('ps_ppn_nom'))}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_cash)}}</strong></td>
                                                <td style="font-size:12px;"><strong>{{number_format($total_credit)}}</strong></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodePembelian') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="supplier_id">
                                    <option value="0">-All Supplier-</option>
                                    @foreach($suppliers as $supplier)
                                    <option value="{{$supplier->spl_kode}}" <?php if($spl_kode==$supplier->spl_kode) echo 'selected'?>>{{$supplier->spl_nama}}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date" value="{{$end_date}}"/>
                                <input type="hidden" name="tipe_laporan" value="pembelianTunaiKredit">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop