<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mJPK extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];
  
  public $incrementing = false;
  protected $table = 'tb_jenis_pembiayaan_kendaraan';
  protected $primaryKey = 'jpk_kode';
  public $timestamps = false;

  public function perkiraan()
  {
    return $this->belongsTo('App\Models\mPerkiraan', 'coa_biaya', 'mst_kode_rekening');
  }
}
