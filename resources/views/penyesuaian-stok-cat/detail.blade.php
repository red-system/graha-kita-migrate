@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <style media="screen">
  td.wrapok {
    white-space:nowrap
  }

  td.fontsize{
    font-size:10px
  }
  </style>
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('js/penyesuaianSC.js') }}" type="text/javascript"></script> --}}
  <script type="text/javascript">
  var l = window.location;
  var base_url = l.protocol + "//" + l.host + "/";
  var table = $('#table_1').DataTable({
    destroy : true,
    processing: true,
    serverSide: true,
    ajax: base_url+'penyesuaian-stok-cat/{{$id}}/history',
    columns: [
      // {data: 'no', class:"fontsize"},
      // {data: 'pco_kode', class:"wrapok fontsize"},
      // {data: 'brg_kode', class:"wrapok fontsize"},
      {data: 'brg_barcode', class:"wrapok fontsize"},
      {data: 'nama_barang', class:"wrapok fontsize"},
      {data: 'stn_nama', class:"wrapok fontsize"},
      {data: 'gdg_nama', class:"wrapok fontsize"},
      {data: 'qty', class:"wrapok fontsize"},
      {data: 'brg_no_seri', class:"wrapok fontsize"},
      {data: 'brg_hpp', class:"wrapok fontsize"},
    ]
  });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="tab-content">
              <div class="tab-pane active in" id="tab_1_1">
                <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_1">
                  <thead>
                    <tr class="">
                      <th>Barcode</th>
                      <th>Nama Barang</th>
                      <th>Satuan</th>
                      <th>Gudang</th>
                      <th>QTY</th>
                      <th>No Seri</th>
                      <th>HPP</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
