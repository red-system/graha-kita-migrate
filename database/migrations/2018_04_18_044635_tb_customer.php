<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_customer', function (Blueprint $table) {
         $table->increments('cus_kode');
         $table->string('cus_nama')->nullable();
         $table->string('cus_alamat')->nullable();
         $table->string('cus_telp')->nullable();
         $table->string('cus_tipe')->nullable();
         $table->string('username')->nullable();
         $table->string('password')->nullable();
         $table->integer('cus_potongan')->nullable();
         $table->integer('kry_kode')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_customer');
     }
}
