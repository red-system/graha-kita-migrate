<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\mArusStok;
use Meneses\LaravelMpdf\Facades\LaravelMpdf As MPDF;
use App\Models\mDetailSuratJalanPL;
use App\Models\mDetailSuratJalanPT;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mPenjualanTitipan;
use App\Models\mStok;
use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mPenjualanLangsung;
use App\Models\mSuratJalanPenjualanLangsung;
use App\Models\mSuratJalanPenjualanTitipan;
use App\Models\mPerkiraan;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mDeliveryOrder;
use App\Models\mDeliveryOrderDetail;
use Validator,
    DB,
    View,
    PDF;

class SuratJalan extends Controller
{

    private $main;
    private $title;
    private $kodeLabel;
    private $menu;

    function __construct()
    {
        $this->main = new Main();
        $this->title = 'Surat Jalan';
        $this->kodeLabel = 'suratJalan';
        $this->menu = 'surat_jalan';
    }

    function stokRow(Request $request)
    {
        $brg_kode = $request->input('brg_kode');
        $brg_no_seri = $request->input('brg_no_seri');
        $gdg_kode = $request->input('gdg_kode');

        $stk_gdg = mStok::select('stok')
            ->where('brg_kode', $brg_kode)
            ->where('brg_no_seri', $brg_no_seri)
            ->where('gdg_kode', $gdg_kode)
            ->first();

        // $response = [
        //     'stok'=>$stk_gdg,
        // ];

        return $stk_gdg;
    }

    protected function createNoSJPL()
    {
        $kode = $this->main->kodeLabel('suratJalanPL');
        $user = '.' . \Auth::user()->kode;
        $preffix = date('ymd') . '.' . $kode . '.';
        // $lastorder = mSuratJalanPenjualanLangsung::where(\DB::raw('LEFT(sjl_kode, '.strlen($preffix).')'), $preffix)
        // ->selectRaw('RIGHT(sjl_kode, 4) as no_order')->orderBy('no_order', 'desc')->first();
        //
        // if (!empty($lastorder)) {
        //   $now = intval($lastorder->no_order)+1;
        //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        // } else {
        //   $no = '0001';
        // }

        $lastorder = mSuratJalanPenjualanLangsung::where('sjl_kode', 'like', '%' . $preffix . '%')->max('sjl_kode');

        if ($lastorder != null) {
            $lastno = substr($lastorder, 13, 4);
            $now = $lastno + 1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix . $no . $user;
    }

    protected function createNoSJPT()
    {
        $kode = $this->main->kodeLabel('suratJalanPT');
        $user = '.' . \Auth::user()->kode;
        $preffix = date('ymd') . '.' . $kode . '.';
        // $lastorder = mSuratJalanPenjualanTitipan::where(\DB::raw('LEFT(sjt_kode, '.strlen($preffix).')'), $preffix)
        // ->selectRaw('RIGHT(sjt_kode, 4) as no_order')->orderBy('no_order', 'desc')->first();
        //
        // if (!empty($lastorder)) {
        //   $now = intval($lastorder->no_order)+1;
        //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        // } else {
        //   $no = '0001';
        // }

        $lastorder = mSuratJalanPenjualanTitipan::where('sjt_kode', 'like', '%' . $preffix . '%')->max('sjt_kode');

        if ($lastorder != null) {
            $lastno = substr($lastorder, 13, 4);
            $now = $lastno + 1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix . $no . $user;
    }

    protected function createNoSJDO($id)
    {
        $kode = $this->main->kodeLabel('suratJalan');
        $date = substr($id, 0, 6);
        $number = substr($id, -4);
        $preffix = $date . '-' . $kode . 'PT' . '-' . $number;

        return $preffix;
    }

    protected function createDO()
    {
        $kode = $this->main->kodeLabel('delivery_order');
        $user = '.' . \Auth::user()->kode;
        $preffix = date('ymd') . '.' . $kode . '.';
        $lastorder = mDeliveryOrder::where('do_kode', 'like', '%' . $preffix . '%')->max('do_kode');
        // $lastorder = mDeliveryOrder::where(\DB::raw('LEFT(do_kode, '.strlen($preffix).')'), $preffix)
        // ->selectRaw('RIGHT(do_kode, 4) as no_order, do_kode')->orderBy('no_order', 'desc')->first();
        // return $lastorder;

        if ($lastorder != null) {
            $lastno = substr($lastorder, 11, 4);
            $now = $lastno + 1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        // if (!empty($lastorder)) {
        //   $now = intval($lastorder->no_order)+1;
        //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        // } else {
        //   $no = '0001';
        // }

        return $preffix . $no . $user;
    }

    function index()
    {

        ini_set("memory_limit", "-1");
        ini_set('MAX_EXECUTION_TIME', -1);

        $breadcrumb = [
            'Surat Jalan' => route('suratJalanList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = $this->menu;
        // $data['title'] = $this->title;
        $data['title'] = 'DO/SJ';
        $data['kodeCustomer'] = $this->main->kodeLabel('customer');

        $penjualanLangsung = mPenjualanLangsung
            ::with('detail_PL:pl_no_faktur,qty,terkirim,retur')
            ->orderBy('created_at', 'desc')
            ->get(['pl_tgl','pl_no_faktur','cus_kode','cus_nama','cus_alamat','pl_transaksi']);
        foreach ($penjualanLangsung as $keyPL) {
            $qtyPL = 0;
            $terkirimPL = 0;
            $returPL = 0;
            foreach ($keyPL->detail_PL as $keyPLDet) {
                $qtyPL += $keyPLDet->qty;
                $terkirimPL += $keyPLDet->terkirim;
                $returPL += $keyPLDet->retur;
            }
            $keyPL['SJQTY'] = $qtyPL;
            $keyPL['SJterkirim'] = $terkirimPL;
            $keyPL['SJretur'] = $returPL;

        }
        //
        // $PL = $penjualanLangsung;
        // $historyPL = $penjualanLangsung;
        //
        // $filteredPL = $PL->filter(function ($value, $key) {
        //   return $value->SJQTY > $value->SJterkirim;
        // });
        // $data['PL'] = $filteredPL->all();
        //
        // $filteredPLHistory = $historyPL->filter(function ($value, $key) {
        //   return $value->SJQTY = $value->SJterkirim;
        // });
        // $data['historyPL'] = $filteredPLHistory->all();

//        $penjualanTitipan = mPenjualanTitipan::orderBy('created_at', 'desc')->get();
//        foreach ($penjualanTitipan as $keyPT) {
//            $qtyPT = 0;
//            $terkirimPT = 0;
//            $returPT = 0;
//            $keyPT['detail'] = $keyPT->detail_PT;
//            unset($keyPT->detail_PT);
//            foreach ($keyPT['detail'] as $keyPTDet) {
//                $qtyPT += $keyPTDet->qty;
//                $terkirimPT += $keyPTDet->terkirim;
//                $returPT += $keyPTDet->retur;
//            }
//            $keyPT['SJQTY'] = $qtyPT;
//            $keyPT['SJterkirim'] = $terkirimPT;
//            $keyPT['SJretur'] = $returPT;
//        }

        // $PT = $penjualanTitipan;
        // $historyPT = $penjualanTitipan;
        // return $filteredPT;

        $data['PL'] = $penjualanLangsung;
//        $data['PT'] = $penjualanTitipan;

        $data['no_3'] = 1;
        $data['no_4'] = 1;
        return view('suratJalan/suratJalanList_PenjualanLangsung', $data);
    }

    function penjualanTitipan() {
        ini_set("memory_limit", "-1");
        ini_set('MAX_EXECUTION_TIME', -1);

        $breadcrumb = [
            'Surat Jalan' => route('suratJalanList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = $this->menu;
        // $data['title'] = $this->title;
        $data['title'] = 'DO/SJ';
        $data['kodeCustomer'] = $this->main->kodeLabel('customer');

//        $penjualanLangsung = mPenjualanLangsung::orderBy('created_at', 'desc')->get();
//        foreach ($penjualanLangsung as $keyPL) {
//            $qtyPL = 0;
//            $terkirimPL = 0;
//            $returPL = 0;
//            $keyPL['detail'] = $keyPL->detail_PL;
//            unset($keyPL->detail_PL);
//            foreach ($keyPL['detail'] as $keyPLDet) {
//                $qtyPL += $keyPLDet->qty;
//                $terkirimPL += $keyPLDet->terkirim;
//                $returPL += $keyPLDet->retur;
//            }
//            $keyPL['SJQTY'] = $qtyPL;
//            $keyPL['SJterkirim'] = $terkirimPL;
//            $keyPL['SJretur'] = $returPL;
//
//        }
        //
        // $PL = $penjualanLangsung;
        // $historyPL = $penjualanLangsung;
        //
        // $filteredPL = $PL->filter(function ($value, $key) {
        //   return $value->SJQTY > $value->SJterkirim;
        // });
        // $data['PL'] = $filteredPL->all();
        //
        // $filteredPLHistory = $historyPL->filter(function ($value, $key) {
        //   return $value->SJQTY = $value->SJterkirim;
        // });
        // $data['historyPL'] = $filteredPLHistory->all();

        $penjualanTitipan = mPenjualanTitipan
            ::with('detail_PT:pt_no_faktur,qty,terkirim,retur')
            ->orderBy('created_at', 'desc')
            ->get([
                'pt_tgl','pt_no_faktur','cus_kode','cus_nama','cus_alamat','pt_transaksi'
            ]);
        foreach ($penjualanTitipan as $keyPT) {
            $qtyPT = 0;
            $terkirimPT = 0;
            $returPT = 0;
            foreach ($keyPT->detail_PT as $keyPTDet) {
                $qtyPT += $keyPTDet->qty;
                $terkirimPT += $keyPTDet->terkirim;
                $returPT += $keyPTDet->retur;
            }
            $keyPT['SJQTY'] = $qtyPT;
            $keyPT['SJterkirim'] = $terkirimPT;
            $keyPT['SJretur'] = $returPT;
        }

        // $PT = $penjualanTitipan;
        // $historyPT = $penjualanTitipan;
        // return $filteredPT;

//        $data['PL'] = $penjualanLangsung;
        $data['PT'] = $penjualanTitipan;

        $data['no_3'] = 1;
        $data['no_4'] = 1;
        return view('suratJalan/suratJalanList_PenjualanTitipan', $data);
    }

    function penjualanLangsungHistory() {
        ini_set("memory_limit", "-1");
        ini_set('MAX_EXECUTION_TIME', -1);

        $breadcrumb = [
            'Surat Jalan' => route('suratJalanList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = $this->menu;
        // $data['title'] = $this->title;
        $data['title'] = 'DO/SJ';
        $data['kodeCustomer'] = $this->main->kodeLabel('customer');

        $penjualanLangsung = mPenjualanLangsung
            ::with('detail_PL:pl_no_faktur,qty,terkirim,retur')
            ->orderBy('created_at', 'desc')
            ->get(['pl_tgl','pl_no_faktur','cus_kode','cus_nama','cus_alamat','pl_transaksi']);
        foreach ($penjualanLangsung as $keyPL) {
            $qtyPL = 0;
            $terkirimPL = 0;
            $returPL = 0;
            foreach ($keyPL->detail_PL as $keyPLDet) {
                $qtyPL += $keyPLDet->qty;
                $terkirimPL += $keyPLDet->terkirim;
                $returPL += $keyPLDet->retur;
            }
            $keyPL['SJQTY'] = $qtyPL;
            $keyPL['SJterkirim'] = $terkirimPL;
            $keyPL['SJretur'] = $returPL;

        }
        //
        // $PL = $penjualanLangsung;
        // $historyPL = $penjualanLangsung;
        //
        // $filteredPL = $PL->filter(function ($value, $key) {
        //   return $value->SJQTY > $value->SJterkirim;
        // });
        // $data['PL'] = $filteredPL->all();
        //
        // $filteredPLHistory = $historyPL->filter(function ($value, $key) {
        //   return $value->SJQTY = $value->SJterkirim;
        // });
        // $data['historyPL'] = $filteredPLHistory->all();

//        $penjualanTitipan = mPenjualanTitipan::orderBy('created_at', 'desc')->get();
//        foreach ($penjualanTitipan as $keyPT) {
//            $qtyPT = 0;
//            $terkirimPT = 0;
//            $returPT = 0;
//            $keyPT['detail'] = $keyPT->detail_PT;
//            unset($keyPT->detail_PT);
//            foreach ($keyPT['detail'] as $keyPTDet) {
//                $qtyPT += $keyPTDet->qty;
//                $terkirimPT += $keyPTDet->terkirim;
//                $returPT += $keyPTDet->retur;
//            }
//            $keyPT['SJQTY'] = $qtyPT;
//            $keyPT['SJterkirim'] = $terkirimPT;
//            $keyPT['SJretur'] = $returPT;
//        }

        // $PT = $penjualanTitipan;
        // $historyPT = $penjualanTitipan;
        // return $filteredPT;

        $data['PL'] = $penjualanLangsung;
//        $data['PT'] = $penjualanTitipan;

        $data['no_3'] = 1;
        $data['no_4'] = 1;
        return view('suratJalan/suratJalanList_PenjualanLangsungHistory', $data);
    }

    function penjualanTitipanHistory() {
        ini_set("memory_limit", "-1");
        ini_set('MAX_EXECUTION_TIME', -1);

        $breadcrumb = [
            'Surat Jalan' => route('suratJalanList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = $this->menu;
        // $data['title'] = $this->title;
        $data['title'] = 'DO/SJ';
        $data['kodeCustomer'] = $this->main->kodeLabel('customer');

//        $penjualanLangsung = mPenjualanLangsung::orderBy('created_at', 'desc')->get();
//        foreach ($penjualanLangsung as $keyPL) {
//            $qtyPL = 0;
//            $terkirimPL = 0;
//            $returPL = 0;
//            $keyPL['detail'] = $keyPL->detail_PL;
//            unset($keyPL->detail_PL);
//            foreach ($keyPL['detail'] as $keyPLDet) {
//                $qtyPL += $keyPLDet->qty;
//                $terkirimPL += $keyPLDet->terkirim;
//                $returPL += $keyPLDet->retur;
//            }
//            $keyPL['SJQTY'] = $qtyPL;
//            $keyPL['SJterkirim'] = $terkirimPL;
//            $keyPL['SJretur'] = $returPL;
//
//        }
        //
        // $PL = $penjualanLangsung;
        // $historyPL = $penjualanLangsung;
        //
        // $filteredPL = $PL->filter(function ($value, $key) {
        //   return $value->SJQTY > $value->SJterkirim;
        // });
        // $data['PL'] = $filteredPL->all();
        //
        // $filteredPLHistory = $historyPL->filter(function ($value, $key) {
        //   return $value->SJQTY = $value->SJterkirim;
        // });
        // $data['historyPL'] = $filteredPLHistory->all();

        $penjualanTitipan = mPenjualanTitipan
            ::with('detail_PT:pt_no_faktur,qty,terkirim,retur')
            ->orderBy('created_at', 'desc')
            ->get([
                'pt_tgl','pt_no_faktur','cus_kode','cus_nama','cus_alamat','pt_transaksi'
            ]);
        foreach ($penjualanTitipan as $keyPT) {
            $qtyPT = 0;
            $terkirimPT = 0;
            $returPT = 0;
            foreach ($keyPT->detail_PT as $keyPTDet) {
                $qtyPT += $keyPTDet->qty;
                $terkirimPT += $keyPTDet->terkirim;
                $returPT += $keyPTDet->retur;
            }
            $keyPT['SJQTY'] = $qtyPT;
            $keyPT['SJterkirim'] = $terkirimPT;
            $keyPT['SJretur'] = $returPT;
        }

        // $PT = $penjualanTitipan;
        // $historyPT = $penjualanTitipan;
        // return $filteredPT;

//        $data['PL'] = $penjualanLangsung;
        $data['PT'] = $penjualanTitipan;

        $data['no_3'] = 1;
        $data['no_4'] = 1;
        return view('suratJalan/suratJalanList_PenjualanTitipanHistory', $data);
    }

    function detail($kode, $tipe)
    {
        $breadcrumb = [
            'Surat Jalan' => route('suratJalanList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['title'] = 'DO/SJ';
        $data['menu'] = $this->menu;
        $kodeCustomer = $this->main->kodeLabel('customer');
        $kodeGudang = $this->main->kodeLabel('gudang');
        $kodeBarang = $this->main->kodeLabel('barang');
        $kodeKaryawan = $this->main->kodeLabel('karyawan');
        $karyawan = mKaryawan::all();
        $sopir = mKaryawan::where('kry_posisi', 'sopir')->get();
        // $gudang = mGudang::get();
        $tanggal = date('Y-m-d');
        if ($tipe == 'langsung') {
            $list = mDetailPenjualanLangsung::with('gdg:gdg_kode,gdg_nama')->where('pl_no_faktur', $kode)->get();
            $historyDO = mDeliveryOrder::where('no_faktur', $kode)->orderBy('created_at', 'desc')->get();
            $row = mPenjualanLangsung::where('pl_no_faktur', $kode)->first();
            $noFaktur = $row->pl_no_faktur;
            $customer = mCustomer::find($row->cus_kode);
            if ($customer == null) {
                $customer = $row->cus_kode;
                $pelanggan = $kodeCustomer . $row->cus_kode;
                $alamat = '';
                $telp = '';
            } else {
                $pelanggan = $kodeCustomer . $customer->cus_kode;
                $alamat = $customer->cus_alamat;
                $telp = $customer->cus_telp;
            }
            $idSurat = 'sjl_kode';
            $tglSurat = 'sjl_tgl';
            $idSopir = 'sjl_sopir';

            $do_kode_last = $this->createDO();
            $do = $do_kode_last;
            $data['DO'] = $do;
        } else {
            $list = mDetailPenjualanTitipan::with('gdg:gdg_kode,gdg_nama')->where('pt_no_faktur', $kode)->get();
            $historySJ = mSuratJalanPenjualanTitipan::where('pt_no_faktur', $kode)->orderBy('created_at', 'desc')->get();
            $historyDO = mDeliveryOrder::where('no_faktur', $kode)->orderBy('created_at', 'desc')->get();
            $row = mPenjualanTitipan::where('pt_no_faktur', $kode)->first();
            $noFaktur = $row->pt_no_faktur;
            $customer = mCustomer::find($row->cus_kode);
            if ($customer == null) {
                $customer = $row->cus_kode;
                $pelanggan = $kodeCustomer . $row->cus_kode;
                $alamat = '';
                $telp = '';
            } else {
                $pelanggan = $kodeCustomer . $customer->cus_kode;
                $alamat = $customer->cus_alamat;
                $telp = $customer->cus_telp;
            }
            $idSurat = 'sjt_kode';
            $tglSurat = 'sjt_tgl';
            $idSopir = 'sjt_sopir';

            $sjt_kode_last = $this->createNoSJPT();
            $sj = $sjt_kode_last;
            $data['SJ'] = $sj;

            $do_kode_last = $this->createDO();
            $do = $do_kode_last;
            $data['DO'] = $do;
            $data['historySJ'] = $historySJ;
        }

        $data['list'] = $list;
        // $data['gudang'] = $gudang;
        $data['historyDO'] = $historyDO;
        $data['row'] = $row;
        // $data['kodeCustomer'] = $kodeCustomer;
        $data['kodeGudang'] = $kodeGudang;
        $data['kodeBarang'] = $kodeBarang;
        $data['kodeKaryawan'] = $kodeKaryawan;
        $data['karyawan'] = $karyawan;
        $data['sopir'] = $sopir;
        // $data['customer'] = $customer;
        $data['noFaktur'] = $noFaktur;
        $data['pelanggan'] = $pelanggan;
        $data['alamat'] = $alamat;
        $data['telp'] = $telp;
        $data['tanggal'] = $tanggal;
        $data['kode'] = $kode;
        $data['tipe'] = $tipe;
        $data['idSurat'] = $idSurat;
        $data['tglSurat'] = $tglSurat;
        $data['idSopir'] = $idSopir;
        $data['no_3'] = 1;

        // return $data;
        return view('suratJalan/suratJalanDetail', $data);
    }

    function insert(Request $request)
    {
        // return $request;
        DB::beginTransaction();
        try {
            // $do_kode = $this->createDO();
            $time = Carbon::now();
            $input_tanggal = $request->input('tanggal');
            if ($input_tanggal == null) {
                $input_tanggal = $time;
            }

            $detail_kode = $request->input('detail_kode');
            $brg_kode = $request->input('brg_kode');
            $brg_no_seri = $request->input('brg_no_seri');
            $gdg_kode = $request->input('gdg_kode');
            $stn_kode = $request->input('stn_kode');
            $dikirim = $request->input('dikirim');
            $harga_jual = $request->input('harga_jual');
            $ppn_brg = $request->input('ppn');
            $disc = $request->input('disc');
            $brg_hpp = $request->input('brg_hpp');

            $unique_gdg = array_unique($gdg_kode);

            if ($request->input('tipe') == 'langsung') {
                $DO_array = array();
                foreach ($unique_gdg as $keyGdg => $valueGdg) {
                    $gudang = mGudang::where('gdg_kode', $valueGdg)->first();
                    $do = new mDeliveryOrder();
                    $new_do_kode = $this->createDO();
                    $do->do_kode = $new_do_kode;
                    $do->do_tgl = $time;
                    $do->no_faktur = $request->input('kode');
                    $do->gudang = $gudang->gdg_nama;
                    $do->gudang_alamat = $gudang->gdg_alamat;
                    $do->cus_kode = $request->input('cus_kode');
                    $do->do_sopir = $request->input('sopir');
                    $do->do_catatan = $request->input('catatan');
                    $do->cus_alamat = $request->input('cus_alamat');
                    $do->cus_telp = $request->input('cus_telp');
                    $do->do_checker = $request->input('checker');
                    $do->save();

                    array_push($DO_array, $new_do_kode);

                    foreach ($brg_kode as $key => $value) {
                        if ($dikirim[$key] != 0 && $gdg_kode[$key] == $valueGdg) {
                            $detailDO = new mDeliveryOrderDetail();
                            $detailDO->do_kode = $new_do_kode;
                            $detailDO->brg_kode = $brg_kode[$key];
                            $detailDO->brg_no_seri = $brg_no_seri[$key];
                            $detailDO->stn_kode = $stn_kode[$key];
                            $detailDO->gdg_kode = $gdg_kode[$key];
                            $detailDO->dikirim = $dikirim[$key];
                            $detailDO->det_kode = $detail_kode[$key];
                            $detailDO->save();

                            $detailPL = mDetailPenjualanLangsung::where('detail_pl_kode', $detail_kode[$key])->first();
                            $detailPL->terkirim = ($detailPL->terkirim + $dikirim[$key]);
                            $detailPL->save();
                        }
                    }

                    // foreach ($detail_kode as $key => $value) {
                    //   $detailPL = mDetailPenjualanLangsung::where('detail_pl_kode', $detail_kode[$key])->first();
                    //   $detailPL->terkirim = ($detailPL->terkirim+$dikirim[$key]);
                    //   $detailPL->save();
                    // }
                }
                // return $DO_array;
                DB::commit();

                return [
                    'redirect' => route('suratJalanDetail', ['kode' => $request->input('kode'), 'tipe' => $request->input('tipe')]),
                    'do' => $DO_array,
                    'tipe' => 'langsung'
                ];
            } else {
                $DO_array = array();
                foreach ($unique_gdg as $keyGdg => $valueGdg) {
                    $gudang = mGudang::where('gdg_kode', $valueGdg)->first();
                    $do = new mDeliveryOrder();
                    $new_do_kode = $this->createDO();
                    $do->do_kode = $new_do_kode;
                    $do->do_tgl = $time;
                    $do->no_faktur = $request->input('kode');
                    $do->gudang = $gudang->gdg_nama;
                    $do->gudang_alamat = $gudang->gdg_alamat;
                    $do->cus_kode = $request->input('cus_kode');
                    $do->do_sopir = $request->input('sopir');
                    $do->do_catatan = $request->input('catatan');
                    $do->cus_alamat = $request->input('cus_alamat');
                    $do->cus_telp = $request->input('cus_telp');
                    $do->do_checker = $request->input('checker');
                    $do->save();

                    array_push($DO_array, $new_do_kode);

                    foreach ($brg_kode as $key => $value) {
                        if ($dikirim[$key] != 0 && $gdg_kode[$key] == $valueGdg) {
                            $detailDO = new mDeliveryOrderDetail();
                            $detailDO->do_kode = $new_do_kode;
                            $detailDO->brg_kode = $brg_kode[$key];
                            $detailDO->brg_no_seri = $brg_no_seri[$key];
                            $detailDO->stn_kode = $stn_kode[$key];
                            $detailDO->gdg_kode = $gdg_kode[$key];
                            $detailDO->dikirim = $dikirim[$key];
                            $detailDO->det_kode = $detail_kode[$key];
                            $detailDO->save();

                            $detailPT = mDetailPenjualanTitipan::where('detail_pt_kode', $detail_kode[$key])->first();
                            $detailPT->terkirim = ($detailPT->terkirim + $dikirim[$key]);
                            $detailPT->tgl_kirim_terakhir = $input_tanggal;
                            $detailPT->save();
                        }
                    }
                }

                $NewSj = $this->createNoSJPT();

                $SuratJalanPT = new mSuratJalanPenjualanTitipan();
                $SuratJalanPT->sjt_kode = $NewSj;
                $SuratJalanPT->sjt_tgl = $input_tanggal;
                $SuratJalanPT->pt_no_faktur = $request->input('kode');
                $SuratJalanPT->cus_kode = $request->input('cus_kode');
                $SuratJalanPT->sjt_sopir = $request->input('sopir');
                $SuratJalanPT->sjt_catatan = $request->input('catatan');
                $SuratJalanPT->cus_alamat = $request->input('cus_alamat');
                $SuratJalanPT->cus_telp = $request->input('cus_telp');
                $SuratJalanPT->sjt_checker = $request->input('checker');
                $SuratJalanPT->save();

                $kode_surat_jalan = $NewSj;
                // $detail_pt_kode = mDetailPenjualanTitipan::where('detail_pt_kode', $kode_surat_jalan)->get();

                // $disc_penjualan = mPenjualanTitipan::where('pt_no_faktur', $request->input('kode'))->first()['pt_ppn'];
                // $disc_penjualan = mPenjualanTitipan::where('pt_no_faktur', $request->input('kode'))->first()['pt_disc'];
                $disc_penjualan = mPenjualanTitipan::where('pt_no_faktur', $request->input('kode'))->first()['pt_disc_nom'];
                $penjualan = 0;
                $ppn = 0;
                $ppn_total = 0;
                $persedian = 0;
                $persedian_total = 0;
                $hutang = 0;
                $hpp = 0;
                $potongan = 0;
                $potongan_ats = 0;
                $potongan_ats_total = 0;
                $potongan_bwh = 0;
                $potongan_bwh_total = 0;
                $harga_net = 0;
                $harga_net_total = 0;
                foreach ($brg_kode as $key => $value) {
                    if ($dikirim[$key] != 0) {
                        $persedian = $brg_hpp[$key] * $dikirim[$key];
                        $hpp += $persedian;
                        $persedian_total += $persedian;

                        if ($disc != 0) {
                            $potongan_ats = round($harga_jual[$key] * ($disc[$key] / 100));
                            $potongan_ats_total += round($potongan_ats * $dikirim[$key]);
                        }

                        if ($ppn_brg[$key] != 0) {
                            $ppn = round(($harga_jual[$key] - $potongan_ats) * ($ppn_brg[$key] / 100));
                            $ppn_total += $ppn * $dikirim[$key];
                        }

                        $harga_net = $harga_jual[$key] - $potongan_ats + $ppn;
                        $harga_net_total += $harga_net * $dikirim[$key];

                        $penjualan += round($harga_jual[$key] * $dikirim[$key]);

                        // if ($disc_penjualan != 0) {
                        //   $potongan_bwh += (($harga_jual[$key]-$potongan_ats)*$dikirim[$key])*($disc_penjualan/100);
                        // }
                        // $potongan = $potongan_ats + $potongan_bwh;
                        //
                        // if ($ppn_brg[$key] == 0) {
                        //   $ppn += 0;
                        // }
                        // else {
                        //   $ppn += ($penjualan-$potongan)*($ppn_brg[$key]/100);
                        // }

                        // $persedian += $brg_hpp[$key]*$dikirim[$key];
                        // $hutang += $penjualan-$potongan+$ppn;
                        // $hutang = floor($hutang / 0.01) * 0.01;
                        // $hpp += $persedian;

                        $detailSuratPT = new mDetailSuratJalanPT();
                        $detailSuratPT->sjt_kode = $kode_surat_jalan;
                        $detailSuratPT->brg_kode = $brg_kode[$key];
                        $detailSuratPT->brg_no_seri = $brg_no_seri[$key];
                        $detailSuratPT->stn_kode = $stn_kode[$key];
                        $detailSuratPT->gdg_kode = $gdg_kode[$key];
                        $detailSuratPT->dikirim = $dikirim[$key];
                        $detailSuratPT->det_kode = $detail_kode[$key];
                        $detailSuratPT->save();

                        $kurang = mStok::where('brg_kode', $brg_kode[$key])->where('gdg_kode', $gdg_kode[$key])->where('brg_no_seri', $brg_no_seri[$key])->first();
                        $kurang->stok = ($kurang->stok - $dikirim[$key]);
                        $kurang->stok_titipan = ($kurang->stok_titipan - $dikirim[$key]);
                        $kurang->save();

                        $total = 0;
                        $data = mStok::where('brg_kode', $brg_kode[$key])->get();
                        foreach ($data as $gdg) {
                            $total += $gdg->stok;
                        }
                        $QOH_keluar = $total;

                        $time = Carbon::now();
                        $arusStok = new mArusStok();
                        $arusStok->ars_stok_date = $time;
                        $arusStok->brg_kode = $brg_kode[$key];
                        $arusStok->stok_in = 0;
                        $arusStok->stok_out = $dikirim[$key];
                        $arusStok->stok_prev = $QOH_keluar;
                        $arusStok->gdg_kode = $gdg_kode[$key];
                        $arusStok->keterangan = 'Surat Jalan ' . $kode_surat_jalan;
                        $arusStok->save();
                    }
                }
                if ($disc_penjualan != 0) {
                    // $potongan_bwh = $harga_net_total*($disc_penjualan/100);
                    $potongan_bwh = $disc_penjualan;
                }
                $potongan = $potongan_ats_total + $potongan_bwh;

                $hutang += round($penjualan) - round($potongan) + round($ppn_total);
                // $hutang = floor($hutang / 0.01) * 0.01;
                // $hutang = ceil($hutang / 0.01) * 0.01;
                $hutang = round($hutang);
                // $hutang = $hutang;

                // foreach ($detail_kode as $key => $value) {
                //   $detailPT = mDetailPenjualanTitipan::where('detail_pt_kode', $detail_kode[$key])->first();
                //   $detailPT->terkirim = ($detailPT->terkirim+$dikirim[$key]);
                //   $detailPT->save();
                // }

                $kode_bukti_id = $kode_surat_jalan;

                // General
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $date_db = date('Y-m-d H:i:s');

                $where = [
                    'jmu_year' => $year,
                    'jmu_month' => $month,
                    'jmu_day' => $day
                ];
                $jmu_no = mJurnalUmum::where($where)
                    ->orderBy('jmu_no', 'DESC')
                    ->select('jmu_no')
                    ->limit(1);
                if ($jmu_no->count() == 0) {
                    $jmu_no = 1;
                } else {
                    $jmu_no = $jmu_no->first()->jmu_no + 1;
                }

                $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id', 'DESC')
                    ->limit(1)
                    ->first();
                $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

                $data_jurnal = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'no_invoice' => $kode_bukti_id,
                    'id_pel' => 'CUS' . $request->input('cus_kode'),
                    // 'kode_bukti_id'=>$kode_bukti_id,
                    'jmu_no' => $jmu_no,
                    'jmu_tanggal' => date('Y-m-d'),
                    'jmu_year' => $year,
                    'jmu_month' => $month,
                    'jmu_day' => $day,
                    'jmu_keterangan' => 'Surat Jalan Penjualan Titipan ' . $request->input('kode'),
                    'jmu_date_insert' => $date_db,
                    'jmu_date_update' => $date_db,
                ];
                mJurnalUmum::insert($data_jurnal);

                // Transaksi
                $tipe_arus_kas = 'Operasi';

                // Jurnal Umum
                // $kode_bukti_id = $request->input('kode_bukti_id');
                // $jurnalID = mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $kode_bukti_id)->first();

                $master_id_kredit = ["2102", "311013", "41101", "311011", "2205", "1601"];
                foreach ($master_id_kredit as $key => $val) {
                    $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
                    $master_idK = $masterK->master_id;
                    $trs_kode_rekening = $masterK->mst_kode_rekening;
                    $trs_nama_rekening = $masterK->mst_nama_rekening;
                    $debet_nom = 0;

                    if ($trs_kode_rekening == "2102") {
                        $debet_nom = round($hutang);
                        $kredit_nom = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                    } elseif ($trs_kode_rekening == "311013") {
                        $debet_nom = round($potongan);
                        $kredit_nom = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                    } elseif ($trs_kode_rekening == "41101") {
                        $debet_nom = $hpp;
                        $kredit_nom = 0;
                        $trs_jenis_transaksi = 'debet';
                        $trs_catatan = 'debet';
                    } elseif ($trs_kode_rekening == "311011") {
                        $debet_nom = 0;
                        $kredit_nom = round($penjualan);
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                    } elseif ($trs_kode_rekening == "2205") {
                        $debet_nom = 0;
                        $kredit_nom = round($ppn_total);
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                    } elseif ($trs_kode_rekening == "1601") {
                        $debet_nom = 0;
                        $kredit_nom = $persedian_total;
                        $trs_jenis_transaksi = 'kredit';
                        $trs_catatan = 'kredit';
                    }

                    $data_transaksiK = [
                        'jurnal_umum_id' => $jurnal_umum_id,
                        'master_id' => $master_idK,
                        'trs_jenis_transaksi' => $trs_jenis_transaksi,
                        'trs_debet' => $debet_nom,
                        'trs_kredit' => $kredit_nom,
                        'user_id' => 0,
                        'trs_year' => $year,
                        'trs_month' => $month,
                        'trs_kode_rekening' => $trs_kode_rekening,
                        'trs_nama_rekening' => $trs_nama_rekening,
                        'trs_tipe_arus_kas' => $tipe_arus_kas,
                        'trs_catatan' => $trs_catatan,
                        'trs_date_insert' => $date_db,
                        'trs_date_update' => $date_db,
                        'tgl_transaksi' => date('Y-m-d'),
                    ];
                    mTransaksi::insert($data_transaksiK);
                }

                // $sjt_kode_last = mSuratJalanPenjualanTitipan::orderBy('sjt_kode','DESC')->limit(1)->first();
                // $sjt_kode_last = $sjt_kode_last['sjt_kode']+1;

                DB::commit();

                return [
                    'redirect' => route('suratJalanDetail', ['kode' => $request->input('kode'), 'tipe' => $request->input('tipe')]),
                    'sj' => $kode_surat_jalan,
                    'do' => $DO_array,
                    'tipe' => 'titipan'
                ];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    function cetakView($kode, $tipe)
    {
        $breadcrumb = [
            'Surat Jalan' => route('suratJalanList'),
            'Cetak' => ''
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $kodeCustomer = $this->main->kodeLabel('customer');
        $kodeGudang = $this->main->kodeLabel('gudang');
        $kodeBarang = $this->main->kodeLabel('barang');
        $tanggal = date('Y-m-d');

        $row = mSuratJalanPenjualanTitipan::leftJoin('tb_penjualan_titipan AS pt', 'pt.pt_no_faktur', '=', 'tb_surat_jalan_penjualan_titipan.pt_no_faktur')
            ->where('tb_surat_jalan_penjualan_titipan.sjt_kode', $kode)
            ->first();
        $list = mDetailSuratJalanPT::where('sjt_kode', $row->sjt_kode)->get();
        foreach ($list as $key) {
            $key->SuratJalan->noFaktur;
            // $key->barang->satuan;
            $key->barang;
            $key->gudang;
        }
        $customer = mCustomer::find($row->cus_kode);
        $noFaktur = $row->pt_no_faktur;
        if ($customer == null) {
            $customer = $row->cus_kode;
            $pelanggan = $kodeCustomer . $row->cus_kode;
            $alamat = '';
            $telp = '';
        } else {
            $pelanggan = $kodeCustomer . $customer->cus_kode;
            $alamat = $customer->cus_alamat;
            $telp = $customer->cus_telp;
        }
        $sj = $row->sjt_kode;
        $kodeDO = $row->sjt_kode;
        $idSurat = 'sjt_kode';
        $tglSurat = 'sjt_tgl';
        $idSopir = 'sjt_sopir';
        $idNoFaktur = 'pt_no_faktur';
        $catatan = $row->sjt_catatan;
        $sopir = mKaryawan::find($row['sjt_sopir']);
        $sopir = $sopir['kry_nama'];

        $data['list'] = $list;
        $data['row'] = $row;
        $data['kodeCustomer'] = $kodeCustomer;
        $data['kodeGudang'] = $kodeGudang;
        $data['kodeBarang'] = $kodeBarang;
        $data['title'] = $this->title;
        $data['menu'] = $this->menu;
        $data['customer'] = $customer;
        $data['noFaktur'] = $noFaktur;
        $data['pelanggan'] = $pelanggan;
        $data['alamat'] = $alamat;
        $data['telp'] = $telp;
        $data['SJ'] = $sj;
        $data['kodeDO'] = $kodeDO;
        $data['tanggal'] = $tanggal;
        $data['kode'] = $kode;
        $data['tipe'] = $tipe;
        $data['idSurat'] = $idSurat;
        $data['tglSurat'] = $tglSurat;
        $data['idSopir'] = $idSopir;
        $data['idNoFaktur'] = $idNoFaktur;
        $data['catatan'] = $catatan;
        $data['sopir'] = $sopir;

        // return $list;
        return view('suratJalan/suratJalanCetakView', $data);
    }

    function cetak($kode, $tipe)
    {
        $data = $this->main->data([], $this->kodeLabel);
        $kodeCustomer = $this->main->kodeLabel('customer');
        $kodeGudang = $this->main->kodeLabel('gudang');
        $kodeBarang = $this->main->kodeLabel('barang');
        $kodeKaryawan = $this->main->kodeLabel('karyawan');
        $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
        $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
        $kodeSuratJalan = $this->main->kodeLabel('suratJalan');
        $karyawan = mKaryawan::all();
        $tanggal = date('Y-m-d');
        if ($tipe == 'langsung') {
            $row = mSuratJalanPenjualanLangsung::leftJoin('tb_penjualan_langsung AS pl', 'pl.pl_no_faktur', '=', 'tb_surat_jalan_penjualan_langsung.pl_no_faktur')
                ->where('tb_surat_jalan_penjualan_langsung.sjl_kode', $kode)
                ->first();
            $list = mDetailSuratJalanPL::where('sjl_kode', $row->sjl_kode)->get();
            foreach ($list as $key) {
                $key->SuratJalan->noFaktur;
                $key->barang->satuan;
                $key->gudang;
            }
            $customer = mCustomer::find($row->cus_kode);
            $kodeDetail = $this->main->kodeLabel('penjualanLangsung');
            $noFaktur = $kodePenjualanLangsung . $row->pl_no_faktur;
            if ($customer == null) {
                $customer = $row->cus_kode;
                $pelanggan = $kodeCustomer . $row->cus_kode;
                $alamat = '';
                $telp = '';
            } else {
                $pelanggan = $kodeCustomer . $customer->cus_kode;
                $alamat = $customer->cus_alamat;
                $telp = $customer->cus_telp;
            }
            $do = $kodeSuratJalan . $row->sjt_kode;
            $kodePenjualan = $kodePenjualanLangsung;
            $idSurat = 'sjl_kode';
            $tglSurat = 'sjl_tgl';
            $idSopir = 'sjl_sopir';
            $idNoFaktur = 'pl_no_faktur';
            $catatan = $row->sjl_catatan;
            $sopir = mKaryawan::find($row['sjl_sopir']);
            $sopir = $sopir['kry_nama'];
            $checker = mKaryawan::find($row['pl_checker']);
            $checker = $checker['kry_nama'];
        } else {
            $row = mSuratJalanPenjualanTitipan::leftJoin('tb_penjualan_titipan AS pt', 'pt.pt_no_faktur', '=', 'tb_surat_jalan_penjualan_titipan.pt_no_faktur')
                ->where('tb_surat_jalan_penjualan_titipan.sjt_kode', $kode)
                ->first();
            $list = mDetailSuratJalanPT::where('sjt_kode', $row->sjt_kode)->get();
            foreach ($list as $key) {
                $key->SuratJalan->noFaktur;
                $key->barang->satuan;
                $key->gudang;
            }
            $kodeDetail = $this->main->kodeLabel('penjualanTitipan');
            $customer = mCustomer::find($row->cus_kode);
            $noFaktur = $kodePenjualanTitipan . $row->pt_no_faktur;
            if ($customer == null) {
                $customer = $row->cus_kode;
                $pelanggan = $kodeCustomer . $row->cus_kode;
                $alamat = '';
                $telp = '';
            } else {
                $pelanggan = $kodeCustomer . $customer->cus_kode;
                $alamat = $customer->cus_alamat;
                $telp = $customer->cus_telp;
            }
            $do = $kodeSuratJalan . $row->sjt_kode;
            $kodePenjualan = $kodePenjualanTitipan;
            $idSurat = 'sjt_kode';
            $tglSurat = 'sjt_tgl';
            $idSopir = 'sjt_sopir';
            $idNoFaktur = 'pt_no_faktur';
            $catatan = $row->sjt_catatan;
            $sopir = mKaryawan::find($row['sjt_sopir']);
            $sopir = $sopir['kry_nama'];
            $checker = mKaryawan::find($row['pt_checker']);
            $checker = $checker['kry_nama'];
        }


        $data['list'] = $list;
        $data['row'] = $row;
        $data['kodeCustomer'] = $kodeCustomer;
        $data['kodeGudang'] = $kodeGudang;
        $data['kodeBarang'] = $kodeBarang;
        $data['kodeDetail'] = $kodeDetail;
        $data['kodeKaryawan'] = $kodeKaryawan;
        $data['kodePenjualan'] = $kodePenjualan;
        $data['kodeSuratJalan'] = $kodeSuratJalan;
        $data['title'] = $this->title;
        $data['menu'] = $this->menu;
        $data['karyawan'] = $karyawan;
        $data['kodeDetail'] = $kodeDetail;
        $data['customer'] = $customer;
        $data['noFaktur'] = $noFaktur;
        $data['pelanggan'] = $pelanggan;
        $data['alamat'] = $alamat;
        $data['telp'] = $telp;
        $data['do'] = $do;
        $data['tanggal'] = $tanggal;
        $data['kode'] = $kode;
        $data['tipe'] = $tipe;
        $data['idSurat'] = $idSurat;
        $data['tglSurat'] = $tglSurat;
        $data['idSopir'] = $idSopir;
        $data['idNoFaktur'] = $idNoFaktur;
        $data['catatan'] = $catatan;
        $data['sopir'] = $sopir;
        $data['checker'] = $checker;

        // return $data;

        //return view('suratJalan/suratJalanPdf', $data);

        // $pdf = PDF::loadView('suratJalan/suratJalanPdf', $data);
        // return $pdf->download($kodeSuratJalan.$kode.'-'.$tipe.'.pdf');

        $pdf = PDF::loadView('suratJalan.suratJalanPdf', $data)
            ->setPaper('a4', 'potrait');
        return $pdf->stream();
    }

    function rules($request)
    {
        $rules = [
            'cus_kode' => 'required',
            'pl_transaksi' => 'required',
            'pl_sales_person' => 'required',
            'pl_checker' => 'required',
            'pl_sopir' => 'required',
            'pl_subtotal' => 'required',
            'pl_disc' => 'required',
            'pl_disc_nom' => 'required',
            'pl_ppn' => 'required',
            'pl_ppn_nom' => 'required',
            'pl_ongkos_angkut' => 'required',
            'grand_total'
        ];

        if ($request['pl_tgl_jatuh_tempo'] == '') {
            $rules['pl_tgl_jatuh_tempo'] = 'required';
        }

        $customeMessage = [
            'required' => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();

    }

    function insert1(Request $request)
    {
        $this->rules($request->all());
        $data = $request->except(array('_token', 'sample_1_length'));
        $data['pl_tgl'] = date('Y-m-d H:i:s');
        mPenjualanLangsung::insert($data);
        return [
            'redirect' => route('penjualanLangsungList')
        ];
    }

    function update(Request $request, $pl_no_faktur)
    {
        $this->rules($request->all());
        $data = $request->except(array('_token', 'sample_1_length'));
        $data['pl_tgl'] = date('Y-m-d H:i:s');
        mPenjualanLangsung::where(['pl_no_faktur' => $pl_no_faktur])->update($data);
        return [
            'redirect' => route('penjualanLangsungList')
        ];
    }

    function delete($pl_no_faktur)
    {
        mPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)->delete();
    }

    function deleteDO($DO, $TYPE)
    {
        \DB::beginTransaction();
        try {
            if ($TYPE == 'langsung') {
                $detail = mDeliveryOrderDetail::where('do_kode', $DO)->get();
                foreach ($detail as $key => $value) {
                    $detailPL = mDetailPenjualanLangsung::where('detail_pl_kode', $value->det_kode)->first();
                    $detailPL->terkirim = ($detailPL->terkirim - $value->dikirim);
                    $detailPL->save();
                }
                mDeliveryOrderDetail::where('do_kode', $DO)->delete();
                mDeliveryOrder::where('do_kode', $DO)->delete();
            } else {
                mDeliveryOrder::where('do_kode', $DO)->delete();
                mDeliveryOrderDetail::where('do_kode', $DO)->delete();
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    function deleteSJ($SJ)
    {
        \DB::beginTransaction();
        try {
            mSuratJalanPenjualanTitipan::where('sjt_kode', $SJ)->delete();
            $detail = mDetailSuratJalanPT::where('sjt_kode', $SJ)->get();
            foreach ($detail as $key => $value) {
                $detailPT = mDetailPenjualanTitipan::where('detail_pt_kode', $value->det_kode)->first();
                $detailPT->terkirim = ($detailPT->terkirim - $value->dikirim);
                $detailPT->save();

                $tambah = mStok::where('brg_kode', $value->brg_kode)
                    ->where('brg_no_seri', $value->brg_no_seri)
                    ->where('gdg_kode', $value->gdg_kode)
                    ->first();
                $tambah->stok = ($tambah->stok + $value->dikirim);
                $tambah->save();

                $total_x = 0;
                $data = mStok::where('brg_kode', $value->brg_kode)->get();
                foreach ($data as $gdg) {
                    $total_x += $gdg->stok;
                }
                $QOH_masuk = $total_x;

                $time = Carbon::now();
                $arusStok = new mArusStok();
                $arusStok->ars_stok_date = $time;
                $arusStok->brg_kode = $value->brg_kode;
                $arusStok->stok_in = $value->dikirim;
                $arusStok->stok_out = 0;
                $arusStok->stok_prev = $QOH_masuk;
                $arusStok->gdg_kode = $value->gdg_kode;
                $arusStok->keterangan = 'Delete Surat Jalan ' . $SJ;
                $arusStok->save();
            }
            mDetailSuratJalanPT::where('sjt_kode', $SJ)->delete();

            $jurnalID = mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $SJ)->first();
            if ($jurnalID) {
                mTransaksi::where('jurnal_umum_id', $jurnalID->jurnal_umum_id)->delete();
            }
            mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $SJ)->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    function SuratJalanPL($id = '', $do = '')
    {
        $faktur = mSuratJalanPenjualanLangsung::where('sjl_kode', $id)->first();
        $faktur['detail'] = mSuratJalanPenjualanLangsung::leftJoin('tb_detail_surat_jalan_penjualan_langsung', 'tb_surat_jalan_penjualan_langsung.sjl_kode', '=', 'tb_detail_surat_jalan_penjualan_langsung.sjl_kode')
            ->leftJoin('tb_barang', 'tb_detail_surat_jalan_penjualan_langsung.brg_kode', '=', 'tb_barang.brg_kode')
            ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
            ->where('tb_surat_jalan_penjualan_langsung.sjl_kode', $id)
            ->get();

        $faktur['no'] = 1;
        $faktur['tglPrint'] = date('d/m/Y');
        $kodeSuratJalanPL = $this->main->kodeLabel('suratJalanPL');
        $kodeDO = $this->main->kodeLabel('suratJalan');
        if ($do == null) {
            $sjl_kode_last = $id + 1;
            $faktur['do'] = $kodeDO . $sjl_kode_last;
            $faktur['sj'] = $kodeSuratJalanPL . $sjl_kode_last;
        } else {
            $faktur['do'] = $kodeDO . $do;
            $faktur['sj'] = $kodeSuratJalanPL . $do;
        }
        $kodePL = $this->main->kodeLabel('penjualanLangsung');
        $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
        $faktur['kode_bukti_id'] = $kodePL . $faktur->pl_no_faktur;
        $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjl_checker)->first();
        $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjl_sopir)->first();
        $faktur['cus_nama'] = mCustomer::select('cus_nama')->where('cus_kode', $faktur->cus_kode)->first();

        // return $faktur;

        // return view('suratJalan.ReportSuratJalan', ['faktur' => $faktur]);
        return MPDF::loadView('suratJalan.ReportSuratJalan', ['faktur' => $faktur], [], [
            'title' => 'Faktur Jual',
            'format' => 'A5-L'
        ])
            ->stream('suratJalan.ReportSuratJalan.pdf')
            ->getMpdf()->SetDisplayMode('fullpage', 'continuous');
    }

    function DOPL($id = '', $do = '')
    {
        // $faktur = mSuratJalanPenjualanLangsung::where('sjl_kode', $id)->first();
        // $faktur['detail'] = mSuratJalanPenjualanLangsung::leftJoin('tb_detail_surat_jalan_penjualan_langsung', 'tb_surat_jalan_penjualan_langsung.sjl_kode', '=', 'tb_detail_surat_jalan_penjualan_langsung.sjl_kode')
        // ->leftJoin('tb_barang', 'tb_detail_surat_jalan_penjualan_langsung.brg_kode', '=', 'tb_barang.brg_kode')
        // ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
        // ->where('tb_surat_jalan_penjualan_langsung.sjl_kode', $id)
        // ->get();
        //
        // $faktur['no'] = 1;
        // $faktur['tglPrint'] = date('d/m/Y');
        // $kodeSuratJalanPL = $this->main->kodeLabel('suratJalanPL');
        // $kodeDO = $this->main->kodeLabel('suratJalan');
        // if ($do == null) {
        //   $sjl_kode_last = $id+1;
        //   $faktur['do'] = $kodeDO.$sjl_kode_last;
        //   $faktur['sj'] = $kodeSuratJalanPL.$sjl_kode_last;
        // }
        // else {
        //   $faktur['do'] = $kodeDO.$do;
        //   $faktur['sj'] = $kodeSuratJalanPL.$do;
        // }
        // $kodePL = $this->main->kodeLabel('penjualanLangsung');
        // $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
        // $faktur['kode_bukti_id'] = $kodePL.$faktur->pl_no_faktur;
        // $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjl_checker)->first();
        // $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjl_sopir)->first();
        // $faktur['cus_nama'] = mCustomer::select('cus_nama')->where('cus_kode', $faktur->cus_kode)->first();


        $faktur = mDeliveryOrder::where('do_kode', $id)->first();
        $faktur['detail'] = mDeliveryOrder::leftJoin('tb_delivery_order_detail', 'tb_delivery_order.do_kode', '=', 'tb_delivery_order_detail.do_kode')
            ->leftJoin('tb_barang', 'tb_delivery_order_detail.brg_kode', '=', 'tb_barang.brg_kode')
            ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
            ->where('tb_delivery_order.do_kode', $id)
            ->get();

        $faktur['no'] = 1;
        $faktur['tglPrint'] = date('d/m/Y');
        $faktur['do'] = $faktur->do_kode;
        $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
        $faktur['kode_bukti_id'] = $faktur->no_faktur;
        $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_checker)->first();
        $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_sopir)->first();
        $faktur['cus_nama'] = mCustomer::select('cus_nama')->where('cus_kode', $faktur->cus_kode)->first();

        // $sj = mSuratJalanPenjualanTitipan::where('sjt_kode', $id)->first();
        // $faktur['sj'] = $sj->sjt_kode;

        $faktur['sj'] = '-';

        return view('suratJalan.ReportDO', compact('faktur'));

        // return MPDF::loadView('suratJalan.ReportDO', ['faktur' => $faktur], [], [
        //   'title' => 'Faktur Jual',
        //   'format' => 'A5-L'
        // ])
        // ->stream('suratJalan.ReportDO.pdf')
        // ->getMpdf()->SetDisplayMode('fullpage','continuous');
    }

    function SuratJalanPT($id = '', $do = '')
    {
        // $faktur = mSuratJalanPenjualanTitipan::where('sjt_kode', $id)->first();
        // $faktur['detail'] = mSuratJalanPenjualanTitipan::leftJoin('tb_detail_surat_jalan_penjualan_titipan', 'tb_surat_jalan_penjualan_titipan.sjt_kode', '=', 'tb_detail_surat_jalan_penjualan_titipan.sjt_kode')
        // ->leftJoin('tb_barang', 'tb_detail_surat_jalan_penjualan_titipan.brg_kode', '=', 'tb_barang.brg_kode')
        // ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
        // ->where('tb_surat_jalan_penjualan_titipan.sjt_kode', $id)
        // ->get();
        //
        // $faktur['no'] = 1;
        // $faktur['tglPrint'] = date('d/m/Y');
        // $faktur['do'] = $this->createNoSJDO($id);
        // $faktur['sj'] = $faktur->sjt_kode;
        // $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
        // $faktur['kode_bukti_id'] = $faktur->pt_no_faktur;
        // $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_checker)->first();
        // $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_sopir)->first();
        // $faktur['cus_nama'] = mCustomer::select('cus_nama')->where('cus_kode', $faktur->cus_kode)->first();

        $faktur = mSuratJalanPenjualanTitipan::where('sjt_kode', $id)->first();
        $faktur['detail'] = mSuratJalanPenjualanTitipan::leftJoin('tb_detail_surat_jalan_penjualan_titipan', 'tb_surat_jalan_penjualan_titipan.sjt_kode', '=', 'tb_detail_surat_jalan_penjualan_titipan.sjt_kode')
            ->leftJoin('tb_barang', 'tb_detail_surat_jalan_penjualan_titipan.brg_kode', '=', 'tb_barang.brg_kode')
            ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
            ->where('tb_surat_jalan_penjualan_titipan.sjt_kode', $id)
            ->get();

        $faktur['no'] = 1;
        $faktur['tglPrint'] = date('d/m/Y');
        $faktur['sj'] = $faktur->sjt_kode;
        $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
        $faktur['kode_bukti_id'] = $faktur->pt_no_faktur;
        $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_checker)->first();
        $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_sopir)->first();
        $faktur['cus_nama'] = mCustomer::select('cus_nama')->where('cus_kode', $faktur->cus_kode)->first();

        // $do = mDeliveryOrder::where('do_kode', $do)->first();
        // $faktur['do'] = $do->do_kode;

        $faktur['do'] = '-';

        return view('suratJalan.ReportSuratJalan', compact('faktur'));

        // return MPDF::loadView('suratJalan.ReportSuratJalan', ['faktur' => $faktur], [], [
        //   'title' => 'Faktur Jual',
        //   'format' => 'A5-L'
        // ])
        // ->stream('suratJalan.ReportSuratJalan.pdf')
        // ->getMpdf()->SetDisplayMode('fullpage','continuous');
    }

    function DOPT($id = '', $do = '')
    {
        // $faktur = mSuratJalanPenjualanTitipan::where('sjt_kode', $id)->first();
        // $faktur['detail'] = mSuratJalanPenjualanTitipan::leftJoin('tb_detail_surat_jalan_penjualan_titipan', 'tb_surat_jalan_penjualan_titipan.sjt_kode', '=', 'tb_detail_surat_jalan_penjualan_titipan.sjt_kode')
        // ->leftJoin('tb_barang', 'tb_detail_surat_jalan_penjualan_titipan.brg_kode', '=', 'tb_barang.brg_kode')
        // ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
        // ->where('tb_surat_jalan_penjualan_titipan.sjt_kode', $id)
        // ->get();
        //
        // $faktur['no'] = 1;
        // $faktur['tglPrint'] = date('d/m/Y');
        // $faktur['do'] = $this->createNoSJDO($id);
        // $faktur['sj'] = $faktur->sjt_kode;
        // $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
        // $faktur['kode_bukti_id'] = $faktur->pt_no_faktur;
        // $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_checker)->first();
        // $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_sopir)->first();
        // $faktur['cus_nama'] = mCustomer::select('cus_nama')->where('cus_kode', $faktur->cus_kode)->first();

        // return $faktur;

        $faktur = mDeliveryOrder::where('do_kode', $id)->first();
        $faktur['detail'] = mDeliveryOrder::leftJoin('tb_delivery_order_detail', 'tb_delivery_order.do_kode', '=', 'tb_delivery_order_detail.do_kode')
            ->leftJoin('tb_barang', 'tb_delivery_order_detail.brg_kode', '=', 'tb_barang.brg_kode')
            ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
            ->where('tb_delivery_order.do_kode', $id)
            ->get();

        $faktur['no'] = 1;
        $faktur['tglPrint'] = date('d/m/Y');
        $faktur['do'] = $faktur->do_kode;
        $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
        $faktur['kode_bukti_id'] = $faktur->no_faktur;
        $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_checker)->first();
        $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->sjt_sopir)->first();
        $faktur['cus_nama'] = mCustomer::select('cus_nama')->where('cus_kode', $faktur->cus_kode)->first();

        // $sj = mSuratJalanPenjualanTitipan::where('sjt_kode', $id)->first();
        // $faktur['sj'] = $sj->sjt_kode;

        $faktur['sj'] = '-';

        return view('suratJalan.ReportDO', compact('faktur'));

        // return MPDF::loadView('suratJalan.ReportDO', ['faktur' => $faktur], [], [
        //   'title' => 'Faktur Jual',
        //   'format' => 'A5-L'
        // ])
        // ->stream('suratJalan.ReportDO.pdf')
        // ->getMpdf()->SetDisplayMode('fullpage','continuous');
    }

    function returnStok(Request $request)
    {
        DB::beginTransaction();
        try {
            // return $request;
            $kode = $request->kode;
            $type = $request->type_penjualan;
            $det_kode = $request->detail_kode;
            $gudang = $request->gdg_kode;
            $barang = $request->brg_kode;
            $qty = $request->qty;

            // $stok = mStok::where('gdg_kode', $gudang)->where('brg_kode', $barang)->where('brg_no_seri', 'BX1')->first();
            // $stok->stok = ($stok->stok + $qty);
            // $stok->save();

            // return $stok;

            if ($type == 'langsung') {
                $detail = mDetailPenjualanLangsung::where('detail_pl_kode', $det_kode)->first();
                $stok = mStok::where('gdg_kode', $gudang)->where('brg_kode', $barang)->where('brg_no_seri', $detail->brg_no_seri)->first();
                $stok->stok = ($stok->stok + $qty);
                $stok->save();
                $hasil = ($detail->qty - $qty);
                if ($hasil <= 0) {
                    mDetailPenjualanLangsung::where('detail_pl_kode', $det_kode)->delete();
                } else {
                    $detail->qty = $hasil;
                    $detail->save();
                }
            } else {
                $detail = mDetailPenjualanTitipan::where('detail_pt_kode', $det_kode)->first();
                $stok = mStok::where('gdg_kode', $gudang)->where('brg_kode', $barang)->where('brg_no_seri', $detail->brg_no_seri)->first();
                $stok->stok = ($stok->stok + $qty);
                $stok->save();
                $hasil = ($detail->qty - $qty);
                if ($hasil <= 0) {
                    mDetailPenjualanTitipan::where('detail_pt_kode', $det_kode)->delete();
                } else {
                    $detail->qty = $hasil;
                    $detail->save();
                }
            }

            DB::commit();

            return [
                'redirect' => route('suratJalanDetail', ['kode' => $kode, 'tipe' => $type])
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
