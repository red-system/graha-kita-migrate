<?php

namespace App\Exports;

use App\Models\mKaryawan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class OmsetSalesDetailExport implements FromView
{
    private $start;
    private $end;
    private $mrk_kode;

    public function __construct($start, $end, $mrk_kode)
    {
      $this->start = $start;
      $this->end = $end;
      $this->mrk_kode = $mrk_kode;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;
      $mrk_kode = $this->mrk_kode;

      $data['no'] = 1;
      $data['no_2'] = 1;
      $data['no_3'] = 1;


      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      if ($mrk_kode != 'all') {
        $rowPL = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_langsung.pl_tgl', 'tb_penjualan_langsung.grand_total',
        'tb_penjualan_langsung.pl_no_faktur', 'tb_penjualan_langsung.cus_nama', 'tb_detail_penjualan_langsung.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_langsung.qty', 'tb_detail_penjualan_langsung.harga_net',
        'tb_detail_penjualan_langsung.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_langsung.pl_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
        ->where('pl_tgl', '>=', $start)
        ->where('pl_tgl', '<=', $end)
        ->where('tb_barang.mrk_kode', '=', $mrk_kode)
        ->get();

        $rowPT = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_titipan.pt_tgl', 'tb_penjualan_titipan.grand_total',
        'tb_penjualan_titipan.pt_no_faktur', 'tb_penjualan_titipan.cus_nama', 'tb_detail_penjualan_titipan.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_titipan.qty', 'tb_detail_penjualan_titipan.harga_net',
        'tb_detail_penjualan_titipan.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
        ->where('pt_tgl', '>=', $start)
        ->where('pt_tgl', '<=', $end)
        ->where('tb_barang.mrk_kode', '=', $mrk_kode)
        ->get();
      }
      else {
        $rowPL = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_langsung.pl_tgl', 'tb_penjualan_langsung.grand_total',
        'tb_penjualan_langsung.pl_no_faktur', 'tb_penjualan_langsung.cus_nama', 'tb_detail_penjualan_langsung.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_langsung.qty', 'tb_detail_penjualan_langsung.harga_net',
        'tb_detail_penjualan_langsung.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_langsung.pl_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
        ->where('pl_tgl', '>=', $start)
        ->where('pl_tgl', '<=', $end)
        ->get();

        $rowPT = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_titipan.pt_tgl', 'tb_penjualan_titipan.grand_total',
        'tb_penjualan_titipan.pt_no_faktur', 'tb_penjualan_titipan.cus_nama', 'tb_detail_penjualan_titipan.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_titipan.qty', 'tb_detail_penjualan_titipan.harga_net',
        'tb_detail_penjualan_titipan.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
        ->where('pt_tgl', '>=', $start)
        ->where('pt_tgl', '<=', $end)
        ->get();
      }

      $groupedPL = $rowPL->groupBy('kry_nama');
      $groupedPL->toArray();

      $groupedPT = $rowPT->groupBy('kry_nama');
      $groupedPT->toArray();

      $data['dataPL'] = $groupedPL;
      $data['dataPT'] = $groupedPT;

      return view('export.laporan-omset-sales-detail', $data);
    }
}
