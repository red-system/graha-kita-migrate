<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDetailOnlineOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_detail_online_order', function (Blueprint $table) {
           $table->increments('detail_order_kode');
           $table->integer('order_kode')->nullable();
           $table->integer('brg_kode')->nullable();
           $table->integer('harga')->nullable();
           $table->integer('qty')->nullable();
           $table->integer('biaya_tambahan')->nullable();
           $table->string('keterangan')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_detail_online_order');
     }
}
