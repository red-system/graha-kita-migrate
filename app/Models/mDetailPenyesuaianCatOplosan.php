<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailPenyesuaianCatOplosan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_detail_penyesuaian_cat_oplosan';
  protected $primaryKey = 'detail_pco_kode';
  // public $timestamps = false;

  public function PenyesuaianCatOplosan()
  {
    return $this->belongsTo('App\Models\mPenyesuaianCatOplosan', 'pco_kode');
  }

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode')->select('brg_kode', 'brg_barcode');
  }
}
