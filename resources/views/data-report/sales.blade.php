@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css"> --}}
  <style media="screen">
  td.wrapok {
    white-space:nowrap
  }

  td.fontsize{
    font-size:10px
  }
  </style>
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script> --}}
  <script type="text/javascript">
  var l = window.location;
  var base_url = l.protocol + "//" + l.host + "/";
  $('#sample_x').DataTable({
    destroy : true,
    processing: true,
    serverSide: true,
    ajax: base_url+'datatable/sales',
    columns: [
      {data: 'no', class:"fontsize"},
      // {data: 'brg_barcode', class:"wrapok fontsize"},
      {data: 'brg_nama', class:"wrapok fontsize"},
      {data: 'stn_nama', class:"wrapok fontsize"},
      {data: 'ktg_nama', class:"wrapok fontsize"},
      {data: 'grp_nama', class:"wrapok fontsize"},
      {data: 'mrk_nama', class:"wrapok fontsize"},
      // {data: 'brg_harga_beli_terakhir', class:"fontsize", render: function ( data, type, row ) {
      //   return numeral(data).format('0,0[.]00');
      //   }
      // },
      // {data: 'brg_harga_beli_tertinggi', class:"fontsize", render: function ( data, type, row ) {
      //   return numeral(data).format('0,0[.]00');
      //   }
      // },
      // {data: 'hargaPPN', class:"fontsize", render: function ( data, type, row ) {
      //   return numeral(data).format('0,0[.]00');
      //   }
      // },
      // {data: 'brg_hpp', class:"fontsize", render: function ( data, type, row ) {
      //   return numeral(data).format('0,0[.]00');
      //   }
      // },
      // {data: 'brg_harga_jual_eceran', class:"fontsize", render: function ( data, type, row ) {
      //   return numeral(data).format('0,0[.]00');
      //   }
      // },
      {data: 'HargaPPN', class:"fontsize", render: function ( data, type, row ) {
        return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        }
      },
      {data: 'QOH', class:"fontsize"},
      {data: 'QOH_titipan', class:"fontsize"},
      // {data: 'brg_status', class:"wrapok fontsize"},
    ]
  });
  </script>
  <script src="{{ asset('js/barang.js') }}" type="text/javascript"></script>
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script> --}}
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light">
            <br /><br />
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_x">
              <thead>
                <tr class="">
                  <th style="font-size:10px"> No </th>
                  {{-- <th style="font-size:10px"> Kode Barang </th> --}}
                  <th style="font-size:10px"> Nama Barang </th>
                  <th style="font-size:10px"> Satuan </th>
                  <th style="font-size:10px"> Kategory </th>
                  <th style="font-size:10px"> Group Stok </th>
                  <th style="font-size:10px"> Merek </th>
                  {{-- <th style="font-size:10px"> H.Beli Akhir </th>
                  <th style="font-size:10px"> H.Beli Max </th>
                  <th style="font-size:10px"> H.Beli + PPN </th>
                  <th style="font-size:10px"> HPP </th>
                  <th style="font-size:10px"> H.Jual Retail </th> --}}
                  <th style="font-size:10px"> H.Jual Partai </th>
                  <th style="font-size:10px"> QOH </th>
                  <th style="font-size:10px"> QOH Titipan</th>
                  {{-- <th style="font-size:10px"> Aktif </th> --}}
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
