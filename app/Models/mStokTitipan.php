<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mStokTitipan extends Model
{
    public $incrementing = false;
    protected $table = 'tb_detail_penjualan_titipan';
    protected $primaryKey = 'detail_pt_kode';
    public $timestamps = false;

    protected $fillable = [];

    public function titipan()
    {
        return $this->hasMany('App\Models\mStok', 'brg_kode');
    }
}
