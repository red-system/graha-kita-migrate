$(document).ready(function () {

    set_harga();
    set_qty();
    set_ppn();
    set_disc();
    set_disc_nom();

    function set_harga(){
        $('[name="harga_beli[]"]').keyup(function() {
            var harga       = $(this).val();
            var qty         = $(this).parents('td').siblings('td.qty').children('input').val();
            var ppn         = $(this).parents('td').siblings('td.ppn').children('input[name="ppn[]"]').val();
            // var ppn_nom     = $(this).parents('td').siblings('td.ppn').children('input[name="ppn_nom[]"]').val();
            var disc        = $(this).parents('td').siblings('td.disc').children('input').val();
            var disc_nom    = $(this).parents('td').siblings('td.disc_nom').children('input').val();
            var subtotal    = $(this).parents('td').siblings('td.total').children('input');
            var harga_net   = $(this).parents('td').siblings('td.harga_net').children('input');
            if(harga!='' || harga>0){
                var hrg_diskon   = parseFloat(harga)-parseFloat(disc_nom);
                var ppn_nominal  = (parseFloat(ppn) / parseFloat(100)) * parseFloat(hrg_diskon);
                var hrg_net      = parseFloat(hrg_diskon)+parseFloat(ppn_nominal);
                var sub_total    = parseFloat(hrg_net)*parseFloat(qty);
                total = Number.parseFloat(sub_total).toFixed(2);
                subtotal.val(total);
                harga_net.val(hrg_net.toFixed(2));
                // disc_nom.val(disc_nominal.toFixed(2));
                grand_total();
            }
        });
    }

    function set_qty(){
        $('[name="qty[]"]').keyup(function() {
            var harga       = $(this).parents('td').siblings('td.harga_beli').children('input').val();
            var qty         = $(this).val();
            var ppn         = $(this).parents('td').siblings('td.ppn').children('input[name="ppn[]"]').val();
            // var ppn_nom     = $(this).parents('td').siblings('td.ppn').children('input[name="ppn_nom[]"]').val();
            var disc        = $(this).parents('td').siblings('td.disc').children('input').val();
            var disc_nom    = $(this).parents('td').siblings('td.disc_nom').children('input').val();
            var subtotal    = $(this).parents('td').siblings('td.total').children('input');
            var harga_net   = $(this).parents('td').siblings('td.harga_net').children('input');
            if(qty!='' || qty>0){
                var ppn_nominal = (parseFloat(ppn) / parseFloat(100)) * parseFloat(harga);
                
                var hrg_net   = (parseFloat(harga)+parseFloat(ppn_nominal))-parseFloat(disc_nom);
                var sub_total = parseFloat(hrg_net)*parseFloat(qty);
                total = Number.parseFloat(sub_total).toFixed(2);
                subtotal.val(total);
                harga_net.val(hrg_net.toFixed(2));
                grand_total();
            }
        });
    }

    function set_ppn(){
        $('[name="ppn[]"]').keyup(function() {
            var harga       = $(this).parents('td').siblings('td.harga_beli').children('input').val();
            var qty         = $(this).parents('td').siblings('td.qty').children('input').val();
            var ppn         = $(this).val();
            // var ppn_nom     = $(this).parents('td').siblings('td.ppn').children('input[name="ppn_nom[]"]');
            var disc        = $(this).parents('td').siblings('td.disc').children('input').val();
            var disc_nom    = $(this).parents('td').siblings('td.disc_nom').children('input').val();
            var subtotal    = $(this).parents('td').siblings('td.total').children('input');
            var harga_net   = $(this).parents('td').siblings('td.harga_net').children('input');
            if(ppn!='' || ppn>0){
                // var disc_nominal = (parseFloat(disc) / parseFloat(100)) * parseFloat(harga);
                var hrg_diskon   = parseFloat(harga)-parseFloat(disc_nom);
                var ppn_nominal  = (parseFloat(ppn) / parseFloat(100)) * parseFloat(hrg_diskon);
                var hrg_net      = parseFloat(hrg_diskon)+parseFloat(ppn_nominal);
                var sub_total    = parseFloat(hrg_net)*parseFloat(qty);
                total = Number.parseFloat(sub_total).toFixed(2);
                subtotal.val(total);
                // disc_nom.val(disc_nominal.toFixed(2));
                harga_net.val(hrg_net.toFixed(2));
                grand_total();
            }
        });
    }

    function set_disc(){
        $('[name="disc[]"]').keyup(function() {
            var harga       = $(this).parents('td').siblings('td.harga_beli').children('input').val();
            var qty         = $(this).parents('td').siblings('td.qty').children('input').val();
            var ppn         = $(this).parents('td').siblings('td.ppn').children('input[name="ppn[]"]').val();
            // var ppn_nom     = $(this).parents('td').siblings('td.ppn').children('input[name="ppn_nom[]"]');
            var disc        = $(this).val();
            var disc_nom    = $(this).parents('td').siblings('td.disc_nom').children('input');
            var subtotal    = $(this).parents('td').siblings('td.total').children('input');
            var harga_net   = $(this).parents('td').siblings('td.harga_net').children('input');
            if(ppn!='' || ppn>0){
                var disc_nominal = (parseFloat(disc) / parseFloat(100)) * parseFloat(harga);                
                var hrg_diskon   = parseFloat(harga)-parseFloat(disc_nominal);
                var ppn_nominal  = (parseFloat(ppn) / parseFloat(100)) * parseFloat(hrg_diskon);
                var hrg_net      = parseFloat(hrg_diskon)+parseFloat(ppn_nominal);
                var sub_total    = parseFloat(hrg_net)*parseFloat(qty);
                total = Number.parseFloat(sub_total).toFixed(2);
                subtotal.val(total);
                harga_net.val(hrg_net.toFixed(2));
                disc_nom.val(disc_nominal.toFixed(2));
                grand_total();
            }
        });
    }

    function set_disc_nom(){
        $('[name="disc_nom[]"]').keyup(function() {
            var harga       = $(this).parents('td').siblings('td.harga_beli').children('input').val();
            var qty         = $(this).parents('td').siblings('td.qty').children('input').val();
            var ppn         = $(this).parents('td').siblings('td.ppn').children('input[name="ppn[]"]').val();
            // var ppn_nom     = $(this).parents('td').siblings('td.ppn').children('input[name="ppn_nom[]"]');
            var disc        = $(this).parents('td').siblings('td.disc').children('input');
            var disc_nominal= $(this).val();
            var subtotal    = $(this).parents('td').siblings('td.total').children('input');
            var harga_net   = $(this).parents('td').siblings('td.harga_net').children('input');
            if(ppn!='' || ppn>0){
                var disc_persen = (parseFloat(disc_nominal) * parseFloat(100)) / parseFloat(harga);                
                var hrg_diskon   = parseFloat(harga)-parseFloat(disc_nominal);
                var ppn_nominal  = (parseFloat(ppn) / parseFloat(100)) * parseFloat(hrg_diskon);
                var hrg_net      = parseFloat(hrg_diskon)+parseFloat(ppn_nominal);
                var sub_total    = parseFloat(hrg_net)*parseFloat(qty);
                total = Number.parseFloat(sub_total).toFixed(2);
                subtotal.val(total);
                harga_net.val(hrg_net.toFixed(2));
                disc.val(disc_persen.toFixed(2));
                grand_total();
            }
        });
    }

    

    function grand_total(){
        var total = 0;
        $('[name="total[]"]').each(function(key, val) {
          var val = $(this).val();
          total = parseFloat(total) + parseFloat(val);
        });
        total = Number.parseFloat(total).toFixed(2);

        $('[name="grand_total"]').val(total);
        $('[name="pl_subtotal"]').val(total);
        var currency = parseFloat(total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        $('#grandtotal').html('Rp.'+currency);
    }

    $('[name="pl_ppn"]').keyup(function(){
        setGrandPPN();
        setGrandTotal();
    });

    $('[name="pl_disc"]').keyup(function(){
        setGrandDisc();
        setGrandTotal();
    });

    $('[name="pl_disc_nom"]').keyup(function(){
        setGrandDisc2();
        setGrandTotal();
    });

    $('[name="pl_ongkos_angkut"]').keyup(function(){
        setGrandTotal();
    });

    function setGrandPPN(){
        var pb_ppn      = $('[name="pl_ppn"]').val();
        var pl_subtotal     = $('[name="pl_subtotal"]').val();

        if(pb_ppn!=='' || pl_ppn>0){
            var pb_ppn_nom  = (parseFloat(pb_ppn) / parseFloat(100)) * parseFloat(pl_subtotal);
            $('[name="pl_ppn_nom"]').val(pb_ppn_nom.toFixed(2));
        }
    }

    function setGrandDisc(){
        var pb_disc         = $('[name="pl_disc"]').val();
        var pl_subtotal     = $('[name="pl_subtotal"]').val();

        if(pb_disc!=='' || pb_disc>0){
            var pb_disc_nom     = (parseFloat(pb_disc) / parseFloat(100)) * parseFloat(pl_subtotal);
            $('[name="pl_disc_nom"]').val(pb_disc_nom.toFixed(2));
        }
    }

    function setGrandDisc2(){
        var pb_disc_nom         = $('[name="pl_disc_nom"]').val();

        if(pb_disc_nom!=='' || pb_disc_nom>0){
            var pb_disc     = (parseFloat(pb_disc_nom) * parseFloat(100)) / parseFloat(pl_subtotal);
            $('[name="pl_disc"]').val(pb_disc.toFixed(2));
        }
    }

    function setGrandTotal(){
        var subtotal = $('[name="pl_subtotal"]').val();
        var disc_nom = $('[name="pl_disc_nom"]').val();
        var ppn_nom = $('[name="pl_ppn_nom"]').val();
        var ongkos_angkut = $('[name="pl_ongkos_angkut"]').val();

        var grand_total = parseFloat(subtotal) - parseFloat(disc_nom) + parseFloat(ppn_nom) + parseInt(ongkos_angkut);

        $('[name="grand_total"]').val(grand_total.toFixed(2));
    }  

	$('.btn-payment').click(function(){

        $('#modal-payment').modal("show");
        var grandtotal=$('[name="grand_total"]').val();
        // $('.nominal-grand-total').html(grandtotal);
        // $('.nominal-sisa').html(grandtotal);
        const formatter = new Intl.NumberFormat()
        var balance = formatter.format(grandtotal); // "$1,000.00"
        $('[name="sisa_payment"]').val(grandtotal);
        $('#balance').html('Rp. '+balance);
        $('#view_grand_total_payment').html('Rp. '+balance);
        $('[name="grand_total_payment"]').val(grandtotal);
        payment();

    });

    $('.btn-row-payment-plus').click(function() {
        var balance = $('[name="sisa_payment"]').val();
        if(balance<1){
            swal({
                title: 'Perhatian',
                text: 'Tidak dapat menambahkan transaksi karena Balance sudah 0',
                type: 'warning'
            });
        }else{
            var row_payment = $('.table-row-payment tbody').html();
            var el          = $(row_payment);
            $('.table-data-payment tbody').append(el);

            btn_row_delete_payment();
            payment();
            // set_charge();
            // setor();
            payment_third_party();
            // selectKodeMaster();
            btn_selectpickerx(el);
            master_id();
        }
        
    });

    function btn_selectpickerx(el) {
        el.children('td:nth-child(1)').children('select').selectpicker();
    }

    function btn_row_delete_payment() {
        $('.btn-row-delete-payment').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_sisa();
        });
    }

    function payment() {
        $('[name="payment[]"]').keyup(function() {
            var payment = $(this).val();           

            if(payment!='' || payment>0){
            	$(this).parents('td').siblings('td.payment_total').children('input').val(payment);
                kalkulasi_sisa();
            }
        });
    }

    function kalkulasi_sisa() {
        var payment_total = 0;
        var grand_total = $('[name="grand_total_payment"]').val();
        $('[name="payment_total[]"]').each(function(key, val) {
            // var total_payment = $(this).val();
            var payment_total_ini = $(this).val();
            if(payment_total_ini != '') {
                payment_total = parseFloat(payment_total)+parseFloat(payment_total_ini);
            }
        });
        sisa = grand_total - payment_total;
        const formatter = new Intl.NumberFormat()
        var balance = formatter.format(sisa); // "$1,000.00"
        $('#balance').html('Rp. '+balance);
        //$('.sisa-payment').val(sisa);
        $('[name="sisa_payment"]').val(parseFloat(sisa.toFixed(2)));
    }

    function payment_third_party() {
        $('[name="tgl_pencairan[]"]').datepicker();
        //$('[name="master_id[]"]').select2();
    }

    function master_id() {
    $('[name="master_id[]"]').change(function() {
      var master_id = $(this).find(':selected').data('kode-rek')
      if (master_id == 2101) {
        var total = $('[name="sisa_payment"]').val();
        // var currency = parseFloat(total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        // console.log(currency);
        $(this).parents('td').siblings('td.payment_total').children('input').val(total);
        $(this).parents('td').siblings('td.payment').children('input').val(total);
        // $(this).parents('td').siblings('td.no_check_bg').children('input').prop('disabled', true).prop('required', false);
        kalkulasi_sisa();
      }
      else{
        var total = $('[name="sisa_payment"]').val();
        $(this).parents('td').siblings('td.payment_total').children('input').val(total);
        $(this).parents('td').siblings('td.payment').children('input').val(total);
        // $(this).parents('td').siblings('td.no_check_bg').children('input').prop('disabled', true).prop('required', false);
        kalkulasi_sisa();
      }
    });

    $('#form-pembelian').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
            
        $('#btn-submit-pembelian').attr('disabled', true);
        var balance = $('[name="sisa_payment"]').val();
        if(balance != 0) {
            swal({
                title: 'Perhatian',
                text: 'Data Belum Balance',
                type: 'error'
            });
            $('#btn-submit-pembelian').attr('disabled', false);
        }else{
            $.ajax({
                url: ini.attr('action'),
                type: ini.attr('method'),
                data: ini.serialize(),
                success: function(data) {
                    if(data.redirect) {
                        window.location.href = data.redirect;
                    }
                },
                        // error: function(request, status, error) {
                        //     swal({
                        //       title: 'Perhatian',
                        //       text: 'Data Gagal Disimpan!',
                        //       type: 'error'
                        //     });
                        // }
            });

        }

        return false;
    });

    
  }

});