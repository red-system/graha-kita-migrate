<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mKendaraanOperasional extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  protected $table = 'tb_kendaraan_operasional';
  protected $primaryKey = 'kendaraan_operasional_kode';
  public $timestamps = false;

  public function BiayaKendaraan()
  {
    return $this->hasMany('App\Models\mBiayaKendaraan', 'kendaraan_operasional_kode');
  }
}
