@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <a class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                            Pilih Periode
                        </a>
                        <a type="button" class="btn btn-danger" href="{{route('printNeraca', ['bulan'=>$bulan, 'tahun'=>$tahun_periode])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>

                        <h2><center><strong>LAPORAN NERACA</strong></center></h2>
                        <h3><center><strong>Periode {{$bln}} {{$tahun_periode}}</strong></center></h3>

                        <table class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr class="success">
                                    <th width="25%"><center>ASET</center></th>
                                    <th width="25%"><center>LIABILITAS</center></th>
                                    <th width="25%"><center>EKUITAS</center></th>
                                    <th width="25%"><center>STATUS</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{number_format($total_asset)}}</td>
                                    <td>{{number_format($total_liabilitas)}}</td>
                                    <td>{{number_format($total_ekuitas)}}</td>
                                    @if($status > 0)
                                    <td><font color="red"><strong>NOT BALANCE : {{number_format($status)}}</strong></font></td>
                                    @endif
                                    @if($status < 0)
                                    <td><font color="red"><strong>NOT BALANCE : {{number_format($status)}}</strong></font></td>
                                    @endif
                                    @if($status == 0)
                                    <td><font color="green"><strong>BALANCE</strong></font></td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#data_asset" data-toggle="tab"> DATA ASSET </a>
                                </li>
                                <li>
                                    <a href="#data_liabilitas" data-toggle="tab"> DATA LIABILITAS </a>
                                </li>
                                <li>
                                    <a href="#data_ekuitas" data-toggle="tab"> DATA EKUITAS </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="data_asset">
                                    <h2><center><strong>DATA ASSET</strong></center></h2>
                                    <h3><center><strong>Periode {{$bln}} {{$tahun_periode}}</strong></center></h3>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_asset" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        <?php
                                            

                                            $total_assets = 0;
                                            $total_liabilitass = 0;
                                            $total_ekuitass = 0;

                                        ?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->perkiraan->mst_neraca_tipe == 'asset')
                                            <tr>
                                                <td>{{$detail->perkiraan->mst_kode_rekening}}</td>
                                                <td>{{$detail->perkiraan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->perkiraan->mst_normal == 'kredit'){
                                                        $asset  = $detail->msd_awal_kredit;
                                                    }
                                                    if($detail->perkiraan->mst_normal == 'debet'){
                                                        $asset  = $detail->msd_awal_debet;
                                                    }
                                                    foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($detail->perkiraan->mst_normal == 'kredit'){
                                                            $asset  = $asset+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($detail->perkiraan->mst_normal == 'debet'){
                                                            $asset  = $asset-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }

                                                    }
                                                    $total_assets = $total_assets+$asset;
                                                ?>
                                                <td align="right">{{number_format($asset)}}</td>
                                            </tr>
                                            @endif
                                        @endforeach                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_assets)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="data_liabilitas">
                                    <h2><center><strong>DATA LIABILITAS</strong></center></h2>
                                    <h3><center><strong>Periode {{$bln}} {{$tahun_periode}}</strong></center></h3>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_liabilitas" width="100%"> 
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->perkiraan->mst_neraca_tipe == 'liabilitas')
                                            <tr>
                                                <td width="25%">{{$detail->perkiraan->mst_kode_rekening}}</td>
                                                <td width="45%">{{$detail->perkiraan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->perkiraan->mst_normal == 'kredit'){
                                                        $liabilitas  = $detail->msd_awal_kredit;
                                                    }
                                                    if($detail->perkiraan->mst_normal == 'debet'){
                                                        $liabilitas  = $detail->msd_awal_debet;
                                                    }
                                                    foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($detail->perkiraan->mst_normal == 'kredit'){
                                                            $liabilitas  = $liabilitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($detail->perkiraan->mst_normal == 'debet'){
                                                            $liabilitas  = $liabilitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }
                                                                        
                                                    }
                                                    $total_liabilitass     = $total_liabilitass+$liabilitas;
                                                ?>
                                                <td width="20%">{{number_format($liabilitas)}}</td>
                                            </tr>
                                            @endif
                                        @endforeach                                           
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_liabilitass)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="data_ekuitas">
                                    <h2><center><strong>DATA EKUITAS</strong></center></h2>
                                    <h3><center><strong>Periode {{$bln}} {{$tahun_periode}}</strong></center></h3>                                   
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_ekuitas" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->perkiraan->mst_neraca_tipe == 'ekuitas')
                                            <tr>
                                                <td width="25%">{{$detail->perkiraan->mst_kode_rekening}}</td>
                                                <td width="45%">{{$detail->perkiraan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->perkiraan->mst_normal == 'kredit'){
                                                        $ekuitas  = $detail->msd_awal_kredit;
                                                    }
                                                    if($detail->perkiraan->mst_normal == 'debet'){
                                                        $ekuitas  = $detail->msd_awal_debet;
                                                    }
                                                    foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($detail->perkiraan->mst_normal == 'kredit'){
                                                            $ekuitas  = $ekuitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($detail->perkiraan->mst_normal == 'debet'){
                                                            $ekuitas  = $ekuitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                            
                                                        }
                                                                        
                                                    }
                                                    $total_ekuitass     = $total_ekuitass+$ekuitas;
                                                ?>
                                                <td width="20%">{{number_format($ekuitas)}}</td>
                                            </tr>
                                            @endif
                                        @endforeach                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_ekuitass)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeBukuBesar') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Bulan</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-inline input-medium" name="bulan" required>
                                    <option value="">Pilih Bulan</option>
                                    @foreach($tanggal as $tgl)
                                    <option value="{{$tgl}}">{{$tgl}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pilih Tahun</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-inline input-medium" name="tahun" required>
                                    <option value="">--Pilih Tahun--</option>
                                    @foreach($tahun as $thn)
                                    <option value="{{$thn}}">{{$thn}}</option>
                                    @endforeach
                                </select>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop